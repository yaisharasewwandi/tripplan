<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('debug', 'DebugController@index')->name('debug');
Route::get('/', 'frontend\HomeController@index')->name('index');
Route::post('/search-result', 'frontend\HomeController@search_result')->name('home-search-result');
Route::get('/blog_list', 'frontend\HomeController@blog_list')->name('blog_list');
Route::get('/searchBlogList', 'frontend\HomeController@searchBlogList')->name('searchBlogList');
Route::get('/blog_single/{id}', 'frontend\HomeController@blog_single')->name('blog_single');
Route::get('blogreview', 'frontend\HomeController@blogreview');

Route::get('/listing-list', 'frontend\HomeController@listing')->name('listing');
Route::get('/listing-single/{id}', 'frontend\HomeController@listing_single')->name('listing-single');
Route::post('/listing-review', 'frontend\HomeController@review_post')->name('listing-review');

Route::get('/map-direction-list/{id}', 'frontend\HomeController@maplist')->name('direction-search');
Route::get('/map-direction-lists/{id}', 'frontend\HomeController@maplist_restaurant')->name('directionres-search');
Route::get('/map-directions-list/{id}', 'frontend\HomeController@maplist_touragency')->name('directiontour-search');
//Route::get('direction-search', 'frontend\HomeController@direction_search')->name('direction-search');


Route::get('/car-list', 'frontend\CarController@car_list')->name('car-list');
Route::get('/car-single/{id}', 'frontend\CarController@car_single')->name('car-single');
Route::post('/car-book', 'frontend\CarController@car_book')->name('car-book');
Route::post('/insert-car-booking', 'frontend\CarController@insert_booking')->name('insert-car-booking');
Route::post('/car-review', 'frontend\CarController@review_post')->name('car-review');

Route::get('/guider-list', 'frontend\GuiderController@guider_list')->name('guider-list');
Route::get('/guider-single/{id}', 'frontend\GuiderController@guider_single')->name('guider-single');
Route::post('/guid-review', 'frontend\GuiderController@review_post')->name('guid-review');


Route::get('/agency-list', 'frontend\TravelAgencyController@agency_list')->name('agency-list');
Route::get('/agency-single/{id}', 'frontend\TravelAgencyController@agency_single')->name('agency-single');
Route::get('/package/details', 'frontend\TravelAgencyController@packages_details')->name('package-details');
Route::post('/package-book', 'frontend\TravelAgencyController@package_book')->name('package-book');
Route::post('/insert-package-booking', 'frontend\TravelAgencyController@insert_booking')->name('insert-package-booking');
Route::post('/travel-review', 'frontend\TravelAgencyController@review_post')->name('travel-review');


Route::get('/restaurant-list', 'frontend\RestaurantController@restaurant_list')->name('restaurant-list');
Route::get('/restaurant-single/{id}', 'frontend\RestaurantController@restaurant_single')->name('restaurant-single');
Route::post('/restaurant-review', 'frontend\RestaurantController@review_post')->name('restaurant-review');


Route::get('/districts-list', 'frontend\HomeController@districts_list')->name('districts-list');

Route::get('wislist', 'frontend\WishlistController@wislistSave')->name('wislist-save');
Route::get('wishlist', 'frontend\WishlistController@wishlist')->name('wishlist');

Route::get('/add-listing', 'frontend\HomeController@add_listing')->name('add-listing');
Route::get('/customer-profile', 'frontend\HomeController@customer_profile')->name('customer-profile');
Route::get('/listing-view', 'frontend\HomeController@listing_view')->name('listing-view');
Route::get('/listing-booking', 'frontend\HomeController@listing_booking')->name('listing-booking');
Route::get('/customer-review', 'frontend\HomeController@customer_review')->name('customer-review');
Route::get('/customer-edit-profile', 'frontend\HomeController@customer_edit_profile')->name('customer-edit-profile');
Route::post('/add-booking', 'frontend\HomeController@add_booking')->name('add-booking');
Route::post('/insert-booking', 'frontend\HomeController@insert_booking')->name('insert-booking');
Route::get('/rooms/details', 'frontend\HomeController@rooms_details')->name('rooms-details');



Auth::routes();

Route::group(['middleware' => ['auth']], function () {
Route::get('/add-businessstep1', 'frontend\AccountController@add_businessstep1')->name('add-businessstep1');
Route::post('/add-businessstep1', 'frontend\AccountController@create_businessstep1')->name('add-businessstep1');

Route::get('/add-businessstep2', 'frontend\AccountController@add_businessstep2')->name('add-businessstep2');
Route::post('/add-businessstep2', 'frontend\AccountController@create_businessstep2')->name('add-businessstep2');

Route::get('/add-businessstep3', 'frontend\AccountController@add_businessstep3')->name('add-businessstep3');
Route::post('/add-businessstep3', 'frontend\AccountController@create_businessstep3')->name('add-businessstep3');


Route::post('/add-businessstep-yourself', 'frontend\AccountController@create_yourself_businessstep')->name('add-businessstep-yourself');

Route::get('myform/ajax/{id}',array('as'=>'myform.ajax','uses'=>'AccountController@myformAjax'));

Route::get('/user-dashboard', 'frontend\UserDashboardController@user_dashboard')->name('user-dashboard');
Route::get('/user-accommodation/index', 'frontend\UserAccommodationController@index')->name('user-accommodation.index');
Route::get('/user-accommodation/view/{id}', 'frontend\UserAccommodationController@view')->name('user-accommodation.view');
    Route::post('/user-accommodation/store', 'frontend\UserAccommodationController@store_rooms')->name('user-accommodation.add-rooms');
    Route::post('projects/media', 'frontend\UserAccommodationController@storeMedia')
        ->name('projects.storeMedia');
});
Route::post('/user-accommodation/update/{id}', 'frontend\UserAccommodationController@update_rooms')->name('user-accommodation.update-rooms');
Route::get('/user-accommodation/destroy/{id}', 'frontend\UserAccommodationController@destroy_image')->name('user-accommodation.destroyimage');
Route::get('/user-accommodation/destroyroom/{id}', 'frontend\UserAccommodationController@destroy_room')->name('user-accommodation.destroyroom');
Route::get('/user-accommodation/destroyacc/{id}', 'frontend\UserAccommodationController@destroy_acc')->name('user-accommodation.acc_destroy');


Route::get('/user-restaurant/index', 'frontend\UserRestaurantController@index')->name('user-restaurant.index');
Route::get('/user-guider/index', 'frontend\UserGuiderController@index')->name('user-guider.index');
Route::get('/user-agency/index', 'frontend\UserAgencyController@index')->name('user-agency.index');


Route::get('/auth/redirect/{provider}', 'SocialController@redirect');
Route::get('/callback/{provider}', 'SocialController@callback');

Route::post('/edit-profile/update/{id}', 'frontend\UserProfileController@editprofile')->name('edit-profile.update');
Route::get('/status/update', 'frontend\UserProfileController@updateStatus')->name('users.update.status');

Route::group(['middleware' => ['role_or_permission:Super Admin|Hotel Owner|Restaurant Owner|Agency Owner|Guider|Car Renter', 'auth']], function () {
	Route::get('/dashboard', 'backend\HomeController@index')->name('home');
      Route::resource('roles','backend\RoleController');
    Route::resource('users','backend\UserController');
    Route::resource('permissions','backend\PermissionController');
    Route::resource('blogs','backend\BlogController');
    Route::get('blogchangeStatus', 'backend\BlogController@changeStatus');

    //accommodation route
    Route::resource('accommodations','backend\AccommodationController');
    Route::get('/accommodations/imagedelete/{id}', 'backend\AccommodationController@images_destroy')->name('accommodation.delete-images');
    Route::get('accommodationchangeStatus', 'backend\AccommodationController@changeStatus');
    Route::resource('accommodation-rooms','backend\AccommodationRoomController');
    Route::get('/accommodations-room/imagedelete/{id}', 'backend\AccommodationRoomController@images_destroy')->name('accommodation-room.delete-images');
    Route::get('accommodationroomchangeStatus', 'backend\AccommodationRoomController@changeStatus');
    Route::get('accommodationdistrict', 'backend\AccommodationController@dependancydistrict');
    Route::resource('accomodation-booking','backend\AccomodationBookingController');
    Route::get('accbookStatus', 'backend\AccomodationBookingController@changeStatus');

    //car route
    Route::resource('cars','backend\CarController');
    Route::get('carchangeStatus', 'backend\CarController@changeStatus');
    Route::resource('car-booking','backend\CarBookingController');
    Route::get('carbookStatus', 'backend\CarBookingController@changeStatus');
    Route::get('/car/imagedelete/{id}', 'backend\CarController@images_destroy')->name('car.delete-images');

    //agency route
    Route::resource('travel-agencies','backend\TravelAgencyController');
    Route::get('agencychangeStatus', 'backend\TravelAgencyController@changeStatus');
    Route::get('travelagencies/create', 'backend\TravelAgencyController@check_slug')->name('agencies.check_slug');
    Route::resource('tour-packages','backend\TourPackageController');
    Route::get('packagesStatus', 'backend\TourPackageController@changeStatus');
    Route::get('tour-packages/create', 'backend\TourPackageController@check_slug')->name('package.check_slug');
    Route::resource('package-booking','backend\PackageBookingController');
    Route::get('packagebookStatus', 'backend\PackageBookingController@changeStatus');
    Route::get('/tour/imagedelete/{id}', 'backend\TourPackageController@images_destroy')->name('tour.delete-images');

    //Restaurant
    Route::resource('restaurants','backend\RestaurantController');
    Route::get('restaurant/create', 'backend\RestaurantController@check_slug')->name('restaurant.check_slug');
    Route::get('restaurantStatus', 'backend\RestaurantController@changeStatus');
    Route::get('/restaurant/imagedelete/{id}', 'backend\RestaurantController@images_destroy')->name('restaurant.delete-images');


    //Restaurant Item route
    Route::resource('items','backend\ItemController');
    Route::get('item/create', 'backend\ItemController@check_slug')->name('item.check_slug');
    Route::get('itemStatus', 'backend\ItemController@changeStatus');
    Route::get('/items/imagedelete/{id}', 'backend\ItemController@images_destroy')->name('items.delete-images');


    //Location route
    Route::resource('locations','backend\LocationController');
    Route::get('city_search', 'backend\LocationController@citysearch');

    //Guiders route
    Route::resource('guiders','backend\GuiderController');
    Route::get('guiderdistrict', 'backend\GuiderController@dependancydistrict');
    Route::get('guiderchangeStatus', 'backend\GuiderController@changeStatus');

    //giuderdetails
    Route::resource('guider-details','backend\GuiderDetailsController');
    Route::get('travelStatus', 'backend\GuiderDetailsController@changeStatus');
    Route::get('/traveldetail/imagedelete/{id}', 'backend\GuiderDetailsController@images_destroy')->name('traveldetail.delete-images');

    //Review route
    Route::get('reviews/hotels', 'backend\ReviewController@hotel')->name('reviews.hotels');

    //USER Profile
    Route::get('user/profile', 'backend\ProfileController@index')->name('profile.index');

});

