<?php

namespace App\Providers;

use App\AccommodationWishlist;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider as ServiceProvider;;

class WishListServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
//
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
//        $user_id = Auth::id();
//        $wishlists = AccommodationWishlist::with(['accommodations','accommodations.images' => function ($query){
//            $query->where('is_featured',1);
//        }])->get();
//
//        view()->composer('frontend.wish-list', function ($view) use ($wishlists) {
//
//            View::share('wishlists', $wishlists);
//
//        });
//
//        $count = $wishlists->count();
//        view()->composer('frontend.layout.partials.header', function ($view) use ($count) {
//
//            View::share('count', $count);
//
//        });;
    }
}
