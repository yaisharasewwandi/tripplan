<?php

namespace App;

use App\Traits\Multitenantable;
use Illuminate\Database\Eloquent\Model;

class CarBooking extends Model
{
    use Multitenantable;
    protected $table='car_booking';
    protected $fillable =[
        'car_id',
        'ref_no',
        'from',
        'to',
        'days',
        'taxes_fees',
        'country',
        'town',
        'street',
        'state',
        'postal_code',
        'note',
        'confirm_status',
        'user_id',
        'paid_at',
    ];

    public function car()
    {
        return $this->belongsTo(Car::class, 'car_id','id')->withoutGlobalScopes();
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }

}
