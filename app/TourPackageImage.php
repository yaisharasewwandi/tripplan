<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourPackageImage extends Model
{
    protected $table ='tour_package_images';
    protected $fillable=[
        'image_name',
        'is_featured',
        'tour_package_id'
    ];
}
