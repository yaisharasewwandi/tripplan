<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourpackageSpecialTime extends Model
{
    protected $table ='tour_package_special_times';
    protected $fillable=[
        'tour_package_id',
        'day',
        'time',
    ];
}
