<?php

namespace App;

use App\Traits\Multitenantable;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
   
    protected $table ='locations';
    protected $fillable =[
        'location',
        'lat',
        'lng',
        'city_id',
        'district_id',
    ];
    public function districts()
    {
        return $this->belongsTo(District::class, 'district_id');
    }
    public function cities()
    {
        return $this->belongsTo(City::class, 'city_id');
    }
}
