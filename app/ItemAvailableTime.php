<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemAvailableTime extends Model
{
    protected $table = 'item_available_times';
    protected $fillable =[
        'item_id',
        'day',
        'start_time',
        'end_time',
    ];
}
