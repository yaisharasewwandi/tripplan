<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccommodationType extends Model
{
    protected $table = 'accommodation_types';
    protected $fillable =[
        'name',
        'image',
        'description',

    ];
}