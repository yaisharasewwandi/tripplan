<?php
/**
 * Created by PhpStorm.
 * User: yaish
 * Date: 6/3/2020
 * Time: 10:38 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class RoomType extends Model
{
    protected $table = 'rooms_type';
    protected $fillable =[
        'name',
    ];
}