<?php

namespace App;

use App\Traits\Multitenantable;
use Illuminate\Database\Eloquent\Model;

class GuiderDetails extends Model
{
    protected $table='guiders_travel_info';
    protected $fillable=[
        'name',
        'location',
        'description',
        'price',
        'days',
        'publish_status',
        'guider_id'
    ];
    public function images(){
        return $this->hasMany(GuiderTravelImage::class,'guider_travel_info_id');
    }
}
