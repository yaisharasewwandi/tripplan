<?php
/**
 * Created by PhpStorm.
 * User: yaish
 * Date: 6/4/2020
 * Time: 11:32 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccommodationRoom extends Model
{
    use SoftDeletes;
    protected $table = 'accommodation_rooms';
    protected $fillable =[
        'room_name',
        'number_of_room',
        'number_of_bed',
        'room_size',
        'price',
        'max_adult',
        'max_children',
        'accommodation_id'
    ];
    public function images(){
        return $this->hasMany(RoomImage::class,'accommodation_room_id');
    }
    public function roomFeatures()
    {
        return $this->belongsToMany(RoomFeature::class);
    }

    public function roomTypes(){
        return $this->belongsTo(RoomType::class,'room_type_id','id');
    }
    public function accommodation(){
        return $this->belongsTo(Accommodation::class,'accommodation_id','id')->withoutGlobalScopes();
    }
}
