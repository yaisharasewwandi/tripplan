<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantOpenHour extends Model
{
    protected $table = 'restaurant_open_hour';
    protected $fillable = [
        'restaurant_id',
        'day',
        'start_time',
        'end_time'
    ];
}
