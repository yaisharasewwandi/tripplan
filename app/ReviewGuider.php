<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewGuider extends Model
{
    protected $table = 'review_guiders';
    protected $fillable =[
        'safety',
        'communication',
        'care',
        'comment',
        'confidence',
        'guider_id',
        'user_id',
    ];
    public function scopeReviewMsg($query){
        return $query->whereNotNull('comment');
    }
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
