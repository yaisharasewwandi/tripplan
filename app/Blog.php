<?php

namespace App;

use App\Traits\Multitenantable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{
    use Multitenantable;
    protected $fillable = [
        'title', 'description', 'image', 'publish_status', 'user_id'
    ];
    public function users(){
        return $this->belongsTo(User::class, 'user_id');
    }
    public function reviewBlogs(){
        return $this->hasMany(ReviewBlog::class, 'blog_id');
    }
}
