<?php
namespace App;


use Illuminate\Database\Eloquent\Model;

class ReviewBlog extends Model
{
    protected $fillable = [
        'name',  'email','comment', 'blog_id', 'user_id'
    ];
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}