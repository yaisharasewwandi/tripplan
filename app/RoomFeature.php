<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomFeature extends Model
{
    protected $table = 'room_features';
    protected $fillable =[
        'name',
    ];
    public function accommodationRoom()
    {
        return $this->belongsToMany(AccommodationRoom::class);
    }
}