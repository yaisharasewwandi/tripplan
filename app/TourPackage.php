<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TourPackage extends Model
{

    protected $table='tour_packages';
    protected $fillable=[
        'travel_agency_id',
        'title',
        'slug',
        'description',
        'location',
        'duration',
        'min_person',
        'max_person',
        'budget',
        'includes',
        'excludes',
        'publish_status',
        'available_times'
    ];
    public function images(){
        return $this->hasMany(TourPackageImage::class,'tour_package_id');
    }
    public function categories()
    {
        return $this->belongsToMany(TourCategory::class, 'tour_package_categories');
    }
    public function travelAgency(){
        return $this->belongsTo(TravelAgency::class,'travel_agency_id','id')->withoutGlobalScopes();
    }
}
