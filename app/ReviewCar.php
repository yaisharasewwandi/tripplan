<?php


namespace App;


use App\Traits\Multitenantable;
use Illuminate\Database\Eloquent\Model;

class ReviewCar extends Model
{
    use Multitenantable;
    protected $table = 'review_car';
    protected $fillable =[
        'user_id',
        'sub_avg',
        'comment',
        'consumption',
        'seating_capacity',
        'confatable',
        'facilities',
        'acar_id',
    ];
    public function scopeReviewMsg($query){
        return $query->whereNotNull('comment');
    }
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
