<?php

namespace App;

use App\Traits\Multitenantable;
use Illuminate\Database\Eloquent\Model;

class TourPackageBooking extends Model
{
    use Multitenantable;
    protected $table='tour_package_booking';
    protected $fillable =[
        'tour_id',
        'ref_no',
        'from',
        'to',
        'days',
        'taxes_fees',
        'country',
        'town',
        'street',
        'state',
        'postal_code',
        'note',
        'confirm_status',
        'user_id',
        'paid_at',
    ];
    public function tour()
    {
        return $this->belongsTo(TourPackage::class, 'tour_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }
}
