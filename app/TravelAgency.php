<?php

namespace App;

use App\Traits\Multitenantable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TravelAgency extends Model
{
    use Multitenantable;
    protected $table='travel_agencies';
    protected $fillable=[
        'title',
        'address',
        'slug',
        'email',
        'location',
        'description',
        'banner_image',
        'featured_image',
        'contact_number',
        'fb_page_link',
        'instergram_link',
        'twiter_link',
        'lat',
        'lng',
        'publish_status',
        'district_id',
        'city_id',
        'user_id'

    ];
//    public function images()
//    {
//        return $this->hasMany(AgenciesImage::class , 'travel_agencies_id');
//    }
    public function agancyPackages(){
        return $this->hasMany(TourPackage::class, 'travel_agency_id');
    }
    public function reviewAgancy(){
        return $this->hasMany(ReviewAgency::class, 'agency_id');
    }
    public static function getByDistance($lat, $lng, $distance)
    {
        $results = DB::select(DB::raw('SELECT id, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $lng . ') ) + sin( radians(' . $lat .') ) * sin( radians(lat) ) ) ) AS distance FROM travel_agencies HAVING distance < ' . $distance . ' ORDER BY distance') );

        return $results;
    }
}