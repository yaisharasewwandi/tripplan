<?php

namespace App\Http\Controllers;

use App\Accommodation;
use App\AccommodationBooking;
use Illuminate\Http\Request;

class DebugController extends Controller
{
    public function index(Request $request){
        return  $ref_no= 'REF'. str_pad(100, 6, "0", STR_PAD_LEFT);;
    }
}
