<?php

namespace App\Http\Controllers\backend;

use App\Car;
use App\CardType;
use App\CarFeatures;
use App\CarImages;
use App\CarType;
use App\City;
use App\District;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class CarController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:car-list|car-create|car-edit|car-delete', ['only' => ['index','store']]);
        $this->middleware('permission:car-create', ['only' => ['create','store']]);
        $this->middleware('permission:car-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:car-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Auth::user()->hasRole(['Admin','Super Admin'])) {
        $cars = Car::withoutGlobalScopes()->orderBy('id','desc')->get();
        }elseif (Auth::user()->hasRole(['Car Renter'])) {
            $cars = Car::orderBy('id','desc')->get();
        }

        return view('backend.cars.index',compact('cars'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = CarType::all();
        $features = CarFeatures::all();
        $districts = District::all();
        $cities = City::all();
        return view('backend.cars.create',compact('types','features','districts','cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'contact_number' => 'required',
            'email' => 'required',
            'type_id' => 'sometimes|required',
            'features' => 'required',
            'district_id' => 'required',
            'city_id' => 'required',
            'address' => 'required',
            'location' => 'required',
            'banner_image' => 'required',
            'feature_image' => 'required',
            'gallery_image' => 'required',
            'passenger' => 'required',
            'gearshift' => 'required',
            'baggage' => 'required',
            'door' => 'required',
            'car_number' => 'required',
            'price' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ]);
        $user_id = Auth::id();

        $car = Car::create(['title' => $request->input('title'),
            'description' => $request->input('description'),
            'contact_number' => $request->input('contact_number'),
            'email' => $request->input('email'),
            'type_id' => $request->input('type_id'),
            'district_id' => $request->input('district_id'),
            'city_id' => $request->input('city_id'),
            'address' => $request->input('address'),
            'location' => $request->input('location'),
            'lat' => $request->input('lat'),
            'lng' => $request->input('lng'),
            'passenger' => $request->input('passenger'),
            'gearshift' => $request->input('gearshift'),
            'baggage' => $request->input('baggage'),
            'door' => $request->input('door'),
            'car_number' => $request->input('car_number'),
            'price' => $request->input('price'),
            'fb_page_link' => $request->input('fb_page_link'),
            'twiter_link' => $request->input('twiter_link'),
            'instergram_link' => $request->input('instergram_link'),
            'website_link' => $request->input('website_link'),
            'user_id' => $user_id,
        ]);
        $car->features()->attach($request->features);


        $filebanner = $request->file('banner_image');
        $filefeature = $request->file('feature_image');

        if (!empty($filebanner)) {
            $destinationPath = 'uploads/cars/banner/';
            $filename = $filebanner->getClientOriginalName();
            $filebanner->move($destinationPath, $filename);
            $imageModel = new CarImages();
            $imageModel->image_name = $destinationPath . $filename;
            $imageModel->car_id = $car->id;
            $imageModel->is_featured = 0;
            $imageModel->save();

        }
        if (!empty($filefeature)) {
            $destinationPath = 'uploads/cars/feature/';
            $filename = $filefeature->getClientOriginalName();
            $filefeature->move($destinationPath, $filename);
            $imageModel = new CarImages();
            $imageModel->image_name = $destinationPath . $filename;
            $imageModel->car_id = $car->id;
            $imageModel->is_featured = 1;
            $imageModel->save();
        }
        $files = $request->gallery_image;

        foreach ($files as $file) {
            $destinationPath = 'uploads/cars/all/';
            $filename = $file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $imageModel = new CarImages(); // this model you have to create
            $imageModel->image_name = $destinationPath . '' . $filename;
            $imageModel->car_id = $car->id;
            $imageModel->save();
        }
        toastr()->success('Data has been saved successfully!');
        return redirect()->route('cars.index');

    }
    public function changeStatus(Request $request)
    {
        $user = Car::withoutGlobalScopes()->find($request->car_id);
        $user->publish_status = $request->publish_status;
        $user->save();

        return response()->json(['success'=>'Status change successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $car = Car::withoutGlobalScopes()->with(['features','images'])->find($id);
        return view('backend.cars.show',compact('car'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $car = Car::withoutGlobalScopes()->find($id);
        $types = CarType::all();
        $features = CarFeatures::all();
        $districts = District::all();
        $cities = City::all();
        $carFacility = DB::table("car_car_features")->where("car_car_features.car_id",$id)
            ->pluck('car_car_features.car_features_id','car_car_features.car_features_id')
            ->all();
        return view('backend.cars.edit',compact('car','types','features','districts','cities','carFacility'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'contact_number' => 'required',
            'email' => 'required',
            'type_id' => 'sometimes|required',
            'district_id' => 'required',
            'city_id' => 'required',
            'address' => 'required',
            'location' => 'required',
            'passenger' => 'required',
            'gearshift' => 'required',
            'baggage' => 'required',
            'door' => 'required',
            'car_number' => 'required',
            'price' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ]);
        $user_id = Auth::id();

        $car = Car::withoutGlobalScopes()->find($id);
        $car->title = $request->input('title');
            $car->description = $request->input('description');
            $car->contact_number = $request->input('contact_number');
            $car->email = $request->input('email');
            $car->type_id = $request->input('type_id');
            $car->district_id = $request->input('district_id');
            $car->city_id = $request->input('city_id');
            $car->address = $request->input('address');
            $car->location = $request->input('location');
            $car->lat = $request->input('lat');
            $car->lng = $request->input('lng');
            $car->passenger = $request->input('passenger');
            $car->gearshift = $request->input('gearshift');
            $car->baggage = $request->input('baggage');
            $car->door = $request->input('door');
            $car->car_number = $request->input('car_number');
            $car->price = $request->input('price');
            $car->fb_page_link = $request->input('fb_page_link');
            $car->twiter_link = $request->input('twiter_link');
            $car->instergram_link = $request->input('instergram_link');
            $car->website_link = $request->input('website_link');
            $car->user_id = $user_id;
        $car->save();

        $car->features()->sync($request->features);


        $filebanner = $request->file('banner_image');
        $filefeature = $request->file('feature_image');

        if (!empty($filebanner)) {
            $carpath = CarImages::where('is_featured',0)->where('car_id',$id)->first();
            if (!empty($carpath)) {
                $destinationPath = 'uploads/cars/banner/';
                File::delete($destinationPath . $filebanner->getClientOriginalName());//delete current image from storage
                $filename = $filebanner->getClientOriginalName();
                $filebanner->move($destinationPath, $filename);
                $carpath->image_name = $destinationPath . "/" . $filename;
                $carpath->save();
            }else {
                $destinationPath = 'uploads/cars/banner/';
                $filename = $filebanner->getClientOriginalName();
                $filebanner->move($destinationPath, $filename);
                $imageModel = new CarImages();
                $imageModel->image_name = $destinationPath . $filename;
                $imageModel->car_id = $car->id;
                $imageModel->is_featured = 0;
                $imageModel->save();
            }
        }
        if (!empty($filefeature)) {
            $carpath = CarImages::where('is_featured',1)->where('car_id',$id)->first();
            if (!empty($carpath)) {
                $destinationPath = 'uploads/cars/feature/';
                File::delete($destinationPath . $filebanner->getClientOriginalName());//delete current image from storage
                $filename = $filebanner->getClientOriginalName();
                $filebanner->move($destinationPath, $filename);
                $carpath->image_name = $destinationPath . "/" . $filename;
                $carpath->save();
            }else {
                $destinationPath = 'uploads/cars/feature/';
                $filename = $filefeature->getClientOriginalName();
                $filefeature->move($destinationPath, $filename);
                $imageModel = new CarImages();
                $imageModel->image_name = $destinationPath . $filename;
                $imageModel->car_id = $car->id;
                $imageModel->is_featured = 1;
                $imageModel->save();
            }
        }
        $files = $request->gallery_image;

        if (!empty($files)) {
            foreach ($files as $file) {
                $destinationPath = 'uploads/cars/all/';
                $filename = $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $imageModel = new CarImages(); // this model you have to create
                $imageModel->image_name = $destinationPath . '' . $filename;
                $imageModel->car_id = $car->id;
                $imageModel->save();
            }
        }
        toastr()->success('Data has been saved successfully!');
        return redirect()->route('cars.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $car = Car::withoutGlobalScopes()->with('features','images')->whereId($id);
        $car->delete();

        return redirect()->route('cars.index')
            ->with('success','Car deleted successfully');
    }
    public function images_destroy($id){
        $acc = CarImages::whereId($id)->first();
        $car = CarImages::whereId($id);
        $car->delete();

        return redirect()->route('cars.edit',$acc->car_id)
            ->with('success','Image deleted successfully');
    }
}
