<?php

namespace App\Http\Controllers\backend;

use App\City;
use App\Day;
use App\District;
use App\Http\Controllers\Controller;
use App\Restaurant;
use App\RestaurantFacility;
use App\RestaurantImage;
use App\RestaurantOpenHour;
use App\RestaurantType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class RestaurantController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:restaurant-list|restaurant-create|restaurant-edit|restaurant-delete', ['only' => ['index','store']]);
        $this->middleware('permission:restaurant-create', ['only' => ['create','store']]);
        $this->middleware('permission:restaurant-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:restaurant-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Auth::user()->hasRole(['Admin','Super Admin'])) {
        $restaurants = Restaurant::withoutGlobalScopes()->orderBy('id','desc')->get();
        }elseif (Auth::user()->hasRole(['Restaurant Owner'])) {
            $restaurants = Restaurant::orderBy('id','desc')->get();
        }
        //dd($roles);
        return view('backend.restaurants.index',compact('restaurants'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $days = Restaurant::DAYS;
        $types = RestaurantType::all();
        $facilities = RestaurantFacility::all();
        $districts = District::all();
        $cities = City::all();
        return view('backend.restaurants.create',compact('types','facilities','districts','cities','days'));
    }
    public function check_slug(Request $request)
    {
        $slug = Str::slug($request['title'], '-');;
        return response()->json(['slug' => $slug]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required',
            'description' => 'required',
            'contact_number' => 'required',
            'email' => 'required',
            'restaurant_type_id' => 'required',
            'facilities' => 'required',
            'district_id' => 'required',
            'city_id' => 'required',
            'address' => 'required',
            'location' => 'required',
            'banner_image' => 'required',
            'feature_image' => 'required',
            'images' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'any_time' => 'required|in:1,0',
            'days' => 'required_if:any_time,1',
            'start_time' => 'required_if:any_time,1',
            'end_time' => 'required_if:any_time,1',
        ]);
        //dd($request->all());
        $user_id = Auth::id();

        $restaurant = Restaurant::create(['title' => $request->input('title'),
            'slug' => $request->input('slug'),
            'description' => $request->input('description'),
            'contact_number' => $request->input('contact_number'),
            'email' => $request->input('email'),
            'restaurant_type_id' => $request->input('restaurant_type_id'),
            'district_id' => $request->input('district_id'),
            'city_id' => $request->input('city_id'),
            'address' => $request->input('address'),
            'location' => $request->input('location'),
            'lat' => $request->input('lat'),
            'lng' => $request->input('lng'),
            'fb_page_link' => $request->input('fb_page_link'),
            'twiter_link' => $request->input('twiter_link'),
            'instergram_link' => $request->input('instergram_link'),
            'website_link' => $request->input('website_link'),
            'any_time' => $request->input('any_time'),
            'user_id' => $user_id,
        ]);
        $restaurant->facilities()->attach($request->facilities);

        $filebanner = $request->file('banner_image');
        $filefeature = $request->file('feature_image');

        if (!empty($filebanner)) {
            $destinationPath = 'uploads/restaurant/banner/';
            $filename = $filebanner->getClientOriginalName();
            $filebanner->move($destinationPath, $filename);
            $imageModel = new RestaurantImage();
            $imageModel->restaurant_image = $destinationPath . $filename;
            $imageModel->restaurant_id = $restaurant->id;
            $imageModel->is_featured = 0;
            $imageModel->save();

        }
        if (!empty($filefeature)) {
            $destinationPath = 'uploads/restaurant/feature/';
            $filename = $filefeature->getClientOriginalName();
            $filefeature->move($destinationPath, $filename);
            $imageModel = new RestaurantImage();
            $imageModel->restaurant_image = $destinationPath . $filename;
            $imageModel->restaurant_id = $restaurant->id;
            $imageModel->is_featured = 1;
            $imageModel->save();
        }

        $files = $request->images;

        foreach ($files as $file) {
            $destinationPath = 'uploads/restaurant/all/';
            $filename = $file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $imageModel = new RestaurantImage(); // this model you have to create
            $imageModel->restaurant_image = $destinationPath . '' . $filename;
            $imageModel->restaurant_id = $restaurant->id;
            $imageModel->save();
        }
        $days = $request->days;
        $start_times = $request->start_time;
        $end_times = $request->end_time;

        foreach ($days as $key=>$day) {
            $openhours = new RestaurantOpenHour(); // this model you have to create
            $openhours->day = $day;
            $openhours->restaurant_id = $restaurant->id;
            $openhours->start_time = $start_times[$key];
            $openhours->end_time = $end_times[$key];
            $openhours->save();
        }

        toastr()->success('Data has been saved successfully!');
        return redirect()->route('restaurants.index');
    }
    public function changeStatus(Request $request)
    {
        $accommodation = Restaurant::withoutGlobalScopes()->find($request->restaurant_id);
        $accommodation->publish_status = $request->publish_status;
        $accommodation->save();

        return response()->json(['success'=>'Status change successfully.']);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Restaurant  $restaurant
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $restaurant = Restaurant::withoutGlobalScopes()->with(['facilities','images','type'])->find($id);
        return view('backend.restaurants.show',compact('restaurant'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Restaurant  $restaurant
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $restaurant = Restaurant::withoutGlobalScopes()->find($id);
        $days = Restaurant::DAYS;
        $types = RestaurantType::all();
        $facilities = RestaurantFacility::all();
        $districts = District::all();
        $cities = City::all();
        $slots = RestaurantOpenHour::whereRestaurantId($id)->get();
        $resFacility = DB::table("restaurant_restaurant_facility")->where("restaurant_restaurant_facility.restaurant_id",$id)
            ->pluck('restaurant_restaurant_facility.restaurant_facility_id','restaurant_restaurant_facility.restaurant_facility_id')
            ->all();
        return view('backend.restaurants.edit',compact('restaurant','types','facilities','districts','cities','days','resFacility','slots'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Restaurant  $restaurant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required',
            'description' => 'required',
            'contact_number' => 'required',
            'email' => 'required',
            'restaurant_type_id' => 'required',
            'facilities' => 'required',
            'district_id' => 'required',
            'city_id' => 'required',
            'address' => 'required',
            'location' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'any_time' => 'required|in:1,0',
            'days' => 'required_if:any_time,1',
            'start_time' => 'required_if:any_time,1',
            'end_time' => 'required_if:any_time,1',
        ]);
        //dd($request->all());
        $user_id = Auth::id();

        $restaurant = Restaurant::withoutGlobalScopes()->find($id);
        $restaurant->title = $request->input('title');
            $restaurant->slug = $request->input('slug');
            $restaurant->description = $request->input('description');
            $restaurant->contact_number = $request->input('contact_number');
            $restaurant->email = $request->input('email');
            $restaurant->restaurant_type_id = $request->input('restaurant_type_id');
            $restaurant->district_id = $request->input('district_id');
            $restaurant->city_id = $request->input('city_id');
            $restaurant->address = $request->input('address');
            $restaurant->location = $request->input('location');
            $restaurant->lat = $request->input('lat');
            $restaurant->lng = $request->input('lng');
            $restaurant->fb_page_link = $request->input('fb_page_link');
            $restaurant->twiter_link = $request->input('twiter_link');
            $restaurant->instergram_link = $request->input('instergram_link');
            $restaurant->website_link = $request->input('website_link');
            $restaurant->any_time = $request->input('any_time');
            $restaurant->user_id = $user_id;
            $restaurant->save();

        $restaurant->facilities()->sync($request->facilities);

        $filebanner = $request->file('banner_image');
        $filefeature = $request->file('feature_image');

        if (!empty($filebanner)) {
            $respath = RestaurantImage::where('is_featured',0)->where('restaurant_id',$id)->first();
            if (!empty($respath)) {
                $destinationPath = 'uploads/restaurant/banner/';
                File::delete($destinationPath . $filebanner->getClientOriginalName());//delete current image from storage
                $filename = $filebanner->getClientOriginalName();
                $filebanner->move($destinationPath, $filename);
                $respath->restaurant_image = $destinationPath . "/" . $filename;
                $respath->save();
            }else {
                $destinationPath = 'uploads/restaurant/banner/';
                $filename = $filebanner->getClientOriginalName();
                $filebanner->move($destinationPath, $filename);
                $imageModel = new RestaurantImage();
                $imageModel->restaurant_image = $destinationPath . $filename;
                $imageModel->restaurant_id = $restaurant->id;
                $imageModel->is_featured = 0;
                $imageModel->save();
            }
        }
        if (!empty($filefeature)) {
            $respath = RestaurantImage::where('is_featured',1)->where('restaurant_id',$id)->first();
            if (!empty($respath)) {
                $destinationPath = 'uploads/restaurant/feature/';
                File::delete($destinationPath . $filefeature->getClientOriginalName());//delete current image from storage
                $filename = $filefeature->getClientOriginalName();
                $filefeature->move($destinationPath, $filename);
                $respath->restaurant_image = $destinationPath . "/" . $filename;
                $respath->save();
            }else {
                $destinationPath = 'uploads/restaurant/feature/';
                $filename = $filefeature->getClientOriginalName();
                $filefeature->move($destinationPath, $filename);
                $imageModel = new RestaurantImage();
                $imageModel->restaurant_image = $destinationPath . $filename;
                $imageModel->restaurant_id = $restaurant->id;
                $imageModel->is_featured = 1;
                $imageModel->save();
            }
        }

        $files = $request->images;
        if (!empty($files)) {
            foreach ($files as $file) {
                $destinationPath = 'uploads/restaurant/all/';
                $filename = $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $imageModel = new RestaurantImage(); // this model you have to create
                $imageModel->restaurant_image = $destinationPath . '' . $filename;
                $imageModel->restaurant_id = $restaurant->id;
                $imageModel->save();
            }
        }
        $days = $request->days;
        $start_times = $request->start_time;
        $end_times = $request->end_time;
        $openhour = RestaurantOpenHour::whereRestaurantId($id)->get(['id']);
        RestaurantOpenHour::destroy($openhour->toArray());
        foreach ($days as $key=>$day) {
            $openhours = new RestaurantOpenHour();// this model you have to create
            $openhours->day = $day;
            $openhours->restaurant_id = $restaurant->id;
            $openhours->start_time = $start_times[$key];
            $openhours->end_time = $end_times[$key];
            $openhours->save();
        }

        toastr()->success('Data has been saved successfully!');
        return redirect()->route('restaurants.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Restaurant  $restaurant
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = Restaurant::withoutGlobalScopes()->with('facilities','images','items')->whereId($id);
        $res->delete();

        return redirect()->route('restaurants.index')
            ->with('success','Restaurant deleted successfully');
    }
    public function images_destroy($id){
        $restaurant = RestaurantImage::whereId($id)->first();
        $res = RestaurantImage::whereId($id);
        $res->delete();

        return redirect()->route('restaurants.edit',$restaurant->restaurant_id)
            ->with('success','Image deleted successfully');
    }
}
