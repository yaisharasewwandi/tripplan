<?php

namespace App\Http\Controllers\backend;

use App\Blog;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:blog-list|blog-create|blog-edit|blog-delete', ['only' => ['index','store']]);
        $this->middleware('permission:blog-create', ['only' => ['create','store']]);
        $this->middleware('permission:blog-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:blog-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Auth::user()->hasRole(['Admin', 'Super Admin'])) {
            $blogs = Blog::withoutGlobalScopes()->orderBy('id', 'desc')->get();
        } else{
            $blogs = Blog::orderBy('id', 'desc')->get();
        }
        //dd($roles);
        return view('backend.blogs.index',compact('blogs'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.blogs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'image' => 'required',
        ]);
        $user_id = Auth::id();

        $blog = Blog::create(['title' => $request->input('title'),
            'description' => $request->input('description'),
            'user_id' => $user_id,
        ]);

        $file = $request->file('image');

        if (!empty($file)) {
            $destinationPath = 'uploads/blog/';
            $filename = $file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $blog->image = $destinationPath . $filename;
            $blog->save();
        }

        return redirect()->route('blogs.index')
            ->with('success','Blog created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog = Blog::withoutGlobalScopes()->find($id);
        return view('backend.blogs.show',compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::withoutGlobalScopes()->find($id);


        return view('backend.blogs.edit',compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',

        ]);


        $blog = Blog::withoutGlobalScopes()->find($id);

        $blog->title = $request->input('title');
        $blog->description = $request->input('description');
        $file = $request->file('image');

        if (!empty($file)) {
            $destinationPath = 'uploads/blog/';
            $filename = $file->getClientOriginalName();
            $file->move(public_path('uploads/blog/'), $filename);
            $blog->image = $destinationPath . $filename;
        }

        $blog->update();

        return redirect()->route('blogs.index')
            ->with('success','Blog updated successfully');
    }

    public function changeStatus(Request $request)
    {
        $user = Blog::withoutGlobalScopes()->find($request->blog_id);
        $user->publish_status = $request->publish_status;
        $user->save();

        return response()->json(['success'=>'Status change successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Blog::withoutGlobalScopes()->find($id);
        $blog->delete();
//        dd($blog);
        toastr()->success('Blog Delete Successful!');
        return redirect()->route('blogs.index');
    }
}
