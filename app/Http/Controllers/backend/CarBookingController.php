<?php

namespace App\Http\Controllers\backend;

use App\CarBooking;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CarBookingController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:car-booking-list|car-booking-create|car-booking-edit|car-booking-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:car-booking-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:car-booking-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:car-booking-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $booking = CarBooking::withoutGlobalScopes();
        if (Auth::user()->hasRole(['Admin','Super Admin'])) {
            $booking->with('car')->orderBy('id','desc');
        }else{
            $booking->whereHas('car', function ($q) {
                $q->where('user_id',Auth::id());
            });
        }
        $booking = $booking->get();
        return view('backend.car-booking.index',compact('booking'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    public function changeStatus(Request $request)
    {
        $user = CarBooking::find($request->booking_id);
        $user->approve = $request->approve;
        $user->save();

        return response()->json(['success'=>'Status change successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $car = CarBooking::findorfail($id);
        $car->delete();
        return redirect()->route('car-booking.index')
            ->with('success','Car Booking deleted successfully');
    }
}
