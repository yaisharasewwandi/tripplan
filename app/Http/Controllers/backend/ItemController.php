<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Item;
use App\ItemAvailableTime;
use App\ItemImage;
use App\ItemType;
use App\Restaurant;
use App\RestaurantType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required',
            'description' => 'required',
            'restaurants_id' => 'required',
            'item_type_id' => 'required',
            'price' => 'required',
            'special_price' => 'required',
            'any_time' => 'required|in:1,0',
            'days' => 'required_if:any_time,0',
            'start_time' => 'required_if:any_time,0',
            'end_time' => 'required_if:any_time,0',
            'is_adult_product' => 'required|in:1,0',
            'governmet_regulations' => 'required_if:is_adult_product,1',
            'is_delivery' => 'required|in:2,1,0',
            'delivery_fee' => 'required_if:is_delivery,1',
        ]);
        //dd($request->all());


        $item = Item::create(['title' => $request->input('title'),
            'slug' => $request->input('slug'),
            'description' => $request->input('description'),
            'restaurants_id' => $request->input('restaurants_id'),
            'item_type_id' => $request->input('item_type_id'),
            'price' => $request->input('price'),
            'special_price' => $request->input('special_price'),
            'any_time' => $request->input('any_time'),
            'nutritions' => $request->input('nutritions'),
            'benifits' => $request->input('	benifits'),
            'contains' => $request->input('	contains'),
            'sugar_level' => $request->input('sugar_level'),
            'fat_level' => $request->input('fat_level'),
            'salt_level' => $request->input('salt_level'),
            'is_adult_product' => $request->input('is_adult_product'),
            'governmet_regulations' => $request->input('governmet_regulations'),
            'is_delivery' => $request->input('is_delivery'),
            'delivery_fee' => $request->input('delivery_fee'),

        ]);


        $files = $request->images;

        foreach ($files as $file) {
            $destinationPath = 'uploads/items/';
            $filename = $file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $imageModel = new ItemImage(); // this model you have to create
            $imageModel->item_image = $destinationPath . '' . $filename;
            $imageModel->item_id = $item->id;
            $imageModel->save();
        }
        $days = $request->days;
        $start_times = $request->start_time;
        $end_times = $request->end_time;
        if (!empty($days)) {
            foreach ($days as $key => $day) {
                $openhours = new ItemAvailableTime(); // this model you have to create
                $openhours->day = $day;
                $openhours->item_id = $item->id;
                $openhours->start_time = $start_times[$key];
                $openhours->end_time = $end_times[$key];
                $openhours->save();
            }
        }

        toastr()->success('Data has been saved successfully!');
        return redirect()->route('items.show', [$request->restaurants_id]);
    }
    public function changeStatus(Request $request)
    {
        if (Auth::user()->hasRole(['Admin','Super Admin'])) {
        $accommodation = Item::withoutGlobalScopes()->find($request->item_id);
        }elseif (Auth::user()->hasRole(['Restaurant Owner'])) {
            $accommodation = Item::find($request->item_id);
        }
        $accommodation->publish_status = $request->publish_status;
        $accommodation->save();

        return response()->json(['success'=>'Status change successfully.']);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $types = ItemType::all();
        $items = Item::withoutGlobalScopes()->whereIn('restaurants_id',[$id])->get();
        $days = Item::DAYS;
        return view('backend.items.create',compact('types','items','id','days'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function check_slug(Request $request)
    {
        $slug = Str::slug($request['title'], '-');;
        return response()->json(['slug' => $slug]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $types = ItemType::all();
        $items = Item::find($id);
        $days = Item::DAYS;
        $slots = ItemAvailableTime::whereItemId($id)->get();
        return view('backend.items.edit',compact('types','items','id','days','slots'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required',
            'description' => 'required',
            'restaurants_id' => 'required',
            'item_type_id' => 'required',
            'price' => 'required',
            'special_price' => 'required',
            'any_time' => 'required|in:1,0',
            'days' => 'required_if:any_time,0',
            'start_time' => 'required_if:any_time,0',
            'end_time' => 'required_if:any_time,0',
            'is_adult_product' => 'required|in:1,0',
            'governmet_regulations' => 'required_if:is_adult_product,1',
            'is_delivery' => 'required|in:2,1,0',
            'delivery_fee' => 'required_if:is_delivery,1',
        ]);
        //dd($request->all());


        $item = Item::withoutGlobalScopes()->find($id);
        $item->title = $request->input('title');
        $item->slug = $request->input('slug');
        $item->description = $request->input('description');
        $item->restaurants_id = $request->input('restaurants_id');
        $item->item_type_id = $request->input('item_type_id');
        $item->price = $request->input('price');
        $item->special_price = $request->input('special_price');
        $item->any_time = $request->input('any_time');
        $item->nutritions = $request->input('nutritions');
        $item->benifits = $request->input('	benifits');
        $item->contains = $request->input('	contains');
        $item->sugar_level = $request->input('sugar_level');
        $item->fat_level = $request->input('fat_level');
        $item->salt_level = $request->input('salt_level');
        $item->is_adult_product = $request->input('is_adult_product');
        $item->governmet_regulations = $request->input('governmet_regulations');
        $item->is_delivery = $request->input('is_delivery');
        $item->delivery_fee = $request->input('delivery_fee');
        $item->save();


        $files = $request->images;
        if (!empty($files)) {
            foreach ($files as $file) {
                $destinationPath = 'uploads/items/';
                $filename = $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $imageModel = new ItemImage(); // this model you have to create
                $imageModel->item_image = $destinationPath . '' . $filename;
                $imageModel->item_id = $item->id;
                $imageModel->save();
            }
        }
        $days = $request->days;
        $start_times = $request->start_time;
        $end_times = $request->end_time;
        $openhour = ItemAvailableTime::whereItemId($id)->get(['id']);
        ItemAvailableTime::destroy($openhour->toArray());
        if (!empty($days)) {
            foreach ($days as $key => $day) {
                $openhours = new ItemAvailableTime(); // this model you have to create
                $openhours->day = $day;
                $openhours->item_id = $item->id;
                $openhours->start_time = $start_times[$key];
                $openhours->end_time = $end_times[$key];
                $openhours->save();
            }
        }

        toastr()->success('Data has been saved successfully!');
        return redirect()->route('items.edit',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $acc = Item::withoutGlobalScopes()->whereId($id)->first();
        $item = Item::withoutGlobalScopes()->with('images')->whereId($id);
        $item->delete();

        return redirect()->route('items.show',$acc->restaurants_id)
            ->with('success','Item deleted successfully');
    }
    public function images_destroy($id){
        $acc = ItemImage::whereId($id)->first();
        $item = ItemImage::whereId($id);
        $item->delete();

        return redirect()->route('items.edit',$acc->item_id)
            ->with('success','Image deleted successfully');
    }
}
