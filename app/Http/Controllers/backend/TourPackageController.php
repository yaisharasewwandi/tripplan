<?php

namespace App\Http\Controllers\backend;

use App\TourCategory;
use App\TourPackage;
use App\TourPackageImage;
use App\TourpackageSpecialTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class TourPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'travel_agency_id' => 'required',
            'title' => 'required',
            'slug' => 'required',
            'description' => 'required',
            'location' => 'required',
            'duration' => 'required',
            'min_person' => 'required',
            'max_person' => 'required',
            'budget' => 'required',
            'includes' => 'required',
            'excludes' => 'required',
            'special_day' => 'required|in:1,0',
            'days' => 'required_if:special_day,1',
            'times' => 'required_if:special_day,1',
            'available_times' => 'required_if:special_day,0',

        ]);
        $includes = json_encode($request->input('includes'));
        $excludes =json_encode($request->input('excludes'));
        $available_times =json_encode($request->input('available_times'));

        $packages = TourPackage::create(['title' => $request->input('title'),
            'slug' => $request->input('slug'),
            'description' => $request->input('description'),
            'location' => $request->input('location'),
            'duration' => $request->input('duration'),
            'min_person' => $request->input('min_person'),
            'max_person' => $request->input('max_person'),
            'budget' => $request->input('budget'),
            'includes' => $includes,
            'excludes' => $excludes,
            'available_times' => $available_times,
            'travel_agency_id' => $request->input('travel_agency_id'),
        ]);

        $packages->categories()->attach($request->categories);

        $filefeature = $request->file('feature_image');

        if (!empty($filefeature)) {
            $destinationPath = 'uploads/tour_package/featured/';
            $filename = $filefeature->getClientOriginalName();
            $filefeature->move($destinationPath, $filename);
            $imageModel = new TourPackageImage();
            $imageModel->image_name = $destinationPath . $filename;
            $imageModel->tour_package_id = $packages->id;
            $imageModel->is_featured = 1;
            $imageModel->save();

        }

        $files = $request->images;

        foreach ($files as $file) {
            $destinationPath = 'uploads/tour_package/all/';
            $filename = $file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $imageModel = new TourPackageImage(); // this model you have to create
            $imageModel->image_name = $destinationPath . '' . $filename;
            $imageModel->tour_package_id = $packages->id;
            $imageModel->save();
        }
        $days = $request->days;
        $times = $request->times;

        foreach ($days as $key=>$day) {
            $openhours = new TourpackageSpecialTime(); // this model you have to create
            $openhours->day = $day;
            $openhours->tour_package_id = $packages->id;
            $openhours->time = $times[$key];
            $openhours->save();
        }
        toastr()->success('Data has been saved successfully!');
        return redirect()->route('tour-packages.show', [$request->travel_agency_id]);

    }
    public function changeStatus(Request $request)
    {
        $user = TourPackage::withoutGlobalScopes()->find($request->packages_id);
        $user->publish_status = $request->publish_status;
        $user->save();

        return response()->json(['success'=>'Status change successfully.']);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\TourPackage  $tourPackage
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        if (Auth::user()->hasRole(['Admin','Super Admin'])) {
        $tourcategories = TourCategory::withoutGlobalScopes()->get();
        }elseif (Auth::user()->hasRole(['Agency Owner'])) {
            $tourcategories = TourCategory::all();
        }
        $packages = TourPackage::whereIn('travel_agency_id',[$id])->get();
        //dd($rooms);
        return view('backend.tour-packages.create',compact('tourcategories','packages','id'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }
    public function check_slug(Request $request)
    {
        $slug = Str::slug($request['title'], '-');;
        return response()->json(['slug' => $slug]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TourPackage  $tourPackage
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tourcategories = TourCategory::all();
        $packages = TourPackage::withoutGlobalScopes()->find($id);
        $selectcategory = DB::table("tour_package_categories")->where("tour_package_categories.tour_package_id",$id)
            ->pluck('tour_package_categories.tour_category_id','tour_package_categories.tour_category_id')
            ->all();
        //dd($rooms);
        return view('backend.tour-packages.edit',compact('tourcategories','packages','id','selectcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TourPackage  $tourPackage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [

            'travel_agency_id' => 'required',
            'title' => 'required',
            'slug' => 'required',
            'description' => 'required',
            'location' => 'required',
            'duration' => 'required',
            'min_person' => 'required',
            'max_person' => 'required',
            'budget' => 'required',
            'includes' => 'required',
            'excludes' => 'required',
            'special_day' => 'required|in:1,0',
            'days' => 'required_if:special_day,1',
            'times' => 'required_if:special_day,1',
            'available_times' => 'required_if:special_day,0',

        ]);
        $includes = json_encode($request->input('includes'));
        $excludes =json_encode($request->input('excludes'));
        $available_times =json_encode($request->input('available_times'));

        $packages = TourPackage::withoutGlobalScopes()->find($id);
        $packages->title = $request->input('title');
        $packages->slug =$request->input('slug');
        $packages->description = $request->input('description');
        $packages->location =$request->input('location');
        $packages->duration = $request->input('duration');
        $packages->min_person =$request->input('min_person');
        $packages->max_person = $request->input('max_person');
        $packages->budget = $request->input('budget');
        $packages->includes = $includes;
        $packages->excludes = $excludes;
        $packages->available_times = $available_times;
        $packages->travel_agency_id =$request->input('travel_agency_id');
        $packages->save();

        $packages->categories()->sync($request->categories);

        $filefeature = $request->file('feature_image');

        if (!empty($filefeature)) {
            $tourpath = TourPackageImage::where('is_featured',1)->where('tour_package_id',$id)->first();
            if (!empty($tourpath)) {
                $destinationPath = 'uploads/tour_package/featured/';
                File::delete($destinationPath . $filefeature->getClientOriginalName());//delete current image from storage
                $filename = $filefeature->getClientOriginalName();
                $filefeature->move($destinationPath, $filename);
                $tourpath->room_image_name = $destinationPath . "/" . $filename;
                $tourpath->save();
            }else {
                $destinationPath = 'uploads/tour_package/featured/';
                $filename = $filefeature->getClientOriginalName();
                $filefeature->move($destinationPath, $filename);
                $imageModel = new TourPackageImage();
                $imageModel->image_name = $destinationPath . $filename;
                $imageModel->tour_package_id = $packages->id;
                $imageModel->is_featured = 1;
                $imageModel->save();
            }

        }

        $files = $request->images;
        if (!empty($files)) {
            foreach ($files as $file) {
                $destinationPath = 'uploads/tour_package/all/';
                $filename = $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $imageModel = new TourPackageImage(); // this model you have to create
                $imageModel->image_name = $destinationPath . '' . $filename;
                $imageModel->tour_package_id = $packages->id;
                $imageModel->save();
            }
        }
        $days = $request->days;
        $times = $request->times;
        $openhour = TourpackageSpecialTime::whereTourPackageId($id)->get(['id']);
        TourpackageSpecialTime::destroy($openhour->toArray());
        if (!empty($days)) {
            foreach ($days as $key => $day) {
                $openhours = new TourpackageSpecialTime(); // this model you have to create
                $openhours->day = $day;
                $openhours->tour_package_id = $packages->id;
                $openhours->time = $times[$key];
                $openhours->save();
            }
        }
        toastr()->success('Data has been saved successfully!');
        return redirect()->route('tour-packages.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TourPackage  $tourPackage
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $acc = TourPackage::withoutGlobalScopes()->whereId($id)->first();
        $item = TourPackage::withoutGlobalScopes()->with('images')->whereId($id);
        $item->delete();

        return redirect()->route('tour-packages.show',$acc->travel_agency_id)
            ->with('success','Package deleted successfully');
    }
    public function images_destroy($id){
        $acc = TourPackageImage::withoutGlobalScopes()->whereId($id)->first();
        $tour = TourPackageImage::withoutGlobalScopes()->whereId($id);
        $tour->delete();

        return redirect()->route('tour-packages.edit',$acc->travel_agency_id)
            ->with('success','Image deleted successfully');
    }
}
