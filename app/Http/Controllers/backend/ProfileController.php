<?php

namespace App\Http\Controllers\backend;

use App\City;
use App\District;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(){
        $districts = District::all();
        $cities = City::all();
        return view('backend.profile.create',compact('districts','cities'));
    }
}
