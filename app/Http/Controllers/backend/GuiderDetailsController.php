<?php

namespace App\Http\Controllers\backend;

use App\GuiderDetails;
use App\GuiderTravelImage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GuiderDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'guider_id' => 'required',
            'name' => 'required',
            'description' => 'required',
            'location' => 'required',
            'days' => 'required',
            'price' => 'required',
            'images' => 'required',

        ]);

        $details = GuiderDetails::create(['name' => $request->input('name'),
            'description' => $request->input('description'),
            'location' => $request->input('location'),
            'days' => $request->input('days'),
            'price' => $request->input('price'),
            'guider_id' => $request->input('guider_id'),
        ]);


        $files = $request->images;

        foreach ($files as $file) {
            $destinationPath = 'uploads/guider-travel/all/';
            $filename = $file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $imageModel = new GuiderTravelImage(); // this model you have to create
            $imageModel->image_name = $destinationPath . '' . $filename;
            $imageModel->guider_travel_info_id = $details->id;
            $imageModel->save();
        }

        toastr()->success('Data has been saved successfully!');
        return redirect()->route('guider-details.show', [$request->guider_id]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GuiderDetails  $guiderDetails
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        if (Auth::user()->hasRole(['Admin','Super Admin'])) {
        $guiderDetails = GuiderDetails::withoutGlobalScopes()->whereIn('guider_id',[$id])->get();
        }elseif (Auth::user()->hasRole(['Guider'])) {
            $guiderDetails = GuiderDetails::whereIn('guider_id', [$id])->get();
        }
        //dd($rooms);
        return view('backend.guider-details.create',compact('guiderDetails','id'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }
    public function changeStatus(Request $request)
    {
        $user = GuiderDetails::find($request->travel_id);
        $user->publish_status = $request->publish_status;
        $user->save();

        return response()->json(['success'=>'Status change successfully.']);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GuiderDetails  $guiderDetails
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $details = GuiderDetails::find($id);

        //dd($rooms);
        return view('backend.guider-details.edit',compact('details'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GuiderDetails  $guiderDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [

            'name' => 'required',
            'description' => 'required',
            'location' => 'required',
            'days' => 'required',
            'price' => 'required',

        ]);

        $details = GuiderDetails::find($id);
        $details->name = $request->input('name');
        $details->description = $request->input('description');
        $details->location = $request->input('location');
        $details->days = $request->input('days');
        $details->price = $request->input('price');
       $details->save();


        $files = $request->images;
        if (!empty($files)) {
            foreach ($files as $file) {
                $destinationPath = 'uploads/guider-travel/all/';
                $filename = $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $imageModel = new GuiderTravelImage(); // this model you have to create
                $imageModel->image_name = $destinationPath . '' . $filename;
                $imageModel->guider_travel_info_id = $details->id;
                $imageModel->save();
            }
        }

        toastr()->success('Data has been saved successfully!');
        return redirect()->route('guider-details.edit', $id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GuiderDetails  $guiderDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $acc = GuiderDetails::whereId($id)->first();
        $item = GuiderDetails::with('images')->whereId($id);
        $item->delete();

        return redirect()->route('guider-details.show',$acc->guider_id)
            ->with('success','Travel Details deleted successfully');
    }
    public function images_destroy($id){
        $acc = GuiderTravelImage::whereId($id)->first();
        $guid = GuiderTravelImage::whereId($id);
        $guid->delete();

        return redirect()->route('guider-details.edit',$acc->guider_travel_info_id)
            ->with('success','Image deleted successfully');
    }
}
