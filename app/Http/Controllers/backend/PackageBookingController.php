<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\TourPackageBooking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PackageBookingController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:package-booking-list|package-booking-create|package-booking-edit|package-booking-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:package-booking-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:package-booking-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:package-booking-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $booking = TourPackageBooking::withoutGlobalScopes();
        if (Auth::user()->hasRole(['Admin','Super Admin'])) {
        $booking->with('tour')->orderBy('id','desc')->get();
        }else {
            $booking->whereHas('tour.travelAgency', function ($q) {
                $q->where('user_id',Auth::id());
            });
        }
        $booking =$booking->get();
//dd(Auth::id());
        return view('backend.package-booking.index',compact('booking'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function changeStatus(Request $request)
    {
        $user = TourPackageBooking::find($request->booking_id);
        $user->approve = $request->approve;
        $user->save();

        return response()->json(['success'=>'Status change successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $package = TourPackageBooking::findorfail($id);
        $package->delete();
        return redirect()->route('package-booking.index')
            ->with('success','Package Booking deleted successfully');
    }
}
