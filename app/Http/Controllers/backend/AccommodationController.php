<?php

namespace App\Http\Controllers\backend;

use App\Accommodation;
use App\AccommodationImage;
use App\AccommodationRoom;
use App\AccommodationType;
use App\City;
use App\District;
use App\Facility;
use App\Http\Controllers\Controller;
use App\Service;
use App\Town;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class AccommodationController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:hotel-list|hotel-create|hotel-edit|hotel-delete', ['only' => ['index','store']]);
        $this->middleware('permission:hotel-create', ['only' => ['create','store']]);
        $this->middleware('permission:hotel-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:hotel-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Auth::user()->hasRole(['Admin','Super Admin'])) {
            $accommodations = Accommodation::withoutGlobalScopes()->orderBy('id', 'desc')->get();
        }elseif (Auth::user()->hasRole(['Hotel Owner'])){
            $accommodations = Accommodation::orderBy('id', 'desc')->get();
        }
        //dd($roles);
        return view('backend.accommodations.index',compact('accommodations'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $types = AccommodationType::all();
        $facilities = Facility::all();
        $services = Service::all();
        $districts = District::all();
        $cities = City::all();
        return view('backend.accommodations.create',compact('types','facilities','services','districts','cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'contact_number' => 'required',
            'email' => 'required',
            'type_id' => 'required',
            'facilities' => 'required',
            'services' => 'required',
            'district_id' => 'required',
            'city_id' => 'required',
            'address' => 'required',
            'location' => 'required',
            'banner_image' => 'required',
            'feature_image' => 'required',
            'term_condition' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'check_in' => 'required',
            'check_out' => 'required',
            'price' => 'required',
            'images' => 'required',
        ]);
        //dd($request->all());
        $user_id = Auth::id();

        $accommodation = Accommodation::create(['title' => $request->input('title'),
            'description' => $request->input('description'),
            'contact_number' => $request->input('contact_number'),
            'email' => $request->input('email'),
            'type_id' => $request->input('type_id'),
            'district_id' => $request->input('district_id'),
            'city_id' => $request->input('city_id'),
            'address' => $request->input('address'),
            'location' => $request->input('location'),
            'lat' => $request->input('lat'),
            'lng' => $request->input('lng'),
            'check_in' => $request->input('check_in'),
            'check_out' => $request->input('check_out'),
            'price' => $request->input('price'),
            'term_condition' => $request->input('term_condition'),
            'fb_page_link' => $request->input('fb_page_link'),
            'twiter_link' => $request->input('twiter_link'),
            'instergram_link' => $request->input('instergram_link'),
            'website_link' => $request->input('website_link'),
            'user_id' => $user_id,
        ]);
        $accommodation->facilities()->attach($request->facilities);
        $accommodation->services()->attach($request->services);

        $filebanner = $request->file('banner_image');
        $filefeature = $request->file('feature_image');

        if (!empty($filebanner)) {
            $destinationPath = 'uploads/accommodation/banner/';
            $filename = $filebanner->getClientOriginalName();
            $filebanner->move($destinationPath, $filename);
            $imageModel = new AccommodationImage();
            $imageModel->accommodation_image = $destinationPath . $filename;
            $imageModel->accommodation_id = $accommodation->id;
            $imageModel->is_featured = 0;
            $imageModel->save();

        }
        if (!empty($filefeature)) {
            $destinationPath = 'uploads/accommodation/feature/';
            $filename = $filefeature->getClientOriginalName();
            $filefeature->move($destinationPath, $filename);
            $imageModel = new AccommodationImage();
            $imageModel->accommodation_image = $destinationPath . $filename;
            $imageModel->accommodation_id = $accommodation->id;
            $imageModel->is_featured = 1;
            $imageModel->save();
        }

        $files = $request->images;

        foreach ($files as $file) {
            $destinationPath = 'uploads/accommodation/all/';
            $filename = $file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $imageModel = new AccommodationImage(); // this model you have to create
            $imageModel->accommodation_image = $destinationPath . '' . $filename;
            $imageModel->accommodation_id = $accommodation->id;
            $imageModel->save();
        }
        toastr()->success('Data has been saved successfully!');
        return redirect()->route('accommodations.index');

    }
    public function changeStatus(Request $request)
    {
        $accommodation = Accommodation::withoutGlobalScopes()->find($request->accommodation_id);
        $accommodation->publish_status = $request->publish_status;
        $accommodation->save();

        return response()->json(['success'=>'Status change successfully.']);
    }
    public function dependancydistrict(Request $request)
    {
        return District::Find($request->did)->cities->pluck('name_en','id');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $accommodation = Accommodation::withoutGlobalScopes()->with(['facilities','services','types','images'])->find($id);
        return view('backend.accommodations.show',compact('accommodation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $accommodation = Accommodation::withoutGlobalScopes()->find($id);
        $types = AccommodationType::all();
        $facilities = Facility::all();
        $services = Service::all();
        $districts = District::all();
        $cities = City::all();
        $accoFacility = DB::table("accommodation_facility")->where("accommodation_facility.accommodation_id",$id)
            ->pluck('accommodation_facility.facility_id','accommodation_facility.facility_id')
            ->all();
        $accoServices= DB::table("accommodation_service")->where("accommodation_service.accommodation_id",$id)
            ->pluck('accommodation_service.service_id','accommodation_service.service_id')
            ->all();
        return view('backend.accommodations.edit',compact('accommodation','types','facilities','services','districts','cities','accoFacility','accoServices'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'contact_number' => 'required',
            'email' => 'required',
            'type_id' => 'required',
            'facilities' => 'required',
            'services' => 'required',
            'district_id' => 'required',
            'city_id' => 'required',
            'address' => 'required',
            'location' => 'required',
//            'banner_image' => 'required',
//            'feature_image' => 'required',
            'term_condition' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'check_in' => 'required',
            'check_out' => 'required',
            'price' => 'required',
//            'images' => 'required',
        ]);

        $accommodation = Accommodation::withoutGlobalScopes()->find($id);
        $accommodation->title = $request->input('title');
        $accommodation->description = $request->input('description');
        $accommodation->contact_number = $request->input('contact_number');
        $accommodation->email = $request->input('email');
        $accommodation->type_id = $request->input('type_id');
        $accommodation->district_id = $request->input('district_id');
        $accommodation->city_id = $request->input('city_id');
        $accommodation->address = $request->input('address');
        $accommodation->location = $request->input('location');
        $accommodation->lat = $request->input('lat');
        $accommodation->lng = $request->input('lng');
        $accommodation->check_in = $request->input('check_in');
        $accommodation->check_out = $request->input('check_out');
        $accommodation->price = $request->input('price');
        $accommodation->term_condition = $request->input('term_condition');
        $accommodation->fb_page_link = $request->input('fb_page_link');
        $accommodation->twiter_link = $request->input('twiter_link');
        $accommodation->instergram_link = $request->input('instergram_link');
        $accommodation->website_link = $request->input('website_link');
        $accommodation->save();

        $filebanner = $request->file('banner_image');
        $filefeature = $request->file('feature_image');
        if (!empty($filebanner)) {
            $accommodationpath = AccommodationImage::where('is_featured',0)->where('accommodation_id',$id)->first();
            if (!empty($accommodationpath)) {
                $destinationPath = 'uploads/accommodation/banner';
                File::delete($destinationPath . $filebanner->getClientOriginalName());//delete current image from storage
                $filename = $filebanner->getClientOriginalName();
                $filebanner->move($destinationPath, $filename);
                $accommodationpath->accommodation_image = $destinationPath . "/" . $filename;
                $accommodationpath->save();
            }else {
                $destinationPath = 'uploads/accommodation/banner/';
                $filename = $filebanner->getClientOriginalName();
                $filebanner->move($destinationPath, $filename);
                $imageModel = new AccommodationImage();
                $imageModel->accommodation_image = $destinationPath . $filename;
                $imageModel->accommodation_id = $accommodation->id;
                $imageModel->is_featured = 0;
                $imageModel->save();
            }
        }
        if (!empty($filefeature)) {
            $accommodationpath = AccommodationImage::where('is_featured',1)->where('accommodation_id',$id)->first();
            if (!empty($accommodationpath)) {
                $destinationPath = 'uploads/accommodation/feature';
                File::delete($destinationPath . $filefeature->getClientOriginalName());//delete current image from storage
                $filename = $filefeature->getClientOriginalName();
                $filefeature->move($destinationPath, $filename);
                $accommodationpath->accommodation_image = $destinationPath . "/" . $filename;
                $accommodationpath->save();
            }else {
                $destinationPath = 'uploads/accommodation/feature/';
                $filename = $filefeature->getClientOriginalName();
                $filefeature->move($destinationPath, $filename);
                $imageModel = new AccommodationImage();
                $imageModel->accommodation_image = $destinationPath . $filename;
                $imageModel->accommodation_id = $accommodation->id;
                $imageModel->is_featured = 1;
                $imageModel->save();
            }
        }
        $files = $request->images;
        if (!empty($files)) {
            foreach ($files as $file) {
                $destinationPath = 'uploads/accommodation/all/';
                $filename = $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $imageModel = new AccommodationImage(); // this model you have to create
                $imageModel->accommodation_image = $destinationPath . '' . $filename;
                $imageModel->accommodation_id = $accommodation->id;
                $imageModel->save();
            }
        }
        $accommodation->facilities()->sync($request->input('facilities'));
        $accommodation->services()->sync($request->input('services'));


        toastr()->success('Hotel updated successfully!');
        return redirect()->route('accommodations.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $accommodation = Accommodation::withoutGlobalScopes()->with('facilities','accommodationrooms','accommodationrooms.roomFeatures','accommodationrooms.images','services','images')->whereId($id);
        $accommodation->delete();
        toastr()->success('Accommodation deleted successfully!');
        return redirect()->route('accommodations.index');
    }

    public function images_destroy($id){
        $acc = AccommodationImage::whereId($id)->first();
        $accommodation = AccommodationImage::whereId($id);
        $accommodation->delete();
        toastr()->success('Image deleted successfully!');
        return redirect()->route('accommodations.edit',$acc->accommodation_id);
    }
}
