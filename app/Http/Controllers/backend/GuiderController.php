<?php

namespace App\Http\Controllers\backend;

use App\Agency;
use App\City;
use App\District;
use App\Guider;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class GuiderController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:guider-list|guider-create|guider-edit|guider-delete', ['only' => ['index','store']]);
        $this->middleware('permission:guider-create', ['only' => ['create','store']]);
        $this->middleware('permission:guider-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:guider-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Auth::user()->hasRole(['Admin','Super Admin'])) {
            $guiders = Guider::withoutGlobalScopes()->with('user')->get();
        }elseif (Auth::user()->hasRole(['Guider'])) {
            $guiders = Guider::with('user')->get();
        }
        return view('backend.guiders.index',compact('guiders'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $districts = District::all();
        $cities = City::all();
        return view('backend.guiders.create',compact('districts','cities'));
    }
    public function dependancydistrict(Request $request)
    {
        return District::Find($request->did)->cities->pluck('name_en','id');

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'description' => 'required',
            'contact_number' => 'required',
            'email' => 'required',
            'district_id' => 'required',
            'city_id' => 'required',
            'address' => 'required',
            'location' => 'required',
            'banner_image' => 'required',
            'feature_image' => 'required',
            'lat' => 'required',
            'lng' => 'required',

        ]);
        //dd($request->all());
        $user_id = Auth::id();

        $user = User::find($user_id);
        $user->name = $request->name;
        $user->contact_number = $request->contact_number;
        $user->address = $request->address;
        $user->email = $request->email;
        $user->save();

        $guiders = Guider::create([
            'description' => $request->input('description'),
            'district_id' => $request->input('district_id'),
            'city_id' => $request->input('city_id'),
            'address' => $request->input('address'),
            'location' => $request->input('location'),
            'lat' => $request->input('lat'),
            'lng' => $request->input('lng'),
            'fb_page_link' => $request->input('fb_page_link'),
            'twiter_link' => $request->input('twiter_link'),
            'instergram_link' => $request->input('instergram_link'),
            'website_link' => $request->input('website_link'),
            'user_id' => $user_id,
        ]);

        $filebanner = $request->file('banner_image');
        $filefeature = $request->file('feature_image');

        if (!empty($filebanner)) {
            $destinationPath = 'uploads/guider/banner/';
            $filename = $filebanner->getClientOriginalName();
            $filebanner->move($destinationPath, $filename);
            $guiders->banner_image = $destinationPath . $filename;
            $guiders->save();

        }
        if (!empty($filefeature)) {
            $destinationPath = 'uploads/guider/feature/';
            $filename = $filefeature->getClientOriginalName();
            $filefeature->move($destinationPath, $filename);
            $guiders->featured_image = $destinationPath . $filename;
            $guiders->save();
        }
        toastr()->success('Data has been saved successfully!');
        return redirect()->route('guiders.index');
    }
    public function changeStatus(Request $request)
    {
        $accommodation = Guider::withoutGlobalScopes()->find($request->aguider_id);
        $accommodation->publish_status = $request->publish_status;
        $accommodation->save();

        return response()->json(['success'=>'Status change successfully.']);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Guider  $guider
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $guider = Guider::withoutGlobalScopes()->find($id);
        return view('backend.guiders.show',compact('guider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Guider  $guider
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $guider = Guider::withoutGlobalScopes()->find($id);
        $districts = District::all();
        $cities = City::all();
        return view('backend.guiders.edit',compact('districts','cities','guider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Guider  $guider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->validate($request, [
            'description' => 'required',
            'contact_number' => 'required',
            'email' => 'required',
            'district_id' => 'required',
            'city_id' => 'required',
            'address' => 'required',
            'location' => 'required',
            'lat' => 'required',
            'lng' => 'required',

        ]);
        //dd($request->all());
        $user_id = Auth::id();

        $user = User::find($user_id);
        $user->name = $request->name;
        $user->contact_number = $request->contact_number;
        $user->address = $request->address;
        $user->email = $request->email;
        $user->save();

        $guiders = Guider::withoutGlobalScopes()->find($id);
        $guiders->description = $request->input('description');
        $guiders->district_id = $request->input('district_id');
        $guiders->city_id = $request->input('city_id');
        $guiders->address = $request->input('address');
        $guiders->location = $request->input('location');
        $guiders->lat = $request->input('lat');
        $guiders->lng = $request->input('lng');
        $guiders->fb_page_link = $request->input('fb_page_link');
        $guiders->twiter_link = $request->input('twiter_link');
        $guiders->instergram_link = $request->input('instergram_link');
        $guiders->website_link = $request->input('website_link');
        $guiders->user_id = $user_id;

        $filebanner = $request->file('banner_image');
        $filefeature = $request->file('feature_image');
        if (!empty($filebanner)) {
            $destinationPath = 'uploads/guider/banner/';
            File::delete($destinationPath.$guiders->banner_image);//delete current image from storage
            $filename = $filebanner->getClientOriginalName();
            $filebanner->move($destinationPath, $filename);
            $guiders->banner_image = $destinationPath ."/".$filename;
        }

        if (!empty($filefeature)) {
            $destinationPath = 'uploads/guider/feature/';
            File::delete($destinationPath.$guiders->featured_image);//delete current image from storage
            $filename = $filefeature->getClientOriginalName();
            $filefeature->move($destinationPath, $filename);
            $guiders->featured_image = $destinationPath ."/".$filename;
        }

        $guiders->save();

        toastr()->success('Data has been saved successfully!');
        return redirect()->route('guiders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Guider  $guider
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $guider = Guider::withoutGlobalScopes()->whereId($id);
        $guider->delete();

        return redirect()->route('guiders.index')
            ->with('success','Guider deleted successfully');
    }
}
