<?php

namespace App\Http\Controllers\backend;

use App\Accommodation;
use App\AccommodationRoom;
use App\Http\Controllers\Controller;
use App\RoomFeature;
use App\RoomImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class AccommodationRoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [

            'room_name' => 'required',
            'feature_image' => 'required',
            'gallery_images' => 'required',
            'number_of_room' => 'required',
            'number_of_bed' => 'required',
            'room_size' => 'required',
            'price' => 'sometimes|required',
            'max_adult' => 'required',
            'max_children' => 'required',
            'accommodation_id' => 'required',

        ]);
        $accommodationRooms = AccommodationRoom::create(['room_name' => $request->input('room_name'),
            'number_of_room' => $request->input('number_of_room'),
            'number_of_bed' => $request->input('number_of_bed'),
            'room_size' => $request->input('room_size'),
            'price' => $request->input('price'),
            'max_adult' => $request->input('max_adult'),
            'max_children' => $request->input('max_children'),
            'accommodation_id' => $request->input('accommodation_id'),
        ]);

        $accommodationRooms->roomFeatures()->attach($request->room_features);

        $filefeature = $request->file('feature_image');

        if (!empty($filefeature)) {
            $destinationPath = 'uploads/accommodation_rooms/';
            $filename = $filefeature->getClientOriginalName();
            $filefeature->move($destinationPath, $filename);
            $imageModel = new RoomImage();
            $imageModel->room_image_name = $destinationPath . $filename;
            $imageModel->accommodation_room_id = $accommodationRooms->id;
            $imageModel->is_featured = 0;
            $imageModel->save();

        }

        $files = $request->gallery_images;

        foreach ($files as $file) {
            $destinationPath = 'uploads/accommodation_rooms/';
            $filename = $file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $imageModel = new RoomImage(); // this model you have to create
            $imageModel->room_image_name = $destinationPath . '' . $filename;
            $imageModel->accommodation_room_id = $accommodationRooms->id;
            $imageModel->save();
        }
        toastr()->success('Data has been saved successfully!');
        return redirect()->route('accommodation-rooms.show', [$request->accommodation_id]);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        if (Auth::user()->hasRole(['Admin','Super Admin'])) {
        $accommodation = Accommodation::withoutGlobalScopes()->find($id);
        }elseif (Auth::user()->hasRole(['Hotel Owner'])) {
            $accommodation = Accommodation::find($id);
        }
        $roomfeatures = RoomFeature::all();
        $rooms = AccommodationRoom::whereIn('accommodation_id',[$id])->get();
        //dd($rooms);
        return view('backend.accommodation-rooms.create',compact('accommodation','roomfeatures','rooms','id'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }


    public function changeStatus(Request $request)
    {
        $user = AccommodationRoom::withoutGlobalScopes()->find($request->accommodation_room_id);
        $user->publish_status = $request->publish_status;
        $user->save();

        return response()->json(['success'=>'Status change successfully.']);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roomfeatures = RoomFeature::all();
        $roomFacility = DB::table("accommodation_room_room_feature")->where("accommodation_room_room_feature.accommodation_room_id",$id)
            ->pluck('accommodation_room_room_feature.room_feature_id','accommodation_room_room_feature.room_feature_id')
            ->all();
        $room= AccommodationRoom::withoutGlobalScopes()->with(['roomFeatures','images'])->find($id);
        return view('backend.accommodation-rooms.edit',compact('room','roomfeatures','roomFacility'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [

            'room_name' => 'required',
            'number_of_room' => 'required',
            'number_of_bed' => 'required',
            'room_size' => 'required',
            'price' => 'sometimes|required',
            'max_adult' => 'required',
            'max_children' => 'required',


        ]);
        $accommodationRooms = AccommodationRoom::withoutGlobalScopes()->find($id);
        $accommodationRooms->room_name = $request->input('room_name');
        $accommodationRooms->number_of_room = $request->input('number_of_room');
        $accommodationRooms->number_of_bed = $request->input('number_of_bed');
        $accommodationRooms->room_size = $request->input('room_size');
         $accommodationRooms->price = $request->input('price');
         $accommodationRooms->max_adult = $request->input('max_adult');
         $accommodationRooms->max_children = $request->input('max_children');
        $accommodationRooms->save();

        $accommodationRooms->roomFeatures()->sync($request->room_features);

        $filefeature = $request->file('feature_image');

        if (!empty($filefeature)) {
            $accommodationpath = RoomImage::where('is_featured',0)->where('accommodation_room_id',$id)->first();
            if (!empty($accommodationpath)) {
                $destinationPath = 'uploads/accommodation_rooms/';
                File::delete($destinationPath . $filefeature->getClientOriginalName());//delete current image from storage
                $filename = $filefeature->getClientOriginalName();
                $filefeature->move($destinationPath, $filename);
                $accommodationpath->room_image_name = $destinationPath . "/" . $filename;
                $accommodationpath->save();
            }else {
                $destinationPath = 'uploads/accommodation_rooms/';
                $filename = $filefeature->getClientOriginalName();
                $filefeature->move($destinationPath, $filename);
                $imageModel = new RoomImage();
                $imageModel->room_image_name = $destinationPath . $filename;
                $imageModel->accommodation_room_id = $accommodationRooms->id;
                $imageModel->is_featured = 0;
                $imageModel->save();
            }
        }

        $files = $request->gallery_images;
        if (!empty($files)) {
            foreach ($files as $file) {
                $destinationPath = 'uploads/accommodation_rooms/';
                $filename = $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $imageModel = new RoomImage(); // this model you have to create
                $imageModel->room_image_name = $destinationPath . '' . $filename;
                $imageModel->accommodation_room_id = $accommodationRooms->id;
                $imageModel->save();
            }
        }


        toastr()->success('Data has been saved successfully!');
        return redirect()->route('accommodation-rooms.edit', $id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $acc = AccommodationRoom::withoutGlobalScopes()->whereId($id)->first();
        $accommodationroom = AccommodationRoom::withoutGlobalScopes()->with('roomFeatures','images')->whereId($id);
        $accommodationroom->delete();

        return redirect()->route('accommodation-rooms.show',$acc->accommodation_id)
            ->with('success','Room deleted successfully');
    }
    public function images_destroy($id){
        $acc = RoomImage::whereId($id)->first();
        $room = RoomImage::whereId($id);
        $room->delete();

        return redirect()->route('accommodation-rooms.edit',$acc->accommodation_room_id)
            ->with('success','Image deleted successfully');
    }
}
