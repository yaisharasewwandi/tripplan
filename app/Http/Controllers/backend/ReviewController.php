<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\ReviewAccommodation;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public function hotel(Request $request)
    {
        $hotelreviews = ReviewAccommodation::all();
       return view('backend.reviews.hotels',compact('hotelreviews')) ->with('i', ($request->input('page', 1) - 1) * 5);
    }
}
