<?php

namespace App\Http\Controllers\backend;

use App\AccommodationBooking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AccomodationBookingController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:hotel-booking-list|hotel-booking-create|hotel-booking-edit|hotel-booking-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:hotel-booking-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:hotel-booking-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:hotel-booking-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $booking = AccommodationBooking::withoutGlobalScopes();
        if (Auth::user()->hasRole(['Admin','Super Admin'])) {
            $booking->with('room')->orderBy('id', 'desc');
        }else {
            $booking->whereHas('room.accommodation', function ($q) {
                $q->where('user_id',Auth::id());
            });
        }
        $booking = $booking->get();

        return view('backend.accomodation-booking.index',compact('booking'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    public function changeStatus(Request $request)
    {
        $user = AccommodationBooking::find($request->booking_id);
        $user->approve = $request->approve;
        $user->save();

        return response()->json(['success'=>'Status change successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = AccommodationBooking::findorfail($id);
        $blog->delete();
        return redirect()->route('accomodation-booking.index')
            ->with('success','Hotel Booking deleted successfully');
    }
}
