<?php

namespace App\Http\Controllers\backend;

use App\AgenciesImage;
use App\City;
use App\District;
use App\TravelAgency;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class TravelAgencyController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:travelagency-list|travelagency-create|travelagency-edit|travelagency-delete', ['only' => ['index','store']]);
        $this->middleware('permission:travelagency-create', ['only' => ['create','store']]);
        $this->middleware('permission:travelagency-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:travelagency-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Auth::user()->hasRole(['Admin','Super Admin'])) {
        $agencies = TravelAgency::withoutGlobalScopes()->orderBy('id','desc')->get();
        }elseif (Auth::user()->hasRole(['Agency Owner'])) {
            $agencies = TravelAgency::orderBy('id', 'desc')->get();
        }
        //dd($roles);
        return view('backend.travel-agency.index',compact('agencies'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $districts = District::all();
        $cities = City::all();
        return view('backend.travel-agency.create',compact('districts','cities'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required',
            'description' => 'required',
            'contact_number' => 'required',
            'email' => 'required',
            'district_id' => 'required',
            'city_id' => 'required',
            'address' => 'required',
            'location' => 'required',
            'banner_image' => 'required',
            'feature_image' => 'required',
            'lat' => 'required',
            'lng' => 'required',

        ]);
        //dd($request->all());
        $user_id = Auth::id();

        $travels = TravelAgency::create(['title' => $request->input('title'),
            'slug' => $request->input('slug'),
            'description' => $request->input('description'),
            'contact_number' => $request->input('contact_number'),
            'email' => $request->input('email'),
            'district_id' => $request->input('district_id'),
            'city_id' => $request->input('city_id'),
            'address' => $request->input('address'),
            'location' => $request->input('location'),
            'lat' => $request->input('lat'),
            'lng' => $request->input('lng'),
            'fb_page_link' => $request->input('fb_page_link'),
            'twiter_link' => $request->input('twiter_link'),
            'instergram_link' => $request->input('instergram_link'),
            'website_link' => $request->input('website_link'),
            'user_id' => $user_id,
        ]);

        $filebanner = $request->file('banner_image');
        $filefeature = $request->file('feature_image');

        if (!empty($filebanner)) {
            $destinationPath = 'uploads/travelagency/banner/';
            $filename = $filebanner->getClientOriginalName();
            $filebanner->move($destinationPath, $filename);
            $travels->banner_image = $destinationPath . $filename;
            $travels->save();

        }
        if (!empty($filefeature)) {
            $destinationPath = 'uploads/travelagency/feature/';
            $filename = $filefeature->getClientOriginalName();
            $filefeature->move($destinationPath, $filename);
            $travels->featured_image = $destinationPath . $filename;
            $travels->save();
        }
        toastr()->success('Data has been saved successfully!');
        return redirect()->route('travel-agencies.index');
    }

    public function changeStatus(Request $request)
    {
        $user = TravelAgency::withoutGlobalScopes()->find($request->agency_id);
        $user->publish_status = $request->publish_status;
        $user->save();

        return response()->json(['success'=>'Status change successfully.']);
    }
    public function check_slug(Request $request)
    {
        $slug = Str::slug($request['title'], '-');;
        return response()->json(['slug' => $slug]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TravelAgency  $travelAgency
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $travelAgency = TravelAgency::withoutGlobalScopes()->find($id);
        return view('backend.travel-agency.show',compact('travelAgency'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TravelAgency  $travelAgency
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agency = TravelAgency::withoutGlobalScopes()->find($id);
        $districts = District::all();
        $cities = City::all();
        return view('backend.travel-agency.edit',compact('districts','cities','agency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TravelAgency  $travelAgency
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required',
            'description' => 'required',
            'contact_number' => 'required',
            'email' => 'required',
            'district_id' => 'required',
            'city_id' => 'required',
            'address' => 'required',
            'location' => 'required',
            'lat' => 'required',
            'lng' => 'required',

        ]);
        //dd($request->all());
        $user_id = Auth::id();

        $travels = TravelAgency::withoutGlobalScopes()->find($id);
        $travels->title = $request->input('title');
        $travels->slug = $request->input('slug');
        $travels->description = $request->input('description');
        $travels->contact_number = $request->input('contact_number');
        $travels->email = $request->input('email');
        $travels->district_id = $request->input('district_id');
        $travels->city_id = $request->input('city_id');
        $travels->address = $request->input('address');
        $travels->location = $request->input('location');
        $travels->lat = $request->input('lat');
        $travels->lng = $request->input('lng');
        $travels->fb_page_link = $request->input('fb_page_link');
        $travels->twiter_link = $request->input('twiter_link');
        $travels->instergram_link = $request->input('instergram_link');
        $travels->website_link = $request->input('website_link');
        $travels->user_id = $user_id;


        $filebanner = $request->file('banner_image');
        $filefeature = $request->file('feature_image');
        if (!empty($filebanner)) {
            $destinationPath = 'uploads/travelagency/banner/';
            File::delete($destinationPath.$travels->banner_image);//delete current image from storage
            $filename = $filebanner->getClientOriginalName();
            $filebanner->move($destinationPath, $filename);
            $travels->banner_image = $destinationPath ."/".$filename;
        }

        if (!empty($filefeature)) {
            $destinationPath = 'uploads/travelagency/feature/';
            File::delete($destinationPath.$travels->featured_image);//delete current image from storage
            $filename = $filefeature->getClientOriginalName();
            $filefeature->move($destinationPath, $filename);
            $travels->featured_image = $destinationPath ."/".$filename;
        }
        $travels->save();
        toastr()->success('Data has been saved successfully!');
        return redirect()->route('travel-agencies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TravelAgency  $travelAgency
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $agency = TravelAgency::withoutGlobalScopes()->find($id);
        $agency->delete();

        return redirect()->route('travel-agencies.index')
            ->with('success','Agency deleted successfully');
    }
}
