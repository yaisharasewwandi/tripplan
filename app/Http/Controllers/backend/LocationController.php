<?php

namespace App\Http\Controllers\backend;

use App\City;
use App\District;
use App\Http\Controllers\Controller;
use App\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $locations = Location::orderBy('id','desc')->get();
        //dd($roles);
        return view('backend.locations.index',compact('locations'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $districts = District::all();
        $cities = City::all();
        return view('backend.locations.create');
    }
    public function citysearch(Request $request){
        $city = $request->city;
        $data = City::with('districts')->where('name_en',$city)->get();
        dd($data);
        return response()->json(['success'=>'Status change successfully.']);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request, [
            'city' => 'required',
            'location' => 'required',
            'searchInput' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ]);
        //dd($request->all());
        $user_id = Auth::id();
        $name = $request->name;
        $city = $request->city;
        $location = $request->searchInput;
        $lat = $request->lat;
        $lng = $request->lng;

        $this->addlocation($city,$location,$lat,$lng,$name);
        toastr()->success('Data has been saved successfully!');
        return redirect()->route('locations.index');
    }
    public function addlocation($city,$location,$lat,$lng,$name){

        $data = City::with('districts')->where('name_en',$city)->first();

        $store = Location::firstOrCreate([
            'location' => $location,
        ], [
            'lat' => $lat,
            'lng' => $lng,
            'name' => $name,
            'city_id'=>$data->id,
            'district_id'=>$data->districts->id,
        ]);

        return $store;
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $location)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        //
    }
}
