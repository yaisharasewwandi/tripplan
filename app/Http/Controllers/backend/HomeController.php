<?php

namespace App\Http\Controllers\backend;

use App\Accommodation;
use App\AccommodationBooking;
use App\CarBooking;
use App\Http\Controllers\Controller;
use App\TourPackageBooking;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $hotels = AccommodationBooking::all()->count();
        $users = User::all()->count();
        $cars = CarBooking::all()->count();
        $tour= TourPackageBooking::all()->count();
        $acc= Accommodation::all()->count();

        return view('backend.dashboard.index',compact('hotels','users','cars','tour','acc'));
    }
}
