<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if ($data['ven_gest'] == 1){
            $contact_number = null;
            $address = null;
        }else{
            $contact_number = $data['contact_number'];
            $address = $data['address'];
        }
        if (!empty($data['role'])){
        if ($data['role']=='Guider'){
            $status = 1;
                }else{
            $status = 0;
        }}else{ $status = 0;}

        return $user= User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'contact_number' => $contact_number,
            'address' => $address,
            'status' => $status,
            'password' => Hash::make($data['password']),

        ]);

    }

    public function register(Request $request)
    {
        $validation = $this->validator($request->all());
        if ($validation->fails())  {
            toastr()->error('Please try again!', 'Unfortunately');
            return redirect()->to('/');
        }
        else{
            $guest = 'Guest';
            $user = $this->create($request->all());
            if ($request->input('ven_gest') == 2) {
                $user->assignRole($request->input('role'));

            }else{

                $user->assignRole($guest);
            }
            Auth::login($user);
            toastr()->success('You can create Listing.', 'Registration Successful!');
            return redirect()->to('/');
        }
    }
}
