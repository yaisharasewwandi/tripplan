<?php

namespace App\Http\Controllers\frontend;

use App\Accommodation;
use App\Agency;
use App\Guider;
use App\Http\Controllers\Controller;
use App\Restaurant;
use Illuminate\Http\Request;

class UserDashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function user_dashboard(){

        $accommodations = Accommodation::get()->all();
        $restaurants = Restaurant::get();
        $agencies = Agency::get();
        $guiders = Guider::get()->all();
        return view('frontend.user-dashboard',compact('accommodations','restaurants','agencies','guiders'));
    }
}
