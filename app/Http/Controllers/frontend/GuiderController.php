<?php


namespace App\Http\Controllers\frontend;


use App\City;
use App\Guider;
use App\Http\Controllers\Controller;
use App\ReviewGuider;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class GuiderController extends Controller
{
    public function guider_list(Request $request)
    {
        $select_city = $request->city;
        $select_price = $request->price;
        $price_ = explode(';', $select_price);
        $select_facility = $request->check;

        $lat = $request->lat;
        $lng = $request->lng;
        $distance = 50;
        if (!empty($lat && $lng && $distance)) {
            $select_data = Guider::getByDistance($lat, $lng, $distance);
            $ids = [];
            foreach ($select_data as $q) {
                array_push($ids, $q->id);

            }

        }

        $cities = City::pluck('name_en', 'id');
        $guider_set = Guider::withoutGlobalScopes()->with(['reviewGuider','user'])->where('publish_status', 1);
        if (!empty($select_city)) {
            $guider_set->where('city_id', $select_city);
        }

        if (!empty($ids)) {
            $guider_set->where('id', [$ids]);
        }

        $guiders = $guider_set->get();
//        dd($cars);
        return view('frontend.guider-list', compact('guiders',  'cities'));
    }
    public function guider_single($id){

//
        $guiders = Guider::withoutGlobalScopes()->with(['reviewGuider','user'])->whereId($id)
            ->where('publish_status',1)
            ->first();
        //dd($cars);
        $guiderAll = Guider::withoutGlobalScopes()->with(['images' => function ($query){
            $query->where('is_featured',1);
        }])->where('publish_status',1)->whereNotIn('id',[$id])->get();
        $safety = ReviewGuider::withoutGlobalScopes()->where('guider_id',$id)->avg('safety');
        $communication= ReviewGuider::withoutGlobalScopes()->where('guider_id',$id)->avg('communication');
        $care = ReviewGuider::withoutGlobalScopes()->where('guider_id',$id)->avg('care');
        $confidence = ReviewGuider::withoutGlobalScopes()->where('guider_id',$id)->avg('confidence');
        $review = ReviewGuider::withoutGlobalScopes()->with('user')->where('guider_id',$id)->paginate(2);
        return view('frontend.guider-single',compact('guiders','guiderAll','safety','communication','care','confidence','review'));
    }

    public function review_post(Request $request){
        $review = new ReviewGuider();
        $review->comment = $request->comment;
        $review->safety = $request->safety;
        $review->communication = $request->communication;
        $review->care = $request->care;
        $review->confidence = $request->confidence;
        $review->sub_avg = $request->score;
        $review->guider_id = $request->id;
        $review->user_id = Auth::user()->id;
        $review->save();

        $guider_id = ReviewGuider::where('guider_id',$request->id)->avg('sub_avg');
        $guider = Guider::where('id',$request->id)->first();
        $guider->rating = number_format($guider_id,2);
        $guider->update();

        return $review;

    }
}
