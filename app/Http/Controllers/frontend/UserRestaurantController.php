<?php
/**
 * Created by PhpStorm.
 * User: yaish
 * Date: 6/2/2020
 * Time: 10:38 AM
 */

namespace App\Http\Controllers\frontend;


use App\Accommodation;
use App\Agency;
use App\Guider;
use App\Http\Controllers\Controller;
use App\Restaurant;

class UserRestaurantController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        $accommodations = Accommodation::get();
        $restaurants = Restaurant::orderBy('id','DESC')->paginate(5);
        $agencies = Agency::get();
        $guiders = Guider::get()->all();
        return view('frontend.user-restaurant.index',compact('accommodations','restaurants','agencies','guiders'));
    }
    public function storeMedia(Request $request)
    {
        $path = 'uploads/restaurant_rooms/';

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $file = $request->file('file');

        $name = uniqid() . '_' . trim($file->getClientOriginalName());

        $file->move($path, $name);

        return response()->json([
            'name'          => $name,
            'original_name' => $file->getClientOriginalName(),
        ]);
    }

}