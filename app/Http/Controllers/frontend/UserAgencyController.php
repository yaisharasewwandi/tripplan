<?php
/**
 * Created by PhpStorm.
 * User: yaish
 * Date: 6/2/2020
 * Time: 10:58 AM
 */

namespace App\Http\Controllers\frontend;


use App\Accommodation;
use App\Agency;
use App\Guider;
use App\Http\Controllers\Controller;
use App\Restaurant;

class UserAgencyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        $accommodations = Accommodation::get();
        $restaurants = Restaurant::get();
        $agencies = Agency::orderBy('id','DESC')->paginate(5)->all();
        $guiders = Guider::get();
        return view('frontend.user-agency.index',compact('accommodations','restaurants','agencies','guiders'));
    }
}