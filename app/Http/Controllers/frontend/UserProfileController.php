<?php

namespace App\Http\Controllers\frontend;

use App\frontend\UserProfile;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

class UserProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function editprofile(Request $request, $id){
//        $request->validate([
//            'name' => 'required'
//        ]);
        //dd($request->all());
        $profile = UserProfile::find($id);
        $profile->name = $request->name;
        $profile->email = $request->email;
        $profile->contact_number = $request->contact_number;
        $profile->address = $request->address;
        $profile->note = $request->note;
        $profile->facebook_link = $request->facebook_link;
        $profile->twiter_link = $request->twiter_link;
        $profile->twiter_link = $request->twiter_link;
        $profile->instergram_link = $request->instergram_link;
        $file = $request->image_path;

      //  $profile->update($request->all());
        if (!empty($file)) {
            $destinationPath = 'uploads/profile_picture';
            File::delete($destinationPath.$profile->image_path);//delete current image from storage
            $filename = $file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $profile->image_path = $destinationPath ."/".$filename;

        }
        $profile->save();

        return redirect(route('customer-edit-profile'));

    }

    public function updateStatus(Request $request)
    {

        $user = UserProfile::findOrFail($request->user_id);
        $user->status = $request->status;
        $user->save();
        return response()->json(['message' => 'User status updated successfully.']);
    }


}
