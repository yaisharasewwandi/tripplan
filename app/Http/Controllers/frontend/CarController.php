<?php
/**
 * Created by PhpStorm.
 * User: yaish
 * Date: 8/1/2020
 * Time: 7:03 PM
 */

namespace App\Http\Controllers\frontend;


use App\Car;
use App\CarBooking;
use App\CarFeatures;
use App\City;
use App\Http\Controllers\Controller;
use App\ReviewCar;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class CarController extends Controller
{
    public function car_list(Request $request){
        $select_city = $request->city;
        $select_price = $request->price;
        $price_ = explode(';',$select_price);
        $select_facility = $request->check;

        $lat =$request->lat;
        $lng =$request->lng;
        $distance = 50;
        if (!empty($lat && $lng && $distance)) {
            $select_data = Car::getByDistance($lat, $lng, $distance);
            $ids = [];
            foreach($select_data as $q)
            {
                array_push($ids, $q->id);

            }

        }
        $features = CarFeatures::all();
        $cities = City::pluck('name_en','id');
        $cars_set = Car::withoutGlobalScopes()->with(['features','reviewCars',
            'images' => function ($query){
            $query->where('is_featured',1);
        }
        ])->where('publish_status',1);
       if (!empty($select_city)){
           $cars_set->where('city_id',$select_city);
       }
        if (!empty($select_price)){
            $cars_set->whereBetween('price', [$price_[0], $price_[1]]);
        }
        if (!empty($ids)){
            $cars_set->where('id', [$ids]);
        }

        $cars =$cars_set->get();
//        dd($cars);
        return view('frontend.car-list',compact('cars','features','cities'));
    }
    public function car_single($id){

//
        $cars = Car::withoutGlobalScopes()->with(['features','reviewCars', 'images'])->whereId($id)
            ->where('publish_status',1)
            ->first();
        //dd($cars);
        $carAll = Car::withoutGlobalScopes()->with(['images' => function ($query){
            $query->where('is_featured',1);
        }])->where('publish_status',1)->whereNotIn('id',[$id])->get();
        $consumption = ReviewCar::withoutGlobalScopes()->where('car_id',$id)->avg('consumption');
        $seating_capacity = ReviewCar::withoutGlobalScopes()->where('car_id',$id)->avg('seating_capacity');
        $confatable = ReviewCar::withoutGlobalScopes()->where('car_id',$id)->avg('confatable');
        $facilities = ReviewCar::withoutGlobalScopes()->where('car_id',$id)->avg('facilities');
        $review = ReviewCar::withoutGlobalScopes()->with('user')->where('car_id',$id)->paginate(2);
        return view('frontend.car-single',compact('cars','carAll','consumption','seating_capacity','confatable','facilities','review'));
    }
    public function car_book(Request $request){
        $dates = $request->dates;
        $dates_ = explode(' - ',$dates);
        $from = Carbon::parse($dates_[0]);
        $to = Carbon::parse($dates_[1]);
        $countDate =  $to->diffInDays($from);

        $car= Car::withoutGlobalScopes()->with(['images'=>function ($q){
            $q->where('is_featured',0);
        }])->where('id',$request->id)->first();


        return view('frontend.car-book',compact('car','countDate','from','to'));
    }
    public function review_post(Request $request){
        $review = new ReviewCar();
        $review->comment = $request->comment;
        $review->consumption = $request->consumption;
        $review->seating_capacity = $request->seating_capacity;
        $review->confatable = $request->confatable;
        $review->facilities = $request->facilities;
        $review->sub_avg = $request->score;
        $review->car_id = $request->id;
        $review->user_id = Auth::user()->id;
        $review->save();

        $car_id = ReviewCar::where('car_id',$request->id)->avg('sub_avg');
        $car = Car::where('id',$request->id)->first();
        $car->rating = number_format($car_id,2);
        $car->update();

        return $review;

    }
    public function insert_booking(Request $request){

        $validator = Validator::make($request->all(), [
            'room_id' => 'required',
            'adult' => 'required',
            'child' => 'required',
            'date' => 'required',
            'check' => 'required',
        ]);


        $bookDetails = new CarBooking();
        $bookDetails->car_id = $request->carId;
        $bookDetails->from = $request->from;
        $bookDetails->to = $request->to;
        $bookDetails->days = $request->days;
        $bookDetails->amount = $request->amount;
        $bookDetails->country = $request->country;
        $bookDetails->street = $request->street;
        $bookDetails->state = $request->state;
        $bookDetails->postal_code = $request->postal_code;
        $bookDetails->note = $request->note;
        $bookDetails->term_condition = $request->check;
        $bookDetails->confirm_status = 1;
        $bookDetails->user_id = Auth::user()->id;
        $bookDetails->save();
//        dd($bookDetails);
        $ref_no= 'REF'. str_pad($bookDetails->id, 6, "0", STR_PAD_LEFT);
        $bookDetails->ref_no = $ref_no;
        $bookDetails->save();

        $user = User::where('id', Auth::user()->id)->first();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->contact_number = $request->contact_number;
        $user->save();
        toastr()->success('Book Car successfully!');
        return redirect()->route('listing-booking');
//       return view(route('home'));
    }
}
