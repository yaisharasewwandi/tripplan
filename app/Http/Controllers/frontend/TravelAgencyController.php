<?php

namespace App\Http\Controllers\frontend;

use App\Agency;
use App\City;
use App\Http\Controllers\Controller;
use App\ReviewAgency;
use App\TourPackage;
use App\TourPackageBooking;
use App\TravelAgency;
use App\User;
use Facade\Ignition\Support\Packagist\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TravelAgencyController extends Controller
{
    public function agency_list(Request $request){
        $select_city = $request->city;
        $select_address = $request->address;
        $select_price = $request->price;
        $price_ = explode(';',$select_price);

        $location =$request->address;
        $lat =$request->lat;
        $lng =$request->lng;
        $distance = 50;
        if (!empty($lat && $lng && $distance)) {
            $select_tour = TravelAgency::getByDistance($lat, $lng, $distance);

            $idsT = [];
            foreach($select_tour as $tour)
            {
                array_push($idsT, $tour->id);
            }

        }

        $agencies_ = TravelAgency::withoutGlobalScopes()->where('publish_status',1);
        if (!empty($select_city)){
            $agencies_->where('city_id',$select_city);
        }
        if (!empty($select_address)){
            $agencies_->where('location', 'LIKE', "%$select_address%");
        }
        if (!empty($idsT)){
            $agencies_->where('id', [$idsT]);
        }
        $agencies =$agencies_->get();
        $cities = City::pluck('name_en','id');
        return view('frontend.agency-list',compact('agencies','cities'));
    }
    public function agency_single($id){

//
        $travelagencies = TravelAgency::withoutGlobalScopes()->with(['agancyPackages','agancyPackages.images','agancyPackages.categories','reviewAgancy',
            ])->whereId($id)
            ->where('publish_status',1)
            ->first();
        $travelagenciespackage = TravelAgency::withoutGlobalScopes()->with(['agancyPackages' => function ($query) use ($id){$query->where('travel_agency_id',$id);}
            ,'agancyPackages.images','agancyPackages.categories'
            ])->whereId($id)
            ->where('publish_status',1)
            ->first();

        $travelagenciesnAll = TravelAgency::withoutGlobalScopes()->where('publish_status',1)->whereNotIn('id',[$id])->get();
        $package = TourPackage::whereIn('travel_agency_id',[$id])->where('publish_status',1)->get();

        $cleanliness = ReviewAgency::withoutGlobalScopes()->where('agency_id',$id)->avg('cleanliness');
        $comfort = ReviewAgency::withoutGlobalScopes()->where('agency_id',$id)->avg('comfort');
        $facilities = ReviewAgency::withoutGlobalScopes()->where('agency_id',$id)->avg('facilities');
        $staff = ReviewAgency::withoutGlobalScopes()->where('agency_id',$id)->avg('staff');
        $review = ReviewAgency::withoutGlobalScopes()->with('user')->where('agency_id',$id)->paginate(2);
        return view('frontend.agency-single',compact('travelagencies','travelagenciesnAll',
            'travelagenciespackage','package','cleanliness','comfort','facilities','staff','review'));
    }

    public function review_post(Request $request){
        $review = new ReviewAgency();
        $review->comment = $request->comment;
        $review->cleanliness = $request->cleanliness;
        $review->comfort = $request->comfort;
        $review->staff = $request->staf;
        $review->facilities = $request->facilities;
        $review->sub_avg = $request->score;
        $review->agency_id = $request->id;
        $review->user_id = Auth::user()->id;
        $review->save();

        $agn_id = ReviewAgency::withoutGlobalScopes()->where('agency_id',$request->id)->avg('sub_avg');
        $agency = TravelAgency::withoutGlobalScopes()->where('id',$request->id)->first();
        $agency->rating = number_format($agn_id,2);
        $agency->update();
        return $review;
    }
    public function packages_details(Request $request){

        $package_d=TourPackage::withoutGlobalScopes()->select('min_person','max_person','budget')->where('id',$request->id)->first();

        return response()->json($package_d);
    }
    public function package_book(Request $request){
        $dates = $request->dates;
        $dates_ = explode(' - ',$dates);
        $from = Carbon::parse($dates_[0]);
        $to = Carbon::parse($dates_[1]);
        $countDate =  $to->diffInDays($from);

        $package = TourPackage::withoutGlobalScopes()->with(['images' => function ($query){
            $query->where('is_featured',1);
        }])->where('publish_status',1)->where('id',$request->type)->first();

        return view('frontend.package-book',compact('package','countDate','from','to'));
    }
    public function insert_booking(Request $request){

        $validator = Validator::make($request->all(), [
            'room_id' => 'required',
            'adult' => 'required',
            'child' => 'required',
            'date' => 'required',
            'check' => 'required',
        ]);


        $bookDetails = new TourPackageBooking();
        $bookDetails->tour_id = $request->tourId;
        $bookDetails->from = $request->from;
        $bookDetails->to = $request->to;
        $bookDetails->days = $request->days;
        $bookDetails->amount = $request->amount;
        $bookDetails->country = $request->country;
        $bookDetails->street = $request->street;
        $bookDetails->state = $request->state;
        $bookDetails->postal_code = $request->postal_code;
        $bookDetails->note = $request->note;
        $bookDetails->term_condition = $request->check;
        $bookDetails->confirm_status = 1;
        $bookDetails->user_id = Auth::user()->id;
        $bookDetails->save();
//        dd($bookDetails);
        $ref_no= 'REF'. str_pad($bookDetails->id, 6, "0", STR_PAD_LEFT);
        $bookDetails->ref_no = $ref_no;
        $bookDetails->save();

        $user = User::where('id', Auth::user()->id)->first();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->contact_number = $request->contact_number;
        $user->save();
        toastr()->success('Book Tour Package successfully!');
        return redirect()->route('listing-booking');
//       return view(route('home'));
    }
}
