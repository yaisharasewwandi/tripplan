<?php

namespace App\Http\Controllers\frontend;

use App\AccommodationWishlist;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class WishlistController extends Controller
{
    public function wislistSave(Request $request){
        $accommodation = $request->id;
        $user_id = Auth::id();
        $wishlist = AccommodationWishlist::firstOrCreate(['user_id' => $user_id ,
            'accommodation_id' => $accommodation,]);
        return response()->json(['success'=>'Status change successfully.']);
    }

    public function wishlist(){
        $wishlists = AccommodationWishlist::with(['accommodations','accommodations.images' => function ($query){
            $query->where('is_featured',1);
        }])->get();

        $view = View::make('frontend.wish-list',compact('wishlists'));
        return $view->render();

    }
}
