<?php
/**
 * Created by PhpStorm.
 * User: yaish
 * Date: 8/7/2020
 * Time: 1:36 PM
 */

namespace App\Http\Controllers\frontend;


use App\City;
use App\Http\Controllers\Controller;
use App\ItemAvailableTime;
use App\Restaurant;
use App\ReviewRestaurant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RestaurantController extends Controller
{
    public function restaurant_list(Request $request){
        $select_city = $request->city;
        $location =$request->address;
        $lat =$request->lat;
        $lng =$request->lng;
        $distance = 50;
        if (!empty($lat && $lng && $distance)) {

            $selectdata = Restaurant::getByDistance($lat, $lng, $distance);
            $idsR = [];
            foreach($selectdata as $q)
            {
                array_push($idsR, $q->id);
            }


        }

        $restaurants_ = Restaurant::withoutGlobalScopes()->with(['images'])->where('publish_status',1);
        if (!empty($select_city)){
            $restaurants_->where('city_id',$select_city);
        }
        if (!empty($idsR)){
            $restaurants_->where('id',[$idsR]);
        }
          $restaurants =  $restaurants_->get();
        $cities = City::pluck('name_en','id');
//        dd($restaurant);
        return view('frontend.restaurant-list',compact('restaurants','cities'));
    }

    public function restaurant_single($id)
    {
        $restaurants = Restaurant::withoutGlobalScopes()->with(['items.images','items.itemAvailableHourse','reviewRestaurant','facilities','openHourse',
            'images' => function ($query){
                $query->where('is_featured',1);
            },
            'items' => function ($query){
                $query->where('publish_status',1);
            }])->whereId($id)
            ->where('publish_status',1)
            ->first();
       // dd($restaurants);
        $restaurantAll = Restaurant::withoutGlobalScopes()->with(['images' => function ($query){
            $query->where('is_featured',1);
        }])->where('publish_status',1)->whereNotIn('id',[$id])->get();

        $cleanliness = ReviewRestaurant::withoutGlobalScopes()->where('restaurant_id',$id)->avg('cleanliness');
        $comfort = ReviewRestaurant::withoutGlobalScopes()->where('restaurant_id',$id)->avg('comfort');
        $facilities = ReviewRestaurant::withoutGlobalScopes()->where('restaurant_id',$id)->avg('facilities');
        $staff = ReviewRestaurant::withoutGlobalScopes()->where('restaurant_id',$id)->avg('staff');
        $review = ReviewRestaurant::withoutGlobalScopes()->with('user')->where('restaurant_id',$id)->paginate(2);

        return view('frontend.restaurant-single', compact('restaurants', 'restaurantAll','cleanliness',
        'comfort','facilities','staff','review'));
    }
    public function review_post(Request $request){
        $review = new ReviewRestaurant();
        $review->comment = $request->comment;
        $review->cleanliness = $request->cleanliness;
        $review->comfort = $request->comfort;
        $review->staff = $request->staf;
        $review->facilities = $request->facilities;
        $review->sub_avg = $request->score;
        $review->restaurant_id = $request->id;
        $review->user_id = Auth::user()->id;
        $review->save();

        $acc_id = ReviewRestaurant::withoutGlobalScopes()->where('restaurant_id',$request->id)->avg('sub_avg');
        $accommodation = Restaurant::withoutGlobalScopes()->where('id',$request->id)->first();
        $accommodation->rating = number_format($acc_id,2);
        $accommodation->update();
        toastr()->success('Review Added successfully!');
        return $review;
    }
}
