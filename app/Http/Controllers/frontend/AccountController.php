<?php

namespace App\Http\Controllers\frontend;

use App\Accommodation;
use App\AccommodationType;
use App\Agency;
use App\CardType;
use App\District;
use App\Facility;
use App\frontend\Account;
use App\frontend\UserProfile;
use App\Guider;
use App\Http\Controllers\Controller;

use App\Restaurant;
use App\RestaurantFacility;
use App\RestaurantType;
use App\ReviewAccommodation;
use App\ReviewAgency;
use App\ReviewGuider;
use App\ReviewRestaurant;
use App\Town;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;


class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add_businessstep1(Request $request){
        $business = $request->session()->get('business');
        return view('frontend.business.add-type',compact('business'));
    }

    public function create_businessstep1(Request $request){
        $validatedData = $request->validate([
            'business_type' => 'required|in:1,2',
            'company_name' => 'required_if:business_type,2'
        ]);
        // dd($request->all());
        if(empty($request->session()->get('business'))){
            $business = new UserProfile();
            $business->fill($validatedData);
            $request->session()->put('business', $business);
        }else{
            $business = $request->session()->get('business');
            $business->fill($validatedData);
            $request->session()->put('business', $business);
        }
        return redirect('/add-businessstep2');

    }

    public function add_businessstep2(Request $request){
        $business = $request->session()->get('business');

        return view('frontend.business.add-account',compact('business'));
    }
    public function create_businessstep2(Request $request){
        $validatedData = $request->validate([
            'account_email' => 'required|email',
            'f_name' => 'required',
            'l_name' => 'required',
            'contact_number' => 'required'
        ]);

        if(empty($request->session()->get('business'))){
            $business = new Accommodation();
            $business->fill($validatedData);
            $request->session()->put('business', $business);

        }else{
            $business = $request->session()->get('business');
            $business->fill($validatedData);
            $request->session()->put('business', $business);


        }

        return redirect('/add-businessstep3');

    }
    public function add_businessstep3(Request $request){

        $business = $request->session()->get('business');
        if($business->business_type == 1) {
            $business = $request->session()->get('business');
            $districts = District::pluck('name','id');
            $towns = Town::pluck('name','id');
            $card_types = CardType::all();
            return view('frontend.business.add-yourselfbusiness-workplace', compact('business','districts','towns','card_types'));
        }elseif ($business->business_type == 2){
            $business = $request->session()->get('business');
            $districts = District::pluck('name','id');
            $towns = Town::pluck('name','id');
            $accommodation_types = AccommodationType::pluck('name','id');
            $restaurant_types = RestaurantType::pluck('name','id');
            $facility = Facility::all();
            $card_types = CardType::all();
            $restaurant_facilities= RestaurantFacility::all();
           // dd($restaurant_types);
            return view('frontend.business.add-otherbusiness-workplace', compact('business','accommodation_types','restaurant_types','districts','towns','facility','card_types','restaurant_facilities'));
        }
    }
    public function create_businessstep3(Request $request){
        $validatedData = $request->validate([
            'otherbiss_type' => 'required|in:1,2',
            'accommodation_type' => 'required_if:otherbiss_type,1',
            'facilities' => 'required_if:otherbiss_type,1',
            'restaurant_type' => 'required_if:otherbiss_type,2',
            'restaurant_facilities' => 'required_if:otherbiss_type,2',
            'company_name' => 'required',
            'f_name' => 'required',
            'l_name' => 'required',
            'contact_number' => 'required',
            'place_contact_number' => 'required',
            'district_id' => 'required',
            'town_id' => 'required',
            'place_address' => 'required',
            'location' => 'required',
            'lng' => 'required',
            'lat' => 'required',
            'rating' => 'required',
            'term_condition' => 'required',
            'image' => 'required',
        ]);

        $user_id = Auth::id();

        $business = $request->session()->get('business');

        if ($request->otherbiss_type == 1) {

            $accommodation = new Accommodation();
            $accommodation->company_name = $request->company_name;
                $accommodation->f_name = $request->f_name;
                $accommodation->l_name = $request->l_name;
                $accommodation->account_email = $request->account_email;
                $accommodation->contact_number = $request->contact_number;
                $accommodation->place_contact_number = $request->place_contact_number;
                $accommodation->district_id = $request->district_id;
                $accommodation->town_id = $request->town_id;
                $accommodation->place_address = $request->place_address;
                $accommodation->location = $request->location;
                $accommodation->type_id = $request->accommodation_type;
                $accommodation->lng = $request->lng;
                $accommodation->lat = $request->lat;
                $accommodation->term_condition = $request->term_condition;
                $accommodation->fb_page_link = $request->fb_page_link;
                $accommodation->instergram_link = $request->instergram_link;
                $accommodation->twiter_link = $request->twiter_link;
                $accommodation->payment_option = $request->payment_option;
                $accommodation->user_id = $user_id;
                $accommodation->save();

            $file = $request->file('image');
            if (!empty($file)) {
                    $destinationPath = 'uploads/accommodation/';
                $filename = $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $accommodation->image = $destinationPath . $filename;
                $accommodation->save();
            }
            $fname = $request->f_name;
            $lname = $request->l_name;

            $rating = new ReviewAccommodation();
            $rating->name = $fname .' '. $lname;
            $rating->email = $request->email;
            $rating->contact_number = $request->contact_number;
            $rating->cleanliness = $request->rating;
            $rating->comfort = $request->rating;
            $rating->staff = $request->rating;
            $rating->facilities = $request->rating;
            $rating->accommodation_id = $accommodation->id;
            $rating->save();

            $accommodation->facilities()->attach($request->facilities);
            $accommodation->cardTypes()->attach($request->card_type);

        }elseif ($request->otherbiss_type == 2){
            $restaurant = new Restaurant();
            $restaurant->company_name = $request->company_name;
            $restaurant->f_name = $request->f_name;
            $restaurant->l_name = $request->l_name;
            $restaurant->account_email = $request->account_email;
            $restaurant->contact_number = $request->contact_number;
            $restaurant->place_contact_number = $request->place_contact_number;
            $restaurant->district_id = $request->district_id;
            $restaurant->town_id = $request->town_id;
            $restaurant->place_address = $request->place_address;
            $restaurant->location = $request->location;
            $restaurant->type_id = $request->restaurant_type;
            $restaurant->lng = $request->lng;
            $restaurant->lat = $request->lat;
            $restaurant->term_condition = $request->term_condition;
            $restaurant->fb_page_link = $request->fb_page_link;
            $restaurant->instergram_link = $request->instergram_link;
            $restaurant->twiter_link = $request->twiter_link;
            $restaurant->payment_option = $request->payment_option;
            $restaurant->user_id = $user_id;
            $restaurant->save();

            $file = $request->file('image');
            if (!empty($file)) {
                $destinationPath = 'uploads/restaurant/';
                $filename = $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $restaurant->image = $destinationPath . $filename;
                $restaurant->save();
            }
            $fname = $request->f_name;
            $lname = $request->l_name;

            $rating = new ReviewRestaurant();
            $rating->name = $fname .' '. $lname;
            $rating->email = $request->email;
            $rating->contact_number = $request->contact_number;
            $rating->cleanliness = $request->rating;
            $rating->comfort = $request->rating;
            $rating->staff = $request->rating;
            $rating->facilities = $request->rating;
            $rating->restaurant_id = $restaurant->id;
            $rating->save();

            $restaurant->cardTypes()->attach($request->card_type);
            $restaurant->restaurantfacilities()->attach($request->restaurant_facilities);

        }
      return redirect(route('user-dashboard'));

    }



    public function myformAjax($id)
    {
        $towns = DB::table("towns")
            ->where("district_id",$id)
            ->lists("name","id");
        return json_encode($towns);
        dd($towns);
    }



    public function create_yourself_businessstep(Request $request){
        $validatedData = $request->validate([
            'yourself_type' => 'required|in:1,2',
            'company_name' => 'required_if:yourself_type,2',
            'f_name' => 'required',
            'l_name' => 'required',
            'contact_number' => 'required',
            'place_contact_number' => 'required',
            'district_id' => 'required_if:yourself_type,2',
            'town_id' => 'required_if:yourself_type,2',
            'place_address' => 'required_if:yourself_type,2',
            'location' => 'required_if:yourself_type,2',
            'lng' => 'required_if:yourself_type,2',
            'lat' => 'required_if:yourself_type,2',
            'rating' => 'required',
            'term_condition' => 'required',
            'image' => 'required',
        ]);
        // dd($request->all());
        $user_id = Auth::id();
        $business = $request->session()->get('business');
        if ($request->yourself_type == 1){
            $guider = new Guider();
            $guider->company_name = $request->company_name;
            $guider->f_name = $request->f_name;
            $guider->l_name = $request->l_name;
            $guider->account_email = $request->account_email;
            $guider->contact_number = $request->contact_number;
            $guider->place_contact_number = $request->place_contact_number;
            $guider->term_condition = $request->term_condition;
            $guider->fb_page_link = $request->fb_page_link;
            $guider->instergram_link = $request->instergram_link;
            $guider->twiter_link = $request->twiter_link;
            $guider->payment_option = $request->payment_option;
            $guider->user_id = $user_id;
            $guider->save();

            $file = $request->file('image');
            if (!empty($file)) {
                $destinationPath = 'uploads/guider/';
                $filename = $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $guider->image = $destinationPath . $filename;
                $guider->save();
            }
            $fname = $request->f_name;
            $lname = $request->l_name;

            $rating = new ReviewGuider();
            $rating->name = $fname .' '. $lname;
            $rating->email = $request->email;
            $rating->contact_number = $request->contact_number;
            $rating->cleanliness = $request->rating;
            $rating->comfort = $request->rating;
            $rating->staff = $request->rating;
            $rating->facilities = $request->rating;
            $rating->guider_id = $guider->id;
            $rating->save();

            $guider->cardTypes()->attach($request->card_type);

        }elseif ($request->yourself_type == 2){
            $agency = new Agency();
            $agency->company_name = $request->company_name;
            $agency->f_name = $request->f_name;
            $agency->l_name = $request->l_name;
            $agency->account_email = $request->account_email;
            $agency->contact_number = $request->contact_number;
            $agency->place_contact_number = $request->place_contact_number;
            $agency->district_id = $request->district_id;
            $agency->town_id = $request->town_id;
            $agency->place_address = $request->place_address;
            $agency->location = $request->location;
            $agency->lng = $request->lng;
            $agency->lat = $request->lat;
            $agency->term_condition = $request->term_condition;
            $agency->fb_page_link = $request->fb_page_link;
            $agency->instergram_link = $request->instergram_link;
            $agency->twiter_link = $request->twiter_link;
            $agency->payment_option = $request->payment_option;
            $agency->user_id = $user_id;
            $agency->save();

            $file = $request->file('image');
            if (!empty($file)) {
                $destinationPath = 'uploads/agency/';
                $filename = $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $agency->image = $destinationPath . $filename;
                $agency->save();
            }
            $fname = $request->f_name;
            $lname = $request->l_name;

            $rating = new ReviewAgency();
            $rating->name = $fname .' '. $lname;
            $rating->email = $request->email;
            $rating->contact_number = $request->contact_number;
            $rating->cleanliness = $request->rating;
            $rating->comfort = $request->rating;
            $rating->staff = $request->rating;
            $rating->facilities = $request->rating;
            $rating->agency_id = $agency->id;
            $rating->save();

            $agency->cardTypes()->attach($request->card_type);
        }
        return redirect(route('user-dashboard'));

    }
}