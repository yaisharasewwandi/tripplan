<?php
/**
 * Created by PhpStorm.
 * User: yaish
 * Date: 6/2/2020
 * Time: 9:37 AM
 */

namespace App\Http\Controllers\frontend;


use App\Accommodation;
use App\AccommodationRoom;
use App\Agency;
use App\Guider;
use App\Http\Controllers\Controller;
use App\Restaurant;
use App\RoomFeature;
use App\RoomImage;
use App\RoomType;
use Illuminate\Http\Request;
use Symfony\Component\Console\Input;

class UserAccommodationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        $accommodations = Accommodation::with('facilities')->orderBy('id','DESC')->paginate(5)->all();

        $restaurants = Restaurant::get();
        $agencies = Agency::get();
        $guiders = Guider::get()->all();
        $room_features = RoomFeature::all();
        $room_types = RoomType::pluck('name','id');
        return view('frontend.user-accommodation.index',compact('accommodations','restaurants','agencies','guiders','room_features','room_types'));
    }
    public function storeMedia(Request $request)
    {
        $path = 'uploads/accommodation_rooms/';

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $file = $request->file('file');

        $name = uniqid() . '_' . trim($file->getClientOriginalName());

        $file->move($path, $name);

        return response()->json([
            'name'          => $name,
            'original_name' => $file->getClientOriginalName(),
        ]);
    }
    public function store_rooms(Request $request){
//        $validatedData = $request->validate([
//            'room_type_id' => 'required',
//            'room_qty' => 'required',
//            'bed_type' => 'required',
//            'bed_qty' => 'required',
//            'guest_qty' => 'required',
//            'price_range' => 'required',
//
//        ]);
        $room_size = $request->room_size;
        $units = $request->units;

        $rooms = new AccommodationRoom();
        $rooms->room_type_id = $request->room_type;
        $rooms->room_name = $request->room_name;
        $rooms->room_qty = $request->room_qty;
        $rooms->bed_type = $request->bed_type;
        $rooms->bed_qty = $request->bed_qty;
        $rooms->guest_qty = $request->guest_qty;
        $rooms->room_size = $room_size .' '. $units;
        $rooms->price_range = $request->price_range;
        $rooms->accommodation_id = $request->accommodation_id;
        $rooms->save();

        $rooms->roomFeatures()->attach($request->feature);

        $files = $request->input('document',[]);


            foreach ($files as $file) {
                $destinationPath = 'uploads/accommodation_rooms/';
                $imageModel = new RoomImage(); // this model you have to create
                $imageModel->room_image_name = $destinationPath . '' . $file;
                $imageModel->accommodation_room_id = $rooms->id;
                $imageModel->save();
            }


        toastr()->success('Room added successful!');
        return redirect(route('user-accommodation.index'));
    }

    public function view($id){
       // $accommodations = Accommodation::find($id);
        $restaurants = Restaurant::get();
        $agencies = Agency::get();
        $guiders = Guider::get()->all();
        $room_types = RoomType::pluck('name','id');
        $room_features = RoomFeature::all();
        $accommodations = Accommodation::with('accommodationrooms','accommodationrooms.images')->whereId($id)->first();
       // dd($test);
        return view('frontend.user-accommodation.view', compact('accommodations','room_features','restaurants','agencies','guiders','room_types'));
    }

    public function destroy_image($id){
        $image = RoomImage::findOrFail($id);
        $image->delete();
        toastr()->success('Room Image deleted successful!');
        return redirect()->back();
    }
    public function destroy_room($id){
        $rooms = AccommodationRoom::findOrFail($id);
        $rooms->images()->delete();
        $rooms->delete();

        toastr()->success('Room deleted successful!');
        return redirect()->back();
    }
     public function destroy_acc($id){
         $accommodation = Accommodation::with('accommodationrooms','accommodationrooms.images')->whereId($id);
         $accommodation->delete();

        toastr()->success('Room deleted successful!');
        return redirect()->back();
        }

    public function update_rooms(Request $request,$id){
//        $validatedData = $request->validate([
//            'room_type_id' => 'required',
//            'room_qty' => 'required',
//            'bed_type' => 'required',
//            'bed_qty' => 'required',
//            'guest_qty' => 'required',
//            'price_range' => 'required',
//
//        ]);
        $room_size = $request->room_size;
        $units = $request->units;

        $rooms =  AccommodationRoom::find($id);
        $rooms->room_type_id = $request->room_type;
        $rooms->room_name = $request->room_name;
        $rooms->room_qty = $request->room_qty;
        $rooms->bed_type = $request->bed_type;
        $rooms->bed_qty = $request->bed_qty;
        $rooms->guest_qty = $request->guest_qty;
        $rooms->room_size = $room_size .' '. $units;
        $rooms->price_range = $request->price_range;
        $rooms->save();

        $rooms->roomFeatures()->sync($request->feature);

        $files = $request->input('document',[]);


        foreach ($files as $file) {
            $destinationPath = 'uploads/accommodation_rooms/';
            $imageModel = new RoomImage(); // this model you have to create
            $imageModel->room_image_name = $destinationPath . '' . $file;
            $imageModel->accommodation_room_id = $rooms->id;
            $imageModel->save();
        }


        toastr()->success('Room added successful!');
        return redirect()->back();
    }
}