<?php

namespace App\Http\Controllers\frontend;

use App\Accommodation;
use App\AccommodationBooking;
use App\AccommodationRoom;
use App\AccommodationWishlist;
use App\Blog;
use App\Car;
use App\CarBooking;
use App\City;
use App\Restaurant;
use App\ReviewAccommodation;
use App\ReviewBlog;
use App\District;
use App\frontend\UserProfile;
use App\Http\Controllers\Controller;
use App\TourPackageBooking;
use App\TravelAgency;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use RealRashid\SweetAlert\Facades\Alert;

class HomeController extends Controller
{
    public function index(Request $request){
        $location =$request->address;
        $lat =$request->lat;
        $lng =$request->lng;
        $distance = 50;
        if (!empty($lat && $lng && $distance)) {
            $select_data = Accommodation::getByDistance($lat, $lng, $distance);
            $ids = [];
            foreach($select_data as $q)
            {
                array_push($ids, $q->id);

            }
            $selectdata = Restaurant::getByDistance($lat, $lng, $distance);
            $idsR = [];
            foreach($selectdata as $q)
            {
                array_push($idsR, $q->id);
            }
            $select_tour = TravelAgency::getByDistance($lat, $lng, $distance);

            $idsT = [];
            foreach($select_tour as $tour)
            {
                array_push($idsT, $tour->id);
            }

        }

        $accommodations_ = Accommodation::withoutGlobalScopes()->with(['facilities','reviewAccommodations',
            'images' => function ($query){
                $query->where('is_featured',1);
            }
        ]);
        if (!empty($ids)){
            $accommodations_->whereIn('id',[$ids]);
        }
        $accommodations =   $accommodations_->where('publish_status',1)->get();

        $populorAccommodations = Accommodation::withoutGlobalScopes()->with(['reviewAccommodations',
         'images' => function ($query){
            $query->where('is_featured',1);
        }])->orderBy('rating', 'desc')->take(5)->get();

        $restaurants_ = Restaurant::withoutGlobalScopes()->with(['reviewRestaurant',
            'images' => function ($query){
                $query->where('is_featured',1);
            }
        ]);
        if (!empty($idsR)){
            $restaurants_->whereIn('id',[$idsR]);
        }
        $restaurants =  $restaurants_->get();


        $travelagencies_ = TravelAgency::withoutGlobalScopes()->with('reviewAgancy');
        if (!empty($idsT)){
            $restaurants_->whereIn('id',[$idsT]);
        }
        $travelagencies = $travelagencies_->get();

        $districts = District::take(5)->get();

        $blogs = Blog::withoutGlobalScopes()->with('users')->where('publish_status',1)->latest()->take(3)->get();
        return view('frontend.home',compact('accommodations','districts','blogs','restaurants','travelagencies','populorAccommodations'));
    }

    public function search_result(Request $request){


        $location =$request->location;
        $lat =$request->lat;
        $lng =$request->lng;
        $distance = 10;

        $select_data = Accommodation::getByDistance($lat, $lng, $distance);
        $ids = [];
        foreach($select_data as $q)
        {
            array_push($ids, $q->id);

        }
        $accommodations = Accommodation::withoutGlobalScopes()->with(['facilities','reviewAccommodations',
            'images' => function ($query){
                $query->where('is_featured',1);
            }
        ]) ->where('publish_status',1)
            ->whereIn('id',$ids)
            ->get();

        //RESTAURANT

        $selectdata = Restaurant::getByDistance($lat, $lng, $distance);
        $idsR = [];
        foreach($selectdata as $q)
        {
            array_push($idsR, $q->id);
        }
        $restaurants = Restaurant::withoutGlobalScopes()->with(['reviewRestaurant',
            'images' => function ($query){
                $query->where('is_featured',1);
            }
        ]) ->where('publish_status',1)
            ->whereIn('id',$idsR)
            ->get();

        //TOUR agency
        $select_tour = TravelAgency::getByDistance($lat, $lng, $distance);

        $idsT = [];
        foreach($select_tour as $tour)
        {
            array_push($idsT, $tour->id);
        }

        $touragencies = TravelAgency::withoutGlobalScopes()->with(['reviewAgancy'])->where('publish_status',1)
            ->whereIn('id',$idsT)
            ->get();

        //TOUR agency
        $select_Car = Car::getByDistance($lat, $lng, $distance);

        $idsCar = [];
        foreach($select_Car as $car)
        {
            array_push($idsCar, $car->id);
        }

        $carsresult= Car::withoutGlobalScopes()->where('publish_status',1)
            ->whereIn('id',$idsCar)
            ->get();


        return view('frontend.home-search-result',compact('accommodations','location','restaurants','touragencies','carsresult'));
    }

     public function blog_list(Request $request){
        $keyword =$request->input('blog');
      //  dd($keyword);
        $blogs = Blog::withoutGlobalScopes()->with('users')->where('publish_status',1)->paginate(2);

        return view('frontend.blog-list',compact('blogs'));
    }
    public function searchBlogList(Request $request){
        $keyword =$request->input('blog');
      //  dd($keyword);
        $blog = Blog::with('users')->where('publish_status',1);
            if (!empty($keyword)){
                $blog->where('title','like',"%{$request->get('blog')}%");
            }

         $blogs =$blog->get();
          //  dd($blogs);
        $view = View::make('frontend.blog-list',compact('blogs'));
        return $view->render();

    }



    public function blog_single($id){

        $blog = Blog::withoutGlobalScopes()->with('users','reviewBlogs','reviewBlogs.user')->where('publish_status',1)
            ->where('id',$id)->first();
        $blogs = Blog::withoutGlobalScopes()->with('users','reviewBlogs','reviewBlogs.user')->where('publish_status',1)
           ->get();

        //dd($blog);
        return view('frontend.blog-single',compact('blog','blogs'));
    }

    public function blogreview(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'comment' => 'required',
            'blog_id' => 'required',
        ]);
        $user_id = Auth::id();

        $blog = ReviewBlog::create(['name' => $request->input('name'),
            'email' => $request->input('email'),
            'comment' => $request->input('comment'),
            'blog_id' => $request->input('blog_id'),
            'user_id' => $user_id,
        ]);
        return response()->json(['success'=>'Review change successfully.']);
    }
    public function listing_view(){

        return view('frontend.listing-view');
    }
    public function add_listing(){
        return view('frontend.add-listing');
    }
    public function review_post(Request $request){
        $review = new ReviewAccommodation();
        $review->comment = $request->comment;
        $review->cleanliness = $request->cleanliness;
        $review->comfort = $request->comfort;
        $review->staff = $request->staf;
        $review->facilities = $request->facilities;
        $review->sub_avg = $request->score;
        $review->accommodation_id = $request->id;
        $review->user_id = Auth::user()->id;
        $review->save();

        $acc_id = ReviewAccommodation::withoutGlobalScopes()->where('accommodation_id',$request->id)->avg('sub_avg');
        $accommodation = Accommodation::withoutGlobalScopes()->where('id',$request->id)->first();
        $accommodation->rating = number_format($acc_id,2);
        $accommodation->update();
        return $review;
    }



    public function customer_profile(){

        return view('frontend.customer-profile');
    }




      public function listing_booking(){
        $booking = AccommodationBooking::with('user','room')->paginate(3);
        $cars = CarBooking::with('user')->paginate(3);
        $tourbooks = TourPackageBooking::with('user')->paginate(3);

        return view('frontend.listing-booking',compact('booking','cars','tourbooks'));
    }
     public function customer_review(){
        $reviews = ReviewAccommodation::with('accommodation')->get();

        return view('frontend.customer-review',compact('reviews'));
    }
    public function customer_edit_profile(){
        return view('frontend.edit-profile');
    }

    public function listing(Request $request){
        $select_city = $request->city;

        $select_price = $request->price;
        $price_ = explode(';',$select_price);
        $location =$request->address;
        $lat =$request->lat;
        $lng =$request->lng;
        $distance = 50;
        if (!empty($lat && $lng && $distance)) {
            $select_data = Accommodation::getByDistance($lat, $lng, $distance);
            $ids = [];
            foreach($select_data as $q)
            {
                array_push($ids, $q->id);

            }

        }
        $accommodations_ = Accommodation::withoutGlobalScopes()->with(['facilities','reviewAccommodations',
            'images' => function ($query){
                $query->where('is_featured',1);
            }
        ]) ->where('publish_status',1);

        if (!empty($select_city)){
            $accommodations_->where('city_id',$select_city);
        }
        if (!empty($select_price)){

            $accommodations_->whereBetween('price', [$price_[0], $price_[1]]);

        }
        if (!empty($ids)){

            $accommodations_->where('id',[$ids]);
        }

        $accommodations = $accommodations_->get();
        $cities = City::pluck('name_en','id');
        return view('frontend.listing',compact('accommodations','cities'));
    }

     public function listing_single($id){
         $accommodations = Accommodation::withoutGlobalScopes()->with(['accommodationrooms','reviewAccommodations','accommodationrooms.roomFeatures','accommodationrooms.images','facilities',
             'images' => function ($query){
             $query->where('is_featured',1);
         }])->whereId($id)
             ->where('publish_status',1)
             ->first();
         $accommodationAll = Accommodation::withoutGlobalScopes()->with(['images' => function ($query){
             $query->where('is_featured',1);
         }])->where('publish_status',1)->whereNotIn('id',[$id])->get();

         $rooms = AccommodationRoom::whereIn('accommodation_id',[$id])->where('publish_status',1)->get();
         $cleanliness = ReviewAccommodation::withoutGlobalScopes()->where('accommodation_id',$id)->avg('cleanliness');
         $comfort = ReviewAccommodation::withoutGlobalScopes()->where('accommodation_id',$id)->avg('comfort');
         $facilities = ReviewAccommodation::withoutGlobalScopes()->where('accommodation_id',$id)->avg('facilities');
         $staff = ReviewAccommodation::where('accommodation_id',$id)->avg('staff');
         $review = ReviewAccommodation::withoutGlobalScopes()->with('user')->where('accommodation_id',$id)->paginate(2);
//         return $review;
//         dd($rooms);
        return view('frontend.listing-single',compact('accommodations','accommodationAll','rooms','cleanliness','comfort','facilities','staff','review'));
    }



    public function rooms_details(Request $request){

        $room_d=AccommodationRoom::withoutGlobalScopes()->select('max_adult','max_children','price')->where('id',$request->id)->first();
        return response()->json($room_d);
    }
    public function add_booking(Request $request){
        $dates = $request->dates;
        $dates_ = explode(' - ',$dates);
        $from = Carbon::parse($dates_[0]);
        $to = Carbon::parse($dates_[1]);
        $countDate =  $to->diffInDays($from);
        $acc_room = AccommodationRoom::withoutGlobalScopes()->with(['images'=>function ($q){
            $q->where('is_featured',0);
        },'accommodation'])->where('id',$request->type)->first();

        $date=  $request->date;
        $adult=  $request->adult;
        $child=  $request->child;

        return view('frontend.booking',compact('acc_room','countDate','from','to','adult','child'));
    }
    public function insert_booking(Request $request){

        $validator = Validator::make($request->all(), [
            'room_id' => 'required',
            'adult' => 'required',
            'child' => 'required',
            'days' => 'required',
            'check' => 'required',
        ]);

        $validation_booking1 = AccommodationBooking::where('room_id',$request->roomId)->where('approve',1)
                                ->whereBetween('from',[$request->from,$request->to])
                                ->count();
        $validation_booking2 = AccommodationBooking::where('room_id',$request->roomId)->where('approve',1)
                                ->where('from','>=',$request->from)
                                ->where('to','<=',$request->to)
                                ->count();
        $validation_booking3 = AccommodationBooking::where('room_id',$request->roomId)->where('approve',1)
                                ->where('from','<=',$request->from)
                                ->where('to','>=',$request->to)
                                ->count();
        $validation_booking4 = AccommodationBooking::where('room_id',$request->roomId)->where('approve',1)
                                ->whereBetween('to',[$request->from,$request->to])
                                ->count();
        $validation_rooms = AccommodationRoom::where('id',$request->roomId)->first();

        if($validation_booking1 >= $validation_rooms->number_of_room || $validation_booking2 >= $validation_rooms->number_of_room || $validation_booking3 >= $validation_rooms->number_of_room || $validation_booking4 >= $validation_rooms->number_of_room){
            toastr()->error('Your selected days is unavailable this room.!');
            return redirect()->route('listing');
        }
       

       $bookDetails = new AccommodationBooking();
        $bookDetails->room_id = $request->roomId;
        $bookDetails->adults = $request->adult;
        $bookDetails->child = $request->child;
        $bookDetails->from = $request->from;
        $bookDetails->to = $request->to;
        $bookDetails->days = $request->days;
        $bookDetails->country = $request->country;
        $bookDetails->street = $request->street;
        $bookDetails->state = $request->state;
        $bookDetails->postal_code = $request->postal_code;
        $bookDetails->note = $request->note;
        $bookDetails->term_condition = $request->check;
        $bookDetails->confirm_status = 1;
        $bookDetails->user_id = Auth::user()->id;
        $bookDetails->save();
//        dd($bookDetails);
        $ref_no= 'REF'. str_pad($bookDetails->id, 6, "0", STR_PAD_LEFT);
        $bookDetails->ref_no = $ref_no;
        $bookDetails->save();

        $user = User::where('id', Auth::user()->id)->first();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->contact_number = $request->contact_number;
        $user->save();
        toastr()->success('Book Hotel successfully!');
        return redirect()->route('listing-booking');
//       return view(route('home'));
    }

    public function districts_list(Request $request){
       // dd($request->all());
    }
    public function maplist(Request $request,$id){
        $accommodation = Accommodation::withoutGlobalScopes()->with(['facilities',
            'images' => function ($query){
                $query->where('is_featured',1);
            }
        ]) ->where('publish_status',1)
         ->where('id',$id)
            ->first();

        $location =$accommodation->location;
        $lat =$accommodation->lat;
        $lng =$accommodation->lng;
        $distance = 10;

        $select_data = Accommodation::getByDistance($lat, $lng, $distance);
        $ids = [];
        foreach($select_data as $q)
        {
            array_push($ids, $q->id);
            array_push($ids, $id);
        }
        $accommodations = Accommodation::withoutGlobalScopes()->with(['facilities','reviewAccommodations',
            'images' => function ($query){
                $query->where('is_featured',1);
            }
        ]) ->where('publish_status',1)
            ->whereIn('id',$ids)
            ->get();

        //RESTAURANT

        $selectdata = Restaurant::getByDistance($lat, $lng, $distance);
        $idsR = [];
        foreach($selectdata as $q)
        {
            array_push($idsR, $q->id);
        }
        $restaurants = Restaurant::withoutGlobalScopes()->with(['reviewRestaurant',
            'images' => function ($query){
                $query->where('is_featured',1);
            }
        ]) ->where('publish_status',1)
            ->whereIn('id',$idsR)
            ->get();

        //TOUR agency
        $select_tour = TravelAgency::getByDistance($lat, $lng, $distance);

        $idsT = [];
        foreach($select_tour as $tour)
        {
            array_push($idsT, $tour->id);
        }

        $touragencies = TravelAgency::withoutGlobalScopes()->with(['reviewAgancy'])->where('publish_status',1)
            ->whereIn('id',$idsT)
            ->get();
        return view('frontend.on-map-result',compact('accommodations','location','restaurants','touragencies'));
    }
    public function maplist_restaurant(Request $request,$id){

        $restaurant = Restaurant::withoutGlobalScopes()->with(['reviewRestaurant',
            'images' => function ($query){
                $query->where('is_featured',1);
            }
        ]) ->where('publish_status',1)
            ->where('id',$id)
            ->first();
        $location =$restaurant->location;
        $lat =$restaurant->lat;
        $lng =$restaurant->lng;
        $distance = 6;

        $select_data = Accommodation::getByDistance($lat, $lng, $distance);

        $ids = [];
        foreach($select_data as $q)
        {
            array_push($ids, $q->id);
        }
        $accommodations = Accommodation::withoutGlobalScopes()->with(['facilities','reviewAccommodations',
            'images' => function ($query){
                $query->where('is_featured',1);
            }
        ]) ->where('publish_status',1)
            ->whereIn('id',$ids)
            ->get();

        //RESTAURANT

        $selectdata = Restaurant::getByDistance($lat, $lng, $distance);
        $idsR = [];
        foreach($selectdata as $q)
        {
            array_push($idsR, $q->id);
            array_push($ids, $id);
        }
        $restaurants = Restaurant::withoutGlobalScopes()->with(['reviewRestaurant',
            'images' => function ($query){
                $query->where('is_featured',1);
            }
        ]) ->where('publish_status',1)
            ->whereIn('id',$idsR)
            ->get();


        //TOUR agency
        $select_tour = TravelAgency::getByDistance($lat, $lng, $distance);

        $idsT = [];
        foreach($select_tour as $tour)
        {
            array_push($idsT, $tour->id);
        }

        $touragencies = TravelAgency::withoutGlobalScopes()->with(['reviewAgancy'])->where('publish_status',1)
            ->whereIn('id',$idsT)
            ->get();
        return view('frontend.on-map-result',compact('accommodations','location','restaurants','touragencies'));
    }
    public function maplist_touragency(Request $request,$id){

        $touragency = TravelAgency::withoutGlobalScopes()->with(['reviewAgancy'])->where('publish_status',1)
            ->where('id',$id)
            ->first();

        $location =$touragency->location;
        $lat =$touragency->lat;
        $lng =$touragency->lng;
        $distance = 10;

        $select_tour = TravelAgency::getByDistance($lat, $lng, $distance);

        $idsT = [];
        foreach($select_tour as $tour)
        {
            array_push($idsT, $tour->id);
            array_push($ids, $id);
        }

        $touragencies = TravelAgency::withoutGlobalScopes()->with(['reviewAgancy'])->where('publish_status',1)
            ->whereIn('id',$idsT)
            ->get();
        //dd($touragencies);

        $select_data = Accommodation::getByDistance($lat, $lng, $distance);

        $ids = [];
        foreach($select_data as $q)
        {
            array_push($ids, $q->id);
        }
        $accommodations = Accommodation::withoutGlobalScopes()->with(['facilities','reviewAccommodations',
            'images' => function ($query){
                $query->where('is_featured',1);
            }
        ]) ->where('publish_status',1)
            ->whereIn('id',$ids)
            ->get();

        //RESTAURANT

        $selectdata = Restaurant::getByDistance($lat, $lng, $distance);

        $idsR = [];
        foreach($selectdata as $q)
        {
            array_push($idsR, $q->id);
        }
        $restaurants = Restaurant::withoutGlobalScopes()->with(['reviewRestaurant',
            'images' => function ($query){
                $query->where('is_featured',1);
            }
        ]) ->where('publish_status',1)
            ->whereIn('id',$idsR)
            ->get();

        //TOUR agency

        return view('frontend.on-map-result',compact('accommodations','location','restaurants','touragencies'));
    }

}
