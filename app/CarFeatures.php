<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarFeatures extends Model
{
    protected $table = 'car_features';
    protected $fillable =[
        'name',
    ];
}
