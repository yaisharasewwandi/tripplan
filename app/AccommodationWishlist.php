<?php

namespace App;

use App\Traits\Multitenantable;
use Illuminate\Database\Eloquent\Model;

class AccommodationWishlist extends Model
{
    use Multitenantable;
    protected $table='accommodation_wishlist';
    protected $fillable=[
        'user_id',
        'accommodation_id'
    ];
    public function accommodations()
    {
        return $this->hasMany(Accommodation::class,'id');
    }

    public function users()
    {
        return $this->hasMany(User::class,'id');
    }
}
