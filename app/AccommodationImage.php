<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccommodationImage extends Model
{
    protected $table='accommodation_image';
    protected $fillable =[
        'accommodation_image',
        'accommodation_id',
        'is_featured',

    ];
}
