<?php

namespace App;

use App\Traits\Multitenantable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agency extends Model
{
    use SoftDeletes, Multitenantable;
    protected $table = 'agencies';
    protected $fillable =[
        'company_name',
        'f_name',
        'l_name',
        'account_email',
        'contact_number',
        'user_id',
        'place_contact_number',
        'district_id',
        'town_id',
        'place_address',
        'location',
        'lng',
        'lat',
        'term_condition',
        'otherbiss_type',
        'fb_page_link',
        'twiter_link',
        'instergram_link',
        'payment_option',
        'rating',
    ];
    public function cardTypes()
    {
        return $this->belongsToMany(CardType::class);
    }
    public function reviewAgency(){
        return $this->hasMany(ReviewAgency::class, 'agency_id');
    }
}
