<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'districts';
    protected $fillable =[
        'name',
        'lat',
        'lng',

    ];

    public function accommodations()
    {
        return $this->hasMany(Accommodation::class,'district_id');
    }
    public function locations()
    {
        return $this->hasMany(Location::class,'district_id');
    }
    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
