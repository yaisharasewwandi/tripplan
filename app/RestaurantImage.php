<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantImage extends Model
{
    protected $table = 'restaurant_images';
    protected $fillable =[
        'restaurant_image',
        'restaurant_id',
        'is_featured'
    ];
}
