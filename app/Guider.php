<?php

namespace App;

use App\Traits\Multitenantable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Guider extends Model
{
    use Multitenantable;
    protected $table='guiders';
    protected $fillable=[
        'name',
        'address',
        'location',
        'description',
        'banner_image',
        'featured_image',
        'contact_number',
        'fb_page_link',
        'instergram_link',
        'twiter_link',
        'lat',
        'lng',
        'rating',
        'publish_status',
        'district_id',
        'city_id',
        'user_id'
    ];
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
    public static function getByDistance($lat, $lng, $distance)
    {
        $results = DB::select(DB::raw('SELECT id, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $lng . ') ) + sin( radians(' . $lat .') ) * sin( radians(lat) ) ) ) AS distance FROM cars HAVING distance < ' . $distance . ' ORDER BY distance') );

        return $results;
    }
    public function reviewGuider(){
        return $this->hasMany(ReviewGuider::class, 'guider_id');
    }
    public function travelDetails(){
        return $this->hasMany(GuiderDetails::class, 'guider_id');
    }
}
