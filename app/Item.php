<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    const DAYS = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
    protected $table = 'items';
    protected $fillable = [
        'restaurants_id',
        'item_type_id',
        'title',
        'slug',
        'description',
        'price',
        'special_price',
        'nutritions',
        'benifits',
        'contains',
        'sugar_level',
        'fat_level',
        'salt_level',
        'any_time',
    ];
    public function types(){
        return $this->belongsTo(ItemType::class,'item_type_id','id');
    }
    public function images()
    {
        return $this->hasMany(ItemImage::class, 'item_id');
    }
    public function itemAvailableHourse()
    {
        return $this->hasMany(ItemAvailableTime::class);
    }
}
