<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarImages extends Model
{
    protected $table ='car_images';
    protected $fillable = [
        'image_name',
        'car_id'

    ];
}
