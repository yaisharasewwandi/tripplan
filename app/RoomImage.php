<?php
/**
 * Created by PhpStorm.
 * User: yaish
 * Date: 6/4/2020
 * Time: 11:49 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoomImage extends Model
{
    use SoftDeletes;
    protected $table = 'room_images';
    protected $fillable =[
        'room_image_name',
    ];
    public function accommodationRooms()
    {
        return $this->belongsTo(AccommodationRoom::class);
    }
}