<?php

namespace App;

use App\Traits\Multitenantable;
use Illuminate\Database\Eloquent\Model;

class ReviewAgency extends Model
{
    use Multitenantable;
    protected $table = 'review_agencies';
    protected $fillable =[
        'user_id',
        'comment',
        'cleanliness',
        'comfort',
        'staff',
        'facilities',
        'sub_avg',
    ];
    public function scopeReviewMsg($query){
        return $query->whereNotNull('comment');
    }
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
