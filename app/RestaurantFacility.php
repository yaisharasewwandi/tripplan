<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantFacility extends Model
{
    protected $table ='restaurant_facilities';
    protected $fillable =[
        'name',
    ];

    public function restaurants()
    {
        return $this->belongsToMany(Restaurant::class);
    }
}
