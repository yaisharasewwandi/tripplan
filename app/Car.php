<?php

namespace App;

use App\Traits\Multitenantable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Car extends Model
{
    use Multitenantable;
    protected $fillable =[
        'title',
        'description',
        'contact_number',
        'email',
        'type_id',
        'user_id',
        'district_id',
        'city_id',
        'address',
        'location',
        'lat',
        'lng',
        'passenger',
        'gearshift',
        'baggage',
        'door',
        'car_number',
        'price',
        'sale_price',
        'fb_page_link',
        'twiter_link',
        'instergram_link',
        'website_link',
        'publish_status'

    ];
    public function features()
    {
        return $this->belongsToMany(CarFeatures::class);
    }
    public function images()
    {
        return $this->hasMany(CarImages::class, 'car_id');
    }
    public static function getByDistance($lat, $lng, $distance)
    {
        $results = DB::select(DB::raw('SELECT id, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $lng . ') ) + sin( radians(' . $lat .') ) * sin( radians(lat) ) ) ) AS distance FROM cars HAVING distance < ' . $distance . ' ORDER BY distance') );

        return $results;
    }
    public function reviewCars(){
        return $this->hasMany(ReviewCar::class, 'car_id');
    }
}
