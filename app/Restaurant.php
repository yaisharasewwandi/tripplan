<?php
/**
 * Created by PhpStorm.
 * User: yaish
 * Date: 8/16/2020
 * Time: 12:43 PM
 */

namespace App;


use App\Traits\Multitenantable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Restaurant extends Model
{
    use Multitenantable;
    const DAYS = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

    protected $fillable = [
        'district_id',
        'city_id',
        'title',
        'slug',
        'address',
        'email',
        'location',
        'description',
        'image',
        'contact_number',
        'fb_page_link',
        'instergram_link',
        'twiter_link',
        'restaurant_type_id',
        'lat',
        'lat',
        'user_id',
    ];

    public function items()
    {
        return $this->hasMany(Item::class, 'restaurants_id');
    }
    public function type()
    {
        return $this->belongsTo(RestaurantType::class, 'restaurant_type_id');
    }

    public function images()
    {
        return $this->hasMany(RestaurantImage::class);
    }
    public function openHourse()
    {
        return $this->hasMany(RestaurantOpenHour::class);
    }

    public function reviewRestaurant()
    {
        return $this->hasMany(ReviewRestaurant::class, 'restaurant_id');
    }
    public static function getByDistance($lat, $lng, $distance)
    {
        $results = DB::select(DB::raw('SELECT id, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $lng . ') ) + sin( radians(' . $lat .') ) * sin( radians(lat) ) ) ) AS distance FROM restaurants HAVING distance < ' . $distance . ' ORDER BY distance') );

        return $results;
    }
    public function facilities()
    {
        return $this->belongsToMany(RestaurantFacility::class);
    }
}
