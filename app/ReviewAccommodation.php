<?php

namespace App;

use App\Traits\Multitenantable;
use Illuminate\Database\Eloquent\Model;

class ReviewAccommodation extends Model
{
    use Multitenantable;
    protected $table = 'review_accommodation';
    protected $fillable =[
      'user_id',
      'sub_avg',
      'comment',
      'cleanliness',
      'comfort',
      'staff',
      'facilities',
      'accommodation_id',
    ];
    public function scopeReviewMsg($query){
        return $query->whereNotNull('comment');
    }
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
    public function accommodation(){
        return $this->belongsTo(Accommodation::class, 'accommodation_id');
    }

}
