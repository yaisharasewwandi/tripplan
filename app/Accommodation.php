<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Multitenantable;
use Illuminate\Support\Facades\DB;

class Accommodation extends Model
{
    use SoftDeletes, Multitenantable;
    protected $table = 'accommodations';
    protected $fillable =[
        'title',
        'description',
        'contact_number',
        'email',
        'type_id',
        'user_id',
        'district_id',
        'city_id',
        'address',
        'location',
        'lat',
        'lng',
        'check_in',
        'check_out',
        'price',
        'term_condition',
        'fb_page_link',
        'twiter_link',
        'instergram_link',
        'website_link',

    ];

    public function facilities()
    {
        return $this->belongsToMany(Facility::class);
    }
    public function services()
    {
        return $this->belongsToMany(Service::class);
    }
    public function images()
    {
        return $this->hasMany(AccommodationImage::class);
    }
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
    public function accommodationrooms(){
        return $this->hasMany(AccommodationRoom::class, 'accommodation_id');
    }
    public function reviewAccommodations(){
        return $this->hasMany(ReviewAccommodation::class, 'accommodation_id');
    }

    public function types()
    {
        return $this->belongsTo(AccommodationType::class, 'type_id');
    }
    public static function getByDistance($lat, $lng, $distance)
    {
        $results = DB::select(DB::raw('SELECT id, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $lng . ') ) + sin( radians(' . $lat .') ) * sin( radians(lat) ) ) ) AS distance FROM accommodations HAVING distance < ' . $distance . ' ORDER BY distance') );

        return $results;
    }

}
