<?php

namespace App;

use App\Traits\Multitenantable;
use Illuminate\Database\Eloquent\Model;

class ReviewRestaurant extends Model
{
    use Multitenantable;
    protected $table = 'review_restaurant';
    protected $fillable =[
        'user_id',
        'sub_avg',
        'comment',
        'cleanliness',
        'comfort',
        'staff',
        'facilities',
        'restaurant_id',
    ];
    public function scopeReviewMsg($query){
        return $query->whereNotNull('comment');
    }
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
