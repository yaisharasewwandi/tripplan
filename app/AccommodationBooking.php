<?php

namespace App;

use App\Traits\Multitenantable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccommodationBooking extends Model
{
    use Multitenantable;
    protected $table='accommodation_booking';
    protected $fillable =[
        'room_id',
        'ref_no',
        'from_to',
        'days',
        'adults',
        'child',
        'taxes_fees',
        'country',
        'town',
        'street',
        'state',
        'postal_code',
        'note',
        'confirm_status',
        'user_id',
        'paid_at',
    ];

    public function room()
    {
        return $this->belongsTo(AccommodationRoom::class, 'room_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }



}
