<?php

namespace App\frontend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserProfile extends Model
{
    use SoftDeletes;
    protected $table ='users';
    protected $fillable = [
        'name',
        'email',
        'contact_number',
        'address',
        'note',
        'image_path',
        'facebook_link',
        'twiter_link',
        'instergram_link',
        'status',
        'business_type',
        'company_name'
    ];
}
