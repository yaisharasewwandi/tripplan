<?php

namespace App\frontend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    use SoftDeletes;
    protected $table = 'accounts';
    protected $fillable =[
        'f_name',
        'l_name',
        'account_email',
        'contact_number',
        'user_id',
        'place_contact_number',
        'district_id',
        'town_id',
        'place_address',
        'location',
        'lng',
        'lat',
        'term_condition',
        'otherbiss_type',
    ];
}