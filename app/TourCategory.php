<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourCategory extends Model
{
    protected $table='tour_categories';
    protected $fillable=[
        'name',
        'slug',
        'description',
        'image/icon',
        'publish_status'
    ];
}
