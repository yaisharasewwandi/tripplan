<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgenciesImage extends Model
{
    protected  $table='travel_agencies_image';
    protected $fillable=[
      'agencies_image',
      'travel_agencies_id',
      'is_featured'
    ];
}
