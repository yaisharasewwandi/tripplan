<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model{
    protected $table ='facilities';
    protected $fillable =[
        'name',
    ];
    public function accommodations()
    {
        return $this->belongsToMany(Accommodation::class);
    }
}