<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable =[
        'name_en',
        'district_id',
        'lat',
        'lng',

    ];

    public function districts()
    {
        return $this->belongsTo(District::class, 'district_id');
    }
}
