<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CardType extends Model
{
   protected $table = 'card_type';
   protected $fillable = [
       'name',
       'image_path'
   ];
    public function accommodations()
    {
        return $this->belongsToMany(Accommodation::class);
    }
    public function restaurant()
    {
        return $this->belongsToMany(Restaurant::class);
    }
    public function guiders()
    {
        return $this->belongsToMany(Guider::class);
    }
    public function agencies()
    {
        return $this->belongsToMany(Agency::class);
    }
}
