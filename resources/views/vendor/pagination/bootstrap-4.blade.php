@if ($paginator->hasPages())
    {{--    <nav>--}}
    <div class="pagination" style="display: inline">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <a  class="prevposts-link"><i class="fa fa-caret-left"></i></a>
            {{--                <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">--}}
            {{--                    <span class="page-link" aria-hidden="true">&lsaquo;</span>--}}
            {{--                </li>--}}
        @else
            <a href="{{ $paginator->previousPageUrl() }}" class="prevposts-link"><i class="fa fa-caret-left"></i></a>
            {{--                <li class="page-item">--}}
            {{--                    <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>--}}
            {{--                </li>--}}
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <a class="page-item active current-page" aria-current="page"><span class="page-link">{{ $page }}</span></a>
                    @else
                        <a class="page-link" href="{{ $url }}">{{ $page }}</a>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a href="{{ $paginator->nextPageUrl() }}" class="nextposts-link"><i class="fa fa-caret-right"></i></a>
            {{--                <a class="page-item">--}}
            {{--                    <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>--}}
            {{--                </a>--}}
        @else
            <a  class="nextposts-link"><i class="fa fa-caret-right"></i></a>
            {{--                <a class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">--}}
            {{--                    <span class="page-link" aria-hidden="true">&rsaquo;</span>--}}
            {{--                </a>--}}
        @endif
    </div>
    {{--    </nav>--}}
@endif
