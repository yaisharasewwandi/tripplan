

    <!-- listing-item  -->
    @foreach($accommodations as $accommodation)
        <div class="listing-item has_one_column">
            <article class="geodir-category-listing fl-wrap">
                <div class="geodir-category-img">
                    @foreach($accommodation->images as $image)
                        <a href="{{route('listing-single',$accommodation->id)}}"><img src="{{asset($image['accommodation_image'])}}" alt=""></a>

                    @endforeach

                    <div class="listing-avatar"><a href=""></a>
                        <span class="avatar-tooltip">Added By  <strong>Alisa Noory</strong></span>
                    </div>
                    {{--<div class="sale-window">Sale 20%</div>--}}
                    <div class="geodir-category-opt">
                        <div class="listing-rating card-popup-rainingvis" data-starrating2="{{number_format($accommodation->rating,2)}}"></div>
                        <div class="rate-class-name">
                            <div class="score"><strong>Very Good</strong>{{$accommodation->reviewAccommodations()->count()}} Reviews </div>
                            <span>{{number_format($accommodation->rating,2)}}</span>
                        </div>
                    </div>
                </div>
                <div class="geodir-category-content fl-wrap">
                    <div class="geodir-category-content-title fl-wrap">
                        <div class="geodir-category-content-title-item">
                            <h3 class="title-sin_map"><a href="{{route('listing-single',$accommodation->id)}}">{{$accommodation->title}}</a></h3>
                            <div class="geodir-category-location fl-wrap"><a href="#0" class="map-item"><i class="fas fa-map-marker-alt"></i> {{$accommodation->location}}</a></div>
                        </div>
                    </div>
                    <p>{!! Illuminate\Support\Str::limit(strip_tags($accommodation->description), 100, $end='...') !!}  </p>

                    <ul class="facilities-list fl-wrap">
                        @foreach($accommodation->facilities as $facility)
                            <li><i class="{{$facility->icon}}"></i><span>{{$facility->name}}</span></li>
                        @endforeach
                    </ul>
                    <div class="geodir-category-footer fl-wrap">
                        <div class="geodir-category-price">Awg/Night <br> <span>{{$accommodation->price}} LKR</span></div>
                        <div class="geodir-opt-list">
                            <a href="#"  class="single-map-item" data-newlatitude="{{$accommodation->lat}}" data-newlongitude="{{$accommodation->lng}}"><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip">On the map</span></a>
                            <a  class="geodir-js-favorite wishlist" data-id="{{$accommodation->id}}"><i class="fal fa-heart"></i><span class="geodir-opt-tooltip">Save</span></a>
                            <a href="{{route('direction-search',$accommodation->id)}}"   class="geodir-js-booking "><i class="fal fa-exchange"></i><span class="geodir-opt-tooltip">Find Directions</span></a>
                        </div>
                    </div>
                </div>
            </article>
        </div>
@endforeach
<!-- listing-item end -->

    <!-- listing-item end -->

