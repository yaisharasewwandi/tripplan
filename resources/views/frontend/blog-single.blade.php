@extends('frontend.layouts.master')
@section('content')

    <div id="wrapper">
        <!-- content-->
        <div class="content">
            <!--  section  -->
            <section class="color-bg middle-padding ">
                <div class="wave-bg wave-bg2"></div>
                <div class="container">
                    <div class="flat-title-wrap">
                        <h2><span>Blog Post Title</span></h2>
                        <span class="section-separator"></span>
                        <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut nec tincidunt arcu, sit amet fermentum sem.</h4>
                    </div>
                </div>
            </section>
            <!--  section  end-->
            <div class="breadcrumbs-fs fl-wrap">
                <div class="container">
                    <div class="breadcrumbs fl-wrap"><a href="#">Home</a><a href="#">Blog</a><span>Blog Single</span></div>
                </div>
            </div>
            <!-- section-->
            <section  id="sec1" class="middle-padding grey-blue-bg">
                <div class="container">
                    <div class="row">
                        <!--blog content -->
                        <div class="col-md-8">
                            <!--post-container -->
                            <div class="post-container fl-wrap">
                                <!-- article> -->
                                <article class="post-article">
                                    <div class="list-single-main-media fl-wrap">
                                        <div class="single-slider-wrapper fl-wrap">
                                            <div class="single-slider fl-wrap"  >
                                                <div class="slick-slide-item"><img src="{{asset($blog->image)}}" alt=""></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-single-main-item fl-wrap">
                                        <div class="list-single-main-item-title fl-wrap">
                                            <h3>{{$blog->title}} </h3>
                                        </div>
                                         <blockquote>
                                            <p>{{$blog->description}}</p>
                                        </blockquote>
                                         <div class="post-author"><a href="#"><img src="{{asset($blog->users->image_path)}}" alt=""><span>By , {{$blog->users->name}}</span></a></div>
                                        <div class="post-opt">
                                            <ul>
                                                <li><i class="fal fa-calendar"></i> <span>{{$blog->created_at}}</span></li>
                                                {{--<li><i class="fal fa-eye"></i> <span>264</span></li>--}}
                                                <li><i class="fal fa-tags"></i> <a href="#">Photography</a> , <a href="#">Design</a> </li>
                                            </ul>
                                        </div>


                                    </div>
                                    <!-- list-single-main-item -->
                                    <div class="list-single-main-item fl-wrap" id="sec5">
                                        <div class="list-single-main-item-title fl-wrap">
                                            <h3>Comments </h3>
                                        </div>
                                        <div class="reviews-comments-wrap">
                                        @foreach($blog->reviewBlogs as $data)
                                            <!-- reviews-comments-item -->
                                            <div class="reviews-comments-item">
                                                <div class="review-comments-avatar">
                                                    @if(!empty($data->user['image_path']))
                                                    <img src="{{asset($data->user['image_path'])}}" alt="">
                                                    @else
                                                        <img src="{{asset('frontend/images/user.png')}}" alt="">
                                                    @endif
                                                </div>
                                                <div class="reviews-comments-item-text">
                                                    <h4><a href="#">{{$data['name']}}</a></h4>
                                                    <div class="clearfix"></div>
                                                    <p>" {{$data['comment']}} "</p>
                                                    <div class="reviews-comments-item-date"><span><i class="far fa-calendar-check"></i>{{$data['created_at']}}</span></div>
                                                </div>
                                            </div>
                                            <!--reviews-comments-item end-->
                                            @endforeach

                                        </div>
                                    </div>
                                    <!-- list-single-main-item end -->
                                    <!-- list-single-main-item -->
                                    <div class="list-single-main-item fl-wrap" id="sec6">
                                        <div class="list-single-main-item-title fl-wrap">
                                            <h3>Add Comment</h3>
                                        </div>
                                        <!-- Add Review Box -->
                                        <div id="add-review" class="add-review-box">
                                            <!-- Review Comment -->
                                            <form id="add-comment" class="add-comment  custom-form" name="rangeCalc" >
                                                <fieldset>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label><i class="fal fa-user"></i></label>
                                                            <input type="text" name="name" id="name" placeholder="Your Name *" required/>
                                                            <input type="hidden" name="blog_id" id="blog_id" placeholder="Your Name *" value="{{$blog->id}}" />
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><i class="fal fa-envelope"></i>  </label>
                                                            <input type="email" name="email" id="email" placeholder="Email Address*" required/>
                                                        </div>
                                                    </div>
                                                    <textarea cols="40" name="comment" id="comment" rows="3" placeholder="Your Review:" required></textarea>
                                                </fieldset>
                                                @if (Auth::user() )
                                                <button type="submit"  class="btn  no-shdow-btn float-btn color2-bg " style="margin-top:30px">Submit Comment<i class="fal fa-paper-plane"></i></button>
                                                @else
                                                    <button  class="btn  no-shdow-btn float-btn color2-bg modal-open" style="margin-top:30px">Submit Comment<i class="fal fa-paper-plane"></i></button>
                                                @endif
                                            </form>
                                        </div>
                                        <!-- Add Review Box / End -->
                                    </div>
                                    <!-- list-single-main-item end -->
                                </article>
                                <!-- article end -->
                            </div>
                            <!--post-container end -->
                        </div>
                        <!-- blog content end -->
                        <!--   sidebar  -->
                        <div class="col-md-4">
                            <!--box-widget-wrap -->
                            <div class="box-widget-wrap fl-wrap fixed-bar">
                                <!--box-widget-item -->

                                <!--box-widget-item end -->
                                <!--box-widget-item -->
                                <div class="box-widget-item fl-wrap">
                                    <div class="box-widget">
                                        <div class="box-widget-content">
                                            <div class="box-widget-item-header">
                                                <h3>About Athor</h3>
                                            </div>
                                            <div class="box-widget-author fl-wrap">
                                                <div class="box-widget-author-title fl-wrap">
                                                    <div class="box-widget-author-title-img">
                                                        <img src="{{asset($blog->users->image_path)}}" alt="">
                                                    </div>
                                                    <a href="author-single.html">{{$blog->users->name}}</a>
                                                    <span>Co-manager associated</span>
                                                </div>
                                                <div class="box-widget-author-content">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla.</p>
                                                </div>
                                                <div class="list-widget-social">
                                                    <ul>
                                                        <li><a href="{{$blog->users->facebook_link}}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                                        <li><a href="{{$blog->users->twiter_link}}" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                                        <li><a href="{{$blog->users->instergram_link}}" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--box-widget-item end -->
                                <!--box-widget-item -->
                                <div class="box-widget-item fl-wrap">
                                    <div class="box-widget widget-posts">
                                        <div class="box-widget-content">
                                            <div class="box-widget-item-header">
                                                <h3>Latest Blogs</h3>
                                            </div>
                                            <!--box-image-widget-->
                                            @foreach($blogs as $key=>$blo)
                                                @if($key < 3)
                                            <div class="box-image-widget">
                                                <div class="box-image-widget-media"><img src="{{asset($blo->image)}}" alt="">
                                                    <a href="{{route('blog_single',$blo->id)}}" class="color-bg">Details</a>
                                                </div>
                                                <div class="box-image-widget-details">
                                                    <h4>{{$blo->users['name']}}</h4>
                                                    <p>{{ Illuminate\Support\Str::limit($blo->description, 50, $end='...') }}</p>
                                                    <span class="widget-posts-date"><i class="fal fa-calendar"></i> {{$blo->created_at}} </span>
                                                </div>
                                            </div>
                                                @endif
                                            @endforeach
                                            <!--box-image-widget end -->

                                        </div>
                                    </div>
                                </div>
                                <!--box-widget-item end -->


                            </div>
                            <!--box-widget-wrap end -->
                        </div>
                        <!--   sidebar end  -->
                    </div>
                </div>
                <div class="limit-box fl-wrap"></div>
            </section>
            <!-- section end -->
        </div>
        <!-- content end-->
    </div>
    <!--wrapper end -->
@endsection
@section('script')
    <script>
    jQuery(document).ready(function($) {

    $('#add-comment').on('submit', function (e){
    e.preventDefault();
    var name = $('#name').val();
    var email = $('#email').val();
    var comment = $('#comment').val();
    var blog_id = $('#blog_id').val();

    $.ajax({
            type: "GET",
            dataType: "json",
            url: '/blogreview',
            data: {'name': name,'email': email,'comment': comment,'blog_id': blog_id},
            success: function(data){
                location.reload();
            }
        });
    })
    })

    </script>
@endsection


