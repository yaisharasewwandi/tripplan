@extends('frontend.layouts.master')
@section('content')
<!--  wrapper  -->
{{--      @include('frontend.layouts.includes.profile-nav')--}}
                    <!-- section-->
<div id="wrapper">
    <!-- content-->
    <div class="content">
        <!-- section-->
        <section class="flat-header color-bg adm-header">
            <div class="wave-bg wave-bg2"></div>
            <div class="container">
                <div class="dasboard-wrap fl-wrap">
                    <div class="dasboard-breadcrumbs breadcrumbs"><a href="#">Home</a><a href="#">Dashboard</a><span>Profile page</span></div>
                    <!--dasboard-sidebar-->
                    <div class="dasboard-sidebar">
                        <div class="dasboard-sidebar-content fl-wrap">
                            <div class="dasboard-avatar">
                                @if(!empty(Auth::user()->image_path))
                                    <img src="{{Auth::user()->image_path}}" alt="">
                                @else
                                    <img src="{{asset('frontend/images/user.png')}}" alt="">
                                @endif
                            </div>
                            <div class="dasboard-sidebar-item fl-wrap">
                                <h3>
                                    <span>Welcome </span>
                                    {{Auth::user()->name}}
                                </h3>
                            </div>
                            <button id="hott" class="ed-btn" ><a href="{{route('add-listing')}}" style="color: #ffffff;" >Add Hotel</a></button>

                            {{--                                            <div class="user-stats fl-wrap">--}}
                            {{--                                            <ul>--}}


                            {{--                                                <li>--}}
                            {{--                                                    Reviews--}}
                            {{--                                                    <span>9</span>--}}
                            {{--                                                </li>--}}
                            {{--                                            </ul>--}}
                            {{--                                        </div>--}}
                            <a  href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="log-out-btn color-bg">
                                {{ __('Log Out') }}
                                <i class="far fa-sign-out"></i>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>

                        </div>
                    </div>
                    <!--dasboard-sidebar end-->
                    <!-- dasboard-menu-->
                    <div class="dasboard-menu">
                        <div class="dasboard-menu-btn color3-bg">Dashboard Menu <i class="fal fa-bars"></i></div>
                        <ul class="dasboard-menu-wrap">
                            <li>
                                <a href="{{route('customer-edit-profile')}}" class="user-profile-act"><i class="far fa-user"></i>Profile</a>
                                {{--                                            <ul>--}}
                                {{--                                                <li><a href="dashboard-myprofile.html">Edit profile</a></li>--}}
                                {{--                                                <li><a href="dashboard-password.html">Change Password</a></li>--}}
                                {{--                                            </ul>--}}
                            </li>
                            {{--                                        <li><a href="dashboard-messages.html"><i class="far fa-envelope"></i> Messages <span>3</span></a></li>--}}
                            {{--                                        <li>--}}
                            {{--                                            <a href="dashboard-listing-table.html"><i class="far fa-th-list"></i> My listigs  </a>--}}
                            {{--                                            <ul>--}}
                            {{--                                                <li><a href="#">Active</a><span>5</span></li>--}}
                            {{--                                                <li><a href="#">Pending</a><span>2</span></li>--}}
                            {{--                                                <li><a href="#">Expire</a><span>3</span></li>--}}
                            {{--                                            </ul>--}}
                            {{--                                        </li>--}}
                            <li><a href="{{route('listing-booking')}}"> <i class="far fa-calendar-check"></i> Bookings <span></span></a></li>
                            {{--                                        <li><a href="dashboard-review.html"><i class="far fa-comments"></i> Reviews </a></li>--}}
                        </ul>
                    </div>

                </div>
            </div>
        </section>
        <!-- section end-->

                    <section class="middle-padding">
                        <div class="container">
                            <!--dasboard-wrap-->
                            <div class="dasboard-wrap fl-wrap">
                                <!-- dashboard-content-->
                                <div class="dashboard-content fl-wrap">
                                    <div class="box-widget-item-header">
                                        <h3> Your Profile</h3>
                                    </div>
                                    <form action="{{ route('edit-profile.update', Auth::user()->id) }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                    <!-- profile-edit-container-->
                                    <div class="profile-edit-container">
                                        <div class="custom-form">
                                            <label>Your Name <i class="far fa-user"></i></label>
                                            <input type="text" name="name" placeholder="Enter Your Name" value=" {{Auth::user()->name}}"/>
                                            <label>Email Address<i class="far fa-envelope"></i>  </label>
                                            <input type="text" name="email" placeholder="Enter Your Email" value="{{Auth::user()->email}}"/>
                                            <label>Phone<i class="far fa-phone"></i>  </label>
                                            <input type="text" name="contact_number" placeholder="Enter Your Contact Number" value="{{Auth::user()->contact_number}}"/>
                                            <label> Adress <i class="fas fa-map-marker"></i>  </label>
                                            <input type="text" name="address" placeholder="Enter Your Address" value="{{Auth::user()->address}}"/>
                                            <div class="row">
                                                <div class="col-sm-9">
                                                    <label> Notes</label>
                                                    <textarea cols="40" name="note" rows="3" placeholder="About Me">{{Auth::user()->note}}</textarea>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Change Profile Picture</label>
                                                    <div class="add-list-media-wrap">
                                                        <forms class="fuzone">
                                                            <div class="fu-text">
                                                                <span><i class="fal fa-image"></i> Click here or drop files to upload</span>
                                                            </div>
                                                            <input name="image_path" type="file" id="image_path" class="upload">
                                                        </forms>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- profile-edit-container end-->
                                    {{--<div class="box-widget-item-header mat-top">--}}
                                        {{--<h3>Your Plan</h3>--}}
                                    {{--</div>--}}
                                    <!-- profile-edit-container-->
                                    {{--<div class="profile-edit-container add-list-container">--}}
                                        {{--<div class="custom-form">--}}
                                            {{--<div class="row">--}}
                                                {{--<!--col -->--}}

                                                {{--<!--col end--> --}}
                                                {{--<!--col --> --}}
                                                {{--<div class="col-md-4">--}}
                                                    {{--<div class="act-widget fl-wrap">--}}
                                                        {{--<div class="act-widget-header">--}}
                                                            {{--<h4>I am a Guider</h4>--}}
                                                            {{--<div class="onoffswitch">--}}
                                                                {{--<input type="checkbox" name="status" data-id="{{ Auth::User()->id }}" class="onoffswitch-checkbox" id="myonoffswitch5" {{ Auth::User()->status == 1 ? 'checked' : '' }}>--}}
                                                                {{--<label class="onoffswitch-label" for="myonoffswitch5">--}}
                                                                    {{--<span class="onoffswitch-inner"></span>--}}
                                                                    {{--<span class="onoffswitch-switch"></span>--}}
                                                                {{--</label>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<!--col end--> --}}

                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    <!-- profile-edit-container end-->
                                    <div class="box-widget-item-header mat-top">
                                        <h3>Your  Socials</h3>
                                    </div>
                                    <!-- profile-edit-container-->
                                    <div class="profile-edit-container">
                                        <div class="custom-form">
                                            <label>Facebook <i class="fab fa-facebook"></i></label>
                                            <input type="text" name="facebook_link" placeholder="Enter Your Facebook Link" value="{{Auth::user()->facebook_link}}"/>
                                            <label>Twitter<i class="fab fa-twitter"></i>  </label>
                                            <input type="text" name="twiter_link" placeholder="Enter Your Twitter Link" value="{{Auth::user()->twiter_link}}"/>
                                            <label> Instagram <i class="fab fa-instagram"></i>  </label>
                                            <input type="text" name="instergram_link" placeholder="Enter Your Instagram Link" value="{{Auth::user()->instergram_link}}"/>
                                            <button class="btn    color2-bg  float-btn" type="submit">Save Changes<i class="fal fa-save"></i></button>
                                        </div>
                                    </div>
                                    <!-- profile-edit-container end-->
                                    </form>
                                </div>
                                <!-- dashboard-list-box end-->
                            </div>
                            <!-- dasboard-wrap end-->
                        </div>
                    </section>
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!-- content end-->
            </div>
            <!--wrapper end -->

            @endsection
@section('script')

    <script>
        $(document).ready(function(){
    $('.onoffswitch-checkbox').change(function () {
        let status = $(this).prop('checked') === true ? 1 : 0;
        let userId = $(this).data('id');
        $.ajax({
            type: "GET",
            dataType: "json",
            url: '{{ route('users.update.status') }}',
            data: {'status': status, 'user_id': userId},
            success: function (msg) {
            location.reload();
           toastr.success('You are the guide','updated successfully');

        },
        error: function (msg) {
            toastr.error( 'Inconceivable!');
        }
        });
    });
});
    </script>

@endsection
