@extends('frontend.layouts.master')
@section('content')
    <!--  wrapper  -->
    <div id="wrapper">
        <!-- content-->
        <div class="content">
            <!-- Map -->
            <div class="map-container  fw-map big_map hid-mob-map" >
                <form method="get" action="{{route('restaurant-list')}}">
                    <input id="searchInput" class="input-controls fl-wrap controls-mapwn"  type="text" style="margin-left: -600px" placeholder="Enter a location">
                    <input type="hidden" name="address" id="location">
                    <input type="hidden" name="lat" id="lat">
                    <input type="hidden" name="lng" id="lng">
                    <button type="submit" class=" " style="position: absolute;
    background: #F9B90F  right no-repeat;
    box-shadow: 0 2px 4px 0 rgba(0,0,0,.1);
    width: 80px;
    z-index: 1000;
    border: none;
    padding: 20px;
    border-radius: 4px;margin: 0px 0px 1px -248px;cursor: pointer;">Search</button>
                </form>
                <div class="map" id="map" style="width: 100%; height: 300px;"></div>



                <div class="map-close"><i class="fas fa-times"></i></div>

            </div>
            <!-- Map end -->
            <div class="breadcrumbs-fs fl-wrap">
                <div class="container">
                    <div class="breadcrumbs fl-wrap"><a href="#">Home</a><a href="#">Listing </a><span>Fullwidth Map</span></div>
                </div>
            </div>
            <section class="grey-blue-bg small-padding">
                <div class="container">
                    <div class="row">
                        <!--filter sidebar -->
                        <div class="col-md-4">
                            <div class="mobile-list-controls fl-wrap">
                                <div class="mlc show-hidden-column-map schm"><i class="fal fa-map-marked-alt"></i> Show Map</div>
                                <div class="mlc show-list-wrap-search"><i class="fal fa-filter"></i> Filter</div>
                            </div>
                            <div class="fl-wrap filter-sidebar_item fixed-bar">
                                <form method="get" action="{{route('restaurant-list')}}">
                                <div class="filter-sidebar fl-wrap lws_mobile">
                                    <!--col-list-search-input-item -->
                                    <div class="col-list-search-input-item in-loc-dec fl-wrap not-vis-arrow">
                                        <label>City/Category</label>
                                        <div class="listsearch-input-item">
                                            <select data-placeholder="City" name="city" required class="chosen-select" >
                                                <option>All Cities</option>
                                                @foreach($cities as $key=>$city)
                                                    <option value="{{$key}}">{{$city}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!--col-list-search-input-item  -->
                                    <div class="col-list-search-input-item fl-wrap">
                                        <button class="header-search-button" type="submit">Search <i class="far fa-search"></i></button>
                                    </div>
                                    <!--col-list-search-input-item end-->
                                </div>
                                </form>
                            </div>
                        </div>
                        <!--filter sidebar end-->
                        <!--listing -->
                        <div class="col-md-8">
                            <!--col-list-wrap -->
                            <div class="col-list-wrap fw-col-list-wrap post-container">
                                <!-- list-main-wrap-->
                                <div class="list-main-wrap fl-wrap card-listing">
                                    <!-- list-main-wrap-opt-->
                                    <div class="list-main-wrap-opt fl-wrap">
                                        <div class="list-main-wrap-title fl-wrap col-title">
                                            <h2>Results For : <span> </span></h2>
                                        </div>
                                        <!-- price-opt-->

                                        <!-- price-opt end-->
                                        <!-- price-opt-->
                                        <div class="grid-opt">
                                            <ul>
                                                <li><span class="two-col-grid act-grid-opt"><i class="fas fa-th-large"></i></span></li>
                                                <li><span class="one-col-grid"><i class="fas fa-bars"></i></span></li>
                                            </ul>
                                        </div>
                                        <!-- price-opt end-->
                                    </div>
                                    <!-- list-main-wrap-opt end-->
                                    <!-- listing-item-container -->
                                    <div class="listing-item-container init-grid-items fl-wrap">
                                        <!-- listing-item  -->
                                        @foreach($restaurants as $restaurant)
                                            <div class="article-masonry">
                                                <article class="card-post">
                                                    <div class="card-post-img fl-wrap">
                                                        <div class="list-single-main-media fl-wrap">
                                                            <div class="single-slider-wrapper fl-wrap">
                                                                <div class="single-slider fl-wrap"  >
                                                                    @foreach($restaurant->images as $image)
                                                                    <div class="slick-slide-item"><img src="{{asset($image['restaurant_image'])}}" alt=""></div>
                                                                    @endforeach

                                                                </div>
                                                                <div class="swiper-button-prev sw-btn"><i class="fa fa-long-arrow-left"></i></div>
                                                                <div class="swiper-button-next sw-btn"><i class="fa fa-long-arrow-right"></i></div>
                                                                <div class="listing-avatar"><a href="author-single.html"><img src="images/avatar/1.jpg" alt=""></a>
                                                                    <span class="avatar-tooltip">Added By  <strong>Alisa Noory</strong></span>
                                                                </div>
                                                                {{--<div class="sale-window">Sale 20%</div>--}}
                                                                <div class="geodir-category-opt">
                                                                    <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                                                                    <div class="rate-class-name">
                                                                        <div class="score"><strong>Very Good</strong>27 Reviews </div>
                                                                        <span>5.0</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="geodir-category-content fl-wrap">
                                                        <div class="geodir-category-content-title fl-wrap">
                                                            <div class="geodir-category-content-title-item">
                                                                <h3 class="title-sin_map"><a href="{{route('restaurant-single',$restaurant->id)}}">{{$restaurant->title}}</a></h3>
                                                                <div class="geodir-category-location fl-wrap"><a href="#0" class="map-item"><i class="fas fa-map-marker-alt"></i> {{$restaurant->location}}</a></div>
                                                            </div>
                                                        </div>
                                                        <p>{!! Illuminate\Support\Str::limit(strip_tags($restaurant->description), 100, $end='...') !!}  </p>

                                                        <ul class="facilities-list fl-wrap">

                                                        </ul>
                                                        <div class="geodir-category-footer fl-wrap">
                                                            <div class="geodir-category-price"><span></span></div>
                                                            <div class="geodir-opt-list">
                                                                <a href="#0" class="map-item"><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip">On the map <strong>1</strong></span></a>
                                                                <a href="#" class="geodir-js-favorite"><i class="fal fa-heart"></i><span class="geodir-opt-tooltip">Save</span></a>
                                                                <a href="#" class="geodir-js-booking"><i class="fal fa-exchange"></i><span class="geodir-opt-tooltip">Find Directions</span></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                    @endforeach
                                    <!-- listing-item end -->

                                        <!-- listing-item end -->
                                    </div>
                                    <!-- listing-item-container end-->

                                </div>
                                <!-- list-main-wrap end-->
                            </div>
                            <!--col-list-wrap end -->
                        </div>
                        <!--listing  end-->
                    </div>
                    <!--row end-->
                </div>
                <div class="limit-box fl-wrap"></div>
            </section>
        </div>
        <!-- content end-->
    </div>
@endsection
@section('script')

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA6V8pVX1JyORSc4CwPzeBn2YK3su0Bu0A&libraries=places"></script>

    {{--    <script src='http://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyA6V8pVX1JyORSc4CwPzeBn2YK3su0Bu0A'></script>--}}
    {{--    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA6V8pVX1JyORSc4CwPzeBn2YK3su0Bu0A"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.24/gmaps.js"></script>
    <script>

        function mapModelDetils(model) {
            return `<div class="map-con">
                <div style='float:right; padding: 10px;'>
                <b>${model.title}</b>
                <br/>${model.address}</div>
                <br/>${model.contact_number}</div>

            </div>`;
        }

    </script>

    <script>

        $(document).ready(function() {
            $("#lat_area").addClass("d-none");
            $("#long_area").addClass("d-none");
        });
    </script>
    <script>
        google.maps.event.addDomListener(window, 'load', initialize);

        function initialize() {
            var input = document.getElementById('searchInput');
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace();
                $('#lat').val(place.geometry['location'].lat());
                $('#lng').val(place.geometry['location'].lng());
                $("#lat_area").removeClass("d-none");
                $("#long_area").removeClass("d-none");
            });

            var restaurants = <?php print_r(json_encode($restaurants)) ?>;

            var mymap = new GMaps({
                el: '.map',
                lat: 6.9157,
                lng: 79.8559,
                zoom: 13
            });

            $.each( restaurants, function (index, value) {
                var marker =  mymap.addMarker({
                    map: mymap,
                    position: new google.maps.LatLng( value.lat, value.lng),
                    icon:  "http://maps.google.com/mapfiles/kml/shapes/dining.png",
                    click: function (e) {
                        infowindows.open(mymap, marker);
                    }
                });
                var infowindows = new google.maps.InfoWindow({
                    maxWidth: 300,
                    minHeight: 235,
                    content: mapModelDetils(value)
                });

            });


        }

    </script>
@endsection
