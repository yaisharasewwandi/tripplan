@extends('frontend.layouts.master')
@section('content')
    <!--  wrapper  --> @include('frontend.layouts.includes.profile-nav')
    <!-- section  -->
    <section class="middle-padding" >
        <div class="container">
            <div class="dasboard-wrap fl-wrap">
                <!-- dashboard-content-->
                <div class="dashboard-content fl-wrap">
                    <div class="row">
                        @include('frontend.layouts.includes.sidebar')
                        <div class="col-md-8">
                            <!--   list-single-main-item -->
                            <div class="list-single-main-item fl-wrap" id="faq5">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>Listing</h3>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat .</p>
                            </div>
                            <!--   list-single-main-item end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="limit-box"></div>
    </section>

@endsection