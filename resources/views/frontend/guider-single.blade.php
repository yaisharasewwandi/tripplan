@extends('frontend.layouts.master')
@section('content')
    <div id="wrapper">
        <!-- content-->
        <div class="content">
            <!--  section  -->
            <section class="grey-blue-bg small-padding scroll-nav-container">
                <div class="top-dec"></div>
                <!--  scroll-nav-wrapper  -->
                <div class="scroll-nav-wrapper fl-wrap">
                    <div class="hidden-map-container fl-wrap">
                        <input id="pac-input" class="controls fl-wrap controls-mapwn" type="text" placeholder="What Nearby ?   Bar , Gym , Restaurant ">
                        <div class="map-container">
                            <div id="singleMap" data-latitude="40.7427837" data-longitude="-73.11445617675781"></div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="container">
                        <nav class="scroll-nav scroll-init">
                            <ul>
                                <li><a class="act-scrlink" href="#sec1">Gallery</a></li>
                                <li><a href="#sec2">Details</a></li>
                                <li><a href="#sec3">Travel Places</a></li>
{{--                                <li><a href="#sec4">Rooms</a></li>--}}
                                <li><a href="#sec5">Reviews</a></li>
                            </ul>
                        </nav>
                        {{--                    <a href="#" class="show-hidden-map">  <span>On The Map</span> <i class="fal fa-map-marked-alt"></i></a>--}}
                    </div>
                </div>
                <!--  scroll-nav-wrapper end  -->
                <!--   container  -->
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="list-single-main-container ">
                                <!-- fixed-scroll-column  -->
                                <div class="fixed-scroll-column">
                                    <div class="fixed-scroll-column-item fl-wrap">
                                        <div class="showshare sfcs fc-button"><i class="far fa-share-alt"></i><span>Share </span></div>
                                        <div class="share-holder fixed-scroll-column-share-container">
                                            <div class="share-container  isShare"></div>
                                        </div>
                                        <a class="fc-button custom-scroll-link" href="#sec6"><i class="far fa-comment-alt-check"></i> <span>  Add review </span></a>
                                        <a class="fc-button" href="#"><i class="far fa-heart"></i> <span>Save</span></a>
{{--                                        <a class="fc-button" href="booking-single.html"><i class="far fa-bookmark"></i> <span> Book Now </span></a>--}}
                                    </div>
                                </div>
                                <!-- fixed-scroll-column end   -->
                                <div class="list-single-main-media fl-wrap" id="sec1">
                                    <div class="single-slider-wrapper fl-wrap">
                                        <div class="slider-for fl-wrap">

                                                <div class="slick-slide-item"><img src="{{asset($guiders->featured_image)}}" alt=""></div>
                                                <div class="slick-slide-item"><img src="{{asset($guiders->banner_image)}}" alt=""></div>

                                        </div>
                                        <div class="swiper-button-prev sw-btn"><i class="fal fa-long-arrow-left"></i></div>
                                        <div class="swiper-button-next sw-btn"><i class="fal fa-long-arrow-right"></i></div>
                                    </div>
                                    <div class="single-slider-wrapper fl-wrap">
                                        <div class="slider-nav fl-wrap">
{{--                                                <div class="slick-slide-item"><img src="{{asset($guiders->featured_image)}}" alt=""></div>--}}
{{--                                                <div class="slick-slide-item"><img src="{{asset($guiders->banner_image)}}" alt=""></div>--}}

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <!--  flat-hero-container -->
                            <div class="flat-hero-container fl-wrap">
                                <div class="box-widget-item-header fl-wrap ">
                                    <h3>{{$guiders->title}}</h3>
                                    <div class="listing-rating-wrap">
                                        <div class="listing-rating card-popup-rainingvis" data-starrating2="{{$guiders->rating}}"></div>
                                    </div>
                                </div>
{{--                                <div class="list-single-hero-price fl-wrap">Per Day Price<span> {{number_format($guiders->price,2)}} LKR</span></div>--}}
                                <!--reviews-score-wrap-->
                                <div class="reviews-score-wrap fl-wrap">
                                    <div class="rate-class-name-wrap fl-wrap">
                                        <div class="rate-class-name">
                                            <span>{{number_format($guiders->rating,1)}}</span>
                                            <div class="score"><strong>Very Good</strong>{{$guiders->reviewGuider()->count()}} Reviews </div>
                                        </div>
                                        <a href="#sec6" class="color-bg  custom-scroll-link">Add Review <i class="far fa-comment-alt-check"></i></a>
                                    </div>
                                    <div class="review-score-detail">
                                        <!-- review-score-detail-list-->
                                        <div class="review-score-detail-list">
                                            <!-- rate item-->
                                            <div class="rate-item fl-wrap">
                                                <div class="rate-item-title fl-wrap"><span>Safety</span></div>
                                                <div class="rate-item-bg" data-percent="{{(number_format($safety,2)/5)*100}}%">
                                                    <div class="rate-item-line color-bg"></div>
                                                </div>
                                                <div class="rate-item-percent">{{number_format($safety,2)}}</div>
                                            </div>
                                            <!-- rate item end-->
                                            <!-- rate item-->
                                            <div class="rate-item fl-wrap">
                                                <div class="rate-item-title fl-wrap"><span>Communication</span></div>
                                                <div class="rate-item-bg" data-percent="{{(number_format($communication,2)/5)*100}}%">
                                                    <div class="rate-item-line color-bg"></div>
                                                </div>
                                                <div class="rate-item-percent">{{number_format($communication,2)}}</div>
                                            </div>
                                            <!-- rate item end-->
                                            <!-- rate item-->
                                            <div class="rate-item fl-wrap">
                                                <div class="rate-item-title fl-wrap"><span>Care</span></div>
                                                <div class="rate-item-bg" data-percent="{{(number_format($care,2)/5)*100}}%">
                                                    <div class="rate-item-line color-bg"></div>
                                                </div>
                                                <div class="rate-item-percent">{{number_format($care,2)}}</div>
                                            </div>
                                            <!-- rate item end-->
                                            <!-- rate item-->
                                            <div class="rate-item fl-wrap">
                                                <div class="rate-item-title fl-wrap"><span>Confidence</span></div>
                                                <div class="rate-item-bg" data-percent="{{(number_format($confidence,2)/5)*100}}%">
                                                    <div class="rate-item-line color-bg"></div>
                                                </div>
                                                <div class="rate-item-percent">{{number_format($confidence,2)}}</div>
                                            </div>
                                            <!-- rate item end-->
                                        </div>
                                        <!-- review-score-detail-list end-->
                                    </div>
                                </div>
                                <!-- reviews-score-wrap end -->

                            </div>
                            <!--   flat-hero-container end -->
                        </div>
                    </div>
                    <!--   row  -->
                    <div class="row">
                        <!--   datails -->
                        <div class="col-md-8">
                            <div class="list-single-main-container ">
                                <!-- list-single-header end -->

                                <!--   list-single-main-item -->
                                <div class="list-single-main-item fl-wrap" id="sec2">
                                    <div class="list-single-main-item-title fl-wrap">
                                        <h3>About Guider </h3>
                                    </div>
                                    <P>{!! $guiders->description !!}</P>

                                </div>
                                <!--   list-single-main-item end -->
                                <!--   list-single-main-item -->
                                <div class="list-single-main-item fl-wrap" id="sec3">
                                    <div class="list-single-main-item-title fl-wrap">
                                        <h3>Travel Places</h3>
                                    </div>
                                    <div class="rooms-container fl-wrap">
                                        <!--  rooms-item -->
                                        @foreach($guiders->travelDetails as $dtails)
                                            <div class="rooms-item fl-wrap">
                                                <div class="rooms-media">
                                                    @foreach($dtails->images as $img)
                                                    <img src="{{asset($img->image_name)}}" alt="">
                                                        @break
                                                    @endforeach
                                                    <div class="dynamic-gal more-photos-button" data-dynamicPath="[@foreach($dtails->images as $key=>$images){'src': '{{asset($images->image_name)}}'}, @endforeach]">  View Gallery <span> photos</span> <i class="far fa-long-arrow-right"></i></div>

                                                </div>
                                                <div class="rooms-details">
                                                    <div class="rooms-details-header fl-wrap">
                                                        <span class="rooms-price">{{$dtails->price}} LKR<strong> / Per day</strong></span>
                                                        <h3>{{$dtails->name}}</h3>
                                                        <h5><span></span></h5>
                                                    </div>
                                                    <p>{!! Illuminate\Support\Str::limit(strip_tags($dtails->description), 200, $end='...') !!}</p>
                                                    <div class="facilities-list fl-wrap">

                                                        {{--                                                            <a href="" class="btn color-bg ajax-link">Details<i class="fas fa-caret-right"></i></a>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                </div>
                                <!--   list-single-main-item end -->

                                <!-- list-single-main-item -->
                                <div class="list-single-main-item fl-wrap" id="sec5">
                                    <div class="list-single-main-item-title fl-wrap">
                                        <h3>Item Reviews -  <span> {{$guiders->reviewGuider()->count()}} </span></h3>
                                    </div>
                                    <div class="reviews-score-wrap fl-wrap">
                                        <div class="review-score-total">
                                                    <span>
                                                    {{number_format($guiders->rating,1)}}
                                                    <strong>Very Good</strong>
                                                    </span>
                                            <a href="#" class="color2-bg">Add Review</a>
                                        </div>
                                        <div class="review-score-detail">
                                            <!-- review-score-detail-list-->
                                            <div class="review-score-detail-list">
                                                <!-- rate item-->
                                                <div class="rate-item fl-wrap">
                                                    <div class="rate-item-title fl-wrap"><span>Safety</span></div>
                                                    <div class="rate-item-bg" data-percent="{{(number_format($safety,2)/5)*100}}%">
                                                        <div class="rate-item-line color-bg"></div>
                                                    </div>
                                                    <div class="rate-item-percent">{{number_format($safety,2)}}</div>
                                                </div>
                                                <!-- rate item end-->
                                                <!-- rate item-->
                                                <div class="rate-item fl-wrap">
                                                    <div class="rate-item-title fl-wrap"><span>Communication</span></div>
                                                    <div class="rate-item-bg" data-percent="{{(number_format($communication,2)/5)*100}}%">
                                                        <div class="rate-item-line color-bg"></div>
                                                    </div>
                                                    <div class="rate-item-percent">{{number_format($communication,2)}}</div>
                                                </div>
                                                <!-- rate item end-->
                                                <!-- rate item-->
                                                <div class="rate-item fl-wrap">
                                                    <div class="rate-item-title fl-wrap"><span>Care</span></div>
                                                    <div class="rate-item-bg" data-percent="{{(number_format($care,2)/5)*100}}%">
                                                        <div class="rate-item-line color-bg"></div>
                                                    </div>
                                                    <div class="rate-item-percent">{{number_format($care,2)}}</div>
                                                </div>
                                                <!-- rate item end-->
                                                <!-- rate item-->
                                                <div class="rate-item fl-wrap">
                                                    <div class="rate-item-title fl-wrap"><span>Confidence</span></div>
                                                    <div class="rate-item-bg" data-percent="{{(number_format($confidence,2)/5)*100}}%">
                                                        <div class="rate-item-line color-bg"></div>
                                                    </div>
                                                    <div class="rate-item-percent">{{number_format($confidence,2)}}</div>
                                                </div>
                                                <!-- rate item end-->
                                            </div>
                                            <!-- review-score-detail-list end-->
                                        </div>
                                    </div>
                                    <div class="reviews-comments-wrap">
                                        <!-- reviews-comments-item -->
                                        @if(!empty($review))
                                            @foreach($review as $reviews)
                                                <div class="reviews-comments-item">
                                                    <div class="review-comments-avatar">
                                                        @if(!empty($reviews->user->image_path))
                                                            <img src="{{asset($reviews->user->image_path)}}" alt="">
                                                        @else
                                                            <img src="{{asset('frontend/images/user.png')}}" alt="">
                                                        @endif
                                                    </div>
                                                    <div class="reviews-comments-item-text">
                                                        <h4><a href="#">{{$reviews->user->name}}</a></h4>
                                                        <div class="review-score-user">
                                                            <span>{{$reviews->sub_avg}}</span>
                                                            <strong>Good</strong>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <p>" {{$reviews->comment}} "</p>
                                                        <div class="reviews-comments-item-date"><span><i class="far fa-calendar-check"></i>{{$reviews->created_at->format('y/m/d')}}</span></div>
                                                    </div>
                                                </div>
                                        @endforeach
                                    @endif
                                    {{$review->links()}}
                                    <!--reviews-comments-item end-->

                                    </div>
                                </div>
                                <!-- list-single-main-item end -->
                                <!-- list-single-main-item -->
                                <div class="list-single-main-item fl-wrap" id="sec6">
                                    <div class="list-single-main-item-title fl-wrap">
                                        <h3>Add Review</h3>
                                    </div>

                                    <!-- Add Review Box -->
                                    <div id="add-review" class="add-review-box">
                                        <!-- Review Comment -->
                                        <form id="guid-review" class="add-comment  custom-form" name="rangeCalc" >
                                            <fieldset>
                                                <div class="review-score-form fl-wrap">
                                                    <div class="review-range-container">
                                                        <!-- review-range-item-->
                                                        <div class="review-range-item">
                                                            <div class="range-slider-title">Safety</div>
                                                            <div class="range-slider-wrap ">
                                                                <input type="hidden" id="id" value="{{$guiders->id}}">
                                                                <input type="text" class="rate-range" data-min="0" data-max="5" id="safety"  name="rgcl"  data-step="1" value="4">
                                                            </div>
                                                        </div>
                                                        <!-- review-range-item end -->
                                                        <!-- review-range-item-->
                                                        <div class="review-range-item">
                                                            <div class="range-slider-title">Communication</div>
                                                            <div class="range-slider-wrap ">
                                                                <input type="text" class="rate-range" data-min="0" data-max="5" id="communication"  name="rgcl"  data-step="1"  value="1">
                                                            </div>
                                                        </div>
                                                        <!-- review-range-item end -->
                                                        <!-- review-range-item-->
                                                        <div class="review-range-item">
                                                            <div class="range-slider-title">Care</div>
                                                            <div class="range-slider-wrap ">
                                                                <input type="text" class="rate-range" data-min="0" data-max="5" id="care"  name="rgcl"  data-step="1" value="5" >
                                                            </div>
                                                        </div>
                                                        <!-- review-range-item end -->
                                                        <!-- review-range-item-->
                                                        <div class="review-range-item">
                                                            <div class="range-slider-title">Confidence</div>
                                                            <div class="range-slider-wrap">
                                                                <input type="text" class="rate-range" id="confidence" data-min="0" data-max="5"  name="rgcl"  data-step="1" value="3">
                                                            </div>
                                                        </div>
                                                        <!-- review-range-item end -->
                                                    </div>
                                                    <div class="review-total">
                                                        <span><input type="text" name="rg_total" value="" id="score" data-form="AVG({rgcl})" value="0"></span>
                                                        <strong>Your Score</strong>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label><i class="fal fa-user"></i></label>
                                                        <input type="text" placeholder="Your Name *" id="name" required value=""/>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label><i class="fal fa-envelope"></i>  </label>
                                                        <input type="text" placeholder="Email Address*" id="email" required value=""/>
                                                    </div>
                                                </div>
                                                <textarea cols="40" rows="3" id="comment" required placeholder="Your Review:"></textarea>
                                            </fieldset>
                                            @if (Auth::user() )
                                                <button type="submit" class="btn  big-btn flat-btn float-btn color2-bg" style="margin-top:30px">Submit Review <i class="fal fa-paper-plane"></i></button>
                                            @else
                                                <button type="button" class="btn  big-btn flat-btn float-btn color2-bg modal-open" style="margin-top:30px">Submit Review <i class="fal fa-paper-plane"></i></button>
                                            @endif
                                        </form>
                                    </div>
                                    <!-- Add Review Box / End -->
                                </div>
                                <!-- list-single-main-item end -->
                            </div>
                        </div>
                        <!--   datails end  -->
                        <!--   sidebar  -->
                        <div class="col-md-4">
                            <!--box-widget-wrap -->
                            <div class="box-widget-wrap">
                                <!--box-widget-item -->

                                <!--box-widget-item end -->
                                <!--box-widget-item -->
                                <div class="box-widget-item fl-wrap">
                                    <div class="box-widget">
                                        <div class="box-widget-content">
                                            <div class="box-widget-item-header">
                                                <h3> Contact Information</h3>
                                            </div>
                                            <div class="box-widget-list">
                                                <ul>
                                                    <li><span><i class="fal fa-map-marker"></i> Adress :</span> <a href="#">{{$guiders->address}}</a></li>
                                                    <li><span><i class="fal fa-phone"></i> Phone :</span> <a href="#">{{$guiders->user->contact_number}}</a></li>
                                                    <li><span><i class="fal fa-envelope"></i> Mail :</span> <a href="#">{{$guiders->user->email}}</a></li>
                                                    <li><span><i class="fal fa-browser"></i> Website :</span> <a href="#">{{$guiders->website_link}}</a></li>
                                                </ul>
                                            </div>
                                            <div class="list-widget-social">
                                                <ul>
                                                    <li><a href="{{$guiders->fb_page_link}}" target="_blank" ><i class="fab fa-facebook-f"></i></a></li>
                                                    <li><a href="{{$guiders->instergram_link}}" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                                    <li><a href="{{$guiders->twiter_link}}" target="_blank" ><i class="fab fa-instagram"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--box-widget-item end -->


                            </div>
                            <!--box-widget-wrap end -->
                        </div>
                        <!--   sidebar end  -->
                    </div>
                    <!--   row end  -->
                </div>
                <!--   container  end  -->
            </section>
            <!--  section  end-->
        </div>
        <!-- content end-->
        <div class="limit-box fl-wrap"></div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">

        jQuery(document).ready(function($) {

            $('#guid-review').on('submit', function (e){
                e.preventDefault();
                var id = $('#id').val();
                var safety = $('#safety').val();
                var communication = $('#communication').val();
                var care = $('#care').val();
                var confidence = $('#confidence').val();
                var score = $('#score').val();
                var name = $('#name').val();
                var email = $('#email').val();
                var comment = $('#comment').val();


                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    type: "POST",
                    dataType: "json",
                    url: '/guid-review',
                    data: {'id': id,'safety': safety,'communication': communication,'care': care,'confidence':confidence,'score':score,'name':name,'email':email,'comment':comment},
                    success: function(data){
                        location.reload();
                    }
                });
            })
        })

    </script>
@endsection

