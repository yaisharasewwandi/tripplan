@extends('frontend.layouts.master')

@section('content')
    <style>
        .input-controls{
            top: 10px !important;
        }
    </style>
<div id="wrapper">
                <!-- content-->
                <div class="content">
                    <!--section -->
                    <!-- Map -->
                    <div class="map-container  fw-map big_map hid-mob-map" >
                        <form method="post" action="{{route('home-search-result')}}">
                            @csrf
                         <input id="searchInput" class="input-controls fl-wrap controls-mapwn"  type="text"  placeholder="Enter a location">
                        <input type="hidden" name="address" id="location">
                        <input type="hidden" name="lat" id="lat">
                        <input type="hidden" name="lng" id="lng">
                        <button type="submit" class=" " style="position: absolute;
    background: #F9B90F  right no-repeat;
    box-shadow: 0 2px 4px 0 rgba(0,0,0,.1);
    width: 80px;
    z-index: 1000;
    border: none;
    padding: 20px;
    border-radius: 4px;margin: 8px 0px 1px -300px;cursor: pointer;">Search</button>
                        </form>
                        <div class="map" id="map" style="width: 100%; height: 300px;"></div>



                        <div class="map-close"><i class="fas fa-times"></i></div>

                    </div>
                    <!-- Map end -->


                    <!--section -->

                    <!-- section end -->
                    <!--section -->
                    <section id="sec2">
                        <div class="container">
                                <div class="section-title">
                                    <div class="section-title-separator"><span></span></div>
                                    <h2>Popular Destination</h2>
                                    <span class="section-separator"></span>
                                    <p>Explore some of the best tips from around the city from our partners and friends.</p>
                                </div>
                            </div>
                            <!-- portfolio start -->
                            <div class="gallery-items fl-wrap mr-bot spad home-grid">
                                <!-- gallery-item-->
                                @foreach($districts as $key => $district)

                                <div class="gallery-item @if($key == 1)gallery-item-second @endif"  >
                                    <div class="grid-item-holder">
                                        <div class="listing-item-grid">
                                            <div class="listing-counter" style="margin-right: 10px;"><span>{{$district->accommodations()->count()}} </span> Hotels</div>
                                            <div  style="position: absolute;left: 120px;top: 40px;color: #fff;z-index: 10;font-size: 11px;border-radius: 4px;background: #18458B;color: #fff;padding: 9px 12px; "><span>{{$district->locations()->count()}} </span> Locations</div>

                                            <img  src="{{asset($district->image)}}"   alt="">
                                            <div class="listing-item-cat">
                                                <h3 style="color: #ffffff">{{$district->name_en}}</h3>
                                                <div class="weather-grid"   data-grcity="Rome"></div>
                                                <div class="clearfix"></div>

                                                <p>{!! Illuminate\Support\Str::limit($district->description,50) !!}</p>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                @endforeach
                                <!-- gallery-item end-->

                            </div>
                            <!-- portfolio end -->
{{--                            <a href="{{route('districts-list')}}" class="btn    color-bg">Explore All Cities<i class="fas fa-caret-right"></i></a>--}}

                    </section>
                    <!-- section end -->
                    <!-- section-->
                    <section class="grey-blue-bg">
                        <!-- container-->
                        <div class="container">
                            <div class="section-title">
                                <div class="section-title-separator"><span></span></div>
                                <h2>Recently Added Hotels</h2>
                                <span class="section-separator"></span>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar.</p>
                            </div>
                        </div>
                        <!-- container end-->
                        <!-- carousel -->
                        <div class="list-carousel fl-wrap card-listing ">
                            <!--listing-carousel-->
                            <div class="listing-carousel  fl-wrap ">
                                <!--slick-slide-item-->
                                @foreach($accommodations as $accommodation)
                                <div class="slick-slide-item">
                                    {{--<!-- listing-item  -->{{$accommodation->facilities['name']}}--}}
                                    <div class="listing-item">
                                        <article class="geodir-category-listing fl-wrap">
                                            <div class="geodir-category-img">
                                                @foreach($accommodation->images as $image)
                                                <a href="{{route('listing-single',$accommodation->id)}}"><img src="{{asset($image['accommodation_image'])}}" alt=""></a>
                                                @endforeach
                                                <!-- <div class="listing-avatar"><a href="author-single.html"><img src="{{asset('frontend/images/avatar/1.jpg')}}" alt=""></a>
                                                    <span class="avatar-tooltip">Added By  <strong>Alisa Noory</strong></span>
                                                </div> -->

                                                <div class="geodir-category-opt">
                                                    <div class="listing-rating card-popup-rainingvis" data-starrating2=" {{$accommodation->rating}}
                                                            "></div>
                                                    <div class="rate-class-name">
                                                        <div class="score"><strong>Very Good</strong>{{$accommodation->reviewAccommodations()->ReviewMsg()->count()}} Reviews </div>
                                                        <span>{{$accommodation->rating}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="geodir-category-content fl-wrap title-sin_item">
                                                <div class="geodir-category-content-title fl-wrap" style="height: 60px;">
                                                    <div class="geodir-category-content-title-item">
                                                        <h3 class="title-sin_map"><a href="{{route('listing-single',$accommodation->id)}}">{{$accommodation->title}}</a></h3>
                                                        <div class="geodir-category-location fl-wrap"><a href="#" class="map-item"><i class="fas fa-map-marker-alt"></i>{{$accommodation->location}} </a></div>
                                                    </div>
                                                </div>


                                                <p>{!! Illuminate\Support\Str::limit(strip_tags($accommodation->description), 100, $end='...') !!}  </p>

                                                <ul class="facilities-list fl-wrap">
                                                    @foreach($accommodation->facilities as $facility)
                                                    <li><i class="{{$facility->icon}}"></i><span>{{$facility->name}}</span></li>
                                                    @endforeach

                                                </ul>

                                                <div class="geodir-category-footer fl-wrap">
                                                    <div class="geodir-category-price">Awg/Night<br> <span>{{$accommodation->price}} LKR</span></div>
                                                    <div class="geodir-opt-list">
                                                        <a href="#"  class="single-map-item" data-newlatitude="{{$accommodation->lat}}" data-newlongitude="{{$accommodation->lng}}"><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip">On the map</span></a>
                                                        <a  class="geodir-js-favorite wishlist" data-id="{{$accommodation->id}}"><i class="fal fa-heart"></i><span class="geodir-opt-tooltip">Save</span></a>
                                                        <a href="{{route('direction-search',$accommodation->id)}}"   class="geodir-js-booking "><i class="fal fa-exchange"></i><span class="geodir-opt-tooltip">Find Directions</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                    <!-- listing-item end -->
                                </div>
                                @endforeach
                                <!--slick-slide-item end-->
                                <!--slick-slide-item-->

                              </div>
                            </div>
                            <!--listing-carousel end-->
                            <div class="swiper-button-prev sw-btn"><i class="fa fa-long-arrow-left"></i></div>
                            <div class="swiper-button-next sw-btn"><i class="fa fa-long-arrow-right"></i></div>

                        <!--  carousel end-->
                    </section>
                    <!-- section end -->
                    <!--section -->
                    <section class="parallax-section" data-scrollax-parent="true">
                        <div class="bg"  data-bg="{{asset('frontend/images/bg/2.jpg')}}" data-scrollax="properties: { translateY: '100px' }"></div>
                        <div class="overlay op7"></div>
                        <!--container-->
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="colomn-text fl-wrap pad-top-column-text_small">
                                        <div class="colomn-text-title">
                                            <h3>Most Popular Hotels</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar.</p>
                                            <a href="{{route('listing')}}" class="btn  color2-bg float-btn">View All Hotels<i class="fas fa-caret-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <!--light-carousel-wrap-->
                                    <div class="light-carousel-wrap fl-wrap">
                                        <!--light-carousel-->
                                        <div class="light-carousel">
                                            <!--slick-slide-item-->
                                            @foreach($populorAccommodations as $populorAccommodation)
                                            <div class="slick-slide-item">
                                                <div class="hotel-card fl-wrap title-sin_item">
                                                    <div class="geodir-category-img card-post">
                                                        @foreach($populorAccommodation->images as $image)
                                                        <a href="{{route('listing-single',$populorAccommodation->id)}}"><img src="{{asset($image['accommodation_image'])}}" alt=""></a>
                                                        @endforeach
                                                        <div class="listing-counter">Awg/Night <strong>{{$populorAccommodation->price}} LKR</strong></div>
                                                        {{--<div class="sale-window">Sale 20%</div>--}}
                                                        <div class="geodir-category-opt">
                                                            <div class="listing-rating card-popup-rainingvis" data-starrating2="{{$populorAccommodation->rating}}"></div>
                                                            <h4 class="title-sin_map"><a href="{{route('listing-single',$populorAccommodation->id)}}">{{$populorAccommodation->title}}</a></h4>
                                                            <div class="geodir-category-location"><a href="#" class="single-map-item" data-newlatitude="40.90261483" data-newlongitude="-74.15737152"><i class="fas fa-map-marker-alt"></i> {{$populorAccommodation->location}}</a></div>
                                                            <div class="rate-class-name">
                                                                <div class="score"><strong> Good</strong>{{$populorAccommodation->reviewAccommodations()->ReviewMsg()->count()}} Reviews </div>
                                                                <span>{{$populorAccommodation->rating}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach                                            <!--slick-slide-item end-->

                                        </div>
                                        <!--light-carousel end-->
                                        <div class="fc-cont  lc-prev"><i class="fal fa-angle-left"></i></div>
                                        <div class="fc-cont  lc-next"><i class="fal fa-angle-right"></i></div>
                                    </div>
                                    <!--light-carousel-wrap end-->
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- section end -->
                    <!--section -->
                    <section>
                        <div class="container">
                            <div class="section-title">
                                <div class="section-title-separator"><span></span></div>
                                <h2>Why Choose Us</h2>
                                <span class="section-separator"></span>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar.</p>
                            </div>
                            <!-- -->
                            <div class="row">
                                <div class="col-md-4">
                                    <!-- process-item-->
                                    <div class="process-item big-pad-pr-item">
                                        <span class="process-count"> </span>
                                        <div class="time-line-icon"><i class="fal fa-headset"></i></div>
                                        <h4><a href="#"> Best service guarantee</a></h4>
                                        <p>Proin dapibus nisl ornare diam varius tempus. Aenean a quam luctus, finibus tellus ut, convallis eros sollicitudin turpis.</p>
                                    </div>
                                    <!-- process-item end -->
                                </div>
                                <div class="col-md-4">
                                    <!-- process-item-->
                                    <div class="process-item big-pad-pr-item">
                                        <span class="process-count"> </span>
                                        <div class="time-line-icon"><i class="fal fa-gift"></i></div>
                                        <h4> <a href="#">Exclusive gifts</a></h4>
                                        <p>Proin dapibus nisl ornare diam varius tempus. Aenean a quam luctus, finibus tellus ut, convallis eros sollicitudin turpis.</p>
                                    </div>
                                    <!-- process-item end -->
                                </div>
                                <div class="col-md-4">
                                    <!-- process-item-->
                                    <div class="process-item big-pad-pr-item nodecpre">
                                        <span class="process-count"> </span>
                                        <div class="time-line-icon"><i class="fal fa-credit-card"></i></div>
                                        <h4><a href="#"> Get more from your card</a></h4>
                                        <p>Proin dapibus nisl ornare diam varius tempus. Aenean a quam luctus, finibus tellus ut, convallis eros sollicitudin turpis.</p>
                                    </div>
                                    <!-- process-item end -->
                                </div>
                            </div>
                            <!--process-wrap   end-->
                            <div class=" single-facts fl-wrap mar-top">
                                <!-- inline-facts -->
                                <div class="inline-facts-wrap">
                                    <div class="inline-facts">
                                        <i class="fal fa-users"></i>
                                        <div class="milestone-counter">
                                            <div class="stats animaper">
                                                <div class="num" data-content="0" data-num="254">154</div>
                                            </div>
                                        </div>
                                        <h6>New Visiters Every Week</h6>
                                    </div>
                                </div>
                                <!-- inline-facts end -->
                                <!-- inline-facts  -->
                                <div class="inline-facts-wrap">
                                    <div class="inline-facts">
                                        <i class="fal fa-thumbs-up"></i>
                                        <div class="milestone-counter">
                                            <div class="stats animaper">
                                                <div class="num" data-content="0" data-num="12168">12168</div>
                                            </div>
                                        </div>
                                        <h6>Happy customers every year</h6>
                                    </div>
                                </div>
                                <!-- inline-facts end -->
                                <!-- inline-facts  -->
                                <div class="inline-facts-wrap">
                                    <div class="inline-facts">
                                        <i class="fal fa-award"></i>
                                        <div class="milestone-counter">
                                            <div class="stats animaper">
                                                <div class="num" data-content="0" data-num="172">172</div>
                                            </div>
                                        </div>
                                        <h6>Won Awards</h6>
                                    </div>
                                </div>
                                <!-- inline-facts end -->
                                <!-- inline-facts  -->
                                <div class="inline-facts-wrap">
                                    <div class="inline-facts">
                                        <i class="fal fa-hotel"></i>
                                        <div class="milestone-counter">
                                            <div class="stats animaper">
                                                <div class="num" data-content="0" data-num="732">732</div>
                                            </div>
                                        </div>
                                        <h6>New Listing Every Week</h6>
                                    </div>
                                </div>
                                <!-- inline-facts end -->
                            </div>
                        </div>
                    </section>
                    <!-- section end -->


                    <!--section -->
                    <section class=" middle-padding">
                        <div class="container">
                            <div class="section-title">
                                <div class="section-title-separator"><span></span></div>
                                <h2>Tips & Blogs</h2>
                                <span class="section-separator"></span>
                                <p>Browse the latest articles from our blog.</p>
                            </div>
                            <div class="row home-posts">
                                @foreach($blogs as $key=>$blog)

                                <div class="col-md-4">
                                    <article class="card-post">
                                        <div class="card-post-img fl-wrap">
                                            <a href="{{route('blog_single',$blog->id)}}"><img  src="{{asset($blog->image)}}"   alt=""></a>
                                        </div>
                                        <div class="card-post-content fl-wrap">
                                            <h3><a href="blog-single.html">{{$blog->title}}.</a></h3>
                                            <p>{{ Illuminate\Support\Str::limit($blog->description, 100, $end='...') }} </p>
                                             <div class="post-author" style="margin-bottom: 10px;">@if(!empty($blog->users['image']))<a href="#"><img src="{{asset('frontend/images/avatar/avatar-bg')}}" alt="">@else<a href="#"><img src="{{asset($blog->users['image_path'])}}" alt=""> @endif<span>By {{$blog->users['name']}}</span></a></div>
                                            {{--<div class="post-opt">--}}
                                                {{--<ul>--}}
                                                    {{--<li><i class="fal fa-calendar"></i> <span>{{asset($blog->created_at)}}</span></li>--}}
                                                    {{--<li><i class="fal fa-eye"></i> <span>264</span></li>--}}
                                                    {{--<li><i class="fal fa-tags"></i> <a href="#">Design</a>  </li>--}}
                                                {{--</ul>--}}
                                            {{--</div>--}}
                                        </div>
                                    </article>
                                </div>

                                @endforeach
                            </div>
                            <a href="{{route('blog_list')}}" class="btn    color-bg ">Read All Blogs<i class="fas fa-caret-right"></i></a>
                        </div>
                        <div class="section-decor"></div>
                    </section>

                    <section class="grey-blue-bg">
                        <!-- container-->
                        <div class="container">
                            <div class="section-title">
                                <div class="section-title-separator"><span></span></div>
                                <h2>Recently Added Restaurants</h2>
                                <span class="section-separator"></span>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar.</p>
                            </div>
                        </div>
                        <!-- container end-->
                        <!-- carousel -->
                        <div class="list-carousel fl-wrap card-listing ">
                            <!--listing-carousel-->
                            <div class="listing-carousel  fl-wrap ">
                                <!--slick-slide-item-->
                                @foreach($restaurants as $restaurant)
                                    <div class="slick-slide-item">
                                        {{--<!-- listing-item  -->{{$accommodation->facilities['name']}}--}}
                                        <div class="listing-item">
                                            <article class="geodir-category-listing fl-wrap">
                                                <div class="geodir-category-img">
                                                    @foreach($restaurant->images as $image)
                                                        <a href="{{route('restaurant-single',$restaurant->id)}}"><img src="{{asset($image['restaurant_image'])}}" alt=""></a>
                                                @endforeach
                                                <!-- <div class="listing-avatar"><a href="author-single.html"><img src="{{asset('frontend/images/avatar/1.jpg')}}" alt=""></a>
                                                    <span class="avatar-tooltip">Added By  <strong>Alisa Noory</strong></span>
                                                </div> -->

                                                    <div class="geodir-category-opt">
                                                        <div class="listing-rating card-popup-rainingvis" data-starrating2=" {{number_format($restaurant->rating,1)}}"></div>
                                                        <div class="rate-class-name">
                                                            <div class="score"><strong>Very Good</strong>{{$restaurant->reviewRestaurant()->ReviewMsg()->count()}} Reviews </div>
                                                            <span>{{number_format($restaurant->rating,1)}}
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="geodir-category-content fl-wrap title-sin_item">
                                                    <div class="geodir-category-content-title fl-wrap" style="height: 60px">
                                                        <div class="geodir-category-content-title-item">
                                                            <h3 class="title-sin_map"><a href="{{route('restaurant-single',$restaurant->id)}}">{{$restaurant->title}}</a></h3>
                                                            <div class="geodir-category-location fl-wrap"><a href="#" class="map-item"><i class="fas fa-map-marker-alt"></i>{{$restaurant->location}} </a></div>
                                                        </div>
                                                    </div>


                                                    <p>{!! Illuminate\Support\Str::limit(strip_tags($restaurant->description), 100, $end='...') !!}  </p>

                                                    <div class="geodir-category-footer fl-wrap" style="height: 60px">
                                                        <div class="geodir-category-price">Awg/Night<br> <span>{{$restaurant->price}} LKR</span></div>
                                                        <div class="geodir-opt-list">
                                                            <a href="#" class="single-map-item" data-newlatitude="{{$restaurant->lat}}" data-newlongitude="{{$restaurant->lng}}"><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip">On the map</span></a>
                                                            <a  class="geodir-js-favorite wishlist" data-id="{{$restaurant->id}}"><i class="fal fa-heart"></i><span class="geodir-opt-tooltip">Save</span></a>
                                                            <a href="{{route('directionres-search',$restaurant->id)}}" class="geodir-js-booking"><i class="fal fa-exchange"></i><span class="geodir-opt-tooltip">Find Directions</span></a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </article>
                                        </div>
                                        <!-- listing-item end -->
                                    </div>
                            @endforeach
                            <!--slick-slide-item end-->
                                <!--slick-slide-item-->

                            </div>
                        </div>
                        <!--listing-carousel end-->
                        <div class="swiper-button-prev sw-btn"><i class="fa fa-long-arrow-left"></i></div>
                        <div class="swiper-button-next sw-btn"><i class="fa fa-long-arrow-right"></i></div>

                        <!--  carousel end-->
                    </section>

                    <section class=" middle-padding">
                        <div class="container">
                            <div class="section-title">
                                <div class="section-title-separator"><span></span></div>
                                <h2>Recently Added Tours</h2>
                                <span class="section-separator"></span>
                                <p>Browse the latest articles from our Tours.</p>
                            </div>
                        </div>
                            <div class="list-carousel fl-wrap card-listing ">
                                <!--listing-carousel-->
                                <div class="listing-carousel  fl-wrap ">
                                    <!--slick-slide-item-->
                                    @foreach($travelagencies as $travelagency)
                                        <div class="slick-slide-item">
                                            {{--<!-- listing-item  -->{{$accommodation->facilities['name']}}--}}
                                            <div class="listing-item">
                                                <article class="geodir-category-listing fl-wrap" style="border: 1px solid #c7c4c4;">
                                                    <div class="geodir-category-img">
                                                            <a href="{{route('agency-single',$travelagency->id)}}"><img src="{{asset($travelagency->featured_image)}}" alt=""></a>


                                                        <div class="geodir-category-opt">
                                                            <div class="listing-rating card-popup-rainingvis" data-starrating2="{{number_format($travelagency->rating,1)}}"></div>
                                                            <div class="rate-class-name">
                                                                <div class="score"><strong>Very Good</strong>{{$travelagency->reviewAgancy()->ReviewMsg()->count()}} Reviews </div>
                                                                <span>{{number_format($travelagency->rating,1)}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="geodir-category-content fl-wrap title-sin_item">
                                                        <div class="geodir-category-content-title fl-wrap" >
                                                            <div class="geodir-category-content-title-item">
                                                                <h3 class="title-sin_map"><a href="{{route('agency-single',$travelagency->id)}}">{{$travelagency->title}}</a></h3>
                                                                <div class="geodir-category-location fl-wrap"><a href="#" class="map-item"><i class="fas fa-map-marker-alt"></i>{{$travelagency->location}} </a></div>
                                                            </div>
                                                        </div>


                                                        <p>{!! Illuminate\Support\Str::limit(strip_tags($travelagency->description), 100, $end='...') !!}  </p>
                                                        <div class="geodir-category-footer fl-wrap" style="height: 60px">
                                                            <div class="geodir-category-price">Awg/Night<br> <span>{{$travelagency->price}} LKR</span></div>
                                                            <div class="geodir-opt-list">
                                                                <a href="#" class="single-map-item" data-newlatitude="{{$travelagency->lat}}" data-newlongitude="{{$travelagency->lng}}"><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip">On the map</span></a>
                                                                <a  class="geodir-js-favorite wishlist" data-id="{{$travelagency->id}}"><i class="fal fa-heart"></i><span class="geodir-opt-tooltip">Save</span></a>
                                                                <a href="{{route('directiontour-search',$travelagency->id)}}" class="geodir-js-booking"><i class="fal fa-exchange"></i><span class="geodir-opt-tooltip">Find Directions</span></a>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </article>
                                            </div>
                                            <!-- listing-item end -->
                                        </div>
                                @endforeach
                                <!--slick-slide-item end-->
                                    <!--slick-slide-item-->

                                </div>
                            </div>
                            <!--listing-carousel end-->
                            <div class="swiper-button-prev sw-btn"><i class="fa fa-long-arrow-left"></i></div>
                            <div class="swiper-button-next sw-btn"><i class="fa fa-long-arrow-right"></i></div>

                            <div class="section-decor"></div>

                    </section>
                </div>
                <!-- content end-->

@endsection
    @section('script')

        <script>
   $('.wishlist').click(function(){
       var id = $(this).attr("data-id");
       $.ajax({
            type: "GET",
            dataType: "json",
            url: '{{ route('wislist-save') }}',
            data: {'id': id},
            success: function (data) {
            toastr.success('Save successfully');
            updatewishlist();
        },
        error: function (msg) {
            toastr.error( 'Inconceivable!');
        }
        });
   })
   function updatewishlist(){

    $.ajax({
    type: 'GET',
    url: '{{route("wishlist")}}',
    success: function (data) {
    $('.favorites').empty();
    $('.favorites').append(data);
    }
    });
   }

    </script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA6V8pVX1JyORSc4CwPzeBn2YK3su0Bu0A&libraries=places&callback=initMap"></script>


        <script>
            function singleMap() {
                var markerIcon = {
                    url: '{{asset('frontend/images/marker2.png')}}',
                }
                var myLatLng = {
                    lng: $('#singleMap').data('longitude'),
                    lat: $('#singleMap').data('latitude'),
                };
                var single_map = new google.maps.Map(document.getElementById('singleMap'), {
                    zoom: 14,
                    center: myLatLng,
                    scrollwheel: false,
                    zoomControl: false,
                    fullscreenControl: true,
                    mapTypeControl: false,
                    scaleControl: false,
                    panControl: false,
                    navigationControl: false,
                    streetViewControl: true,
                    styles: [{
                        "featureType": "landscape",
                        "elementType": "all",
                        "stylers": [{
                            "color": "#f2f2f2"
                        }]
                    }]
                });
                var marker = new google.maps.Marker({
                    position: myLatLng,
                    map: single_map,
                    icon: markerIcon,
                    title: 'Location'
                });
                if ($(".controls-mapwn").length) {

                    var input = document.getElementById('pac-input');
                    var searchBox = new google.maps.places.SearchBox(input);
                    single_map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                    single_map.addListener('bounds_changed', function () {
                        searchBox.setBounds(single_map.getBounds());
                    });
                    var markers = [];
                    searchBox.addListener('places_changed', function () {
                        var places = searchBox.getPlaces();

                        if (places.length == 0) {
                            return;
                        }
                        markers.forEach(function (marker) {
                            marker.setMap(null);
                        });
                        markers = [];
                        var bounds = new google.maps.LatLngBounds();
                        places.forEach(function (place) {

                            if (!place.geometry) {
                                console.log("Returned place contains no geometry");
                                return;
                            }
                            var icon = {
                                url: place.icon,
                                size: new google.maps.Size(71, 71),
                                origin: new google.maps.Point(0, 0),
                                anchor: new google.maps.Point(17, 34),
                                scaledSize: new google.maps.Size(25, 25)
                            };

                            // Create a marker for each place.
                            markers.push(new google.maps.Marker({
                                map: single_map,
                                icon: icon,
                                title: place.name,
                                position: place.geometry.location
                            }));

                            if (place.geometry.viewport) {
                                // Only geocodes have viewport.
                                bounds.union(place.geometry.viewport);
                            } else {
                                bounds.extend(place.geometry.location);
                            }
                        });
                        single_map.fitBounds(bounds);
                    });
                }
                var zoomControlDiv = document.createElement('div');
                var zoomControl = new ZoomControl(zoomControlDiv, single_map);
                function ZoomControl(controlDiv, single_map) {
                    zoomControlDiv.index = 1;
                    single_map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(zoomControlDiv);
                    controlDiv.style.padding = '5px';
                    var controlWrapper = document.createElement('div');
                    controlDiv.appendChild(controlWrapper);
                    var zoomInButton = document.createElement('div');
                    zoomInButton.className = "mapzoom-in";
                    controlWrapper.appendChild(zoomInButton);
                    var zoomOutButton = document.createElement('div');
                    zoomOutButton.className = "mapzoom-out";
                    controlWrapper.appendChild(zoomOutButton);
                    google.maps.event.addDomListener(zoomInButton, 'click', function () {
                        single_map.setZoom(single_map.getZoom() + 1);
                    });
                    google.maps.event.addDomListener(zoomOutButton, 'click', function () {
                        single_map.setZoom(single_map.getZoom() - 1);
                    });
                }
                $(".single-map-item").on("click", function (e) {
                    e.preventDefault();
                    google.maps.event.trigger(single_map, 'resize');
                    $(".map-modal-wrap").fadeIn(400);
                    var $that = $(this),
                        newln = $that.data("newlatitude"),
                        newlg = $that.data("newlongitude"),
                        newtitle = $that.parents(".title-sin_item").find(".title-sin_map a").text(),
                        newurl = $that.parents(".title-sin_item").find(".title-sin_map a").attr('href');
                    var latlng = new google.maps.LatLng(newln, newlg);
                    marker.setPosition(latlng);
                    single_map.panTo(latlng);
                    $(".map-modal-container h3 a").text(newtitle).attr("href", newurl);
                });
                $(".map-modal-close , .map-modal-wrap-overlay").on("click", function (e) {
                    $(".map-modal-wrap").fadeOut(400);
                    single_map.setZoom(14);
                    single_map.getStreetView().setVisible(false);
                    $('#pac-input').val('');
                    markers.forEach(function (marker) {
                        marker.setMap(null);
                    });
                    markers = [];
                });
            }
            var single_map = document.getElementById('singleMap');
            if (typeof (single_map) != 'undefined' && single_map != null) {
                google.maps.event.addDomListener(window, 'load', singleMap);
            }
        </script>


        <script>
            /* script */
            function initialize() {
                var latlng = new google.maps.LatLng(6.927079,79.861244);
                var map = new google.maps.Map(document.getElementById('map'), {
                    center: latlng,
                    zoom: 13,
                    gestureHandling: 'cooperative'
                });
                var marker = new google.maps.Marker({
                    map: map,
                    position: latlng,
                    draggable: true,
                    anchorPoint: new google.maps.Point(0, -29)
                });
                var input = document.getElementById('searchInput');
                map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                var geocoder = new google.maps.Geocoder();
                var autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.bindTo('bounds', map);
                var infowindow = new google.maps.InfoWindow();
                autocomplete.addListener('place_changed', function() {
                    infowindow.close();
                    marker.setVisible(false);
                    var place = autocomplete.getPlace();
                    if (!place.geometry) {
                        window.alert("Autocomplete's returned place contains no geometry");
                        return;
                    }

                    // If the place has a geometry, then present it on a map.
                    if (place.geometry.viewport) {
                        map.fitBounds(place.geometry.viewport);
                    } else {
                        map.setCenter(place.geometry.location);
                        map.setZoom(17);
                    }

                    marker.setPosition(place.geometry.location);
                    marker.setVisible(true);

                    bindDataToForm(place.formatted_address,place.geometry.location.lat(),place.geometry.location.lng());
                    infowindow.setContent(place.formatted_address);
                    infowindow.open(map, marker);

                });
                // this function will work on marker move event into map
                google.maps.event.addListener(marker, 'dragend', function() {
                    geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng());
                                infowindow.setContent(results[0].formatted_address);
                                infowindow.open(map, marker);
                            }
                        }
                    });
                });
            }
            function bindDataToForm(address,lat,lng){
                document.getElementById('location').value = address;
                document.getElementById('lat').value = lat;
                document.getElementById('lng').value = lng;
            }
            google.maps.event.addDomListener(window, 'load', initialize);
        </script>





@endsection
