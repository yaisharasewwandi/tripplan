@extends('frontend.layouts.master')
@section('content')
    <!--  wrapper  -->
    <div id="wrapper">
        <!-- content-->
        <div class="content">
            <!-- Map -->
            <div class="map-container  fw-map big_map hid-mob-map" >
                <form method="get" action="{{route('listing')}}">
                    <input id="searchInput" class="input-controls fl-wrap controls-mapwn"  type="text"  placeholder="Enter a location">
                    <input type="hidden" name="address" id="location">
                    <input type="hidden" name="lat" id="lat">
                    <input type="hidden" name="lng" id="lng">
                    <button type="submit" class=" " style="position: absolute;
    background: #F9B90F  right no-repeat;
    box-shadow: 0 2px 4px 0 rgba(0,0,0,.1);
    width: 80px;
    z-index: 1000;
    border: none;
    padding: 20px;
    border-radius: 4px;margin: 0px 0px 1px -300px;cursor: pointer;">Search</button>
                </form>
                <div class="map" id="map" style="width: 100%; height: 300px;"></div>



                <div class="map-close"><i class="fas fa-times"></i></div>

            </div>
            <!-- Map end -->
            <div class="breadcrumbs-fs fl-wrap">
                <div class="container">
                    <div class="breadcrumbs fl-wrap"><a href="#">Home</a><a href="#">Listing </a><span>Fullwidth Map</span></div>
                </div>
            </div>
            <section class="grey-blue-bg small-padding">
                <div class="container">
                    <div class="row">
                        <!--filter sidebar -->
                        <div class="col-md-4">
                            <div class="mobile-list-controls fl-wrap">
                                <div class="mlc show-hidden-column-map schm"><i class="fal fa-map-marked-alt"></i> Show Map</div>
                                <div class="mlc show-list-wrap-search"><i class="fal fa-filter"></i> Filter</div>
                            </div>
                            <div class="fl-wrap filter-sidebar_item fixed-bar">
                                <div class="filter-sidebar fl-wrap lws_mobile">
                                    <!--col-list-search-input-item -->
                                    <div class="col-list-search-input-item in-loc-dec fl-wrap not-vis-arrow">
                                        <label>City/Category</label>
                                        <div class="listsearch-input-item">
                                            <select data-placeholder="City" class="chosen-select" >
                                                <option>All Cities</option>

                                            </select>
                                        </div>
                                    </div>
                                    <!--col-list-search-input-item end-->
                                    <!--col-list-search-input-item -->
                                    <div class="col-list-search-input-item fl-wrap location autocomplete-container">
                                        <label>Destination</label>
                                        <span class="header-search-input-item-icon"><i class="fal fa-map-marker-alt"></i></span>
                                        <input type="text" placeholder="Destination or Hotel Name" class="autocomplete-input" id="autocompleteid3" value=""/>
                                        <a href="#"><i class="fal fa-dot-circle"></i></a>
                                    </div>
                                    <!--col-list-search-input-item end-->
                                    <!--col-list-search-input-item -->
                                {{--                              a--}}
                                <!--col-list-search-input-item end-->
                                    <!--col-list-search-input-item -->
                                    <div class="col-list-search-input-item fl-wrap">
                                        <div class="quantity-item">
                                            <label>Rooms</label>
                                            <div class="quantity">
                                                <input type="number" min="1" max="3" step="1" value="1">
                                            </div>
                                        </div>
                                        <div class="quantity-item">
                                            <label>Adults</label>
                                            <div class="quantity">
                                                <input type="number" min="1" max="5" step="1" value="1">
                                            </div>
                                        </div>
                                        <div class="quantity-item">
                                            <label>Children</label>
                                            <div class="quantity">
                                                <input type="number" min="0" max="3" step="1" value="0">
                                            </div>
                                        </div>
                                    </div>
                                    <!--col-list-search-input-item end-->
                                    <!--col-list-search-input-item -->
                                    <div class="col-list-search-input-item fl-wrap">
                                        <div class="range-slider-title">Price range</div>
                                        <div class="range-slider-wrap fl-wrap">
                                            <input class="range-slider" data-from="300" data-to="1200" data-step="50" data-min="50" data-max="2000" data-prefix="$">
                                        </div>
                                    </div>
                                    <!--col-list-search-input-item end-->


                                    <!--col-list-search-input-item end-->
                                    <!--col-list-search-input-item  -->
                                    <div class="col-list-search-input-item fl-wrap">
                                        <button class="header-search-button" onclick="window.location.href='listing.html'">Search <i class="far fa-search"></i></button>
                                    </div>
                                    <!--col-list-search-input-item end-->
                                </div>
                            </div>
                        </div>
                        <!--filter sidebar end-->
                        <!--listing -->
                        <div class="col-md-8">
                            <!--col-list-wrap -->
                            <div class="col-list-wrap fw-col-list-wrap post-container">
                                <!-- list-main-wrap-->
                                <div class="list-main-wrap fl-wrap card-listing">
                                    <!-- list-main-wrap-opt-->
                                    <div class="list-main-wrap-opt fl-wrap">
                                        <div class="list-main-wrap-title fl-wrap col-title">
                                            <h2>Results For : <span> </span></h2>
                                        </div>
                                        <!-- price-opt-->
                                        <div class="price-opt">
                                            <span class="price-opt-title">Sort results by:</span>
                                            <div class="listsearch-input-item">
                                                <select data-placeholder="Popularity" class="chosen-select no-search-select" >
                                                    <option>Popularity</option>
                                                    <option>Average rating</option>
                                                    <option>Price: low to high</option>
                                                    <option>Price: high to low</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- price-opt end-->
                                        <!-- price-opt-->
                                        <div class="grid-opt">
                                            <ul>
                                                <li><span class="two-col-grid act-grid-opt"><i class="fas fa-th-large"></i></span></li>
                                                <li><span class="one-col-grid"><i class="fas fa-bars"></i></span></li>
                                            </ul>
                                        </div>
                                        <!-- price-opt end-->
                                    </div>
                                    <!-- list-main-wrap-opt end-->
                                    <!-- listing-item-container -->
                                    <div class="listing-item-container init-grid-items fl-wrap">

                                        <!-- listing-item  -->
                                        @foreach($accommodations as $accommodation)
                                            <div class="listing-item">
                                                <article class="geodir-category-listing fl-wrap">
                                                    <div class="geodir-category-img">
                                                        @foreach($accommodation->images as $image)
                                                            <a href="{{route('listing-single',$accommodation->id)}}"><img src="{{asset($image['accommodation_image'])}}" alt=""></a>

                                                        @endforeach

                                                        <div class="listing-avatar"><a href=""></a>
                                                            <span class="avatar-tooltip">Added By  <strong>Alisa Noory</strong></span>
                                                        </div>
                                                        {{--<div class="sale-window">Sale 20%</div>--}}
                                                        <div class="geodir-category-opt">
                                                            <div class="listing-rating card-popup-rainingvis" data-starrating2="{{number_format($accommodation->rating,2)}}"></div>
                                                            <div class="rate-class-name">
                                                                <div class="score"><strong>Very Good</strong>{{$accommodation->reviewAccommodations()->count()}} Reviews </div>
                                                                <span>{{number_format($accommodation->rating,2)}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="geodir-category-content fl-wrap">
                                                        <div class="geodir-category-content-title fl-wrap">
                                                            <div class="geodir-category-content-title-item">
                                                                <h3 class="title-sin_map"><a href="{{route('listing-single',$accommodation->id)}}">{{$accommodation->title}}</a></h3>
                                                                <div class="geodir-category-location fl-wrap"><a href="#0" class="map-item"><i class="fas fa-map-marker-alt"></i> {{$accommodation->location}}</a></div>
                                                            </div>
                                                        </div>
                                                        <p>{!! Illuminate\Support\Str::limit(strip_tags($accommodation->description), 100, $end='...') !!}  </p>

                                                        <ul class="facilities-list fl-wrap">
                                                            @foreach($accommodation->facilities as $facility)
                                                                <li><i class="{{$facility->icon}}"></i><span>{{$facility->name}}</span></li>
                                                            @endforeach
                                                        </ul>
                                                        <div class="geodir-category-footer fl-wrap">
                                                            <div class="geodir-category-price">Awg/Night <br> <span>{{$accommodation->price}} LKR</span></div>
                                                            <div class="geodir-opt-list">
                                                                <a href="#"  class="single-map-item" data-newlatitude="{{$accommodation->lat}}" data-newlongitude="{{$accommodation->lng}}"><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip">On the map</span></a>
                                                                <a  class="geodir-js-favorite wishlist" data-id="{{$accommodation->id}}"><i class="fal fa-heart"></i><span class="geodir-opt-tooltip">Save</span></a>
                                                                <a href="{{route('direction-search',$accommodation->id)}}"   class="geodir-js-booking "><i class="fal fa-exchange"></i><span class="geodir-opt-tooltip">Find Directions</span></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                    @endforeach
                                    <!-- listing-item end -->

                                        <!-- listing-item end -->
                                    </div>
                                    <!-- listing-item-container end-->
                                    {{$accommodations->links()}}
                                </div>
                                <!-- list-main-wrap end-->
                            </div>
                            <!--col-list-wrap end -->
                        </div>
                        <!--listing  end-->
                    </div>
                    <!--row end-->
                </div>
                <div class="limit-box fl-wrap"></div>
            </section>
        </div>
        <!-- content end-->
    </div>
@endsection
@section('script')

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA6V8pVX1JyORSc4CwPzeBn2YK3su0Bu0A&libraries=places&callback=initMap"></script>
    <script>
        /* script */
        function initialize() {
            var latlng = new google.maps.LatLng(6.927079,79.861244);
            var map = new google.maps.Map(document.getElementById('map'), {
                center: latlng,
                zoom: 13,
                gestureHandling: 'cooperative'
            });
            var marker = new google.maps.Marker({
                map: map,
                position: latlng,
                draggable: true,
                anchorPoint: new google.maps.Point(0, -29)
            });
            var input = document.getElementById('searchInput');
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
            var geocoder = new google.maps.Geocoder();
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);
            var infowindow = new google.maps.InfoWindow();
            autocomplete.addListener('place_changed', function() {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    window.alert("Autocomplete's returned place contains no geometry");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);
                }

                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                bindDataToForm(place.formatted_address,place.geometry.location.lat(),place.geometry.location.lng());
                infowindow.setContent(place.formatted_address);
                infowindow.open(map, marker);

            });
            // this function will work on marker move event into map
            google.maps.event.addListener(marker, 'dragend', function() {
                geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng());
                            infowindow.setContent(results[0].formatted_address);
                            infowindow.open(map, marker);
                        }
                    }
                });
            });
        }
        function bindDataToForm(address,lat,lng){
            document.getElementById('location').value = address;
            document.getElementById('lat').value = lat;
            document.getElementById('lng').value = lng;
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>


@endsection
