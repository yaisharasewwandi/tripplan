@extends('frontend.layouts.master')
@section('content')
<!--  wrapper  -->
<div id="wrapper">
    <!-- content-->
    <div class="content">
        <!-- Map -->
        <div class="map-container  fw-map big_map hid-mob-map" >
            <form method="get" action="{{route('listing')}}">
                <input id="searchInput" class="input-controls fl-wrap controls-mapwn"  type="text" style="margin-left: -600px"  placeholder="Enter a location">
                <input type="hidden" name="address" id="location">
                <input type="hidden" name="lat" id="lat">
                <input type="hidden" name="lng" id="lng">
                <button type="submit" class=" " style="position: absolute;
    background: #F9B90F  right no-repeat;
    box-shadow: 0 2px 4px 0 rgba(0,0,0,.1);
    width: 80px;
    z-index: 1000;
    border: none;
    padding: 20px;
    border-radius: 4px;margin: 0px 0px 1px -248px;cursor: pointer;">Search</button>
            </form>
            <div class="map" id="map" style="width: 100%; height: 300px;"></div>



            <div class="map-close"><i class="fas fa-times"></i></div>

        </div>
        <!-- Map end -->
        <div class="breadcrumbs-fs fl-wrap">
            <div class="container">
                <div class="breadcrumbs fl-wrap"><a href="#">Home</a><a href="#">Listing </a><span>Fullwidth Map</span></div>
            </div>
        </div>
        <section class="grey-blue-bg small-padding">
            <div class="container">
                <div class="row">
                    <!--filter sidebar -->
                    <div class="col-md-4">
                        <div class="mobile-list-controls fl-wrap">
                            <div class="mlc show-hidden-column-map schm"><i class="fal fa-map-marked-alt"></i> Show Map</div>
                            <div class="mlc show-list-wrap-search"><i class="fal fa-filter"></i> Filter</div>
                        </div>
                        <div class="fl-wrap filter-sidebar_item fixed-bar">
                            <form action="{{route('listing')}}" method="get">
                            <div class="filter-sidebar fl-wrap lws_mobile">
                                <!--col-list-search-input-item -->
                                <div class="col-list-search-input-item in-loc-dec fl-wrap not-vis-arrow">
                                    <label>City/Category</label>
                                    <select data-placeholder="City" name="city" required class="chosen-select" >
                                    <option value="">Select City</option>
                                        @foreach($cities as $key=>$city)
                                            <option value="{{$key}}">{{$city}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!--col-list-search-input-item end-->

                                <!--col-list-search-input-item -->
                                <div class="col-list-search-input-item fl-wrap">
                                    <div class="range-slider-title">Price range</div>
                                    <div class="range-slider-wrap fl-wrap">
                                        <input class="range-slider" name="price" data-from="300" data-to="10000" data-step="50" data-min="50" data-max="90000" data-prefix="lkr">
                                    </div>
                                </div>
                                <!--col-list-search-input-item end-->


                                <!--col-list-search-input-item end-->
                                <!--col-list-search-input-item  -->
                                <div class="col-list-search-input-item fl-wrap">
                                    <button class="header-search-button" type="submit">Search <i class="far fa-search"></i></button>
                                </div>
                                <!--col-list-search-input-item end-->
                            </div>
                            </form>
                        </div>
                    </div>
                    <!--filter sidebar end-->
                    <!--listing -->
                    <div class="col-md-8">
                        <!--col-list-wrap -->
                        <div class="col-list-wrap fw-col-list-wrap post-container">
                            <!-- list-main-wrap-->
                            <div class="list-main-wrap fl-wrap card-listing">
                                <!-- list-main-wrap-opt-->
                                <div class="list-main-wrap-opt fl-wrap">
                                    <div class="list-main-wrap-title fl-wrap col-title">
                                        <h2>Results For : <span> </span></h2>
                                    </div>
                                    <!-- price-opt-->

                                    <!-- price-opt end-->
                                    <!-- price-opt-->
{{--                                    <div class="grid-opt">--}}
{{--                                        <ul>--}}
{{--                                            <li><span class="two-col-grid act-grid-opt"><i class="fas fa-th-large"></i></span></li>--}}
{{--                                            <li><span class="one-col-grid"><i class="fas fa-bars"></i></span></li>--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
                                    <!-- price-opt end-->
                                </div>
                                <!-- list-main-wrap-opt end-->
                                <!-- listing-item-container -->
                                <div class="listing-item-container init-grid-items fl-wrap campaing">
                                @include('frontend.includes.listing-search')
                                </div>
                                <!-- listing-item-container end-->
{{--                                {{$accommodations->links()}}--}}
                            </div>
                            <!-- list-main-wrap end-->
                        </div>
                        <!--col-list-wrap end -->
                    </div>
                    <!--listing  end-->
                </div>
                <!--row end-->
            </div>
            <div class="limit-box fl-wrap"></div>
        </section>
    </div>
    <!-- content end-->
</div>

@endsection
@section('script')
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA6V8pVX1JyORSc4CwPzeBn2YK3su0Bu0A&libraries=places"></script>

{{--    <script src='http://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyA6V8pVX1JyORSc4CwPzeBn2YK3su0Bu0A'></script>--}}
{{--    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA6V8pVX1JyORSc4CwPzeBn2YK3su0Bu0A"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.24/gmaps.js"></script>
    <script>

        {{--$(document).ready(function() {--}}
        {{--    $('select[name="city"]').on('change', function () {--}}
        {{--        var cityID = $(this).val();--}}
        {{--        if (cityID) {--}}
        {{--            $.get("{{ URL::to('listing-list') }}", { city:cityID }, function(returned){--}}
        {{--                $('.campaing').load(location.href + ".campaing");--}}

        {{--            });--}}

        {{--        }--}}
        {{--    })--}}
        {{--})--}}
    </script>
    <script>

        function mapModelDetils(model) {
            return `<div class="map-con">
                <div style='float:right; padding: 10px;'>
                <b>${model.title}</b>
                <br/>${model.address}</div>
                <br/>${model.contact_number}</div>

            </div>`;
        }

    </script>

    <script>

        $(document).ready(function() {
            $("#lat_area").addClass("d-none");
            $("#long_area").addClass("d-none");
        });
    </script>
    <script>
        google.maps.event.addDomListener(window, 'load', initialize);

        function initialize() {
            var input = document.getElementById('searchInput');
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace();
                $('#lat').val(place.geometry['location'].lat());
                $('#lng').val(place.geometry['location'].lng());
                $("#lat_area").removeClass("d-none");
                $("#long_area").removeClass("d-none");
            });

            var accommodation = <?php print_r(json_encode($accommodations)) ?>;

            var mymap = new GMaps({
                el: '.map',
                lat: 6.9157,
                lng: 79.8559,
                zoom: 13
            });

            $.each( accommodation, function (index, value) {
                var marker =  mymap.addMarker({
                    map: mymap,
                    position: new google.maps.LatLng( value.lat, value.lng),
                    icon: "http://maps.google.com/mapfiles/kml/shapes/lodging.png",
                    click: function (e) {
                        infowindows.open(mymap, marker);
                    }
                });
                var infowindows = new google.maps.InfoWindow({
                    maxWidth: 300,
                    minHeight: 235,
                    content: mapModelDetils(value)
                });

            });

        }

    </script>

@endsection
