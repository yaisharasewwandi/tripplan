<!DOCTYPE HTML>
<html lang="en">

<!-- Mirrored from easybook.kwst.net/index.html by HTTrack Website Copier/3.x [XR&CO'2017], Mon, 14 Oct 2019 08:56:43 GMT -->
<head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        <title>Tour Plan</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
        <!--=============== css  ===============-->
        @include('frontend.layouts.partials.style')
    @toastr_css
        <!--=============== favicons ===============-->
        <link rel="shortcut icon" href="images/favicon.ico">
    </head>
    <body>
         @include('frontend.layouts.partials.header')
            <!--  wrapper  -->
           @yield('content')
            <!--wrapper end -->
            @include('frontend.layouts.partials.footer')
        <!-- Main end -->

        <!--=============== scripts  ===============-->


        @include('frontend.layouts.partials.javascript')
         @yield('script')
         @toastr_js
         @toastr_render


    </body>

<!-- Mirrored from easybook.kwst.net/index.html by HTTrack Website Copier/3.x [XR&CO'2017], Mon, 14 Oct 2019 09:10:49 GMT -->
</html>
