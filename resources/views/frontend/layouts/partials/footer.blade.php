<!--footer -->
            <footer class="main-footer">

                <!--footer-inner-->

                <!--footer-inner end -->
                <div class="footer-bg">
                </div>
                <!--sub-footer-->
                <div class="sub-footer">
                    <div class="container">
                        <div class="copyright"> &#169; TripPlan 2020 .  All rights reserved.</div>

                        <div class="subfooter-nav">
                            <ul>
                                <li><a href="#">Terms of use</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="{{route('blog_list')}}">Blog</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--sub-footer end -->
            </footer>
            <!--footer end -->
            <!--map-modal -->
            <div class="map-modal-wrap">
                <div class="map-modal-wrap-overlay"></div>
                <div class="map-modal-item">
                    <div class="map-modal-container fl-wrap">
                        <div class="map-modal fl-wrap">
                            <div id="singleMap" data-latitude="40.7" data-longitude="-73.1"></div>
                        </div>
                        <h3><i class="fal fa-location-arrow"></i><a href="#">Hotel Title</a></h3>
                        <input id="pac-input" class="controls fl-wrap controls-mapwn" type="text" placeholder="What Nearby ?   Bar , Gym , Restaurant ">
                        <div class="map-modal-close"><i class="fal fa-times"></i></div>
                    </div>
                </div>
            </div>
            <!--map-modal end -->
            <!--register form -->
            <div class="main-register-wrap modal">
                <div class="reg-overlay"></div>
                <div class="main-register-holder">
                    <div class="main-register fl-wrap">
                        <div class="close-reg color-bg"><i class="fal fa-times"></i></div>
                        <ul class="tabs-menu">
                            <li class="current"><a href="#tab-1"><i class="fal fa-sign-in-alt"></i> Login</a></li>
                            <li><a href="#tab-2"><i class="fal fa-user-plus"></i> Register</a></li>
                        </ul>
                        <!--tabs -->
                        <div id="tabs-container">
                            <div class="tab">
                                <!--tab -->
                                <div id="tab-1" class="tab-content">
                                    <h3>Sign In <span>Trip<strong>Plan</strong></span></h3>
                                    <div class="custom-form">
                                        <form method="post"  name="registerform" action="{{ route('login') }}">
                                             @csrf
                                            <label>Email Address <span>*</span> </label>
                                            <input name="email" type="text"   onClick="this.select()" value="{{ old('email') }}">
                                               @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            <label >Password <span>*</span> </label>
                                            <input name="password" type="password"   onClick="this.select()" value=""  class=" @error('password') is-invalid @enderror">
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            <button type="submit" style="width: 100%" class="log-submit-btn color-bg"><span>Log In</span></button>
                                            <div class="clearfix"></div>
                                            <div class="filter-tags">
                                                <input id="check-a" type="checkbox" name="check">
                                                <label for="check-a">Remember me</label>
                                            </div>
                                        </form>
                                        <div class="lost_password">
                                            <a href="#">Lost Your Password?</a>
                                        </div>
                                    </div>
                                </div>
                                <!--tab end -->
                                <!--tab -->
                                <div class="tab">
                                    <div id="tab-2" class="tab-content">
                                        <h3>Sign Up <span>Trip<strong>Plan</strong></span></h3>
                                        <div class="custom-form">
                                            <form method="post"   name="registerform" class="main-register-form" id="main-register-form2" action="{{ route('register') }}">
                                                  @csrf
                                                <label >Full Name <span>*</span> </label>
                                                <input name="name" type="text"   onClick="this.select()" value="{{ old('name') }}" required>
                                                 @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                                <label>Email Address <span>*</span></label>
                                                <input name="email" type="text"  onClick="this.select()" value="{{ old('email') }}" required >
                                                 @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                                <label >Password <span>*</span></label>
                                                <input name="password" type="password"   onClick="this.select()"  required>
                                                @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                                <label >Confirm Password <span>*</span></label>
                                                <input name="password_confirmation" type="password"   onClick="this.select()" required >
                                                <div class="filter-tags" style="margin: 0px 5px 0px 40px">
                                                    <input id="check-a11" type="radio" value="1" name="ven_gest" required/>
                                                    <label class="label" for="check-a11"><img src="{{asset('frontend/images/user.png')}}" /></label>
                                                    <p>I am a Guest</p>

                                                </div>
                                                <div class="filter-tags" style="margin: 0px 45px 0px 10px;float: right">
                                                    <input id="check-a22" value="2" type="radio" name="ven_gest" required/>
                                                    <label class="label" for="check-a22"><img src="{{asset('frontend/images/money.png')}}" /></label>
                                                    <p>I am a Vendor</p>

                                                </div>
                                                <div id="vendor">
                                                <label >Contact No <span>*</span> </label>
                                                <input name="contact_number" id="contact_number" type="text"   onClick="this.select()" value="{{ old('contact_number') }}" required>
                                                @error('contact_no')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                                <label >Address <span>*</span> </label>
                                                <input name="address" id="address" type="text"   onClick="this.select()" value="{{ old('address') }}" required>
                                                @error('address')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                                    <div class="filter-tags" style="margin: 0px 5px 0px 10px">
                                                        <input id="check-a1" class="role" value="Hotel Owner" type="radio"  name="role" required>
                                                        <label class="label" for="check-a1"><img src="{{asset('frontend/images/hotel.png')}}" /></label>
                                                        <p>Hotel Owner</p>
                                                        {{--<label for="check-a">Hotel Owner</label>--}}
                                                    </div>
                                                    <div class="filter-tags" style="margin: 0px 5px 0px 10px">
                                                        <input id="check-a2" class="role" value="Car Renter" type="radio" name="role" required>
                                                        <label class="label" for="check-a2"><img src="{{asset('frontend/images/car.png')}}" /></label>
                                                        <p>Car Renter</p>
                                                    </div>
                                                    <div class="filter-tags" style="margin: 0px 5px 0px 10px">
                                                        <input id="check-a3" class="role" value="Restaurant Owner" type="radio" name="role" required>
                                                        <label class="label" for="check-a3"><img src="{{asset('frontend/images/coffee-shop.png')}}" /></label>
                                                        <p>Restaurant Owner</p>

                                                    </div>
                                                    <div class="filter-tags" style="margin: 0px 5px 0px 10px">
                                                        <input id="check-a4" class="role" value="Agency Owner" type="radio" name="role" required>
                                                        <label class="label" for="check-a4"><img src="{{asset('frontend/images/travel-agency.png')}}" /></label>
                                                        <p>Agency Owner</p>
                                                    </div>
                                                    <div class="filter-tags" style="margin: 0px 5px 0px 10px">
                                                        <input id="check-a5" class="role" value="Guider" type="radio" name="role" required>
                                                        <label class="label" for="check-a5"><img src="{{asset('frontend/images/guide.png')}}" /></label>
                                                        <p>Guider</p>

                                                    </div>
                                                </div><br><br><br>
                                                <div class="soc-log fl-wrap" >
                                                    <button type="submit" style="width: 100%"  id="send_form"   class="log-submit-btn color-bg" required ><span>Register</span></button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!--tab end -->
                            </div>
                            <!--tabs end -->
{{--                            <div class="log-separator fl-wrap"><span>or</span></div>--}}
{{--                            <div class="soc-log fl-wrap">--}}
{{--                                <p>For faster login or register use your social account.</p>--}}
{{--                                <a href="{{ url('/auth/redirect/facebook') }}" class="facebook-log"><i class="fab fa-facebook-f"></i>Connect with Facebook</a>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
            <!--register form end -->
            <a class="to-top"><i class="fas fa-caret-up"></i></a>
        </div>
<script>



</script>
