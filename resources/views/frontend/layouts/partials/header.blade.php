<div class="loader-wrap">
            <div class="pin">
                <div class="pulse"></div>
            </div>
        </div>
        <!--loader end-->
        <!-- Main  -->
        <div id="main">
            <!-- header-->
            <header class="main-header">
                <!-- header-top-->
                <div class="header-top fl-wrap">
                    <div class="container">
                        <div class="logo-holder">
                            <a href="{{url('')}}"><img src="{{asset('frontend/images/logo.png')}}" alt=""></a>
                        </div>

                        @if (Auth::user() )
                        <a href="{{route('customer-edit-profile')}}" class="add-hotel">Edit Profile <span><i class="far fa-plus"></i></span></a>
                        <div class="show-reg-form "><i class="fa fa-sign-in"></i>
                       <a  href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form></div>
                        @else
{{--                        <a href="" class="add-hotel modal-open">Add Your Hotel <span><i class="far fa-plus"></i></span></a>--}}
                        <div class="show-reg-form modal-open"><i class="fa fa-sign-in"></i>Sign In</div>
                       @endif
                    </div>
                </div>
                <!-- header-top end-->
                <!-- header-inner-->
                <div class="header-inner fl-wrap" >
                    <div class="container">
{{--                        <div class="show-search-button"><span>Search</span> <i class="fas fa-search"></i> </div>--}}
                        <div class="wishlist-link"><i class="fal fa-heart"></i><span class="wl_counter">{{$count ?? '0'}} </span></div>
                         @if (Auth::user() )
                         <div class="header-user-menu">
                            <div class="header-user-name">
                                @if(!empty(Auth::user()->image_path))
                                <span><img src="{{Auth::user()->image_path}}" alt=""></span>
                                @else
                                <span><img src="{{asset('frontend/images/user.png')}}" alt=""></span>
                                @endif
                                My account
                            </div>
                            <ul>
                                <li><a href="{{route('customer-edit-profile')}}"> Edit profile</a></li>
                                {{--<li><a href="{{route('add-businessstep1')}}"> Add Listing</a></li>--}}
                                <li><a href="{{route('listing-booking')}}">  Bookings  </a></li>
{{--                                <li><a href="{{route('customer-review')}}"> Reviews </a></li>--}}
                                @if (Auth::user()->hasRole(['Admin','Super Admin','Hotel Owner','Car Renter','Restaurant Owner','Agency Owner','Guider']) )
                                    <li style="background: #18458b;border-radius: 5px"><a href="{{route('home')}}"> Swich to Dashboard </a></li>
                                @endif
                            </ul>
                        </div>

                        @else
                         <div class="header-user-menu modal-open">
                            <div class="header-user-name">
                                {{--<span><img src="images/avatar/4.jpg" alt=""></span>--}}
                                My account
                            </div>
                        </div>
                        @endif
                        <div class="home-btn" ><a href="{{url('')}}"><i class="fas fa-home"></i></a></div>
                        <!-- nav-button-wrap-->
                        <div class="nav-button-wrap color-bg">
                            <div class="nav-button">
                                <span></span><span></span><span></span>
                            </div>
                        </div>
                        <!-- nav-button-wrap end-->
                        <!--  navigation -->
                        <div class="nav-holder main-menu">
                            <nav>
                                <ul>
                                    <li >
                                        <a href="{{url('')}}" class="{{ Request::is('/') ? 'act-link' : '' }}">Home </a>

                                    </li>
                                    <li>
                                        <a href="{{url('/listing-list')}}" class="{{ Request::is('/listing-list') ? 'act-link' : '' }}" >Listings</a>

                                    </li>
                                    <li>
                                        <a href="{{route('car-list')}}" class="{{ Request::is('/car-list') ? 'act-link' : '' }}">Cars </a>

                                    </li>
                                    <li>
                                        <a href="{{route('agency-list')}}">Tour</a>
                                    </li>
                                    <li>
                                        <a href="{{route('restaurant-list')}}">Restaurant</a>
                                    </li>
                                    <li>
                                        <a href="{{route('guider-list')}}">Guiders</a>
                                    </li>
                                    <li>
                                        <a href="{{route('blog_list')}}">Blog</a>
                                    </li>

                                </ul>
                            </nav>
                        </div>
                        <!-- navigation  end -->
                        <!-- wishlist-wrap-->

                        <div class="wishlist-wrap scrollbar-inner novis_wishlist " id="favorites">
                            <div class="box-widget-content ">
                                <div class="widget-posts fl-wrap ">
                                    <ul >
                                        <div class="favorites">
                        {{--@include('frontend.wish-list')--}}
                                        </div>

                                    </ul>
                                </div>
                            </div>
                        </div>

                        <!-- wishlist-wrap end-->
                    </div>
                </div>
                <!-- header-inner end-->
                <!-- header-search -->
                <div class="header-search vis-search">
                    <div class="container">
                        <div class="row">
                            <!-- header-search-input-item -->
                            <div class="col-sm-4">
                                <div class="header-search-input-item fl-wrap location autocomplete-container">
                                    <label>Destination or Hotel Name</label>
                                    <span class="header-search-input-item-icon"><i class="fal fa-map-marker-alt"></i></span>
                                    <input type="text" placeholder="Location" class="autocomplete-input" id="autocompleteid" value=""/>
                                    <a href="#"><i class="fal fa-dot-circle"></i></a>
                                </div>
                            </div>
                            <!-- header-search-input-item end -->
                            <!-- header-search-input-item -->
                            <div class="col-sm-3">
                                <div class="header-search-input-item fl-wrap date-parent">
                                    <label>Date In-Out </label>
                                    <span class="header-search-input-item-icon"><i class="fal fa-calendar-check"></i></span>
                                    <input type="text" placeholder="When" name="header-search"   value=""/>
                                </div>
                            </div>
                            <!-- header-search-input-item end -->
                            <!-- header-search-input-item -->
                            <div class="col-sm-3">
                                <div class="header-search-input-item fl-wrap">
                                    <div class="quantity-item">
                                        <label>Rooms</label>
                                        <div class="quantity">
                                            <input type="number" min="1" max="3" step="1" value="1">
                                        </div>
                                    </div>
                                    <div class="quantity-item">
                                        <label>Adults</label>
                                        <div class="quantity">
                                            <input type="number" min="1" max="3" step="1" value="1">
                                        </div>
                                    </div>
                                    <div class="quantity-item">
                                        <label>Children</label>
                                        <div class="quantity">
                                            <input type="number" min="0" max="3" step="1" value="0">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- header-search-input-item end -->
                            <!-- header-search-input-item -->
                            <div class="col-sm-2">
                                <div class="header-search-input-item fl-wrap">
                                    <button class="header-search-button" onclick="window.location.href='listing.html'">Search <i class="far fa-search"></i></button>
                                </div>
                            </div>
                            <!-- header-search-input-item end -->
                        </div>
                    </div>
                    <div class="close-header-search"><i class="fal fa-angle-double-up"></i></div>
                </div>
                <!-- header-search  end -->
            </header>
            <!--  header end -->
