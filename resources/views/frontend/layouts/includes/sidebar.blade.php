<div class="col-md-4">
    <div class="box-widget-item fl-wrap help-bar">
        <div class="box-widget">
            <div class="box-widget-content">
                <div class="box-widget-item-header">
                    <h3> Frequently Asked Question</h3>
                </div>
                <div class="faq-nav fl-wrap">
                    <ul>
                        @if(!empty($accommodations))
                            <li><a class="custom-scroll-link act-faq-link" href="{{route('user-accommodation.index')}}">Hotel</a></li>
                        @endif
                        @if(!empty($restaurants))
                            <li><a class="custom-scroll-link" href="{{route('user-restaurant.index')}}">Restaurant</a></li>
                        @endif
                        @if(!empty($agencies))
                            <li><a class="custom-scroll-link" href="{{route('user-agency.index')}}">Agency</a></li>
                        @endif
                        @if(!empty($guiders))
                            <li><a class="custom-scroll-link" href="{{route('user-guider.index')}}">Guider</a></li>
                        @endif
                    </ul>
                </div>

            </div>
        </div>
    </div>
</div>