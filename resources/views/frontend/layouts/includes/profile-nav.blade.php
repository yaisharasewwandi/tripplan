      <div id="wrapper">
                <!-- content-->
                <div class="content">
                    <!-- section-->
                    <section class="flat-header color-bg adm-header">
                        <div class="wave-bg wave-bg2"></div>
                        <div class="container">
                            <div class="dasboard-wrap fl-wrap">
                                <div class="dasboard-breadcrumbs breadcrumbs"><a href="#">Home</a><a href="#">Dashboard</a><span>Profile page</span></div>
                                <!--dasboard-sidebar-->
                                <div class="dasboard-sidebar">
                                    <div class="dasboard-sidebar-content fl-wrap">
                                        <div class="dasboard-avatar">
                                            <img src="{{Auth::user()->image_path}}" alt="">
                                        </div>
                                        <div class="dasboard-sidebar-item fl-wrap">
                                            <h3>
                                                <span>Welcome </span>
                                                {{Auth::user()->name}}
                                            </h3>
                                        </div>
                                        <button id="hott" class="ed-btn" ><a href="{{route('add-listing')}}" style="color: #ffffff;" >Add Hotel</a></button>

{{--                                            <div class="user-stats fl-wrap">--}}
{{--                                            <ul>--}}


{{--                                                <li>--}}
{{--                                                    Reviews--}}
{{--                                                    <span>9</span>--}}
{{--                                                </li>--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
                                        <a  href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="log-out-btn color-bg">
                                            {{ __('Log Out') }}
                                            <i class="far fa-sign-out"></i>
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>

                                    </div>
                                </div>
                                <!--dasboard-sidebar end-->
                                <!-- dasboard-menu-->
                                <div class="dasboard-menu">
                                    <div class="dasboard-menu-btn color3-bg">Dashboard Menu <i class="fal fa-bars"></i></div>
                                    <ul class="dasboard-menu-wrap">
                                        <li>
                                            <a href="{{route('customer-edit-profile')}}" class="user-profile-act"><i class="far fa-user"></i>Profile</a>
{{--                                            <ul>--}}
{{--                                                <li><a href="dashboard-myprofile.html">Edit profile</a></li>--}}
{{--                                                <li><a href="dashboard-password.html">Change Password</a></li>--}}
{{--                                            </ul>--}}
                                        </li>
{{--                                        <li><a href="dashboard-messages.html"><i class="far fa-envelope"></i> Messages <span>3</span></a></li>--}}
{{--                                        <li>--}}
{{--                                            <a href="dashboard-listing-table.html"><i class="far fa-th-list"></i> My listigs  </a>--}}
{{--                                            <ul>--}}
{{--                                                <li><a href="#">Active</a><span>5</span></li>--}}
{{--                                                <li><a href="#">Pending</a><span>2</span></li>--}}
{{--                                                <li><a href="#">Expire</a><span>3</span></li>--}}
{{--                                            </ul>--}}
{{--                                        </li>--}}
                                        <li><a href="{{route('listing-booking')}}"> <i class="far fa-calendar-check"></i> Bookings <span></span></a></li>
{{--                                        <li><a href="dashboard-review.html"><i class="far fa-comments"></i> Reviews </a></li>--}}
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </section>
                    <!-- section end-->
