@extends('frontend.layouts.master')
@section('content')
<!--  wrapper  -->
{{--             @include('frontend.layouts.includes.profile-nav')--}}
<div id="wrapper">
    <!-- content-->
    <div class="content">
        <!-- section-->
        <section class="flat-header color-bg adm-header">
            <div class="wave-bg wave-bg2"></div>
            <div class="container">
                <div class="dasboard-wrap fl-wrap">
                    <div class="dasboard-breadcrumbs breadcrumbs"><a href="#">Home</a><a href="#">Dashboard</a><span>Profile page</span></div>
                    <!--dasboard-sidebar-->
                    <div class="dasboard-sidebar">
                        <div class="dasboard-sidebar-content fl-wrap">
                            <div class="dasboard-avatar">
                                <img src="{{Auth::user()->image_path}}" alt="">
                            </div>
                            <div class="dasboard-sidebar-item fl-wrap">
                                <h3>
                                    <span>Welcome </span>
                                    {{Auth::user()->name}}
                                </h3>
                            </div>
                            <button id="hott" class="ed-btn" ><a href="{{route('add-listing')}}" style="color: #ffffff;" >Add Hotel</a></button>

                            {{--                                            <div class="user-stats fl-wrap">--}}
                            {{--                                            <ul>--}}


                            {{--                                                <li>--}}
                            {{--                                                    Reviews--}}
                            {{--                                                    <span>9</span>--}}
                            {{--                                                </li>--}}
                            {{--                                            </ul>--}}
                            {{--                                        </div>--}}
                            <a  href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="log-out-btn color-bg">
                                {{ __('Log Out') }}
                                <i class="far fa-sign-out"></i>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>

                        </div>
                    </div>
                    <!--dasboard-sidebar end-->
                    <!-- dasboard-menu-->
                    <div class="dasboard-menu">
                        <div class="dasboard-menu-btn color3-bg">Dashboard Menu <i class="fal fa-bars"></i></div>
                        <ul class="dasboard-menu-wrap">
                            <li>
                                <a href="{{route('customer-edit-profile')}}" class="user-profile-act"><i class="far fa-user"></i>Profile</a>
                                {{--                                            <ul>--}}
                                {{--                                                <li><a href="dashboard-myprofile.html">Edit profile</a></li>--}}
                                {{--                                                <li><a href="dashboard-password.html">Change Password</a></li>--}}
                                {{--                                            </ul>--}}
                            </li>
                            {{--                                        <li><a href="dashboard-messages.html"><i class="far fa-envelope"></i> Messages <span>3</span></a></li>--}}
                            {{--                                        <li>--}}
                            {{--                                            <a href="dashboard-listing-table.html"><i class="far fa-th-list"></i> My listigs  </a>--}}
                            {{--                                            <ul>--}}
                            {{--                                                <li><a href="#">Active</a><span>5</span></li>--}}
                            {{--                                                <li><a href="#">Pending</a><span>2</span></li>--}}
                            {{--                                                <li><a href="#">Expire</a><span>3</span></li>--}}
                            {{--                                            </ul>--}}
                            {{--                                        </li>--}}
                            <li><a href="{{route('listing-booking')}}"> <i class="far fa-calendar-check"></i> Bookings <span></span></a></li>
                            {{--                                        <li><a href="dashboard-review.html"><i class="far fa-comments"></i> Reviews </a></li>--}}
                        </ul>
                    </div>

                </div>
            </div>
        </section>
        <!-- section end-->

                    <!-- section-->
                    <section class="middle-padding">
                        <div class="container">
                            <!--dasboard-wrap-->
                            <div class="dasboard-wrap fl-wrap">
                                <!-- dashboard-content-->
                                <div class="dashboard-content fl-wrap">
                                    <div class="dashboard-list-box fl-wrap">
                                        <div class="dashboard-header fl-wrap">
                                            <h3>Reviews Hotels</h3>
                                        </div>
                                        <div class="reviews-comments-wrap">
                                            <!-- reviews-comments-item -->
                                            @foreach($reviews as $review)
                                            <div class="reviews-comments-item">
                                                <div class="review-comments-avatar">
{{--                                                    <img src="images/avatar/2.jpg" alt=""> --}}
                                                </div>
                                                <div class="reviews-comments-item-text">
                                                    <h4><a href="#">{{$review->accommodation}}</a> </h4>
                                                    <div class="review-score-user">
                                                        <span>4.4</span>
                                                        <strong>Good</strong>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <p>" Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. "</p>
                                                    <div class="reviews-comments-item-date"><span><i class="far fa-calendar-check"></i>12 April 2018</span><a href="#"><i class="fal fa-reply"></i> Reply</a></div>
                                                </div>
                                            </div>
                                            @endforeach
                                            <!--reviews-comments-item end-->
                                            <!-- reviews-comments-item -->
                                            <div class="reviews-comments-item">
                                                <div class="review-comments-avatar">
                                                    <img src="images/avatar/5.jpg" alt="">
                                                </div>
                                                <div class="reviews-comments-item-text">
                                                    <h4><a href="#">Adam Koncy</a> on <a href="listing-single.html" class="reviews-comments-item-link">Premium Plaza Hotel </a></h4>
                                                    <div class="review-score-user">
                                                        <span>4.7</span>
                                                        <strong>Very Good</strong>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <p>" Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc posuere convallis purus non cursus. Cras metus neque, gravida sodales massa ut. "</p>
                                                    <div class="reviews-comments-item-date"><span><i class="far fa-calendar-check"></i>03 December 2017</span><a href="#"><i class="fal fa-reply"></i> Reply</a></div>
                                                </div>
                                            </div>
                                            <!--reviews-comments-item end-->
                                            <!-- reviews-comments-item -->
                                            <div class="reviews-comments-item">
                                                <div class="review-comments-avatar">
                                                    <img src="images/avatar/1.jpg" alt="">
                                                </div>
                                                <div class="reviews-comments-item-text">
                                                    <h4><a href="#">Liza Rose </a>on  <a href="listing-single.html" class="reviews-comments-item-link">Park Central </a></h4>
                                                    <div class="review-score-user">
                                                        <span>4.4</span>
                                                        <strong>Good</strong>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <p>" Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. "</p>
                                                    <div class="reviews-comments-item-date"><span><i class="far fa-calendar-check"></i>12 April 2018</span><a href="#"><i class="fal fa-reply"></i> Reply</a></div>
                                                </div>
                                            </div>
                                            <!--reviews-comments-item end-->
                                            <!-- reviews-comments-item -->
                                            <div class="reviews-comments-item">
                                                <div class="review-comments-avatar">
                                                    <img src="images/avatar/3.jpg" alt="">
                                                </div>
                                                <div class="reviews-comments-item-text">
                                                    <h4><a href="#">Adam Koncy</a> on  <a href="listing-single.html" class="reviews-comments-item-link">Grand Hero Palace </a></h4>
                                                    <div class="review-score-user">
                                                        <span>4.7</span>
                                                        <strong>Very Good</strong>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <p>" Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc posuere convallis purus non cursus. Cras metus neque, gravida sodales massa ut. "</p>
                                                    <div class="reviews-comments-item-date"><span><i class="far fa-calendar-check"></i>03 December 2017</span><a href="#"><i class="fal fa-reply"></i> Reply</a></div>
                                                </div>
                                            </div>
                                            <!--reviews-comments-item end-->
                                        </div>
                                    </div>
                                    <!-- pagination-->
                                    <div class="pagination">
                                        <a href="#" class="prevposts-link"><i class="fa fa-caret-left"></i></a>
                                        <a href="#">1</a>
                                        <a href="#" class="current-page">2</a>
                                        <a href="#">3</a>
                                        <a href="#">4</a>
                                        <a href="#" class="nextposts-link"><i class="fa fa-caret-right"></i></a>
                                    </div>
                                </div>
                                <!-- dashboard-list-box end-->
                            </div>
                            <!-- dasboard-wrap end-->
                        </div>
                    </section>
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!-- content end-->
            </div>
            <!--wrapper end -->
@include('frontend.layouts.includes.script')
@endsection
