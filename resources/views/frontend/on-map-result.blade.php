@extends('frontend.layouts.master')
@section('content')

    <!--  wrapper  -->
    <div id="wrapper">
        <!-- content-->
        <div class="content">
            <!-- Map -->
            <div class="map-container  fw-map big_map hid-mob-map" >
                {{--<input id="searchInput" id="pac-input" class="input-controls fl-wrap controls-mapwn"  type="text"  placeholder="Enter a location">--}}
               <input type="hidden" id="start" value="colombo , sri lanka">
               <input type="hidden" id="end" value="{!! $location !!}">

                <div class="map" id="map" style="width: 100%; height: 300px;"></div>

                <input type="hidden" name="address" id="location">
                <input type="hidden" name="lat" id="lat">
                <input type="hidden" name="lng" id="lng">

                <div class="map-close"><i class="fas fa-times"></i></div>

            </div>
            <!-- Map end -->
            <div class="breadcrumbs-fs fl-wrap">
                <div class="container">
                    <div class="breadcrumbs fl-wrap"><a href="#">Home</a><a href="#">Listing </a><span>Fullwidth Map</span></div>
                </div>
            </div>
            <section class="grey-blue-bg small-padding">
                <div class="container">
                    <div class="row">
                        <!--filter sidebar -->

                        <!--listing -->
                        <div class="col-md-12">
                            <!--col-list-wrap -->
                            <div class="col-list-wrap fw-col-list-wrap post-container">
                                <!-- list-main-wrap-->
                                <div class="list-main-wrap fl-wrap card-listing">
                                    <!-- list-main-wrap-opt-->
                                    <div class="list-main-wrap-opt fl-wrap">
                                        <div class="list-main-wrap-title fl-wrap col-title">
                                            <h2>Results For : <span> </span></h2>
                                        </div>
                                        <!-- price-opt-->

                                        <!-- price-opt end-->
                                        <!-- price-opt-->
                                        <div class="grid-opt">
                                            <ul>
                                                <li><span class="two-col-grid act-grid-opt"><i class="fas fa-th-large"></i></span></li>
                                                <li><span class="one-col-grid"><i class="fas fa-bars"></i></span></li>
                                            </ul>
                                        </div>
                                        <!-- price-opt end-->
                                    </div>
                                    <!-- list-main-wrap-opt end-->
                                    <!-- listing-item-container -->
                                    @if($accommodations->count()>0)
                                    <h1 style="float: left;color: #3AACED;font-size: 20px;margin-top: 30px;">Nearest Hotels</h1>
                                    <div class="listing-item-container init-grid-items fl-wrap">

                                        <!-- listing-item  -->
                                        @foreach($accommodations as $accommodation)
                                            <div class="listing-item">
                                                <article class="geodir-category-listing fl-wrap">
                                                    <div class="geodir-category-img">
                                                        @foreach($accommodation->images as $image)
                                                            <a href="{{route('listing-single',$accommodation->id)}}"><img src="{{asset($image['accommodation_image'])}}" alt=""></a>

                                                        @endforeach


                                                        {{--<div class="sale-window">Sale 20%</div>--}}
                                                        <div class="geodir-category-opt">
                                                            <div class="listing-rating card-popup-rainingvis" data-starrating2="{{$accommodation->rating}}"></div>
                                                            <div class="rate-class-name">
                                                                <div class="score"><strong>Very Good</strong>{{$accommodation->reviewAccommodations()->ReviewMsg()->count()}} Reviews </div>
                                                                <span>{{$accommodation->rating}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="geodir-category-content fl-wrap">
                                                        <div class="geodir-category-content-title fl-wrap">
                                                            <div class="geodir-category-content-title-item">
                                                                <h3 class="title-sin_map"><a href="{{route('listing-single',$accommodation->id)}}">{{$accommodation->title}}</a></h3>
                                                                <div class="geodir-category-location fl-wrap"><a href="#0" class="map-item"><i class="fas fa-map-marker-alt"></i> {{$accommodation->location}}</a></div>
                                                            </div>
                                                        </div>
                                                        <p>{!! Illuminate\Support\Str::limit(strip_tags($accommodation->description), 100, $end='...') !!}  </p>

                                                        <ul class="facilities-list fl-wrap">
                                                            @foreach($accommodation->facilities as $facility)
                                                                <li><i class="{{$facility->icon}}"></i><span>{{$facility->name}}</span></li>
                                                            @endforeach
                                                        </ul>
                                                        <div class="geodir-category-footer fl-wrap">
                                                            <div class="geodir-category-price">Awg/Night <span></span></div>
                                                            <div class="geodir-opt-list">
                                                                <a href="#0" class="map-item"><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip">On the map <strong>1</strong></span></a>
                                                                <a href="#" class="geodir-js-favorite"><i class="fal fa-heart"></i><span class="geodir-opt-tooltip">Save</span></a>
                                                                <a href="{{route('direction-search',$accommodation->id)}}" class="geodir-js-booking"><i class="fal fa-exchange"></i><span class="geodir-opt-tooltip">Find Directions</span></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                    @endforeach
                                    <!-- listing-item end -->
                                    </div>
                                    @endif
                                    @if($restaurants->count()>0)
                                    <h1 style="float: left;color: #3AACED;font-size: 20px;margin-top: 30px;">Nearest Restaurants</h1>
                                    <div class="listing-item-container init-grid-items fl-wrap">

                                        <!-- listing-item  -->
                                        @foreach($restaurants as $restaurant)
                                            <div class="listing-item">
                                                <article class="geodir-category-listing fl-wrap">
                                                    <div class="geodir-category-img">
                                                        @foreach($restaurant->images as $image)
                                                            <a href="{{route('restaurant-single',$restaurant->id)}}"><img src="{{asset($image['restaurant_image'])}}" alt=""></a>

                                                        @endforeach


                                                        {{--<div class="sale-window">Sale 20%</div>--}}
                                                        <div class="geodir-category-opt">
                                                            <div class="listing-rating card-popup-rainingvis" data-starrating2="{{$restaurant->rating}}"></div>
                                                            <div class="rate-class-name">
                                                                <div class="score"><strong>Very Good</strong>{{$restaurant->reviewRestaurant()->ReviewMsg()->count()}} Reviews </div>
                                                                <span>{{$restaurant->rating}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="geodir-category-content fl-wrap">
                                                        <div class="geodir-category-content-title fl-wrap">
                                                            <div class="geodir-category-content-title-item">
                                                                <h3 class="title-sin_map"><a href="{{route('restaurant-single',$restaurant->id)}}">{{$restaurant->title}}</a></h3>
                                                                <div class="geodir-category-location fl-wrap"><a href="#0" class="map-item"><i class="fas fa-map-marker-alt"></i> {{$restaurant->location}}</a></div>
                                                            </div>
                                                        </div>
                                                        <p>{!! Illuminate\Support\Str::limit(strip_tags($restaurant->description), 100, $end='...') !!}  </p>

                                                        {{--<ul class="facilities-list fl-wrap">--}}
                                                            {{--@foreach($accommodation->facilities as $facility)--}}
                                                                {{--<li><i class="{{$facility->icon}}"></i><span>{{$facility->name}}</span></li>--}}
                                                            {{--@endforeach--}}
                                                        {{--</ul>--}}
                                                        <div class="geodir-category-footer fl-wrap">
                                                            <div class="geodir-category-price">Awg/Night <span></span></div>
                                                            <div class="geodir-opt-list">
                                                                <a href="#0" class="map-item"><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip">On the map <strong>1</strong></span></a>
                                                                <a href="#" class="geodir-js-favorite"><i class="fal fa-heart"></i><span class="geodir-opt-tooltip">Save</span></a>
                                                                <a href="{{route('directionres-search',$restaurant->id)}}" class="geodir-js-booking"><i class="fal fa-exchange"></i><span class="geodir-opt-tooltip">Find Directions</span></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                    @endforeach
                                    <!-- listing-item end -->
                                    </div>
                                    @endif
                                    @if($touragencies->count()>0)
                                        <h1 style="float: left;color: #3AACED;font-size: 20px;margin-top: 30px;">Nearest Tour Agencies</h1>
                                        <div class="listing-item-container init-grid-items fl-wrap">

                                            <!-- listing-item  -->
                                            @foreach($touragencies  as $touragency)
                                                <div class="listing-item">
                                                    <article class="geodir-category-listing fl-wrap">
                                                        <div class="geodir-category-img">

                                                                <a href="{{route('restaurant-single',$touragency->id)}}"><img src="{{asset($touragency['featured_image'])}}" alt=""></a>


                                                            {{--<div class="sale-window">Sale 20%</div>--}}
                                                            <div class="geodir-category-opt">
                                                                <div class="listing-rating card-popup-rainingvis" data-starrating2="{{$touragency->rating}}"></div>
                                                                <div class="rate-class-name">
                                                                    <div class="score"><strong>Very Good</strong>{{$touragency->reviewAgancy()->ReviewMsg()->count()}} Reviews </div>
                                                                    <span>{{$touragency->rating}}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="geodir-category-content fl-wrap">
                                                            <div class="geodir-category-content-title fl-wrap">
                                                                <div class="geodir-category-content-title-item">
                                                                    <h3 class="title-sin_map"><a href="{{route('agency-single',$touragency->id)}}">{{$touragency->title}}</a></h3>
                                                                    <div class="geodir-category-location fl-wrap"><a href="#0" class="map-item"><i class="fas fa-map-marker-alt"></i> {{$touragency->location}}</a></div>
                                                                </div>
                                                            </div>
                                                            <p>{!! Illuminate\Support\Str::limit(strip_tags($touragency->description), 100, $end='...') !!}  </p>

                                                            {{--<ul class="facilities-list fl-wrap">--}}
                                                            {{--@foreach($accommodation->facilities as $facility)--}}
                                                            {{--<li><i class="{{$facility->icon}}"></i><span>{{$facility->name}}</span></li>--}}
                                                            {{--@endforeach--}}
                                                            {{--</ul>--}}
                                                            <div class="geodir-category-footer fl-wrap">
                                                                <div class="geodir-category-price">Awg/Night <span></span></div>
                                                                <div class="geodir-opt-list">
                                                                    <a href="#0" class="map-item"><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip">On the map <strong>1</strong></span></a>
                                                                    <a href="#" class="geodir-js-favorite"><i class="fal fa-heart"></i><span class="geodir-opt-tooltip">Save</span></a>
                                                                    <a href="{{route('directiontour-search',$touragency->id)}}" class="geodir-js-booking"><i class="fal fa-exchange"></i><span class="geodir-opt-tooltip">Find Directions</span></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                </div>
                                        @endforeach
                                        <!-- listing-item end -->

                                    </div>
                                    @endif
                                    <!-- listing-item-container end-->
                                    <a class="load-more-button" href="#">Load more <i class="fal fa-spinner"></i> </a>

                                <!-- list-main-wrap end-->
                            </div>
                            <!--col-list-wrap end -->
                        </div>
                        <!--listing  end-->
                    </div>
                    <!--row end-->
                </div>
                <div class="limit-box fl-wrap"></div>
            </section>
        </div>

        <!-- content end-->
    </div>
@endsection
@section('script')

    <script>
        function initMap() {

            const directionsRenderer = new google.maps.DirectionsRenderer();
            const directionsService = new google.maps.DirectionsService();
            const map = new google.maps.Map(document.getElementById('map'), {
                zoom: 7,
                gestureHandling: 'cooperative',
                center: {lat: 6.927079, lng: 79.861244}
            });
            directionsRenderer.setMap(map);
            calculateAndDisplayRoute(directionsService, directionsRenderer);

        }

        function calculateAndDisplayRoute(directionsService, directionsRenderer) {
            const start = document.getElementById("start").value;
            const end = document.getElementById("end").value;
            directionsService.route(
                {
                    origin: start,
                    destination: end,
                    travelMode: google.maps.TravelMode.DRIVING
                },
                (response, status) => {
                    if (status === "OK") {
                        directionsRenderer.setDirections(response);
                    } else {
                        window.alert("Directions request failed due to " + status);
                    }
                }
            );
        }

        function initialize() {

            var map_canvas = document.getElementById('map');
                    @foreach($accommodations as $location)

            var location = new google.maps.LatLng({{ $location->lat }}, {{ $location->lng }});

                    @endforeach
            var map_options = {
                center: location,
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }

            var map = new google.maps.Map(map_canvas, map_options)

            var marker = new google.maps.Marker({
                position: location,
                map: map,
            });

            marker.setMap(map);

        }

        google.maps.event.addDomListener(window, 'load', initialize);

    </script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA6V8pVX1JyORSc4CwPzeBn2YK3su0Bu0A&callback=initMap&libraries=&v=weekly"
            defer
    ></script>

@endsection
