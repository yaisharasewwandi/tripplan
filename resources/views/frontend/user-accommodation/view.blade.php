@extends('frontend.layouts.master')
@section('content')
    <!--  wrapper  --> @include('frontend.layouts.includes.profile-nav')
    <!-- section  -->
    <section class="middle-padding" >
        <div class="container">
            <div class="dasboard-wrap fl-wrap">
                <!-- dashboard-content-->
                <div class="dashboard-content fl-wrap">
                    <div class="row">
                        @include('frontend.layouts.includes.sidebar')
                        <div class="col-md-8">
                            <div class="dashboard-content fl-wrap">
                                <div class="dashboard-list-box fl-wrap">
                                    <div class="dashboard-header fl-wrap">
                                        <h3>Your Hotels / Accommodation</h3>
                                    </div>
                                    <!-- dashboard-list  -->
                                    <div class="dashboard-list">
                                        <div class="dashboard-message">
                                            {{--<span class="new-dashboard-item">New</span>--}}

                                                <div class="box-widget-item fl-wrap">
                                                    <div class="box-widget widget-posts">
                                                        <div class="box-widget-content">
                                                            <div class="box-widget-item-header">
                                                                <h3>Rooms</h3>
                                                            </div>
                                                            @foreach($accommodations->accommodationrooms as $value)
                                                            <div class="box-image-widget">
                                                                @foreach($value->images as $data)
                                                                <div class="box-image-widget-media" style="margin-right: 20px; margin-bottom: 10px; width: 45%"><img src="{{asset($data['room_image_name'])}}" alt="">
                                                                    <a href="{{route('user-accommodation.destroyimage', $data->id)}}" class="color-bg" style="text-align: center;">Delete<i class="fal fa-trash-alt" style="margin-left: 5px;"></i></a>
                                                                </div>
                                                                @endforeach
                                                                <div class="box-image-widget-details">
                                                                    <h4>{{$value->room_name}}</h4>
                                                                    <p>{{$value->roomTypes->details}}</p>
                                                                    <div class="booking-details fl-wrap">
                                                                        <span class="booking-title">Room Type :</span>
                                                                        <span class="booking-text"><a href="#" target="_top">{{$value->roomTypes->name}}</a></span>
                                                                    </div>
                                                                    <div class="booking-details fl-wrap">
                                                                        <span class="booking-title">Room Quantity :</span>
                                                                        <span class="booking-text"><a href="#" target="_top">{{$value->room_qty}}</a></span>
                                                                    </div>
                                                                    <div class="booking-details fl-wrap">
                                                                        <span class="booking-title">Bed Type :</span>
                                                                        <span class="booking-text"><a href="#" target="_top">{{$value->bed_type}}</a></span>
                                                                    </div>
                                                                    <div class="booking-details fl-wrap">
                                                                        <span class="booking-title">Guest Quantity :</span>
                                                                        <span class="booking-text"><a href="#" target="_top">{{$value->guest_qty}}</a></span>
                                                                    </div>
                                                                    <div class="booking-details fl-wrap">
                                                                        <span class="booking-title">Room Size :</span>
                                                                        <span class="booking-text"><a href="#" target="_top">{{$value->room_size}}</a></span>
                                                                    </div>
                                                                    <div class="booking-details fl-wrap">
                                                                        <span class="booking-title">Price Range :</span>
                                                                        <span class="booking-text"><a href="#" target="_top">{{$value->price_range}} LKR</a></span>
                                                                    </div>
                                                                    <ul class="dashboard-listing-table-opt  fl-wrap" >
                                                                        <li style="margin-right: 5px;"><a data-toggle="modal" data-target="#exampleModal{{$value->id}}" >Edit <i class="fal fa-edit"></i></a></li>
                                                                        <li style="margin-right: 5px;"><a href="{{route('user-accommodation.destroyroom', $value->id)}}" class="del-btn">Delete <i class="fal fa-trash-alt"></i></a></li>

                                                                    </ul>
                                                                </div>
                                                            </div>
                                                                <div class="modal fade" id="exampleModal{{$value->id}}" style="top: 90px;" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header dashboard-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                                <h4 class="modal-title" id="exampleModalLabel">Rooms</h4>
                                                                            </div>
                                                                            <form method="post"  action="{{route('user-accommodation.update-rooms',$value->id )}}" enctype="multipart/form-data" >
                                                                                @csrf
                                                                                <div class="modal-body">
                                                                                    <input type="hidden" name="accommodation_id" value="">
                                                                                    <div class="list-single-main-item-title fl-wrap">
                                                                                        <h3>Layout and Pricing</h3>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="form-group">
                                                                                            <div class="col-md-6">
                                                                                                <label for="recipient-name" class="control-label" style="float: left;">Room Type:</label>
                                                                                                <select type="text" name="room_type" required class="form-control" id="recipient-name">
                                                                                                    <option value="">Room Types</option>
                                                                                                    @foreach($room_types as $key => $data)
                                                                                                        <option value="{{$key}}" @if($value->room_type_id === $key) selected='selected' @endif>{{$data}}</option>
                                                                                                    @endforeach
                                                                                                </select>

                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <label for="recipient-name" class="control-label" style="float: left;">Room Name:</label>
                                                                                                <input type="text" name="room_name" value="{{$value->room_name}}" class="form-control" id="recipient-name">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="form-group">
                                                                                            <div class="col-md-6">
                                                                                                <label for="recipient-name"  class="control-label" style="float: left; margin-top: 10px;">Facilities:</label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="form-group">
                                                                                            @foreach($room_features as $room_feature)
                                                                                                <div class="col-md-6">
                                                                                                    <div class="checkbox" style="float: left">
                                                                                                        <label>
                                                                                                            <input type="checkbox"  name="feature[]" value="{{ $room_feature->id }}" > {{ $room_feature->name }}
                                                                                                        </label>
                                                                                                    </div>
                                                                                                </div>
                                                                                            @endforeach
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="form-group">
                                                                                            <div class="col-md-6">
                                                                                                <label for="recipient-name" class="control-label" style="float: left;">Number of Rooms(of this type):</label>
                                                                                                <input type="number" name="room_qty" value="{{$value->room_qty}}" required class="form-control"  id="recipient-name">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="list-single-main-item-title fl-wrap">
                                                                                        <h3>Bed Option</h3>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="form-group">
                                                                                            <div class="col-md-8">
                                                                                                <label for="recipient-name" class="control-label" style="float: left;">What kind of beds are available in this room?:</label>
                                                                                                <select type="text" name="bed_type"  required class="form-control" id="recipient-name">
                                                                                                    <option value="">Types</option>
                                                                                                    <option value="Twin bed(s) / 90-130 cm wide" {{ $value->bed_type == 'Twin bed(s) / 90-130 cm wide' ? 'selected' : '' }}>Twin bed(s) / 90-130 cm wide</option>
                                                                                                    <option value="Full bed(s) / 131-150 cm wide" {{ $value->bed_type == 'Full bed(s) / 131-150 cm wide' ? 'selected' : '' }}>Full bed(s) / 131-150 cm wide</option>
                                                                                                    <option value="Queen bed(s) / 151-180 cm wide" {{ $value->bed_type == 'Queen bed(s) / 151-180 cm wide' ? 'selected' : '' }}>Queen bed(s) / 151-180 cm wide</option>
                                                                                                    <option value="King bed(s) / 181-210 cm wide" {{ $value->bed_type == 'King bed(s) / 181-210 cm wide' ? 'selected' : '' }}>King bed(s) / 181-210 cm wide</option>
                                                                                                    <option value="Bunk bed / Variable size" {{ $value->bed_type == 'Bunk bed / Variable size' ? 'selected' : '' }}>Bunk bed / Variable size</option>
                                                                                                    <option value="Sofa bed / Variable size" {{ $value->bed_type == 'Sofa bed / Variable size' ? 'selected' : '' }}>Sofa bed / Variable size</option>
                                                                                                    <option value="Futon bed(s) / Variable size" {{ $value->bed_type == 'Futon bed(s) / Variable size' ? 'selected' : '' }}>Futon bed(s) / Variable size</option>
                                                                                                </select>
                                                                                            </div>
                                                                                            <div class="col-md-4">
                                                                                                <label for="recipient-name" class="control-label" style="float: left;">qty:</label>
                                                                                                <select type="text" name="bed_qty" required class="form-control" id="recipient-name">
                                                                                                    <option value="1" {{ $value->bed_qty == 1 ? 'selected' : '' }}>1</option>
                                                                                                    <option value="2" {{ $value->bed_qty == 2 ? 'selected' : '' }}>2</option>
                                                                                                    <option value="3" {{ $value->bed_qty == 3 ? 'selected' : '' }}>3</option>
                                                                                                    <option value="4" {{ $value->bed_qty == 4 ? 'selected' : '' }}>4</option>
                                                                                                    <option value="5" {{ $value->bed_qty == 5 ? 'selected' : '' }}>5</option>
                                                                                                    <option value="6" {{ $value->bed_qty == 6 ? 'selected' : '' }}>6</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="form-group">
                                                                                            <div class="col-md-6">
                                                                                                <label for="recipient-name" class="control-label" style="float: left; margin-top: 10px;">How many guest can stay in this room:</label>
                                                                                                <input type="number" name="guest_qty" value="{{$value->guest_qty}}" required class="form-control" id="recipient-name">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="list-single-main-item-title fl-wrap">
                                                                                        <h3>Room Size (Optional)</h3>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="form-group">
                                                                                            <div class="col-md-6">
                                                                                                <div class="col-md-6">
                                                                                                    <input type="number" name="room_size" value="{{$value->room_size}}" placeholder="0" class="form-control" id="recipient-name">
                                                                                                </div>
                                                                                                <div class="col-md-6">
                                                                                                    <select type="number" name="units" class="form-control" id="recipient-name">
                                                                                                        <option value="meters" {{ $value->units == 'meters' ? 'selected' : '' }}>meters</option>
                                                                                                        <option value="feets" {{ $value->units == 'feets' ? 'selected' : '' }}>feets</option>
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="list-single-main-item-title fl-wrap">
                                                                                        <h3>Best Price per Night</h3>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="form-group">
                                                                                            <div class="col-md-6">
                                                                                                <div class="col-md-6">
                                                                                                    <input type="number" name="price_range" value="{{$value->price_range}}" required placeholder="0" class="form-control" id="recipient-name">
                                                                                                </div>
                                                                                                <div class="col-md-6">
                                                                                                    <label for="recipient-name" class="control-label" style="float: left;">LKR</label>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>


                                                                                    <div class="list-single-main-item-title fl-wrap">
                                                                                        <h3>Photo Gallery</h3>
                                                                                    </div>
                                                                                    <div class="form-group">
                                                                                        <label for="document">Drop rooms images for here</label>
                                                                                        <div class="needsclick dropzone" id="document-dropzone">

                                                                                        </div>
                                                                                    </div>


                                                                                </div>
                                                                                <div class="modal-footer" >
                                                                                    <button type="button" class="btn btn-default" style="background: #5a6268 !important;  " data-dismiss="modal">Close</button>
                                                                                    <button type="submit" class="btn btn-primary" style="background: #265a88 !important; ">Update</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                @endforeach
                                                        </div>
                                                    </div>
                                                </div>

                                        </div>
                                    </div>
                                <!-- dashboard-list end-->

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="limit-box"></div>
    </section>

@endsection
@section('script')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
    <script>
  var uploadedDocumentMap = {}
  Dropzone.options.documentDropzone = {
    url: '{{ route('projects.storeMedia') }}',
    maxFilesize: 2, // MB
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    success: function (file, response) {
      $('form').append('<input type="hidden" name="document[]" value="' + response.name + '">')
      uploadedDocumentMap[file.name] = response.name
    },
    removedfile: function (file) {
      file.previewElement.remove()
      var name = ''
      if (typeof file.file_name !== 'undefined') {
        name = file.file_name
      } else {
        name = uploadedDocumentMap[file.name]
      }
      $('form').find('input[name="document[]"][value="' + name + '"]').remove()
    },
    init: function () {
      @if(isset($project) && $project->document)
            var files =
{!! json_encode($project->document) !!}
            for (var i in files) {
              var file = files[i]
              this.options.addedfile.call(this, file)
              file.previewElement.classList.add('dz-complete')
              $('form').append('<input type="hidden" name="document[]" value="' + file.file_name + '">')
            }
@endif
        }
      }
</script>


@endsection