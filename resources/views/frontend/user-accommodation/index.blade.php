@extends('frontend.layouts.master')
@section('content')
    <!--  wrapper  --> @include('frontend.layouts.includes.profile-nav')
    <!-- section  -->
    <section class="middle-padding" >
        <div class="container">
            <div class="dasboard-wrap fl-wrap">
                <!-- dashboard-content-->
                <div class="dashboard-content fl-wrap">
                    <div class="row">
                        @include('frontend.layouts.includes.sidebar')
                        <div class="col-md-8">
                            <div class="dashboard-content fl-wrap">
                                <div class="dashboard-list-box fl-wrap">
                                    <div class="dashboard-header fl-wrap">
                                        <h3>Your Hotels / Accommodation</h3>
                                    </div>
                                    <!-- dashboard-list  -->
                                    @foreach($accommodations as $accommodation)
                                        <div class="dashboard-list">
                                            <div class="dashboard-message">
                                                <a data-toggle="modal" data-target="#exampleModal{{$accommodation->id}}"> <span class="new-dashboard-item">Add</span></a>
                                                <div class="dashboard-listing-table-image">
                                                    <a href="#"><img src="{{asset($accommodation->image)}}" alt=""></a>
                                                </div>
                                                <div class="dashboard-listing-table-text">
                                                    <h4><a href="#">{{$accommodation->company_name}}</a></h4>

                                                    <span class="dashboard-listing-table-address"><i class="far fa-map-marker"></i><a  href="#">{{$accommodation->location}}</a></span>
                                                    <div class="booking-details fl-wrap">
                                                        <span class="booking-title">Address :</span>
                                                        <span class="booking-text">{{$accommodation->place_address}}</span>
                                                    </div>
                                                    <div class="booking-details fl-wrap">
                                                        <span class="booking-title">Contact Name :</span>
                                                        <span class="booking-text">{{$accommodation->f_name .' '. $accommodation->l_name}}</span>
                                                    </div>

                                                    <div class="booking-details fl-wrap">
                                                        <span class="booking-title">ContactEmail :</span>
                                                        <span class="booking-text">{{$accommodation->account_email}}</span>
                                                    </div>
                                                    <div class="booking-details fl-wrap">
                                                        <span class="booking-title">Place Contact :</span>
                                                        <span class="booking-text">{{$accommodation->place_contact_number}}</span>
                                                    </div>
                                                    <div class="booking-details fl-wrap">
                                                        <span class="booking-title">Type :</span>
                                                        <span class="booking-text">{{$accommodation->types['name']}}</span>
                                                    </div>

                                                    <ul class="dashboard-listing-table-opt  fl-wrap">
                                                        <li><a href="{{route('user-accommodation.view', $accommodation->id)}}">View <i class="fal fa-edit"></i></a></li>
                                                        <li><a href="{{route('user-accommodation.acc_destroy', $accommodation->id)}}" class="del-btn">Delete <i class="fal fa-trash-alt"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach
                                    @foreach($accommodations as $accommodation)
                                        <div class="modal fade" id="exampleModal{{$accommodation->id}}" style="top: 90px;" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header dashboard-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="exampleModalLabel">Rooms</h4>
                                                    </div>
                                                    <form method="post"  action="{{route('user-accommodation.add-rooms')}}" enctype="multipart/form-data" >
                                                        @csrf
                                                        <div class="modal-body">
                                                            <input type="hidden" name="accommodation_id" value="{{$accommodation->id}}">
                                                            <div class="list-single-main-item-title fl-wrap">
                                                                <h3>Layout and Pricing</h3>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group">
                                                                    <div class="col-md-6">
                                                                        <label for="recipient-name" class="control-label" style="float: left;">Room Type:</label>
                                                                        <select type="text" name="room_type" required class="form-control" id="recipient-name">
                                                                            <option value="">Room Types</option>
                                                                            @foreach($room_types as $key=>$value)
                                                                            <option value="{{$key}}">{{$value}}</option>
                                                                                @endforeach
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label for="recipient-name" class="control-label" style="float: left;">Room Name:</label>
                                                                        <input type="text" name="room_name" class="form-control" id="recipient-name">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group">
                                                                    <div class="col-md-6">
                                                                <label for="recipient-name"  class="control-label" style="float: left; margin-top: 10px;">Facilities:</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group">
                                                                    @foreach($room_features as $room_feature)
                                                                    <div class="col-md-6">
                                                                        <div class="checkbox" style="float: left">
                                                                            <label>
                                                                                <input type="checkbox"  name="feature[]" value="{{ $room_feature->id }}"> {{ $room_feature->name }}
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group">
                                                                    <div class="col-md-6">
                                                                        <label for="recipient-name" class="control-label" style="float: left;">Number of Rooms(of this type):</label>
                                                                        <input type="number" name="room_qty" required class="form-control"  id="recipient-name">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="list-single-main-item-title fl-wrap">
                                                                <h3>Bed Option</h3>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group">
                                                                    <div class="col-md-8">
                                                                        <label for="recipient-name" class="control-label" style="float: left;">What kind of beds are available in this room?:</label>
                                                                        <select type="text" name="bed_type" required class="form-control" id="recipient-name">
                                                                            <option value="">Types</option>
                                                                            <option value="Twin bed(s) / 90-130 cm wide">Twin bed(s) / 90-130 cm wide</option>
                                                                            <option value="Full bed(s) / 131-150 cm wide">Full bed(s) / 131-150 cm wide</option>
                                                                            <option value="Queen bed(s) / 151-180 cm wide">Queen bed(s) / 151-180 cm wide</option>
                                                                            <option value="King bed(s) / 181-210 cm wide">King bed(s) / 181-210 cm wide</option>
                                                                            <option value="Bunk bed / Variable size">Bunk bed / Variable size</option>
                                                                            <option value="Sofa bed / Variable size">Sofa bed / Variable size</option>
                                                                            <option value="Futon bed(s) / Variable size">Futon bed(s) / Variable size</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <label for="recipient-name" class="control-label" style="float: left;">qty:</label>
                                                                        <select type="text" name="bed_qty" required class="form-control" id="recipient-name">
                                                                            <option value="1">1</option>
                                                                            <option value="2">2</option>
                                                                            <option value="3">3</option>
                                                                            <option value="4">4</option>
                                                                            <option value="5">5</option>
                                                                            <option value="6">6</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group">
                                                                    <div class="col-md-6">
                                                                        <label for="recipient-name" class="control-label" style="float: left; margin-top: 10px;">How many guest can stay in this room:</label>
                                                                        <input type="number" name="guest_qty" required class="form-control" id="recipient-name">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="list-single-main-item-title fl-wrap">
                                                                <h3>Room Size (Optional)</h3>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group">
                                                                    <div class="col-md-6">
                                                                        <div class="col-md-6">
                                                                        <input type="number" name="room_size" placeholder="0" class="form-control" id="recipient-name">
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                        <select type="number" name="units" class="form-control" id="recipient-name">
                                                                            <option value="meters">meters</option>
                                                                            <option value="feets">feets</option>
                                                                        </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="list-single-main-item-title fl-wrap">
                                                                <h3>Best Price per Night</h3>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group">
                                                                    <div class="col-md-6">
                                                                        <div class="col-md-6">
                                                                            <input type="number" name="price_range" required placeholder="0" class="form-control" id="recipient-name">
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label for="recipient-name" class="control-label" style="float: left;">LKR</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="list-single-main-item-title fl-wrap">
                                                                <h3>Photo Gallery</h3>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="document">Drop rooms images for here</label>
                                                                <div class="needsclick dropzone" id="document-dropzone">

                                                                </div>
                                                            </div>


                                                        </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary">Add Room</button>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    <!-- dashboard-list end-->

                                </div>
                                <!-- pagination-->
                                <div class="pagination">
                                    <a href="#" class="prevposts-link"><i class="fa fa-caret-left"></i></a>
                                    <a href="#">1</a>
                                    <a href="#" class="current-page">2</a>
                                    <a href="#">3</a>
                                    <a href="#">4</a>
                                    <a href="#" class="nextposts-link"><i class="fa fa-caret-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="limit-box"></div>
    </section>



@endsection
@section('script')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
    <script>
  var uploadedDocumentMap = {}
  Dropzone.options.documentDropzone = {
    url: '{{ route('projects.storeMedia') }}',
    maxFilesize: 2, // MB
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    success: function (file, response) {
      $('form').append('<input type="hidden" name="document[]" value="' + response.name + '">')
      uploadedDocumentMap[file.name] = response.name
    },
    removedfile: function (file) {
      file.previewElement.remove()
      var name = ''
      if (typeof file.file_name !== 'undefined') {
        name = file.file_name
      } else {
        name = uploadedDocumentMap[file.name]
      }
      $('form').find('input[name="document[]"][value="' + name + '"]').remove()
    },
    init: function () {
      @if(isset($project) && $project->document)
            var files =
{!! json_encode($project->document) !!}
            for (var i in files) {
              var file = files[i]
              this.options.addedfile.call(this, file)
              file.previewElement.classList.add('dz-complete')
              $('form').append('<input type="hidden" name="document[]" value="' + file.file_name + '">')
            }
@endif
        }
      }
</script>


@endsection