@extends('frontend.layouts.master')
@section('content')
    <!--  wrapper  --> @include('frontend.layouts.includes.profile-nav')
    <!-- section  -->
    <section class="middle-padding" >
        <div class="container">
            <div class="dasboard-wrap fl-wrap">
                <!-- dashboard-content-->
                <div class="dashboard-content fl-wrap">
                    <div class="row">
                        @include('frontend.layouts.includes.sidebar')
                        <div class="col-md-8">
                            <div class="dashboard-content fl-wrap">
                                <div class="dashboard-list-box fl-wrap">
                                    <div class="dashboard-header fl-wrap">
                                        <h3>Your Restaurant</h3>
                                    </div>
                                    <!-- dashboard-list  -->
                                    @foreach($restaurants as $restaurant)
                                        <div class="dashboard-list">
                                            <div class="dashboard-message">
                                                <a data-toggle="modal" data-target="#exampleModal{{$restaurant->id}}"> <span class="new-dashboard-item">Add</span></a>

                                                <div class="dashboard-listing-table-image">
                                                    <a href="#"><img src="{{asset($restaurant->image)}}" alt=""></a>
                                                </div>
                                                <div class="dashboard-listing-table-text">
                                                    <h4><a href="#">{{$restaurant->company_name}}</a></h4>

                                                    <span class="dashboard-listing-table-address"><i class="far fa-map-marker"></i><a  href="#">{{$restaurant->location}}</a></span>
                                                    <div class="booking-details fl-wrap">
                                                        <span class="booking-title">Address :</span> :
                                                        <span class="booking-text">{{$restaurant->place_address}}</span>
                                                    </div>
                                                    <div class="booking-details fl-wrap">
                                                        <span class="booking-title">Email :</span> :
                                                        <span class="booking-text">{{$restaurant->company_email}}</span>
                                                    </div>
                                                    <div class="booking-details fl-wrap">
                                                        <span class="booking-title">Place Contact :</span> :
                                                        <span class="booking-text">{{$restaurant->place_contact_number}}</span>
                                                    </div>
                                                    <div class="booking-details fl-wrap">
                                                        <span class="booking-title">Type :</span> :
                                                        <span class="booking-text">{{$restaurant->types['name']}}</span>
                                                    </div>
                                                    <div class="booking-details fl-wrap">
                                                        <span class="booking-title">Facilities :</span> :
                                                        @foreach($restaurant->restaurantfacilities as $val)
                                                        <span class="booking-text">{{$val->name}},</span>
                                                            @endforeach
                                                    </div>
                                                        <ul class="dashboard-listing-table-opt  fl-wrap">
                                                        <li><a href="#">Edit <i class="fal fa-edit"></i></a></li>
                                                        <li><a href="#" class="del-btn">Delete <i class="fal fa-trash-alt"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                @endforeach
                                <!-- dashboard-list end-->
                                    @foreach($restaurants as $restaurant)
                                        <div class="modal fade" id="exampleModal{{$restaurant->id}}" style="top: 90px;" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header dashboard-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="exampleModalLabel">Rooms</h4>
                                                    </div>
                                                    <form method="post"  action="{{route('user-accommodation.add-rooms')}}" enctype="multipart/form-data" >
                                                        @csrf
                                                        <div class="modal-body">
                                                            <input type="hidden" name="accommodation_id" value="{{$restaurant->id}}">
                                                            <div class="list-single-main-item-title fl-wrap">
                                                                <h3>Layout and Pricing</h3>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group">
                                                                    <div class="col-md-6">
                                                                        <label for="recipient-name" class="control-label" style="float: left;">Room Type:</label>
                                                                        <select type="text" name="room_type" required class="form-control" id="recipient-name">
                                                                            <option value="">Types</option>
                                                                            <option value="Brackfast">Brackfast</option>
                                                                            <option value="Lunch">Lunch</option>
                                                                            <option value="Dinner">Dinner</option>
                                                                            <option value="Desert">Desert</option>
                                                                            <option value="Other">Other</option>

                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label for="recipient-name" class="control-label" style="float: left;">Food Name:</label>
                                                                        <input type="text" name="food_name" class="form-control" id="recipient-name">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="list-single-main-item-title fl-wrap">
                                                                <h3>Best Price per Night</h3>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group">
                                                                    <div class="col-md-6">
                                                                        <div class="col-md-6">
                                                                            <input type="number" name="price_range" required placeholder="0" class="form-control" id="recipient-name">
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label for="recipient-name" class="control-label" style="float: left;">LKR</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="list-single-main-item-title fl-wrap">
                                                                <h3>Photo Gallery</h3>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="document">Drop rooms images for here</label>
                                                                <div class="needsclick dropzone" id="document-dropzone">

                                                                </div>
                                                            </div>


                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-primary">Add Room</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                @endforeach
                                <!-- dashboard-list end-->
                                </div>
                                <!-- pagination-->
                                <div class="pagination">
                                    <a href="#" class="prevposts-link"><i class="fa fa-caret-left"></i></a>
                                    <a href="#">1</a>
                                    <a href="#" class="current-page">2</a>
                                    <a href="#">3</a>
                                    <a href="#">4</a>
                                    <a href="#" class="nextposts-link"><i class="fa fa-caret-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="limit-box"></div>
    </section>

@endsection
@section('script')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
    <script>
  var uploadedDocumentMap = {}
  Dropzone.options.documentDropzone = {
    url: '{{ route('projects.storeMedia') }}',
    maxFilesize: 2, // MB
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    success: function (file, response) {
      $('form').append('<input type="hidden" name="document[]" value="' + response.name + '">')
      uploadedDocumentMap[file.name] = response.name
    },
    removedfile: function (file) {
      file.previewElement.remove()
      var name = ''
      if (typeof file.file_name !== 'undefined') {
        name = file.file_name
      } else {
        name = uploadedDocumentMap[file.name]
      }
      $('form').find('input[name="document[]"][value="' + name + '"]').remove()
    },
    init: function () {
      @if(isset($project) && $project->document)
            var files =
{!! json_encode($project->document) !!}
            for (var i in files) {
              var file = files[i]
              this.options.addedfile.call(this, file)
              file.previewElement.classList.add('dz-complete')
              $('form').append('<input type="hidden" name="document[]" value="' + file.file_name + '">')
            }
@endif
        }
      }
</script>


@endsection