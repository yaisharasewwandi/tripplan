@extends('frontend.layouts.master')
@section('content')
    <div id="wrapper">
        <!-- content-->
        <div class="content">
            <div class="breadcrumbs-fs fl-wrap">
                <div class="container">
                    <div class="breadcrumbs fl-wrap"><a href="#">Home</a><a href="#">Pages</a><span>Booking Page</span></div>
                </div>
            </div>
            <section class="middle-padding gre y-blue-bg">
                <div class="container">
                    <div class="list-main-wrap-title single-main-wrap-title fl-wrap">
                        <h2>Booking form for : <span>{{$car->title}}</span></h2>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="bookiing-form-wrap">
                                <ul id="progressbar">
                                    <li class="active" style="width: 33%"><span>01.</span>Personal Info</li>
                                    <li style="width: 33%"><span>02.</span>Billing Address</li>
                                    {{--                                    <li><span>03.</span>Payment Method</li>--}}
                                    <li style="width: 33%"><span>03.</span>Confirm</li>
                                </ul>
                                <!--   list-single-main-item -->
                                <div class="list-single-main-item fl-wrap hidden-section tr-sec">
                                    <div class="profile-edit-container">
                                        <div class="custom-form">
                                            <form   method="post" action="{{route('insert-car-booking')}}">
                                                @csrf
                                                <fieldset class="fl-wrap">
                                                    <div class="list-single-main-item-title fl-wrap">
                                                        <h3>Your personal Information</h3>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <label> Name <i class="far fa-user"></i></label>
                                                            <input type="hidden" name="carId" id="carId" value="{{$car->id}}">
                                                            <input type="hidden" name="days" id="days" value="{{$countDate}}">
                                                            <input type="hidden" name="amount" id="amount" value="{{($car->price)*($countDate)}}">
                                                            <input type="hidden" name="from" id="from" value="{{$from}}">
                                                            <input type="hidden" name="to" id="to" value="{{$to}}">
                                                            <input type="text" name="name" id="name" placeholder="Your Name" value="{{Auth::user()->name}}" required/>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label>Email Address<i class="far fa-envelope"></i>  </label>
                                                            <input type="text" name="email" id="email" placeholder="yourmail@domain.com" value="{{Auth::user()->email}}" required/>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label>Phone<i class="far fa-phone"></i>  </label>
                                                            <input type="text" id="contact_number" name="contact_number" placeholder="87945612233" value="{{Auth::user()->contact_number}}" required/>
                                                        </div>
                                                    </div>
                                                    {{--                                                    <div class="log-massage">Existing Customer? <a href="#" class="modal-open">Click here to login</a></div>--}}
                                                    {{--                                                    <div class="log-separator fl-wrap"><span>or</span></div>--}}
                                                    {{--                                                    <div class="soc-log fl-wrap">--}}
                                                    {{--                                                        <p>For faster login or register use your social account.</p>--}}
                                                    {{--                                                        <a href="#" class="facebook-log"><i class="fab fa-facebook-f"></i>Connect with Facebook</a>--}}
                                                    {{--                                                    </div>--}}
                                                    <div class="filter-tags">
                                                        <input id="check-a" type="checkbox" value="1"  name="check" required>
                                                        <label for="check-a">By continuing, you agree to the<a href="#" target="_blank">Terms and Conditions</a>.</label>
                                                    </div>
                                                    <span class="fw-separator"></span>
                                                    <a  href="#"  class="next-form action-button btn no-shdow-btn color-bg">Billing Address <i class="fal fa-angle-right"></i></a>
                                                </fieldset>
                                                <fieldset class="fl-wrap">
                                                    <div class="list-single-main-item-title fl-wrap">
                                                        <h3>Billing Address</h3>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label>City <i class="fal fa-globe-asia"></i></label>
                                                            <input type="text" placeholder="Your city" id="city"  name="city" value=""/>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label>Country </label>
                                                            <div class="listsearch-input-item ">
                                                                <select data-placeholder="Your Country" id="country" name="country" class="chosen-select no-search-select" >
                                                                    <option>United states</option>
                                                                    <option>Sri lanka</option>
                                                                    <option>Australia</option>
                                                                    <option>Europe</option>
                                                                    <option>South America</option>
                                                                    <option>Africa</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <label>Street <i class="fal fa-road"></i> </label>
                                                            <input type="text" placeholder="Your Street" id="street" name="street" value=""/>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <label>State<i class="fal fa-street-view"></i></label>
                                                            <input type="text" id="state" name="state" placeholder="Your State" value=""/>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label>Postal code<i class="fal fa-barcode"></i> </label>
                                                            <input type="text" id="postal_code" name="postal_code" placeholder="123456" value=""/>
                                                        </div>
                                                    </div>
                                                    <div class="list-single-main-item-title fl-wrap">
                                                        <h3>Addtional Notes</h3>
                                                    </div>
                                                    <textarea cols="40" id="note" rows="3" placeholder="Notes" name="note"></textarea>
                                                    <span class="fw-separator"></span>
                                                    <a  href="#"  class="previous-form action-button back-form   color-bg"><i class="fal fa-angle-left"></i> Back</a>
                                                    <a  href="#"  class="next-form  action-button btn color2-bg no-shdow-btn">Confirm <i class="fal fa-angle-right"></i></a>
                                                    {{--                                                    <a  href="#"  class="next-form back-form action-button btn no-shdow-btn color-bg insert_data">Payment Step <i class="fal fa-angle-right"></i></a>--}}
                                                </fieldset>
                                                {{--                                                <fieldset class="fl-wrap">--}}
                                                {{--                                                    <div class="list-single-main-item-title fl-wrap">--}}
                                                {{--                                                        <h3>Payment method</h3>--}}
                                                {{--                                                    </div>--}}
                                                {{--                                                    <input type="hidden" id="ref_no" >--}}
                                                {{--                                                    <div id="card_container"></div>--}}


                                                {{--                                                    <div class="log-separator fl-wrap"><span>or</span></div>--}}
                                                {{--                                                    <div class="soc-log fl-wrap">--}}
                                                {{--                                                        <p>Select Other Payment Method</p>--}}
                                                {{--                                                        <a href="#" class="paypal-log"><i class="fab fa-paypal"></i>Pay With Paypal</a>--}}
                                                {{--                                                    </div>--}}
                                                {{--                                                    <span class="fw-separator"></span>--}}
                                                {{--                                                    <a  href="#"  class="previous-form  back-form action-button    color-bg"><i class="fal fa-angle-left"></i> Back</a>--}}
                                                {{--                                                    <a  href="#"  class="next-form  action-button btn color2-bg no-shdow-btn">Confirm and Pay<i class="fal fa-angle-right"></i></a>--}}
                                                {{--                                                </fieldset>--}}
                                                <fieldset class="fl-wrap">
                                                    <div class="list-single-main-item-title fl-wrap">
                                                        <h3>Confirmation</h3>
                                                    </div>
                                                    <div class="success-table-container">
                                                        {{--                                                        <div class="filter-tags">--}}
                                                        {{--                                                            <input id="check-a" type="checkbox"  name="check" required>--}}
                                                        {{--                                                            <label for="check-a">pay with Credit Card</label>--}}
                                                        {{--                                                        </div>--}}
                                                        {{--                                                        <div class="filter-tags">--}}
                                                        {{--                                                            <input id="check-a" type="checkbox"  name="check" required>--}}
                                                        {{--                                                            <label for="check-a">By continuing, you agree to the</label>--}}
                                                        {{--                                                        </div>--}}
                                                        <div class="success-table-header fl-wrap">
                                                            <i class="fal fa-check-circle decsth"></i>
                                                            <h4>Thank you. Your reservation has been received.</h4>
                                                            <div class="clearfix"></div>
                                                            <p>Your payment has been processed successfully.</p>
                                                            {{--                                                            <a href="invoice.html" target="_blank" class="color-bg">View Invoice</a>--}}
                                                        </div>
                                                    </div>
                                                    <span class="fw-separator"></span>
                                                    <a  href="#"  class="previous-form action-button  back-form   color-bg"><i class="fal fa-angle-left"></i> Back</a>
                                                    <button type="submit"  style="float: right;" class=" action-button btn color2-bg no-shdow-btn"> Booking Confirm <i class="fal fa-angle-right"></i></button>

                                                </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!--   list-single-main-item end -->
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="box-widget-item-header">
                                <h3> Your  cart</h3>
                            </div>
                            <!--cart-details  -->
                            <div class="cart-details fl-wrap">
                                <!--cart-details_header-->
                                <div class="cart-details_header">

                                    @foreach($car->images as $image)
                                        <a href="#"  class="widget-posts-img"><img src="{{asset($image->image_name)}}" class="respimg" alt=""></a>
                                    @endforeach

                                    <div class="widget-posts-descr">
                                        <a href="#" title="">{{$car->itle}}</a>
                                        <div class="listing-rating card-popup-rainingvis" data-starrating2="{{$car->rating}}"></div>
                                        <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i> {{$car->address}}</a></div>
                                    </div>

                                </div>
                                <!--cart-details_header end-->
                                <!--ccart-details_text-->
                                <div class="cart-details_text">
                                    <ul class="cart_list">
                                        <li>Per Day <span><strong>{{$car->price}}LKR</strong></span></li>
                                        <li>From <span>{{$from}}</span></li>
                                        <li>To <span>{{$to}}</span></li>
                                        <li>Days <span>{{$countDate}}</span></li>
{{--                                                                                <li>Days<span>3 </span></li>--}}
{{--                                        <li>Adults <span>{{$adult}}</span></li>--}}
{{--                                        <li>Childs <span>{{$child}} <strong></strong></span></li>--}}
                                        {{--                                        <li>Taxes And Fees <span><strong>$12</strong></span></li>--}}
                                    </ul>
                                </div>
                                <!--cart-details_text end -->
                            </div>
                            <!--cart-details end -->
                            <!--cart-total -->
                            <div class="cart-total">
                                <span class="cart-total_item_title">Total Cost</span>
{{--                                <input type="hidden" id="amount" value="{{$acc_room->price}}">--}}
                                <strong>{{($car->price)*($countDate)}} LKR</strong>
                            </div>
                            <!--cart-total end -->
                        </div>
                    </div>
                </div>
            </section>
            <!-- section end -->
        </div>
        <!-- content end-->
    </div>
@endsection
@section('script')
    <script src="https://cdn.directpay.lk/dev/v1/directpayCardPayment.js?v=1"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        jQuery(document).ready(function($) {
            $('#booking-form').on('submit', function (e){
                e.preventDefault();

                var name = $('#name').val();
                var email = $('#email').val();
                var contact_number = $('#contact_number').val();
                var check = $('#check-a').val();
                var city = $('#city').val();
                var country = $('#country').val();
                var postal_code = $('#postal_code').val();
                var street = $('#street').val();
                var state = $('#state').val();
                var note = $('#note').val();
                var roomId = $('#roomId').val();
                var adult = $('#adult').val();
                var child = $('#child').val();
                var date = $('#date').val();

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: '/insert-booking',
                    data: {'name': name,'email': email,'contact_number': contact_number,'city': city,'country': country,'postal_code': postal_code,'street': street,'state': state,'note': note,'roomId':roomId,'adult':adult,'child':child,'date':date},
                    success: function(data){
                        location.reload();
                        // $('#ref_no').val(data.ref_no)
                    }
                });
            })
        })
    </script>

@endsection
