@extends('frontend.layouts.master')
@section('content')
{{--<!--  wrapper  --> @include('frontend.layouts.includes.profile-nav')--}}
<div id="wrapper">
    <!-- content-->
    <div class="content">
        <!-- section-->
        <section class="flat-header color-bg adm-header">
            <div class="wave-bg wave-bg2"></div>
            <div class="container">
                <div class="dasboard-wrap fl-wrap">
                    <div class="dasboard-breadcrumbs breadcrumbs"><a href="#">Home</a><a href="#">Dashboard</a><span>Guid Lines</span></div>
                    <!--dasboard-sidebar-->
                    <div class="dasboard-sidebar">
                        <div class="dasboard-sidebar-content fl-wrap">
                            <div class="dasboard-avatar">
                                <img src="{{Auth::user()->image_path}}" alt="">
                            </div>
                            <div class="dasboard-sidebar-item fl-wrap">
                                <h3>
                                    <span>Welcome </span>
                                    {{Auth::user()->name}}
                                </h3>
                            </div>
                            <button id="hott" class="ed-btn" ><a href="{{route('add-listing')}}" style="color: #ffffff;" >Guid Line</a></button>

                            {{--                                            <div class="user-stats fl-wrap">--}}
                            {{--                                            <ul>--}}


                            {{--                                                <li>--}}
                            {{--                                                    Reviews--}}
                            {{--                                                    <span>9</span>--}}
                            {{--                                                </li>--}}
                            {{--                                            </ul>--}}
                            {{--                                        </div>--}}
                            <a  href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="log-out-btn color-bg">
                                {{ __('Log Out') }}
                                <i class="far fa-sign-out"></i>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>

                        </div>
                    </div>
                    <!--dasboard-sidebar end-->
                    <!-- dasboard-menu-->
                    <div class="dasboard-menu">
                        <div class="dasboard-menu-btn color3-bg">Dashboard Menu <i class="fal fa-bars"></i></div>
                        <ul class="dasboard-menu-wrap">
                            <li>
                                <a href="{{route('customer-edit-profile')}}" class="user-profile-"><i class="far fa-user"></i>Profile</a>
                                {{--                                            <ul>--}}
                                {{--                                                <li><a href="dashboard-myprofile.html">Edit profile</a></li>--}}
                                {{--                                                <li><a href="dashboard-password.html">Change Password</a></li>--}}
                                {{--                                            </ul>--}}
                            </li>
                            {{--                                        <li><a href="dashboard-messages.html"><i class="far fa-envelope"></i> Messages <span>3</span></a></li>--}}
                            {{--                                        <li>--}}
                            {{--                                            <a href="dashboard-listing-table.html"><i class="far fa-th-list"></i> My listigs  </a>--}}
                            {{--                                            <ul>--}}
                            {{--                                                <li><a href="#">Active</a><span>5</span></li>--}}
                            {{--                                                <li><a href="#">Pending</a><span>2</span></li>--}}
                            {{--                                                <li><a href="#">Expire</a><span>3</span></li>--}}
                            {{--                                            </ul>--}}
                            {{--                                        </li>--}}
                            <li><a href="{{route('listing-booking')}}"> <i class="far fa-calendar-check"></i> Bookings <span></span></a></li>
                            {{--                                        <li><a href="dashboard-review.html"><i class="far fa-comments"></i> Reviews </a></li>--}}
                        </ul>
                    </div>

                </div>
            </div>
        </section>
        <!-- section end-->

                    <!-- section  -->
<section class="middle-padding" >
    <div class="container">
        <div class="dasboard-wrap fl-wrap">
            <!-- dashboard-content-->
            <div class="dashboard-content fl-wrap">
                <div class="row">
                    <div class="col-md-4">
                        <div class="box-widget-item fl-wrap help-bar">
                            <div class="box-widget">
                                <div class="box-widget-content">
                                    <div class="box-widget-item-header">
                                        <h3> Frequently Asked Question</h3>
                                    </div>
                                    <div class="faq-nav fl-wrap">
                                        <ul>
                                            <li><a class="custom-scroll-link act-faq-link" href="#faq1">Payments</a></li>
                                            <li><a class="custom-scroll-link" href="#faq2">Suggestions</a></li>
                                            <li><a class="custom-scroll-link" href="#faq3">Reccomendations</a></li>
                                            <li><a class="custom-scroll-link" href="#faq4">Booking</a></li>
                                            <li><a class="custom-scroll-link" href="#faq5">Listing</a></li>
                                        </ul>
                                    </div>
{{--                                    <div class="search-widget">--}}
{{--                                        <form action="#" class="fl-wrap">--}}
{{--                                            <input name="se" id="se" type="text" class="search" placeholder="Enter Keyword" value="">--}}
{{--                                            <button class="search-submit color2-bg" id="submit_btn"><i class="fal fa-search transition"></i> </button>--}}
{{--                                        </form>--}}
{{--                                    </div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <!--   list-single-main-item -->
                        <div class="list-single-main-item fl-wrap" id="faq1">
                            <div class="list-single-main-item-title fl-wrap">
                                <h3>Payments</h3>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat .</p>
                        </div>
                        <!--   list-single-main-item end -->
                        <!--   list-single-main-item -->
                        <div class="list-single-main-item fl-wrap" id="faq2">
                            <div class="list-single-main-item-title fl-wrap">
                                <h3>Suggestions</h3>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat .</p>
                        </div>
                        <!--   list-single-main-item end -->
                        <!--   list-single-main-item -->
                        <div class="list-single-main-item fl-wrap" id="faq3">
                            <div class="list-single-main-item-title fl-wrap">
                                <h3>Reccomendations</h3>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat .</p>
                        </div>
                        <!--   list-single-main-item end -->
                        <!--   list-single-main-item -->
                        <div class="list-single-main-item fl-wrap" id="faq4">
                            <div class="list-single-main-item-title fl-wrap">
                                <h3>Booking</h3>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat .</p>
                        </div>
                        <!--   list-single-main-item end -->
                        <!--   list-single-main-item -->
                        <div class="list-single-main-item fl-wrap" id="faq5">
                            <div class="list-single-main-item-title fl-wrap">
                                <h3>Listing</h3>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat .</p>
                        </div>
                        <!--   list-single-main-item end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="limit-box"></div>
</section>

@endsection
