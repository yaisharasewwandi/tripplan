@extends('frontend.layouts.master')
@section('content')
    <!--  wrapper  --> @include('frontend.layouts.includes.profile-nav')
    <!-- section  -->
    <section class="middle-padding" >
        <div class="container">
            <div class="dasboard-wrap fl-wrap">
                <!-- dashboard-content-->
                <div class="dashboard-content fl-wrap">
                    <div class="row">
                        @include('frontend.layouts.includes.sidebar')
                        <div class="col-md-8">
                            <div class="dashboard-content fl-wrap">
                                <div class="dashboard-list-box fl-wrap">
                                    <div class="dashboard-header fl-wrap">
                                        <h3>Your Hotels</h3>
                                    </div>
                                    <!-- dashboard-list  -->
                                    @foreach($guiders as $guider)
                                        <div class="dashboard-list">
                                            <div class="dashboard-message">
                                                <span class="new-dashboard-item">New</span>
                                                <div class="dashboard-listing-table-image">
                                                    <a href="listing-single.html"><img src="{{asset($guider->image)}}" alt=""></a>
                                                </div>
                                                <div class="dashboard-listing-table-text">
                                                    <h4><a href="listing-single.html">{{$guider->company_name}}</a></h4>
                                                    <div class="booking-details fl-wrap">
                                                        <span class="booking-title">Address :</span>
                                                        <span class="booking-text">{{$guider->place_address}}</span>
                                                    </div>
                                                    <div class="booking-details fl-wrap">
                                                        <span class="booking-title">Contact Name :</span>
                                                        <span class="booking-text">{{$guider->f_name .' '. $guider->l_name}}</span>
                                                    </div>

                                                    <div class="booking-details fl-wrap">
                                                        <span class="booking-title">ContactEmail :</span>
                                                        <span class="booking-text">{{$guider->account_email}}</span>
                                                    </div>
                                                    <div class="booking-details fl-wrap">
                                                        <span class="booking-title">Place Contact :</span>
                                                        <span class="booking-text">{{$guider->place_contact_number}}</span>
                                                    </div>                                                    <ul class="dashboard-listing-table-opt  fl-wrap">
                                                        <li><a href="#">Edit <i class="fal fa-edit"></i></a></li>
                                                        <li><a href="#" class="del-btn">Delete <i class="fal fa-trash-alt"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                @endforeach
                                <!-- dashboard-list end-->

                                </div>
                                <!-- pagination-->
                                <div class="pagination">
                                    <a href="#" class="prevposts-link"><i class="fa fa-caret-left"></i></a>
                                    <a href="#">1</a>
                                    <a href="#" class="current-page">2</a>
                                    <a href="#">3</a>
                                    <a href="#">4</a>
                                    <a href="#" class="nextposts-link"><i class="fa fa-caret-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="limit-box"></div>
    </section>

@endsection