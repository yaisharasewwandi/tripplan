@extends('frontend.layouts.master')
@section('content')
 <!--  wrapper  -->
             @include('frontend.layouts.includes.profile-nav')
                    <!-- section-->
                    <section class="middle-padding">
                        <div class="container">
                            <!--dasboard-wrap-->
                            <div class="dasboard-wrap fl-wrap">
                                <!-- dashboard-content-->
                                <div class="dashboard-content fl-wrap">
                                    <div class="box-widget-item-header">
                                        <h3> Your Profile</h3>
                                    </div>
                                    <!-- profile-edit-container-->
                                    <div class="profile-edit-container">
                                        <div class="custom-form">
                                            <label>Your Name <i class="far fa-user"></i></label>
                                            <input type="text" placeholder="Jessie Manrty" value=""/>
                                            <label>Email Address<i class="far fa-envelope"></i>  </label>
                                            <input type="text" placeholder="JessieManrty@domain.com" value=""/>
                                            <label>Phone<i class="far fa-phone"></i>  </label>
                                            <input type="text" placeholder="+7(123)987654" value=""/>
                                            <label> Adress <i class="fas fa-map-marker"></i>  </label>
                                            <input type="text" placeholder="USA 27TH Brooklyn NY" value=""/>
                                            <label> Website <i class="far fa-globe"></i>  </label>
                                            <input type="text" placeholder="themeforest.net" value=""/>
                                            <div class="row">
                                                <div class="col-sm-9">
                                                    <label> Notes</label>
                                                    <textarea cols="40" rows="3" placeholder="About Me"></textarea>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Change Avatar</label>
                                                    <div class="add-list-media-wrap">
                                                        <form class="fuzone">
                                                            <div class="fu-text">
                                                                <span><i class="fal fa-image"></i> Click here or drop files to upload</span>
                                                            </div>
                                                            <input type="file" class="upload">
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- profile-edit-container end-->
                                    <div class="box-widget-item-header mat-top">
                                        <h3>Your  Socials</h3>
                                    </div>
                                    <!-- profile-edit-container-->
                                    <div class="profile-edit-container">
                                        <div class="custom-form">
                                            <label>Facebook <i class="fab fa-facebook"></i></label>
                                            <input type="text" placeholder="https://www.facebook.com/" value=""/>
                                            <label>Twitter<i class="fab fa-twitter"></i>  </label>
                                            <input type="text" placeholder="https://twitter.com/" value=""/>
                                            <label>Vkontakte<i class="fab fa-vk"></i>  </label>
                                            <input type="text" placeholder="https://vk.com" value=""/>
                                            <label> Instagram <i class="fab fa-instagram"></i>  </label>
                                            <input type="text" placeholder="https://www.instagram.com/" value=""/>
                                            <button class="btn    color2-bg  float-btn">Save Changes<i class="fal fa-save"></i></button>
                                        </div>
                                    </div>
                                    <!-- profile-edit-container end-->
                                </div>
                                <!-- dashboard-list-box end-->
                            </div>
                            <!-- dasboard-wrap end-->
                        </div>
                    </section>
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!-- content end-->
            </div>
            <!--wrapper end -->
@endsection
