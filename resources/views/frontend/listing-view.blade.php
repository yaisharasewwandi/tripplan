@extends('frontend.layouts.master')
@section('content')
    @include('frontend.layouts.includes.profile-nav')
                    <!-- section-->
                    <section class="middle-padding">
                        <div class="container">
                            <!--dasboard-wrap-->
                            <div class="dasboard-wrap fl-wrap">
                                <!-- dashboard-content--> 
                                <div class="dashboard-content fl-wrap">
                                    <div class="dashboard-list-box fl-wrap">
                                        <div class="dashboard-header fl-wrap">
                                            <h3>Your Listings</h3>
                                        </div>
                                        <!-- dashboard-list  -->    
                                        <div class="dashboard-list">
                                            <div class="dashboard-message">
                                                <span class="new-dashboard-item">New</span>
                                                <div class="dashboard-listing-table-image">
                                                    <a href="listing-single.html"><img src="images/gal/8.jpg" alt=""></a>
                                                </div>
                                                <div class="dashboard-listing-table-text">
                                                    <h4><a href="listing-single.html">Premium Plaza Hotel</a></h4>
                                                    <span class="dashboard-listing-table-address"><i class="far fa-map-marker"></i><a  href="#">USA 27TH Brooklyn NY</a></span>
                                                    <ul class="dashboard-listing-table-opt  fl-wrap">
                                                        <li><a href="#">Edit <i class="fal fa-edit"></i></a></li>
                                                        <li><a href="#" class="del-btn">Delete <i class="fal fa-trash-alt"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- dashboard-list end-->    
                                        <!-- dashboard-list -->    
                                        <div class="dashboard-list">
                                            <div class="dashboard-message">
                                                <div class="dashboard-listing-table-image">
                                                    <a href="listing-single.html"><img src="images/gal/4.jpg" alt=""></a>
                                                </div>
                                                <div class="dashboard-listing-table-text">
                                                    <h4><a href="listing-single.html">Grand Hero Palace</a></h4>
                                                    <span class="dashboard-listing-table-address"><i class="far fa-map-marker"></i><a  href="#"> W 85th St, New York,  USA</a></span>
                                                    <ul class="dashboard-listing-table-opt  fl-wrap">
                                                        <li><a href="#">Edit <i class="fal fa-edit"></i></a></li>
                                                        <li><a href="#" class="del-btn">Delete <i class="fal fa-trash-alt"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- dashboard-list end-->                                               
                                        <!-- dashboard-list  -->    
                                        <div class="dashboard-list">
                                            <div class="dashboard-message">
                                                <div class="dashboard-listing-table-image">
                                                    <a href="listing-single.html"><img src="images/gal/6.jpg" alt=""></a>
                                                </div>
                                                <div class="dashboard-listing-table-text">
                                                    <h4><a href="listing-single.html">Park Central</a></h4>
                                                    <span class="dashboard-listing-table-address"><i class="far fa-map-marker"></i><a  href="#">40 Journal Square Plaza, NJ,  USA</a></span>
                                                    <ul class="dashboard-listing-table-opt  fl-wrap">
                                                        <li><a href="#">Edit <i class="fal fa-edit"></i></a></li>
                                                        <li><a href="#" class="del-btn">Delete <i class="fal fa-trash-alt"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- dashboard-list end-->   
                                        <!-- dashboard-list  -->    
                                        <div class="dashboard-list">
                                            <div class="dashboard-message">
                                                <div class="dashboard-listing-table-image">
                                                    <a href="listing-single.html"><img src="images/gal/3.jpg" alt=""></a>
                                                </div>
                                                <div class="dashboard-listing-table-text">
                                                    <h4><a href="listing-single.html">Gold Plaza Hotel </a></h4>
                                                    <span class="dashboard-listing-table-address"><i class="far fa-map-marker"></i><a  href="#">34-42 Montgomery St , NY, USA</a></span>
                                                    <ul class="dashboard-listing-table-opt  fl-wrap">
                                                        <li><a href="#">Edit <i class="fal fa-edit"></i></a></li>
                                                        <li><a href="#" class="del-btn">Delete <i class="fal fa-trash-alt"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- dashboard-list end-->  
                                    </div>
                                    <!-- pagination-->
                                    <div class="pagination">
                                        <a href="#" class="prevposts-link"><i class="fa fa-caret-left"></i></a>
                                        <a href="#">1</a>
                                        <a href="#" class="current-page">2</a>
                                        <a href="#">3</a>
                                        <a href="#">4</a>
                                        <a href="#" class="nextposts-link"><i class="fa fa-caret-right"></i></a>
                                    </div>
                                </div>
                                <!-- dashboard-list-box end--> 
                            </div>
                            <!-- dasboard-wrap end-->
                        </div>
                    </section>
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!-- content end-->
            </div>
            <!--wrapper end -->
@endsection