@extends('frontend.layouts.master')
@section('content')
    <!--  wrapper  -->
    <div id="wrapper">
        <!-- content-->
        <div class="content">
            <!--  section  -->
            <section class="list-single-hero" data-scrollax-parent="true" id="sec1">
                    <div class="bg par-elem "  data-bg="{{asset($travelagencies['banner_image'])}}" data-scrollax="properties: { translateY: '30%' }"></div>
                <div class="list-single-hero-title fl-wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="listing-rating-wrap">
                                    <div class="listing-rating card-popup-rainingvis" data-starrating2="{{$travelagencies->rating}}"></div>
                                </div>
                                <h2><span>{{$travelagencies->title}}</span></h2>
                                <div class="list-single-header-contacts fl-wrap">
                                    <ul>
                                        <li><i class="far fa-phone"></i><a  href="#">{{$travelagencies->contact_number}}</a></li>
                                        <li><i class="far fa-map-marker-alt"></i><a  href="#">{{$travelagencies->location}}</a></li>
                                        <li><i class="far fa-envelope"></i><a  href="#">{{$travelagencies->email}}</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <!--  list-single-hero-details-->
                                <div class="list-single-hero-details fl-wrap">
                                    <!--  list-single-hero-rating-->
                                    <div class="list-single-hero-rating">
                                        <div class="rate-class-name">
                                            <div class="score"><strong>Very Good</strong>{{$travelagencies->reviewAgancy()->count()}} Reviews </div>
                                            <span>{{number_format($travelagencies->rating,1)}}</span>
                                        </div>
                                        <!-- list-single-hero-rating-list-->
                                        <div class="list-single-hero-rating-list">
                                            <!-- rate item-->
                                            <div class="rate-item fl-wrap">
                                                <div class="rate-item-title fl-wrap"><span>Cleanliness</span></div>
                                                <div class="rate-item-bg" data-percent="{{(number_format($cleanliness,2)/5)*100}}%">
                                                    <div class="rate-item-line color-bg"></div>
                                                </div>
                                                <div class="rate-item-percent">{{number_format($cleanliness,2)}}</div>
                                            </div>
                                            <!-- rate item end-->
                                            <!-- rate item-->
                                            <div class="rate-item fl-wrap">
                                                <div class="rate-item-title fl-wrap"><span>Comfort</span></div>
                                                <div class="rate-item-bg" data-percent="{{(number_format($comfort,2)/5)*100}}%">
                                                    <div class="rate-item-line color-bg"></div>
                                                </div>
                                                <div class="rate-item-percent">{{number_format($comfort,2)}}</div>
                                            </div>
                                            <!-- rate item end-->
                                            <!-- rate item-->
                                            <div class="rate-item fl-wrap">
                                                <div class="rate-item-title fl-wrap"><span>Staf</span></div>
                                                <div class="rate-item-bg" data-percent="{{(number_format($staff,2)/5)*100}}%">
                                                    <div class="rate-item-line color-bg"></div>
                                                </div>
                                                <div class="rate-item-percent">{{number_format($staff,2)}}</div>
                                            </div>
                                            <!-- rate item end-->
                                            <!-- rate item-->
                                            <div class="rate-item fl-wrap">
                                                <div class="rate-item-title fl-wrap"><span>Facilities</span></div>
                                                <div class="rate-item-bg" data-percent="{{(number_format($facilities,2)/5)*100}}%">
                                                    <div class="rate-item-line color-bg"></div>
                                                </div>
                                                <div class="rate-item-percent">{{number_format($facilities,2)}}</div>
                                            </div>
                                            <!-- rate item end-->
                                        </div>
                                        <!-- list-single-hero-rating-list end-->
                                    </div>
                                    <!--  list-single-hero-rating  end-->
                                    <div class="clearfix"></div>
                                    <!-- list-single-hero-links-->
                                    <div class="list-single-hero-links">
                                        <a class="lisd-link" href=""><i class="fal fa-bookmark"></i> Book Now</a>
                                        <a class="custom-scroll-link lisd-link" href="#sec6"><i class="fal fa-comment-alt-check"></i> Add review</a>
                                    </div>
                                    <!--  list-single-hero-links end-->
                                </div>
                                <!--  list-single-hero-details  end-->
                            </div>
                        </div>
                        <div class="breadcrumbs-hero-buttom fl-wrap">
                            <div class="breadcrumbs"><a href="#">Home</a><a href="#">Agency</a><span>Agency Single</span></div>
                            <div class="list-single-hero-price">AWG/NIGHT<span>{{$travelagencies->price}} LKR</span></div>
                        </div>
                    </div>
                </div>
            </section>
            <!--  section  end-->
            <!--  section  -->
            <section class="grey-blue-bg small-padding scroll-nav-container" id="sec2">
                <!--  scroll-nav-wrapper  -->
                <div class="scroll-nav-wrapper fl-wrap">
                    <div class="hidden-map-container fl-wrap">
                        <input id="pac-input" class="controls fl-wrap controls-mapwn" type="text" placeholder="What Nearby ?   Bar , Gym , Restaurant ">
                        <div class="map-container">
                            <div id="singleMap" data-latitude="40.7427837" data-longitude="-73.11445617675781"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="container">
                        <nav class="scroll-nav scroll-init">
                            <ul>
                                <li><a class="act-scrlink" href="#sec1">Top</a></li>
                                <li><a href="#sec2">Details</a></li>
                                <li><a href="#sec3">Amenities</a></li>
                                <li><a href="#sec4">Rooms</a></li>
                                <li><a href="#sec5">Reviews</a></li>
                            </ul>
                        </nav>
{{--                        <a href="#" class="show-hidden-map">  <span>On The Map</span> <i class="fal fa-map-marked-alt"></i></a>--}}
                    </div>
                </div>
                <!--  scroll-nav-wrapper end  -->
                <!--   container  -->
                <div class="container">
                    <!--   row  -->
                    <div class="row">
                        <!--   datails -->
                        <div class="col-md-8">
                            <div class="list-single-main-container ">
                                <!-- fixed-scroll-column  -->
                                <div class="fixed-scroll-column">
                                    <div class="fixed-scroll-column-item fl-wrap">
                                        <div class="showshare sfcs fc-button"><i class="far fa-share-alt"></i><span>Share </span></div>
                                        <div class="share-holder fixed-scroll-column-share-container">
                                            <div class="share-container  isShare"></div>
                                        </div>
                                        <a class="fc-button custom-scroll-link" href="#sec6"><i class="far fa-comment-alt-check"></i> <span>  Add review </span></a>
                                        <a class="fc-button" href="#"><i class="far fa-heart"></i> <span>Save</span></a>
                                        <a class="fc-button" href=""><i class="far fa-bookmark"></i> <span> Book Now </span></a>
                                    </div>
                                </div>
                                <!-- fixed-scroll-column end   -->
                                <div class="list-single-main-media fl-wrap">
                                    <!-- gallery-items   -->
                                    <div class="gallery-items grid-small-pad  list-single-gallery three-coulms lightgallery">
                                        <!-- 1 -->
                                        @foreach($travelagencies->agancyPackages as $travelagency)
                                            @foreach($travelagency->images as $key=>$images)

                                                @if($key == 2)
                                                    <div class="gallery-item gallery-item-second">
                                                        @else
                                                            <div class="gallery-item">
                                                                @endif
                                                                <div class="grid-item-holder">
                                                                    <div class="box-item">

                                                                        <img  src="{{asset($images->image_name)}}"   alt="">

                                                                        <a href="{{asset($images->image_name)}}" class="gal-link popup-image"><i class="fa fa-search"></i></a>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                        @endforeach
                                                        <!-- 1 end -->

                                                            <!-- 7 end -->
                                                    </div>
                                                    <!-- end gallery items -->
                                                    About          </div>
                                    <!-- list-single-header end -->

                                    <!--   list-single-main-item -->
                                    <div class="list-single-main-item fl-wrap">
                                        <div class="list-single-main-item-title fl-wrap">
                                            <h3>About Agency </h3>
                                        </div>
                                        <P>{!! $travelagencies->description !!}</P>
                                    </div>
                                    <!--   list-single-main-item end -->
                                    <!--   list-single-main-item -->
                                    <div class="list-single-main-item fl-wrap" id="sec3">
                                        <div class="list-single-main-item-title fl-wrap">
                                            <h3>Includes</h3>
                                        </div>
                                        <div class="listing-features fl-wrap">
                                            <ul>
                                                @foreach($travelagenciespackage->agancyPackages as $travelagency)
                                                    <p>{{str_replace(array('[',']','"'),' ',$travelagency->includes)}} </p>
                                                    @endforeach


                                            </ul>
                                        </div>
                                        <span class="fw-separator"></span>
                                        <div class="list-single-main-item-title no-dec-title fl-wrap">
                                            <h3>Exclude</h3>
                                        </div>
                                        <div class="listing-features fl-wrap">
                                            <ul>
                                                @foreach($travelagenciespackage->agancyPackages as $travelagency)
                                                    <p>{{str_replace(array('[',']','"'),' ',$travelagency->excludes)}} </p>
                                                @endforeach


                                            </ul>
                                        </div>
                                    </div>
                                    <!--   list-single-main-item end -->
                                    <!-- accordion-->
                                {{--<div class="accordion mar-top">--}}
                                {{--<a class="toggle act-accordion" href="#"> Details option   <span></span></a>--}}
                                {{--<div class="accordion-inner visible">--}}
                                {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>--}}
                                {{--</div>--}}
                                {{--<a class="toggle" href="#"> Details option 2  <span></span></a>--}}
                                {{--<div class="accordion-inner">--}}
                                {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>--}}
                                {{--</div>--}}
                                {{--<a class="toggle" href="#"> Details option 3  <span></span></a>--}}
                                {{--<div class="accordion-inner">--}}
                                {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                <!-- accordion end -->
                                    <!--   list-single-main-item -->
                                    <div class="list-single-main-item fl-wrap" id="sec4">
                                        <div class="list-single-main-item-title fl-wrap">
                                            <h3>Available Packages</h3>
                                        </div>
                                        <!--   rooms-container -->
                                        <div class="rooms-container fl-wrap">
                                            <!--  rooms-item -->
                                            @foreach($travelagencies->agancyPackages as $travelagency)
                                                <div class="rooms-item fl-wrap">
                                                    <div class="rooms-media">

                                                        <img src="{{asset($images->image_name)}}" alt="">
                                                        <div class="dynamic-gal more-photos-button" data-dynamicPath="[@foreach($travelagency->images as $key=>$images){'src': '{{asset($images->image_name)}}'}, @endforeach]">  View Gallery <span> photos</span> <i class="far fa-long-arrow-right"></i></div>

                                                    </div>
                                                    <div class="rooms-details">
                                                        <div class="rooms-details-header fl-wrap">
                                                            <span class="rooms-price">{{number_format($travelagency->budget,2)}} LKR<strong> / room</strong></span>
                                                            <h3>{{$travelagency->title}}</h3>
                                                            <h5>Max Person: <span>{{$travelagency->max_person}}</span></h5>
                                                        </div>
                                                        <p>{!! Illuminate\Support\Str::limit(strip_tags($travelagency->description), 100, $end='...') !!}</p>
                                                        <div class="facilities-list fl-wrap">
                                                            <ul>

                                                                    <li><i class=""></i><span></span></li>

                                                            </ul>
{{--                                                            <a href="rooms/room1.html" class="btn color-bg ajax-link">Details<i class="fas fa-caret-right"></i></a>--}}
                                                        </div>
                                                    </div>
                                                </div>
                                        @endforeach
                                        <!--  rooms-item end -->

                                            <!--  rooms-item end -->
                                        </div>
                                        <!--   rooms-container end -->
                                    </div>
                                    <!-- list-single-main-item end -->
                                    <!-- list-single-main-item -->
                                    <div class="list-single-main-item fl-wrap" id="sec5">
                                        <div class="list-single-main-item-title fl-wrap">
                                            <h3>Item Reviews -  <span> {{$travelagencies->reviewAgancy()->count()}}</span></h3>
                                        </div>
                                        <!--reviews-score-wrap-->
                                        <div class="reviews-score-wrap fl-wrap">
                                            <div class="review-score-total">
                                                    <span>
                                                    {{number_format($travelagencies->rating,1)}}
                                                    <strong>Very Good</strong>
                                                    </span>
                                                <a href="#" class="color2-bg">Add Review</a>
                                            </div>
                                            <div class="review-score-detail">
                                                <!-- review-score-detail-list-->
                                                <div class="review-score-detail-list">
                                                    <!-- rate item-->
                                                    <div class="rate-item fl-wrap">
                                                        <div class="rate-item-title fl-wrap"><span>Cleanliness</span></div>
                                                        <div class="rate-item-bg" data-percent="{{(number_format($cleanliness,2)/5)*100}}%">
                                                            <div class="rate-item-line color-bg"></div>
                                                        </div>
                                                        <div class="rate-item-percent">{{number_format($cleanliness,2)}}</div>
                                                    </div>
                                                    <!-- rate item end-->
                                                    <!-- rate item-->
                                                    <div class="rate-item fl-wrap">
                                                        <div class="rate-item-title fl-wrap"><span>Comfort</span></div>
                                                        <div class="rate-item-bg" data-percent="{{(number_format($comfort,2)/5)*100}}%">
                                                            <div class="rate-item-line color-bg"></div>
                                                        </div>
                                                        <div class="rate-item-percent">{{number_format($comfort,2)}}</div>
                                                    </div>
                                                    <!-- rate item end-->
                                                    <!-- rate item-->
                                                    <div class="rate-item fl-wrap">
                                                        <div class="rate-item-title fl-wrap"><span>Staf</span></div>
                                                        <div class="rate-item-bg" data-percent="{{(number_format($staff,2)/5)*100}}%">
                                                            <div class="rate-item-line color-bg"></div>
                                                        </div>
                                                        <div class="rate-item-percent">{{number_format($staff,2)}}</div>
                                                    </div>
                                                    <!-- rate item end-->
                                                    <!-- rate item-->
                                                    <div class="rate-item fl-wrap">
                                                        <div class="rate-item-title fl-wrap"><span>Facilities</span></div>
                                                        <div class="rate-item-bg" data-percent="{{(number_format($facilities,2)/5)*100}}%">
                                                            <div class="rate-item-line color-bg"></div>
                                                        </div>
                                                        <div class="rate-item-percent">{{number_format($facilities,2)}}</div>
                                                    </div>
                                                    <!-- rate item end-->
                                                </div>
                                                <!-- review-score-detail-list end-->
                                            </div>
                                        </div>
                                        <!-- reviews-score-wrap end -->
                                        <div class="reviews-comments-wrap">
                                            <!-- reviews-comments-item -->

                                            <!--reviews-comments-item end-->
                                            @if(!empty($review))
                                                @foreach($review as $reviews)
                                                    <div class="reviews-comments-item">
                                                        <div class="review-comments-avatar">
                                                            @if(!empty($reviews->user->image_path))
                                                                <img src="{{asset($reviews->user->image_path)}}" alt="">
                                                            @else
                                                                <img src="{{asset('frontend/images/user.png')}}" alt="">
                                                            @endif
                                                        </div>
                                                        <div class="reviews-comments-item-text">
                                                            <h4><a href="#">{{$reviews->user->name}}</a></h4>
                                                            <div class="review-score-user">
                                                                <span>{{$reviews->sub_avg}}</span>
                                                                <strong>Good</strong>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <p>" {{$reviews->comment}} "</p>
                                                            <div class="reviews-comments-item-date"><span><i class="far fa-calendar-check"></i>{{$reviews->created_at->format('y/m/d')}}</span></div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                            {{$review->links()}}
                                        </div>
                                    </div>
                                    <!-- list-single-main-item end -->
                                    <!-- list-single-main-item -->
                                    <div class="list-single-main-item fl-wrap" id="sec6">
                                        <div class="list-single-main-item-title fl-wrap">
                                            <h3>Add Review</h3>
                                        </div>
                                        <!-- Add Review Box -->
                                        <div id="add-review" class="add-review-box">
                                            <!-- Review Comment -->
                                            <form id="travel-review" class="add-comment  custom-form" name="rangeCalc" >
                                                <fieldset>
                                                    <div class="review-score-form fl-wrap">
                                                        <div class="review-range-container">
                                                            <!-- review-range-item-->
                                                            <div class="review-range-item">
                                                                <div class="range-slider-title">Cleanliness</div>
                                                                <div class="range-slider-wrap ">
                                                                    <input type="hidden" id="id" value="{{$travelagencies->id}}">
                                                                    <input type="text" id="cleanliness" class="rate-range" data-min="0" data-max="5"  name="rgcl"  data-step="1" value="4">
                                                                </div>
                                                            </div>
                                                            <!-- review-range-item end -->
                                                            <!-- review-range-item-->
                                                            <div class="review-range-item">
                                                                <div class="range-slider-title">Comfort</div>
                                                                <div class="range-slider-wrap ">
                                                                    <input type="text" id="comfort" class="rate-range" data-min="0" data-max="5"  name="rgcl"  data-step="1"  value="1">
                                                                </div>
                                                            </div>
                                                            <!-- review-range-item end -->
                                                            <!-- review-range-item-->
                                                            <div class="review-range-item">
                                                                <div class="range-slider-title">Staf</div>
                                                                <div class="range-slider-wrap ">
                                                                    <input type="text" id="staf" class="rate-range" data-min="0" data-max="5"  name="rgcl"  data-step="1" value="5" >
                                                                </div>
                                                            </div>
                                                            <!-- review-range-item end -->
                                                            <!-- review-range-item-->
                                                            <div class="review-range-item">
                                                                <div class="range-slider-title">Facilities</div>
                                                                <div class="range-slider-wrap">
                                                                    <input type="text" id="facilities" class="rate-range" data-min="0" data-max="5"  name="rgcl"  data-step="1" value="3">
                                                                </div>
                                                            </div>
                                                            <!-- review-range-item end -->
                                                        </div>
                                                        <div class="review-total">
                                                            <span><input type="text" id="score" name="rg_total"  data-form="AVG({rgcl})" value="0"></span>
                                                            <strong>Your Score</strong>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label><i class="fal fa-user"></i></label>
                                                            <input type="text" id="name" placeholder="Your Name *" value=""/>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><i class="fal fa-envelope"></i>  </label>
                                                            <input type="email" id="email" placeholder="Email Address*" value=""/>
                                                        </div>
                                                    </div>
                                                    <textarea cols="40" id="comment" rows="3" placeholder="Your Review:"></textarea>
                                                </fieldset>
                                                @if (Auth::user() )
                                                    <button type="submit" class="btn  big-btn flat-btn float-btn color2-bg" style="margin-top:30px">Submit Review <i class="fal fa-paper-plane"></i></button>
                                                @else
                                                    <button class="btn  big-btn flat-btn float-btn color2-bg modal-open" style="margin-top:30px">Submit Review <i class="fal fa-paper-plane"></i></button>
                                                @endif
                                            </form>
                                        </div>
                                        <!-- Add Review Box / End -->
                                    </div>
                                    <!-- list-single-main-item end -->
                                </div>
                            </div>
                            <!--   datails end  -->
                            <!--   sidebar  -->
                            <div class="col-md-4">
                                <!--box-widget-wrap -->
                                <div class="box-widget-wrap">
                                    <!--box-widget-item -->
                                    <div class="box-widget-item fl-wrap">
                                        <div class="box-widget">
                                            <div class="box-widget-content">
                                                <div class="box-widget-item-header">
                                                    <h3> Book This Hotel</h3>
                                                </div>
                                                <form  action="{{route('package-book')}}" method="post" name="bookFormCalc"  class="book-form custom-form">
                                                    @csrf
                                                    <fieldset>
                                                        <div class="cal-item">
                                                            <div class="listsearch-input-item">
                                                                <label>Package Type</label>
                                                                <select data-placeholder="Select Package" name="type"   class="package_id chosen-select no-search-select" required>
                                                                    <option value="">Select Package</option>
                                                                    @foreach($package as $packa)
                                                                    <option value="{{$packa->id}}" >{{$packa->title}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <!--data-formula -->
                                                                <input type="text" name="item_total" class="hid-input"  value=""  data-form="{repopt}">
                                                            </div>
                                                        </div>
                                                        <div class="cal-item">
                                                            <div class="bookdate-container  fl-wrap">
                                                                <label><i class="fal fa-calendar-check"></i> When </label>
                                                                <input type="text"    placeholder="Date In-Out" name="dates"  required value=""/>
                                                                <div class="bookdate-container-dayscounter"><i class="far fa-question-circle"></i><span>Days : <strong>0</strong></span></div>
                                                            </div>
                                                        </div>
                                                        <div class="cal-item">
                                                            <div class="quantity-item fl-wrap">
                                                                <label> Person</label>
                                                                <div class="quantity">
                                                                    <input type="number" name="person" min=""  class="min-person" max="" step="1" value="">
                                                                    <input type="text" name="item_total" class="hid-input" value="0" data-form="{qty3} * {repopt} - {repopt}">
                                                                </div>
                                                            </div>
{{--                                                            <div class="quantity-item fl-wrap fcit">--}}
{{--                                                                <label> Max Person</label>--}}
{{--                                                                <div class="quantity">--}}
{{--                                                                    <input type="number"  name="qty2" min="" class="max-person" max="" step="1" value="">--}}
{{--                                                                    <select name="sale" class="hid-input">--}}
{{--                                                                        <option value=".7"  selected>sale</option>--}}
{{--                                                                    </select>--}}
{{--                                                                    <input type="text" name="item_total" class="hid-input"  value="0" data-form="({repopt} * {sale})*{qty2}">--}}
{{--                                                                </div>--}}
{{--                                                            </div>--}}
                                                        </div>
                                                    </fieldset>
                                                    <input type="number"  id="totaldays" name="qty5" class="hid-input">
                                                    <div class="total-coast fl-wrap"><strong>Total Cost</strong> <span>$ <input type="text" name="grand_total" value="" class="price" data-form="SUM({item_total}) * {qty5}"></span></div>
                                                    @if (Auth::user() )
                                                        <button type="submit" class="btnaplly color2-bg" >Book Now<i class="fal fa-paper-plane"></i></button>
                                                    @else
                                                        <button type="button" class="btnaplly color2-bg modal-open" >Book Now<i class="fal fa-paper-plane"></i></button>
                                                    @endif
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!--box-widget-item end -->

                                    <!--box-widget-item -->
                                    <div class="box-widget-item fl-wrap">
                                        <div class="box-widget">
                                            <div class="box-widget-content">
                                                <div class="box-widget-item-header">
                                                    <h3> Contact Information</h3>
                                                </div>
                                                <div class="box-widget-list">
                                                    <ul>
                                                        <li><span><i class="fal fa-map-marker"></i> Adress :</span> <a href="#">{{$travelagencies->address}}</a></li>
                                                        <li><span><i class="fal fa-phone"></i> Phone :</span> <a href="#">{{$travelagencies->contact_number}}</a></li>
                                                        <li><span><i class="fal fa-envelope"></i> Mail :</span> <a href="#">{{$travelagencies->email}}</a></li>
                                                        <li><span><i class="fal fa-browser"></i> Website :</span> <a href="#">{{$travelagencies->website_link}}</a></li>
                                                    </ul>
                                                </div>
                                                <div class="list-widget-social">
                                                    <ul>
                                                        <li><a href="{{$travelagencies->fb_page_link}}" target="_blank" ><i class="fab fa-facebook-f"></i></a></li>
                                                        <li><a href="{{$travelagencies->twiter_link}}" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                                        <li><a href="{{$travelagencies->website_link}}" target="_blank" ><i class="fab fa-vk"></i></a></li>
                                                        <li><a href="{{$travelagencies->instergram_link}}" target="_blank" ><i class="fab fa-instagram"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--box-widget-item end -->
                                    <!--box-widget-item -->
{{--                                    <div class="box-widget-item fl-wrap">--}}
{{--                                        <div class="box-widget">--}}
{{--                                            <div class="box-widget-content">--}}
{{--                                                <div class="box-widget-item-header">--}}
{{--                                                    <h3> Available Time </h3>--}}
{{--                                                </div>--}}
{{--                                                <div class="claim-price-wdget fl-wrap">--}}
{{--                                                    <div class="claim-price-wdget-content fl-wrap">--}}
{{--                                                        <div class="pricerange fl-wrap"><span>{{$travelagencies->available_times}}</span> 81$ - 320$ </div>--}}
{{--                                                        <div class="claim-widget-link fl-wrap"><span>Own or work here?</span><a href="#">Claim Now!</a></div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                    <!--box-widget-item end -->
                                    <!--box-widget-item -->


                                    <!--box-widget-item -->
                                {{--<div class="box-widget-item fl-wrap">--}}
                                {{--<div class="box-widget">--}}
                                {{--<div class="box-widget-content">--}}
                                {{--<div class="box-widget-item-header">--}}
                                {{--<h3>Hosted By</h3>--}}
                                {{--</div>--}}
                                {{--<div class="box-widget-author fl-wrap">--}}
                                {{--<div class="box-widget-author-title fl-wrap">--}}
                                {{--<div class="box-widget-author-title-img">--}}
                                {{--<img src="images/avatar/4.jpg" alt="">--}}
                                {{--</div>--}}
                                {{--<a href="user-single.html">Jessie Manrty</a>--}}
                                {{--<span>4 Places Hosted</span>--}}
                                {{--</div>--}}
                                {{--<a href="author-single.html" class="btn flat-btn color-bg   float-btn image-popup">View Profile<i class="fal fa-user-alt"></i></a>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                <!--box-widget-item end -->
                                    <!--box-widget-item -->
                                    <div class="box-widget-item fl-wrap">
                                        <div class="box-widget">
                                            <div class="box-widget-content">
                                                <div class="box-widget-item-header">
                                                    <h3>Similar Listings</h3>
                                                </div>
                                                <div class="widget-posts fl-wrap">
                                                    <ul>
                                                        @foreach($travelagenciesnAll as $key=>$all)
                                                            @if($key < 6)
                                                                <li class="clearfix">
                                                                        <a href="{{route('agency-single',$all->id)}}"  class="widget-posts-img"><img src="{{asset($all->featured_image)}}" class="respimg" alt=""></a>

                                                                    <div class="widget-posts-descr">
                                                                        <a href="{{route('agency-single',$all->id)}}" title="">{{$all->title}}</a>
                                                                        <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                                                                        <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i> {{$all->location}}</a></div>
                                                                        <span class="rooms-price">{{$all->budget}} LKR <strong> </strong></span>
                                                                    </div>
                                                                </li>
                                                            @endif
                                                        @endforeach

                                                    </ul>
                                                    <a class="widget-posts-link" href="{{route('agency-list')}}">See All Listing <i class="fal fa-long-arrow-right"></i> </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--box-widget-item end -->
                                </div>
                                <!--box-widget-wrap end -->
                            </div>
                            <!--   sidebar end  -->
                        </div>
                        <!--   row end  -->
                    </div>
                    <!--   container  end  -->
            </section>
            <!--  section  end-->
        </div>
        <!-- content end-->
        <div class="limit-box fl-wrap"></div>
    </div>
    <!--wrapper end -->
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            $(document).on('change','.package_id',function(){
                var package_id =  $('.package_id').val();
                var a = $(this).parent();

                var op = "";

                $.ajax({
                    type:'get',
                    url:'{!!URL::to('/package/details')!!}',
                    data:{'id':package_id},
                    dataType:'json',//return data will be json
                    success:function(data){
                        // console.log("price");
                        console.log(data);

                        $('.min-person').attr("min",data.min_person);
                        $('.min-person').val(data.min_person);
                        $('.min-person').attr("max",data.max_person);

                        $('.price').val(data.budget);

                    },
                    error:function(){

                    }
                });



            });
        });

        jQuery(document).ready(function($) {

            $('#travel-review').on('submit', function (e){
                e.preventDefault();
                var id = $('#id').val();
                var cleanliness = $('#cleanliness').val();
                var comfort = $('#comfort').val();
                var staf = $('#staf').val();
                var facilities = $('#facilities').val();
                var score = $('#score').val();
                var name = $('#name').val();
                var email = $('#email').val();
                var comment = $('#comment').val();


                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    type: "POST",
                    dataType: "json",
                    url: '/travel-review',
                    data: {'id': id,'cleanliness': cleanliness,'comfort': comfort,'staf': staf,'facilities':facilities,'score':score,'name':name,'email':email,'comment':comment},
                    success: function(data){
                        location.reload();
                    }
                });
            })
        })
    </script>
@endsection
