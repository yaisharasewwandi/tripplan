@extends('frontend.layouts.master')
@section('content')
    <!--  wrapper  -->
    <div id="wrapper">
        <!-- content-->
        <div class="content">

            <section  id="sec1" class="grey-b lue-bg middle-padding">
                <div class="container">
                    @if ($errors->any())
                        <div class="alert" style=" padding: 20px;background-color: #fc8c84; /* Red */color: white;margin-bottom: 15px; margin-top: 40px;">
                            <span class="closebtn" style="margin-left: 15px;color: white;font-weight: bold;float: right;font-size: 22px;line-height: 20px;cursor: pointer;transition: 0.3s;" onclick="this.parentElement.style.display='none';">&times;</span>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach

                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-2">

                        </div>

                        <div class="col-md-8">
                            <div class="list-single-main-item fl-wrap">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>To get stared</h3>
                                </div>

                                <div id="contact-form">
                                    <div id="message"></div>
                                    <form  class="custom-form" action="/add-businessstep3" method="post" name="" id="">
                                        @csrf
                                        <fieldset>

                                            <div class="row" style="margin-left: 40px; ">
                                                <!--col -->
                                                <div class="col-md-6">
                                                    <div class="add-list-media-header" style="margin-bottom:20px">
                                                        <label class="radio inline">
                                                            <input type="radio" name="otherbiss_type" value="1" @if(Session::get('business.otherbiss_type') == '1') checked @endif id="hotel">
                                                            <span>Hotels </span>
                                                        </label>
                                                    </div>

                                                </div>
                                                <!--col end-->

                                                <!--col -->
                                                <div class="col-md-6">
                                                    <div class="add-list-media-header" style="margin-bottom:20px">
                                                        <label class="radio inline">
                                                            <input type="radio" name="otherbiss_type"  value="2"  @if(Session::get('business.otherbiss_type') == '2') checked @endif id="restaurant">
                                                            <span>Restaurant</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <!--col end-->

                                            </div>
                                            <div class="row" id="other-bis-hotel" style="margin-left: 40px; ">
                                                <div class="col-md-12">
                                                <div class="listsearch-input-item">
                                                    <select data-placeholder="Apartments" name="accommodation_type" value="{{ session()->get('business.accommodation_type') }}" class="chosen-select no-search-select" >

                                                        @foreach ($accommodation_types as $key => $value)
                                                            <option value="{{ $key }}">
                                                                {{$value}}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="row" id="other-bis-restaurant" style="margin-left: 40px; ">
                                                <div class="col-md-12">
                                                    <div class="listsearch-input-item">
                                                        <select data-placeholder="Apartments" name="restaurant_type" value="{{ session()->get('business.restaurant_type') }}" class="chosen-select no-search-select" >
                                                            @foreach ($restaurant_types as $keys => $values)
                                                                <option value="{{ $keys }}">
                                                                    {{$values}}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <a class="btn float-btn color-bg" href="/add-businessstep2" style="margin-top:15px; margin-right: 10px;" id="submit">Back<i class="fal fa-angle-left"></i></a>
                                        <button class="btn float-btn color2-bg" type="submit" style="margin-top:15px;" id="submit">Continue<i class="fal fa-angle-right"></i></button>
                                    </form>
                                </div>
                                <!-- contact form  end-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-decor"></div>
            </section>
            <!-- section end -->
        </div>
        <!-- content end-->
    </div>
    <!--wrapper end -->

@endsection
@section('script')

    <script>

        $(function() {
        $('input[name="otherbiss_type"]').on('click', function() {
        if ($(this).val() == 1) {
        $('#other-bis-hotel').show();
        $('#other-bis-restaurant').hide();
        }
        else {
        $('#other-bis-hotel').hide();
        $('#other-bis-restaurant').show();
        }
        });
        $('#other-bis-hotel').hide();
        $('#other-bis-restaurant').hide();
        });

    </script>

@endsection