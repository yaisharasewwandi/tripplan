
@extends('frontend.layouts.master')
@section('content')

    <!--  wrapper  -->
    <div id="wrapper">
        <!-- content-->
        <div class="content">
            <div class="breadcrumbs-fs fl-wrap">
            </div>
            <section class="middle-padding gre y-blue-bg">
                <div class="container">
                    <div class="list-main-wrap-title single-main-wrap-title fl-wrap">
                        <h2>Booking form for : <span>Add your workplace</span></h2>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="bookiing-form-wrap">
                                <ul id="progressbar">
                                    <li class="active"><span>01.</span>Basic Info</li>
                                    <li><span>02.</span>Your Socials</li>
                                    <li><span>03.</span>Photos</li>
                                    <li><span>04.</span>Payment & Policies</li>
                                </ul>
                                <!--   list-single-main-item -->
                                <div class="list-single-main-item fl-wrap hidden-section tr-sec">
                                    <div class="profile-edit-container">
                                        @if ($errors->any())
                                            <div class="alert" style=" padding: 20px;background-color: #fc8c84; /* Red */color: white;margin-bottom: 15px; margin-top: 40px;">
                                                <span class="closebtn" style="margin-left: 15px;color: white;font-weight: bold;float: right;font-size: 22px;line-height: 20px;cursor: pointer;transition: 0.3s;" onclick="this.parentElement.style.display='none';">&times;</span>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach

                                            </div>
                                        @endif
                                        <div class="custom-form">
                                            <form action="/add-businessstep-yourself" method="post" enctype="multipart/form-data">
                                                @csrf
                                                <fieldset class="fl-wrap">
                                                    <div class="list-single-main-item-title fl-wrap">
                                                        <h3>Basic Information</h3>
                                                    </div>
                                                    <div class="row" style="margin-left: 40px; ">
                                                        <!--col -->
                                                        <div class="col-md-6">
                                                            <div class="add-list-media-header" style="margin-bottom:20px">
                                                                <label class="radio inline">
                                                                    <input type="radio" name="yourself_type" value="1" @if(Session::get('business.otherbiss_type') == '1') checked @endif id="guide">
                                                                    <span>Guide for Travel</span>
                                                                </label>
                                                            </div>

                                                        </div>
                                                        <!--col end-->

                                                        <!--col -->
                                                        <div class="col-md-6">
                                                            <div class="add-list-media-header" style="margin-bottom:20px">
                                                                <label class="radio inline">
                                                                    <input type="radio" name="yourself_type"  value="2"  @if(Session::get('business.otherbiss_type') == '2') checked @endif id="agency">
                                                                    <span>Agencies</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <!--col end-->

                                                    </div>

                                                    <div class="list-single-main-item-title fl-wrap">
                                                        <h3>Basic Information</h3>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <label>What's the name of your property <i class="far fa-briefcase"></i></label>
                                                            <input type="text" name="company_name" placeholder="Your Name" value="{{ session()->get('business.company_name') }}"/>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label>Start rating<i class="far fa-envelope"></i>  </label>
                                                            <div class="listsearch-input-item">
                                                                <select data-placeholder="Apartments" name="rating" class="chosen-select no-search-select" >
                                                                    <option>start rating</option>
                                                                    <option value="1">1 *</option>
                                                                    <option value="2">2 **</option>
                                                                    <option value="3">3 ***</option>
                                                                    <option value="4">4 ****</option>
                                                                    <option value="5">5 *****</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">

                                                        </div>
                                                    </div>

                                                    <div class="list-single-main-item-title fl-wrap">
                                                        <h3>Contact Your Property</h3>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label>First Name<i class="far fa-user"></i></label>
                                                            <input type="text" name="f_name" placeholder="Your Name" value="{{ session()->get('business.f_name')}}"/>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label>Last Name<i class="far fa-user"></i></label>
                                                            <input type="text" name="l_name" placeholder="Your Name" value="{{ session()->get('business.l_name')}}"/>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label>Mobile<i class="far fa-phone"></i></label>
                                                            <input type="text" name="contact_number" placeholder="Your contact number" value="{{ session()->get('business.contact_number') }}"/>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label>Place number<i class="far fa-phone"></i></label>
                                                            <input type="text" name="place_contact_number" placeholder="Your place contact number" value="{{ session()->get('business.contact_number_place') }}"/>
                                                        </div>
                                                    </div>
                                                    <div id="location-area">
                                                    <div class="list-single-main-item-title fl-wrap">
                                                        <h3>Where's your property location</h3>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label>District</label>
                                                            <div class="listsearch-input-item">
                                                                <select data-placeholder="Apartments" name="district_id" id="districtid" class="chosen-select no-search-select" >
                                                                    <option>All Districts</option>
                                                                    @foreach ($districts as $key => $value)
                                                                        <option value="{{ $key }}">
                                                                            {{$value}}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label>Town</label>
                                                            <div class="listsearch-input-item">
                                                                <select data-placeholder="Apartments" name="town_id" id="townid" class="chosen-select no-search-select" >
                                                                    <option>All Towns</option>
                                                                    @foreach ($towns as $data => $town)
                                                                        <option value="{{ $data }}">
                                                                            {{$town}}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <label>Address<i class="fal fa-map-marker"></i></label>
                                                    <input type="text" name="place_address" placeholder="Address of your Place" value=""/>
                                                    <label>City / Location<i class="fal fa-map-marker"></i></label>
                                                    <input type="text" name="location" id="autocomplete" placeholder="Location of your Place" value=""/>
                                                    <div class="row">
                                                        <div class="col-md-6" id="long_area">
                                                            <label>Longitude (Drag marker on the map)<i class="fal fa-long-arrow-alt-right"></i>  </label>
                                                            <input type="text" name="lng" id="longitude" placeholder="Map Longitude"   value=""/>
                                                        </div>
                                                        <div class="col-md-6" id="lat_area">
                                                            <label>Latitude (Drag marker on the map) <i class="fal fa-long-arrow-alt-down"></i> </label>
                                                            <input type="text" name="lat" id="latitude" placeholder="Map Latitude"   value=""/>
                                                        </div>
                                                    </div>
                                                    </div>

                                                    <span class="fw-separator"></span>
                                                    <a  href="#"  class="next-form action-button btn no-shdow-btn color-bg">Continue<i class="fal fa-angle-right"></i></a>
                                                </fieldset>
                                                <fieldset class="fl-wrap">
                                                    <div class="box-widget-item-header mat-top">
                                                        <h3>Your  Socials</h3>
                                                    </div>
                                                    <!-- profile-edit-container-->
                                                    <div class="profile-edit-container">
                                                        <div class="custom-form">
                                                            <label>Facebook Page<i class="fab fa-facebook"></i></label>
                                                            <input type="text" name="fb_page_link" placeholder="https://www.facebook.com/" value=""/>
                                                            <label>Twitter<i class="fab fa-twitter"></i>  </label>
                                                            <input type="text" name="twiter_link" placeholder="https://twitter.com/" value=""/>
                                                            <label> Instagram <i class="fab fa-instagram"></i>  </label>
                                                            <input type="text" name="instergram_link" placeholder="https://www.instagram.com/" value=""/>

                                                        </div>
                                                    </div>
                                                    <a  href="#"  class="previous-form action-button back-form   color-bg"><i class="fal fa-angle-left"></i> Back</a>
                                                    <a  href="#"  class="next-form back-form action-button btn no-shdow-btn color-bg">Continue <i class="fal fa-angle-right"></i></a>
                                                </fieldset>
                                                <fieldset class="fl-wrap">
                                                    <div class="list-single-main-item-title fl-wrap">
                                                        <h3>Photos</h3>
                                                    </div>
                                                    <div class="profile-edit-container">
                                                        <div class="custom-form">
                                                            <div class="row">

                                                                <div class="col-md-4">
                                                                    <!-- act-widget-->
                                                                    <div class="act-widget fl-wrap">

                                                                        <div class="add-list-media-wrap">
                                                                            <label>Image</label>
                                                                            <forms class="fuzone">
                                                                                <div class="fu-text">
                                                                                    <span><i class="fal fa-image"></i> Click here or drop files to upload</span>
                                                                                </div>
                                                                                <input type="file" name="image" id="exampleInputFile" class="upload">
                                                                            </forms>
                                                                        </div>
                                                                    </div>
                                                                    <!-- act-widget end-->
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <span class="fw-separator"></span>
                                                    <a  href="#"  class="previous-form action-button back-form   color-bg"><i class="fal fa-angle-left"></i> Back</a>
                                                    <a  href="#"  class="next-form back-form action-button btn no-shdow-btn color-bg">Continue<i class="fal fa-angle-right"></i></a>
                                                </fieldset>
                                                <fieldset class="fl-wrap">
                                                    <div class="list-single-main-item-title fl-wrap">
                                                        <h3>Guest Payment Option</h3>
                                                    </div>
                                                    <p>Specify the payment type you accept tax details, other options like additional charges</p>

                                                    <div class="col-md-8">
                                                        <label>Can You Charge credit cards at the property?</label>
                                                        <div class="row" style="margin-left: 10px; float: left;  ">
                                                            <!--col -->
                                                            <div class="col-md-6">
                                                                <div class="add-list-media-header" style="margin-bottom:20px">
                                                                    <label class="radio inline">
                                                                        <input type="radio" name="payment_option" value="1" id="yes">
                                                                        <span>Yes</span>
                                                                    </label>
                                                                </div>

                                                            </div>
                                                            <!--col end-->

                                                            <!--col -->
                                                            <div class="col-md-6">
                                                                <div class="add-list-media-header" style="margin-bottom:20px">
                                                                    <label class="radio inline">
                                                                        <input type="radio" name="payment_option"  value="2"  id="no">
                                                                        <span>No</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <!--col end-->

                                                        </div>
                                                    </div>
                                                    <!-- profile-edit-container-->
                                                    <div class="profile-edit-container" id="card-type">
                                                        <div class="custom-form">
                                                            <!-- Checkboxes -->
                                                            <ul class="fl-wrap filter-tags">
                                                                @foreach($card_types as $card_type)
                                                                    <li>
                                                                        <input id="check-aaa5" type="checkbox" name="card_type[]" value="{{$card_type['id']}}">
                                                                        <span><img src="{{$card_type['image_path']}}" style="    width: 5%; height: 26px;float: left;margin-left: 4px;" alt=""></span>

                                                                        <label for="check-aaa5">{{$card_type['name']}}</label>
                                                                    </li>
                                                                @endforeach

                                                            </ul>
                                                            <!-- Checkboxes end -->
                                                        </div>
                                                    </div>
                                                    <div class="list-single-main-item-title fl-wrap">
                                                        <h3>Terms & Condition</h3>
                                                    </div>
                                                    <div class="filter-tags">
                                                        <input id="check-a" type="checkbox" name="term_condition" value="1">
                                                        <label for="check-a">By continuing, you agree to the<a href="#" target="_blank">Terms and Conditions</a>.</label>
                                                    </div>
                                                    <span class="fw-separator"></span>
                                                    <a href="#"  class="previous-form action-button  back-form   color-bg"><i class="fal fa-angle-left"></i> Back</a>
                                                    {{--<button  type="submit" class="next-form back-form action-button btn no-shdow-btn color-bg">Complete Register and open the dashboard<i class="fal fa-angle-right"></i></button>--}}
                                                    <button class=" btn    color2-bg  float-btn"  style="float: right" type="submit">Complete Register and open the dashboard<i class="fal fa-save"></i></button>
                                                </fieldset>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!--   list-single-main-item end -->
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            <!-- section end -->
        </div>
        <!-- content end-->
    </div>
    <!--wrapper end -->

@endsection
@section('script')
    <script>

        $(function() {
        $('input[name="yourself_type"]').on('click', function() {
        if ($(this).val() == 2) {
        $('#location-area').show();
        }
        else {
        $('#location-area').hide();
        }
        });
        $('#location-area').hide();
        });

    </script>
    <
    <script>

        $(function() {
        $('input[name="payment_option"]').on('click', function() {
        if ($(this).val() == 2) {
        $('#card-type').hide();
        }
        else {
        $('#card-type').show();
        }
        });
        $('#card-type').hide();
        });

    </script>
    <script>
    $(document).ready(function() {
        $('select[name="district_id"]').on('change', function() {
            var districtID = $(this).val();
            alert(districtID);
            if(districtID) {
                $.ajax({
                    url: '/myform/ajax/'+districtID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {


                        $('select[name="town_id"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="town_id"]').append('<option value="'+ data +'">'+ town +'</option>');
                        });


                    }
                });
            }else{
                $('select[name="town_id"]').empty();
            }
        });
    });
</script>
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyANuk6NO4FfxgvcQrb8N70ZDCKF-vWh6Jg&libraries=places&callback=initAutocomplete" type="text/javascript"></script>
    <script>
   $(document).ready(function() {
        $("#lat_area").addClass("d-none");
        $("#long_area").addClass("d-none");
   });
</script>
    <script>
   google.maps.event.addDomListener(window, 'load', initialize);

   function initialize() {
       var input = document.getElementById('autocomplete');
       var autocomplete = new google.maps.places.Autocomplete(input);
       autocomplete.addListener('place_changed', function() {
           var place = autocomplete.getPlace();
           $('#latitude').val(place.geometry['location'].lat());
           $('#longitude').val(place.geometry['location'].lng());
           $("#lat_area").removeClass("d-none");
           $("#long_area").removeClass("d-none");
       });
   }
</script>


@endsection