@extends('frontend.layouts.master')
@section('content')
    <!--  wrapper  -->
    <div id="wrapper">
        <!-- content-->
        <div class="content">

            <section  id="sec1" class="grey-b lue-bg middle-padding">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <!--   list-single-main-item -->
                            <div class="list-single-main-item fl-wrap">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>Our Office </h3>
                                </div>
                                <div class="list-single-main-media fl-wrap">
                                    <img src="images/all/8.jpg" alt="" class="respimg">
                                </div>
                                <p>Praesent eros turpis, commodo vel justo at, pulvinar mollis eros. Mauris aliquet eu quam id ornare. Morbi ac quam enim. Cras vitae nulla condimentum, semper dolor non, faucibus dolor.   </p>
                                <div class="list-single-main-item-title fl-wrap mar-top">
                                    <h3>Working Hours </h3>
                                </div>
                                <ul class="cat-item">
                                    <li><a href="#">Monday to Friday</a> <span>9am - 7pm</span></li>
                                    <li><a href="#">Saturday to Sunday </a> <span>Closed</span></li>
                                </ul>
                            </div>
                            <!--   list-single-main-item end -->
                        </div>
                        @if ($errors->any())
                            <div class="alert" style=" padding: 20px;background-color: #fc8c84; /* Red */color: white;margin-bottom: 15px; margin-top: 40px;">
                                <span class="closebtn" style="margin-left: 15px;color: white;font-weight: bold;float: right;font-size: 22px;line-height: 20px;cursor: pointer;transition: 0.3s;" onclick="this.parentElement.style.display='none';">&times;</span>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach

                            </div>
                        @endif
                        <div class="col-md-8">
                            <div class="list-single-main-item fl-wrap">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>Create Your Partner Account</h3>
                                </div>
                                <p>Create an account to list and manage your property.</p>
                                <div id="contact-form">
                                    <div id="message"></div>
                                    <form  class="custom-form" action="/add-businessstep2" method="post" name="contactform" id="">
                                        @csrf
                                        <fieldset>

                                            <div class="clearfix"></div>
                                            <label><i class="fal fa-envelope"></i>  </label>
                                            <input type="email"  name="account_email" value="{{ session()->get('business.account_email') }}" id="account_email" placeholder="Email Address*" />
                                            <div class="list-single-main-item-title fl-wrap">
                                                <h3>Contact Details</h3>
                                            </div>
                                            <p>Your full name and phone number are needed to ensure the security of TripPlan.com account.</p>
                                            <div class="row">
                                                <!--col -->
                                                <div class="col-md-6">
                                                    <label><i class="fal fa-user"></i></label>
                                                    <input type="text" name="f_name" id="f_name" placeholder="Your First Name *" value="{{ session()->get('business.f_name') }}"/>
                                                </div>
                                                <div class="col-md-6">
                                                    <label><i class="fal fa-user"></i></label>
                                                    <input type="text" name="l_name" id="l_name" placeholder="Your Last Name *" value="{{ session()->get('business.l_name') }}"/>
                                                </div>
                                            </div>
                                            <label><i class="fal fa-phone"></i>  </label>
                                            <input type="text"  name="contact_number"  placeholder="Contact Number*" value="{{ session()->get('business.contact_number') }}"/>
                                        </fieldset>
                                        <a class="btn float-btn color-bg" href="/add-businessstep1" style="margin-top:15px; margin-right: 10px;" id="submit">Back<i class="fal fa-angle-left"></i></a>
                                        <button class="btn float-btn color2-bg" type="submit" style="margin-top:15px;" id="submit">Continue<i class="fal fa-angle-right"></i></button>
                                    </form>
                                </div>
                                <!-- contact form  end-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-decor"></div>
            </section>
            <!-- section end -->
        </div>
        <!-- content end-->
    </div>
    <!--wrapper end -->

@endsection
@section('script')

    <script>



    </script>

@endsection