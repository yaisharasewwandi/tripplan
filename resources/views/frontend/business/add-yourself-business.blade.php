@extends('frontend.layouts.master')
@section('content')
    <!--  wrapper  -->
    <div id="wrapper">
        <!-- content-->
        <div class="content">

            <section  id="sec1" class="grey-b lue-bg middle-padding">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2">

                        </div>
                        @if ($errors->any())
                            <div class="alert" style=" padding: 20px;background-color: #fc8c84; /* Red */color: white;margin-bottom: 15px; margin-top: 40px;">
                                <span class="closebtn" style="margin-left: 15px;color: white;font-weight: bold;float: right;font-size: 22px;line-height: 20px;cursor: pointer;transition: 0.3s;" onclick="this.parentElement.style.display='none';">&times;</span>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach

                            </div>
                        @endif
                        <div class="col-md-8">
                            <div class="list-single-main-item fl-wrap">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>To get stared</h3>
                                </div>

                                <div id="contact-form">
                                    <div id="message"></div>
                                    <form  class="custom-form" action="/add-your-businessstep" method="post" name="contactform" id="">
                                        @csrf
                                        <fieldset>

                                            <div class="row" style="margin-left: 40px; ">
                                                <!--col -->
                                                <div class="col-md-4">
                                                    <div class="add-list-media-header" style="margin-bottom:20px">
                                                        <label class="radio inline">
                                                            <input type="radio" name="yourself_type" value="1" @if(Session::get('business.yourself_type') == '1') checked @endif id="yourself_bis">
                                                            <span>Guide for Travel</span>
                                                        </label>
                                                    </div>

                                                </div>
                                                <!--col end-->
                                                <!--col -->
                                                <div class="col-md-4">
                                                    <div class="add-list-media-header" style="margin-bottom:20px">
                                                        <label class="radio inline">
                                                            <input type="radio" name="yourself_type"  value="2"  @if(Session::get('business.yourself_type') == '2') checked @endif id="other_bis">
                                                            <span>Agencies</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <!--col end-->

                                            </div>
                                             </fieldset>
                                        <a class="btn float-btn color-bg" href="/add-businessstep2" style="margin-top:15px; margin-right: 10px;" id="submit">Back<i class="fal fa-angle-left"></i></a>
                                        <button class="btn float-btn color2-bg" type="submit" style="margin-top:15px;" id="submit">Continue<i class="fal fa-angle-right"></i></button>
                                    </form>
                                </div>
                                <!-- contact form  end-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-decor"></div>
            </section>
            <!-- section end -->
        </div>
        <!-- content end-->
    </div>
    <!--wrapper end -->

@endsection
@section('script')

    <script>



    </script>

@endsection