@extends('frontend.layouts.master')
@section('content')
<!--  wrapper  -->
{{--            @include('frontend.layouts.includes.profile-nav')--}}
<div id="wrapper">
    <!-- content-->
    <div class="content">
        <!-- section-->
        <section class="flat-header color-bg adm-header">
            <div class="wave-bg wave-bg2"></div>
            <div class="container">
                <div class="dasboard-wrap fl-wrap">
                    <div class="dasboard-breadcrumbs breadcrumbs"><a href="#">Home</a><a href="#">Dashboard</a><span>Profile page</span></div>
                    <!--dasboard-sidebar-->
                    <div class="dasboard-sidebar">
                        <div class="dasboard-sidebar-content fl-wrap">
                            <div class="dasboard-avatar">
                                @if(!empty(Auth::user()->image_path))
                                <img src="{{Auth::user()->image_path}}" alt="">
                                @else
                                    <img src="{{asset('frontend/images/user.png')}}" alt="">
                                @endif
                            </div>
                            <div class="dasboard-sidebar-item fl-wrap">
                                <h3>
                                    <span>Welcome </span>
                                    {{Auth::user()->name}}
                                </h3>
                            </div>
                            <button id="hott" class="ed-btn" ><a href="{{route('add-listing')}}" style="color: #ffffff;" >Add Hotel</a></button>

                            <a  href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="log-out-btn color-bg">
                                {{ __('Log Out') }}
                                <i class="far fa-sign-out"></i>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>

                        </div>
                    </div>
                    <!--dasboard-sidebar end-->
                    <!-- dasboard-menu-->
                    <div class="dasboard-menu">
                        <div class="dasboard-menu-btn color3-bg">Dashboard Menu <i class="fal fa-bars"></i></div>
                        <ul class="dasboard-menu-wrap">
                            <li>
                                <a href="{{route('customer-edit-profile')}}" class="user-profile-"><i class="far fa-user"></i>Profile</a>

                            </li>

                            <li><a href="{{route('listing-booking')}}" class="user-profile-act"> <i class="far fa-calendar-check"></i> Bookings <span></span></a></li>
                            {{--                                        <li><a href="dashboard-review.html"><i class="far fa-comments"></i> Reviews </a></li>--}}
                        </ul>
                    </div>

                </div>
            </div>
        </section>
        <!-- section end-->

                    <!-- section-->
                    <section class="middle-padding">
                        <div class="container">
                            <!--dasboard-wrap-->
                            <div class="dasboard-wrap fl-wrap">
                                <!-- dashboard-content-->
                                <div class="dashboard-content fl-wrap">
                                    <div class="dashboard-list-box fl-wrap">
                                        <div class="dashboard-header fl-wrap">
                                            <h3>Bookings Accommodation</h3>
                                        </div>
                                        <!-- dashboard-list end-->
                                        @if(!empty($booking))
                                        @foreach($booking as $book)
                                        <div class="dashboard-list">
                                            <div class="dashboard-message">
{{--                                                <span class="new-dashboard-item">New</span>--}}

                                                @foreach($book->room->images as $room)
                                                <div class="dashboard-message-avatar">
                                                    <img src="{{asset($room->room_image_name)}}" alt="">
                                                </div>
                                                    @break
                                                @endforeach
                                                <div class="dashboard-message-text">
                                                    <h4>{{$book->user->name}} - <span>{{$book->from_to}}</span></h4>
                                                    <div class="booking-details fl-wrap">
                                                        <span class="booking-title">Listing Item :</span> :
                                                        <span class="booking-text">{{$book->room->accommodation->title}} - {{$book->room->room_name}}</span>
                                                    </div>
                                                    <div class="booking-details fl-wrap">
                                                        <span class="booking-title">Persons :</span>
                                                        <span class="booking-text">{{$book->adults}} Adults</span>
                                                        <span class="booking-text">{{$book->child}} Child</span>
                                                    </div>
                                                    <div class="booking-details fl-wrap">
                                                        <span class="booking-title">Booking Date :</span>
                                                        <span class="booking-text">{{$book->from_to}}</span>
                                                    </div>
                                                    <div class="booking-details fl-wrap">
                                                        <span class="booking-title">Mail :</span>
                                                        <span class="booking-text">{{$book->user->email}}</span>
                                                    </div>
                                                    <div class="booking-details fl-wrap">
                                                        <span class="booking-title">Phone :</span>
                                                        <span class="booking-text">{{$book->user->contact_number}}</span>
                                                    </div>
                                                    <div class="booking-details fl-wrap">
                                                        <span class="booking-title">Booking Status :</span>
                                                        <span class="booking-text"> <strong class="done-paid">@if($book->approve ==0) Not Approve @else Approve @endif </strong> </span>
                                                    </div>
{{--                                                    <span class="fw-separator"></span>--}}
{{--                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc posuere convallis purus non cursus. Cras metus neque, gravida sodales massa ut. </p>--}}
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        @endif

                                    </div>

                                    <!-- pagination-->
                                    {{$booking->links()}}
                                </div>
                                <!-- dashboard-list-box end-->
                                <div class="dashboard-content fl-wrap">
                                    <div class="dashboard-list-box fl-wrap">
                                        <div class="dashboard-header fl-wrap">
                                            <h3>Bookings Car</h3>
                                        </div>
                                        <!-- dashboard-list end-->
                                        @if(!empty($cars))
                                            @foreach($cars as $car)
                                                <div class="dashboard-list">
                                                    <div class="dashboard-message">
                                                        {{--                                                <span class="new-dashboard-item">New</span>--}}

                                                        @foreach($car->car->images as $image)
                                                            <div class="dashboard-message-avatar">
                                                                <img src="{{asset($image->image_name)}}" alt="">
                                                            </div>
                                                            @break
                                                        @endforeach
                                                        <div class="dashboard-message-text">
                                                            <h4>{{$car->user->name}} - <span>{{$car->from}} - {{$car->to}}</span></h4>
                                                            <div class="booking-details fl-wrap">
                                                                <span class="booking-title">Listing Item :</span> :
                                                                <span class="booking-text">{{$car->car->title}}</span>
                                                            </div>

                                                            <div class="booking-details fl-wrap">
                                                                <span class="booking-title">Booking Date :</span>
                                                                <span class="booking-text">{{$car->from}} - {{$car->to}}</span>
                                                            </div>
                                                            <div class="booking-details fl-wrap">
                                                                <span class="booking-title">Mail :</span>
                                                                <span class="booking-text">{{$car->user->email}}</span>
                                                            </div>
                                                            <div class="booking-details fl-wrap">
                                                                <span class="booking-title">Phone :</span>
                                                                <span class="booking-text">{{$car->user->contact_number}}</span>
                                                            </div>
                                                            <div class="booking-details fl-wrap">
                                                                <span class="booking-title">Booking Status :</span>
                                                                <span class="booking-text"> <strong class="done-paid">@if($car->approve ==0) Not Approve @else Approve @endif </strong> </span>
                                                            </div>
                                                            {{--                                                    <span class="fw-separator"></span>--}}
                                                            {{--                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc posuere convallis purus non cursus. Cras metus neque, gravida sodales massa ut. </p>--}}
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif

                                    </div>

                                    <!-- pagination-->
                                    {{$cars->links()}}
                                </div>
                            @if(!empty($tourbooks))
                            <!-- dasboard-wrap end-->
                            <div class="dashboard-content fl-wrap">
                                <div class="dashboard-list-box fl-wrap">
                                    <div class="dashboard-header fl-wrap">
                                        <h3>Bookings Tour Package</h3>
                                    </div>
                                    <!-- dashboard-list end-->

                                        @foreach($tourbooks as $tourbook)
                                            <div class="dashboard-list">
                                                <div class="dashboard-message">
                                                    {{--                                                <span class="new-dashboard-item">New</span>--}}

                                                    @foreach($tourbook->tour->images as $image)
                                                        <div class="dashboard-message-avatar">
                                                            <img src="{{asset($image->image_name)}}" alt="">
                                                        </div>
                                                        @break
                                                    @endforeach
                                                    <div class="dashboard-message-text">
                                                        <h4>{{$tourbook->user->name}} - <span>{{$tourbook->from}} - {{$tourbook->to}}</span></h4>
                                                        <div class="booking-details fl-wrap">
                                                            <span class="booking-title">Listing Item :</span> :
                                                            <span class="booking-text">{{$tourbook->tour->title}}</span>
                                                        </div>

                                                        <div class="booking-details fl-wrap">
                                                            <span class="booking-title">Booking Date :</span>
                                                            <span class="booking-text">{{$tourbook->from}} - {{$tourbook->to}}</span>
                                                        </div>
                                                        <div class="booking-details fl-wrap">
                                                            <span class="booking-title">Mail :</span>
                                                            <span class="booking-text">{{$tourbook->user->email}}</span>
                                                        </div>
                                                        <div class="booking-details fl-wrap">
                                                            <span class="booking-title">Phone :</span>
                                                            <span class="booking-text">{{$tourbook->user->contact_number}}</span>
                                                        </div>
                                                        <div class="booking-details fl-wrap">
                                                            <span class="booking-title">Booking Status :</span>
                                                            <span class="booking-text"> <strong class="done-paid">@if($tourbook->approve ==0) Not Approve @else Approve @endif </strong> </span>
                                                        </div>
                                                        {{--                                                    <span class="fw-separator"></span>--}}
                                                        {{--                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc posuere convallis purus non cursus. Cras metus neque, gravida sodales massa ut. </p>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach


                                </div>

                                <!-- pagination-->
                                {{$tourbooks->links()}}
                            </div>
                                @endif
                        </div>
                        </div>
                    </section>
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!-- content end-->
            </div>
            <!--wrapper end -->
@endsection
