

                @foreach($wishlists as $wislist)
                    @foreach($wislist->accommodations as $accommodation)
                        <li class="clearfix" >
                            @foreach($accommodation->images as $image)
                                <a href="#"  class="widget-posts-img"><img src="{{asset($image->accommodation_image)}}" class="respimg" alt=""></a>
                            @endforeach
                            <div class="widget-posts-descr">
                                <a href="#" title="">{{$accommodation->title}}</a>
                                <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                                <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i> {{$accommodation->location}}</a></div>
                                <span class="rooms-price">$80 <strong> /  Awg</strong></span>
                            </div>
                        </li>
                    @endforeach
                @endforeach



