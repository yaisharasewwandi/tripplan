@extends('frontend.layouts.master')
@section('content')

    <!--  wrapper  -->
    <div id="wrapper">
        <!-- content-->
        <div class="content">
            <!--  section  -->
            <section class="color-bg middle-padding ">
                <div class="wave-bg wave-bg2"></div>
                <div class="container">
                    <div class="flat-title-wrap">
                        <h2><span>Our Blogs</span></h2>
                        <span class="section-separator"></span>
                        <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut nec tincidunt arcu, sit amet fermentum sem.</h4>
                    </div>
                </div>
            </section>
            <!--  section  end-->
            <div class="breadcrumbs-fs fl-wrap">
                <div class="container">
                    <div class="breadcrumbs fl-wrap"><a href="#">Home</a><a href="#">Blog</a><span>Blog Masonry</span></div>
                </div>
            </div>
            <!-- section-->
            <section  id="sec1" class="middle-padding grey-blue-bg">
                <div class="container">
                    <div class="row">
                        <!--blog content -->
                        <div class="col-md-8" >
                            <!--post-container -->
                            <div class="post-container fl-wrap searchblg"  id="searchblg">
                             @foreach($blogs as $key=>$blog)
                                <!--article-masonry -->
                                <div class="article-masonry" id="blog-div">
                                    <article class="card-post">
                                        <div class="card-post-img fl-wrap">
                                            <a href="{{route('blog_single',$blog->id)}}"><img  src="{{asset($blog->image)}}"   alt=""></a>
                                        </div>
                                        <div class="card-post-content fl-wrap">
                                            <h3><a href="{{route('blog_single',$blog->id)}}">{{$blog->title}}</a></h3>
                                            <p>{{$blog->description}}</p>
                                            <div class="post-author"><a href="#"><img src="{{asset($blog->users->image_path)}}" alt=""><span>By ,{{$blog->users->name}}</span></a></div>
                                            <div class="post-opt">
                                                {{--<ul>--}}
                                                    {{--<li><i class="fal fa-calendar"></i> <span>25 April 2018</span></li>--}}
                                                    {{--<li><i class="fal fa-eye"></i> <span>264</span></li>--}}
                                                    {{--<li><i class="fal fa-tags"></i> <a href="#">Design</a>  </li>--}}
                                                {{--</ul>--}}
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                @endforeach
                                <!--article-masonry end -->



                                <!-- pagination-->
                             {{ $blogs->links() }}
{{--                                <div class="pagination">--}}

{{--                                    <a href="#" class="prevposts-link"><i class="fa fa-caret-left"></i></a>--}}
{{--                                    <a href="#" >1</a>--}}
{{--                                    <a href="#" class="current-page">2</a>--}}
{{--                                    <a href="#">3</a>--}}
{{--                                    <a href="#">4</a>--}}
{{--                                    <a href="#" class="nextposts-link"><i class="fa fa-caret-right"></i></a>--}}
{{--                                </div>--}}
                                <!-- pagination end-->
                            </div>
                            <!--post-container end -->
                        </div>
                        <!-- blog content end -->

                        <!--   sidebar  -->
                        <div class="col-md-4">
                            <!--box-widget-wrap -->
                            <div class="box-widget-wrap fl-wrap">
                                <!--box-widget-item -->
                                <div class="box-widget-item fl-wrap">
                                    <div class="box-widget">
                                        <div class="box-widget-content">
                                            <div class="box-widget-item-header">
                                                <h3> Search In Blog </h3>
                                            </div>
                                            <div class="search-widget">
                                                <form  class="fl-wrap" action="" method="get"  id="blog-form">
                                                    <input name="blog"  id="blog" type="text" class="search" placeholder="Search.."  />
                                                    <button type="submit" class="search-submit color2-bg" id="submit_btn"><i class="fal fa-search transition"></i> </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--box-widget-item end -->
                                <!--box-widget-item -->
                                <div class="box-widget-item fl-wrap">
                                    <div class="box-widget">
                                        <div class="box-widget-content">
                                            <div class="box-widget-item-header">
                                                <h3>About Athor</h3>
                                            </div>
                                            <div class="box-widget-author fl-wrap">
                                                <div class="box-widget-author-title fl-wrap">
                                                    <div class="box-widget-author-title-img">
                                                        <img src="{{asset('frontend/images/avatar/4.jpg')}}" alt="">
                                                    </div>
                                                    <a href="author-single.html">Jessie Manrty</a>
                                                    <span>Co-manager associated</span>
                                                </div>
                                                <div class="box-widget-author-content">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla.</p>
                                                </div>
                                                <div class="list-widget-social">
                                                    <ul>
                                                        <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                                        <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                                        <li><a href="#" target="_blank"><i class="fab fa-vk"></i></a></li>
                                                        <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--box-widget-item end -->


                            </div>
                            <!--box-widget-wrap end -->
                        </div>
                        <!--   sidebar end  -->
                    </div>
                </div>
                <div class="limit-box fl-wrap"></div>
            </section>
            <!-- section end -->
        </div>
        <!-- content end-->
    </div>

@endsection
@section('script')
    <script>
    jQuery(document).ready(function($) {

    $('#blog-form').on('submit', function (e){
    e.preventDefault();
    var keyword = $('#blog').val();
    $.ajax({
            type: "GET",
            dataType: "json",
            url: '/searchBlogList',
            data: {'blog': keyword},
            success: function(data){
            searchblog();

            }
        });
    })
    });
     function searchblog(){
      alert("jj");
    $.ajax({
    type: 'GET',
    url: '{{route("searchBlogList")}}',
    success: function (data) {
    alert("jj");
    $('.searchblg').empty();
    $('.searchblg').append(data);
    }
    });
   }

    </script>
@endsection


