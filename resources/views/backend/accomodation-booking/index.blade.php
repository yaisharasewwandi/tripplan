@extends('backend.layouts.master')
@section('content')

    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Hotel Booking table
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Tables</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Booking table</li>
                    </ol>
                </nav>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"> Booking List</h4>
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="order-listing" class="table">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Hotel</th>
                                        <th>Room</th>
                                        <th>Ref No</th>
                                        <th>Booking From</th>
                                        <th>Booking To</th>
                                        <th>Days</th>
                                        <th>Adults</th>
                                        <th>Child</th>


                                        <th>approve</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($booking as $key => $book)
                                        <tr>
                                            <td>{{ $book->id }}</td>
                                            <td>{{ $book->room->accommodation->title }}</td>
                                            <td>{{ $book->room->room_name }}</td>
                                            <td>{{ $book->ref_no }}</td>
                                            <td>{{ $book->from }}</td>
                                            <td>{{ $book->to }}</td>
                                            <td>{{ $book->days }}</td>
                                            <td>{{ $book->adults }}</td>
                                            <td>{{ $book->child }}</td>
                                            @can('hotel-booking-approve')
                                                @if (Auth::user()->hasRole(['Admin','Super Admin']) )
                                                <td>
                                                    @if($book->approve==1)
                                                        <button type="button" class="btn btn-primary">Approve </button>
                                                    @else
                                                        <button type="button" class="btn btn-danger">pending Approve</button>
                                                    @endif
                                               </td>
                                                @else
                                                    <td>
                                                        <input data-id="{{$book->id}}" class="toggle-class publish-acc" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $book->approve ? 'checked' : '' }}>
                                                    </td>
                                                    <td>
                                                        @if($book->approve==1)
                                                            <button type="button" class="btn btn-primary">Approve </button>
                                                        @else
                                                            <button type="button" class="btn btn-danger">pending Approve</button>
                                                        @endif
                                                    </td>

                                                @endif

                                            @endcan
                                            <td>
{{--                                                <a class="btn btn-outline-primary" href="{{ route('blogs.show',$blog->id) }}">View</a>--}}
                                                @can('hotel-booking-edit')
{{--                                                    <a class="btn btn-outline-info" href="{{ route('blogs.edit',$book->id) }}">Edit</a>--}}
                                                @endcan

                                                @can('hotel-booking-delete')
                                                    {!! Form::open(['method' => 'DELETE','route' => ['accomodation-booking.destroy', $book->id],'style'=>'display:inline']) !!}
                                                    {!! Form::submit('Delete', ['class' => 'btn btn-outline-danger','data-toggle'=>'confirmation']) !!}
                                                    {!! Form::close() !!}
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
{{--        @can('role-create')--}}
{{--            <a href="{{ route('blogs.create') }}">--}}
{{--                <div class="theme-setting-wrapper">--}}
{{--                    <div id="settings-trigger"><i class="fas fa-plus"> </i></div>--}}
{{--                </div></a>--}}
{{--        @endcan--}}
        @endsection
        @section('scripts')
            <script src="{{asset('backend/js/data-table.js')}}"></script>
            <script>
                $(function() {
                    $('.publish-acc').change(function() {
                        var approve = $(this).prop('checked') == true ? 1 : 0;
                        var booking_id = $(this).data('id');

                        $.ajax({
                            type: "GET",
                            dataType: "json",
                            url: '/accbookStatus',
                            data: {'approve': approve, 'booking_id': booking_id},
                            success: function(data){
                                toastr.success('Review successfully');
                                console.log(data.success)


                            },
                            error: function (msg) {
                                toastr.error( 'Inconceivable!');
                            }
                        });
                    })
                })
            </script>
@endsection


