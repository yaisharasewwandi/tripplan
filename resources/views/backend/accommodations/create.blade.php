@extends('backend.layouts.master')
@section('content')

    <div class="main-panel">
        {!! Form::open(array('route' => 'accommodations.store','method'=>'POST','class'=>'forms-sample','files'=>'true')) !!}
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Create Hotel
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Forms</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create Hotel</li>
                    </ol>
                </nav>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-lg-8 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Create Hotel</h4>



                            <div class="form-group">
                                <label for="exampleInputName1">Title</label>
                                {!! Form::text('title', null, array('placeholder' => 'Title','class' => 'form-control')) !!}
                            </div>
                            <div class="form-group" id="">
                                <label for="exampleInputEmail3">About Hotel</label>
                                {!! Form::textarea('description', null, array('placeholder' => 'Description','class' => 'form-control summernote','cols'=>'30','rows'=>'10')) !!}

                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Contact Number</label>
                                    {!! Form::text('contact_number', null, array('placeholder' => 'Contact Number','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Email</label>
                                    {!! Form::email('email', null, array('placeholder' => 'Email','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 grid-margin d-flex stretch-card">
                    <div class="row flex-grow">
                        <div class="col-12 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Property</h4>
                                    <div class="card testDiv">
                                        <div class="card-body ">
                                        @foreach($types as $type)
                                        <div class="form-group">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="radio" name="type_id"  class="form-check-input" id="optionsRadios1" value="{{$type['id']}}">
                                                    {{$type['name']}}
                                                </label>
                                            </div>
                                        </div>
                                        @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--tag strats-->
                        <div class="col-12 stretch-card">
                            <div class="card">
                                <div class="card-body" >
                                    <h4 class="card-title">Facilities </h4>
                                    <div class="card testDiv">
                                        <div class="card-body ">
                                        @foreach($facilities as $facility)
                                        <div class="form-group ">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="checkbox" name="facilities[]" class="form-check-input" id="facilities" value="{{$facility['id']}}">
                                                    {{$facility['name']}}
                                                </label>
                                            </div>
                                        </div>
                                        @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--tag ends-->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title d-flex">Banner Image
                                <small class="ml-auto align-self-end">
                                    <a href="dropify.html" class="font-weight-light" target="_blank"></a>
                                </small>
                            </h4>
                            <input type="file" name="banner_image" id="banner_image"/><br>
                            <h4 class="card-title d-flex">Feature Image
                                <small class="ml-auto align-self-end">
                                    <a href="dropify.html"  class="font-weight-light" target="_blank"></a>
                                </small>
                            </h4>
                            {!! Form::file('feature_image', array('class' => '','id'=>'feature_image')) !!}

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title d-flex"> Images
                                <small class="ml-auto align-self-end">
                                    <a href="dropify.html"  class="font-weight-light" target="_blank"></a>
                                </small>
                            </h4>
                            <input type="file" name="images[]" id="images" multiple/>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 grid-margin stretch-card">
                    <div class="card ">
                        <div class="card-body ">
                            <h4 class="card-title">Services </h4>
                            <div class="card testDiv"  id="testDiv">
                                <div class="card-body ">
                                    @foreach($services as $service)
                                    <div class="form-group">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input type="checkbox" name="services[]" id="services" class="form-check-input" value="{{$service['id']}}">
                                                {{$service['name']}}
                                            </label>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 grid-margin  stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Location </h4>
                            <div class="row">
                            <div class="form-group col-md-6">
                                <label for="exampleInputName1">District</label>
                                <select class="js-example-basic-single w-100" name="district_id" id="district_id">
                                    <option value="">Select District</option>
                                    @foreach($districts as $district)
                                        <option value="{{$district['id']}}">{!! $district['name_en'] !!}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="exampleInputName1">City</label>
                                <select class="js-example-basic-single w-100" name="city_id" id="city_id">
                                    <option value="">Select City</option>


                                </select>
                            </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputName1">Real Address</label>
                                {!! Form::text('address', null, array('placeholder' => 'Real Address','class' => 'form-control')) !!}
                            </div>
                            <div class="form-group col-md-6">

                                {!! Form::text('searchInput', null, array('placeholder' => 'Location','class' => 'form-control col-md-8','id'=>'searchInput')) !!}
                            </div>
                            <div class="row">
                            <div class="col-md-12">
                                <div class="form-group ">
                                    <div class="map" id="map" style="width: 100%; height: 300px;"></div>
                                </div>
                            </div>


                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="exampleInputName1">Location</label>

                                    {!! Form::text('location', null, array('placeholder' => 'Location','class' => 'form-control mb-2 mr-sm-2','id' =>'location')) !!}
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputName1">Latitude</label>
                                    {!! Form::text('lat', null, array('placeholder' => 'Latitude','class' => 'form-control mb-2 mr-sm-2','id' =>'lat')) !!}
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputName1">Longitude</label>
                                    {!! Form::text('lng', null, array('placeholder' => 'Longitude','class' => 'form-control mb-2 mr-sm-2','id'=>'lng')) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Check In/ Check Out </h4>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Time for check in</label>
                                    {!! Form::text('check_in', null, array('placeholder' => 'Eg:12:00AM','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Time for check out</label>
                                    {!! Form::text('check_out', null, array('placeholder' => 'Eg:11:00AM','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Pricing</h4>
                            <div class="form-group">
                                <label for="exampleInputName1">Price</label>
                                {!! Form::number('price', null, array('placeholder' => 'Price','class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-12 grid-margin  stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Term & Condition </h4>
                            <div class="form-group" id="">
                                <label for="exampleInputEmail3">Content</label>
                                {!! Form::textarea('term_condition', null, array('placeholder' => 'Term & Condition','class' => 'form-control summernote','cols'=>'30','rows'=>'10')) !!}

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 grid-margin  stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Sosioliest </h4>
                            <div class="form-group">
                                <label for="exampleInputName1">Facebook link</label>
                                {!! Form::text('fb_page_link', null, array('placeholder' => 'Facebook link','class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label for="exampleInputName1">Twitter link</label>
                                {!! Form::text('twiter_link', null, array('placeholder' => 'Twitter link','class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label for="exampleInputName1">Instergram link</label>
                                {!! Form::text('instergram_link', null, array('placeholder' => 'Instergram link','class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label for="exampleInputName1">Website link</label>
                                {!! Form::text('website_link', null, array('placeholder' => 'Website link','class' => 'form-control')) !!}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary mr-2">Submit</button>
            <button class="btn btn-light">Cancel</button>
            {!! Form::close() !!}

        </div>


        @section('scripts')
            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyANuk6NO4FfxgvcQrb8N70ZDCKF-vWh6Jg&libraries=places"></script>
            <script>
/* script */
function initialize() {
   var latlng = new google.maps.LatLng(6.927079,79.861244);
    var map = new google.maps.Map(document.getElementById('map'), {
      center: latlng,
      zoom: 13
    });
    var marker = new google.maps.Marker({
      map: map,
      position: latlng,
      draggable: true,
      anchorPoint: new google.maps.Point(0, -29)
   });
    var input = document.getElementById('searchInput');
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    var geocoder = new google.maps.Geocoder();
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);
    var infowindow = new google.maps.InfoWindow();
    autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }

        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        bindDataToForm(place.formatted_address,place.geometry.location.lat(),place.geometry.location.lng());
        infowindow.setContent(place.formatted_address);
        infowindow.open(map, marker);

    });
    // this function will work on marker move event into map
    google.maps.event.addListener(marker, 'dragend', function() {
        geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {
              bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng());
              infowindow.setContent(results[0].formatted_address);
              infowindow.open(map, marker);
          }
        }
        });
    });
}
function bindDataToForm(address,lat,lng){
   document.getElementById('location').value = address;
   document.getElementById('lat').value = lat;
   document.getElementById('lng').value = lng;
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>
            <script type="text/javascript">
                $(document).ready(function(){
                    $('#district_id').change(function(){
                        var did = $("#district_id").val();
                        $.ajax({
                            url: '/accommodationdistrict',
                            method: 'GET',
                            data: {'did':did}
                        }).done(function(response){
                            console.log(response)
                            jQuery('select[name="city_id"]').empty();
                            jQuery.each(response, function(key,value){
                                $('select[name="city_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                            });
                        })
                    })
                })
            </script>
@endsection

@endsection