@extends('backend.layouts.master')
@section('content')

    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Hotel table
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Tables</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Hotel table</li>
                    </ol>
                </nav>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"> Hotel List</h4>
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="order-listing" class="table">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Title</th>
                                        <th>Email</th>
                                        <th>Contact No</th>
                                        <th>Address</th>
                                        @can('hotel-publish')
                                        <th>Status</th>
                                        @endcan
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($accommodations as $key => $accommodation)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $accommodation->title }}</td>
                                            <td>{{ $accommodation->email }}</td>
                                            <td>{{ $accommodation->contact_number }}</td>
                                            <td>{{ $accommodation->address }}</td>
                                            @can('hotel-publish')
                                            <td>
                                                <input data-id="{{$accommodation->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $accommodation->publish_status ? 'checked' : '' }}>
                                            </td>
                                            @endcan
                                            <td>
                                                @can('hotel-room')
                                                <a class="btn btn-outline-success" style="margin-bottom: 2px" href="{{ route('accommodation-rooms.show',$accommodation->id) }}">Add Rooms</a>
                                                @endcan
                                                @can('hotel-view')
                                                <a class="btn btn-outline-primary" href="{{ route('accommodations.show',$accommodation->id) }}">View</a>
                                                @endcan
                                                @can('hotel-edit')
                                                    <a class="btn btn-outline-info" href="{{ route('accommodations.edit',$accommodation->id) }}">Edit</a>
                                                @endcan
                                                @can('hotel-delete')
                                                    {!! Form::open(['method' => 'DELETE','route' => ['accommodations.destroy', $accommodation->id],'style'=>'display:inline']) !!}
                                                    {!! Form::submit('Delete', ['class' => 'btn btn-outline-danger','data-toggle'=>'confirmation']) !!}
                                                    {!! Form::close() !!}
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @can('hotel-create')
            <a href="{{ route('accommodations.create') }}">
                <div class="theme-setting-wrapper">
                    <div id="settings-trigger"><i class="fas fa-plus"> </i></div>
                </div></a>
        @endcan
        @section('scripts')
            <script src="{{asset('backend/js/data-table.js')}}"></script>
            <script>
  $(function() {
    $('.toggle-class').change(function() {
        var publish_status = $(this).prop('checked') == true ? 1 : 0;
        var accommodation_id = $(this).data('id');

        $.ajax({
            type: "GET",
            dataType: "json",
            url: '/accommodationchangeStatus',
            data: {'publish_status': publish_status, 'accommodation_id': accommodation_id},
            success: function(data){
             toastr.success('Review successfully');
              console.log(data.success)


        },
        error: function (msg) {
            toastr.error( 'Inconceivable!');
        }
        });
    })
  })
</script>
@endsection

@endsection
