@extends('backend.layouts.master')
@section('content')

    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    View Accommodation
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">View</a></li>
                        <li class="breadcrumb-item active" aria-current="page">View Accommodation</li>
                    </ol>
                </nav>
            </div>

            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>type:</strong>
                                    {!! $accommodation->types->name !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>title:</strong>
                                    {{ $accommodation->title }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>contact_number:</strong>
                                    {{ $accommodation->contact_number }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Email:</strong>
                                    {{ $accommodation->email }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>address:</strong>
                                    {{ $accommodation->address }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>location:</strong>
                                    {{ $accommodation->location }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>description:</strong>
                                    {!! $accommodation->description  !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>check_in:</strong>
                                    {{ $accommodation->check_in }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>check_out:</strong>
                                    {{ $accommodation->check_out }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>price:</strong>
                                    {{ $accommodation->price }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>website:</strong>
                                    {{ $accommodation->website_link }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>facebook:</strong>
                                    {{ $accommodation->fb_page_link }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>instergram:</strong>
                                    {{ $accommodation->instergram_link }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>twiter:</strong>
                                    {{ $accommodation->twiter_link }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>term condition:</strong>
                                    {!! $accommodation->term_condition !!}
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>facilities:</strong>

                                        @foreach($accommodation->facilities as $v)
                                            <label class="badge badge-success">{{ $v->name }}</label>
                                        @endforeach

                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>services:</strong>

                                        @foreach($accommodation->services as $vs)
                                            <label class="badge badge-success">{{ $vs->name }}</label>
                                        @endforeach

                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Images:</strong>

                                        @foreach($accommodation->images as $vs)
                                            <img src="{{ asset($vs->accommodation_image) }}" style="width: auto;height: 100px;">
                                        @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <!-- <h2> Show User</h2> -->
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('accommodations.index') }}"> Back</a>
                    </div>
                </div>
            </div>




@endsection