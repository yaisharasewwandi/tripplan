@extends('backend.layouts.master')
@section('content')

    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Cars table
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Tables</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Cars table</li>
                    </ol>
                </nav>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"> Cars List</h4>
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="order-listing" class="table">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Title</th>
                                        <th>Email</th>
                                        <th>Contact No</th>
                                        <th>Address</th>
                                        @can('hotel-publish')
                                        <th>Status</th>
                                        @endcan
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($cars as $key => $car)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $car->title }}</td>
                                            <td>{{ $car->email }}</td>
                                            <td>{{ $car->contact_number }}</td>
                                            <td>{{ $car->address }}</td>
                                            @can('hotel-publish')
                                            <td>
                                                <input data-id="{{$car->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $car->publish_status ? 'checked' : '' }}>
                                            </td>
                                            @endcan
                                            <td>
                                                @can('car-view')
                                                <a class="btn btn-outline-primary" href="{{ route('cars.show',$car->id) }}">View</a>
                                                @endcan
                                                @can('car-edit')
                                                    <a class="btn btn-outline-info" href="{{ route('cars.edit',$car->id) }}">Edit</a>
                                                @endcan
                                                @can('car-delete')
                                                    {!! Form::open(['method' => 'DELETE','route' => ['cars.destroy', $car->id],'style'=>'display:inline']) !!}
                                                    {!! Form::submit('Delete', ['class' => 'btn btn-outline-danger','data-toggle'=>'confirmation']) !!}
                                                    {!! Form::close() !!}
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @can('car-create')
            <a href="{{ route('cars.create') }}">
                <div class="theme-setting-wrapper">
                    <div id="settings-trigger"><i class="fas fa-plus"> </i></div>
                </div></a>
        @endcan
        @section('scripts')
            <script src="{{asset('backend/js/data-table.js')}}"></script>
            <script>
  $(function() {
    $('.toggle-class').change(function() {
        var publish_status = $(this).prop('checked') == true ? 1 : 0;
        var car_id = $(this).data('id');

        $.ajax({
            type: "GET",
            dataType: "json",
            url: '/carchangeStatus',
            data: {'publish_status': publish_status, 'car_id': car_id},
            success: function(data){
             toastr.success('Review successfully');
              console.log(data.success)


        },
        error: function (msg) {
            toastr.error( 'Inconceivable!');
        }
        });
    })
  })
</script>
@endsection

@endsection
