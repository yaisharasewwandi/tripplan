@extends('backend.layouts.master')
@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Edit Blog
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Forms</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Blog</li>
                    </ol>
                </nav>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Edit Blog</h4>

                        {!! Form::model($blog, ['method' => 'PATCH','route' => ['blogs.update', $blog->id],'enctype' => 'multipart/form-data']) !!}
                        <div class="form-group">
                            <label for="exampleInputName1">Title</label>
                            {!! Form::text('title', null, array('placeholder' => 'Title','class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail3">Description</label>

                            {!! Form::textarea('description', null, array('placeholder' => 'Description','class' => 'form-control summernote','cols'=>'30','rows'=>'10')) !!}
                        </div>
                        <img src="{{ asset($blog->image) }}" id="profile-img-tag" height="100" width="100">
                        <div class="form-group">

                            <label for="exampleInputPassword4">Image</label>

                            {!! Form::file('image', array('placeholder' => 'Image','class' => 'form-control')) !!}
                        </div>

                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        <button class="btn btn-light">Cancel</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>



@endsection
