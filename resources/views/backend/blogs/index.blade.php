@extends('backend.layouts.master')
@section('content')

    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Blog table
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Tables</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Blog table</li>
                    </ol>
                </nav>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"> Blog List</h4>
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="order-listing" class="table">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Image</th>
                                        @can('blog-publish')
                                        <th>Status</th>
                                        @endcan
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($blogs as $key => $blog)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $blog->title }}</td>
                                            <td>{{ Illuminate\Support\Str::limit($blog->description, 20, $end='...') }}</td>
                                            <td><img src="{{ $blog->image }}"></td>
                                            @can('blog-publish')
                                            <td>
                                                <input data-id="{{$blog->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $blog->publish_status ? 'checked' : '' }}>
                                            </td>
                                            @endcan
                                            <td>
                                                @can('blog-view')
                                                <a class="btn btn-outline-primary" href="{{ route('blogs.show',$blog->id) }}">View</a>
                                                @endcan
                                                @can('blog-edit')
                                                    <a class="btn btn-outline-info" href="{{ route('blogs.edit',$blog->id) }}">Edit</a>
                                                @endcan
                                                @can('blog-delete')
                                                    {!! Form::open(['method' => 'DELETE','route' => ['blogs.destroy', $blog->id],'style'=>'display:inline']) !!}
                                                    {!! Form::submit('Delete', ['class' => 'btn btn-outline-danger','data-toggle'=>'confirmation']) !!}
                                                    {!! Form::close() !!}
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @can('blog-create')
            <a href="{{ route('blogs.create') }}">
                <div class="theme-setting-wrapper">
                    <div id="settings-trigger"><i class="fas fa-plus"> </i></div>
                </div></a>
        @endcan
@section('scripts')
<script src="{{asset('backend/js/data-table.js')}}"></script>
<script>
  $(function() {
    $('.toggle-class').change(function() {
        var publish_status = $(this).prop('checked') == true ? 1 : 0;
        var blog_id = $(this).data('id');

        $.ajax({
            type: "GET",
            dataType: "json",
            url: '/blogchangeStatus',
            data: {'publish_status': publish_status, 'blog_id': blog_id},
            success: function(data){
             toastr.success('Review successfully');
              console.log(data.success)


        },
        error: function (msg) {
            toastr.error( 'Inconceivable!');
        }
        });
    })
  })
</script>
@endsection

@endsection
