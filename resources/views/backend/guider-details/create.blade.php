@extends('backend.layouts.master')
@section('content')

    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Create Guider Travel Info
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Forms</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create Guider Travel Info</li>
                    </ol>
                </nav>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::open(array('route' => 'guider-details.store','method'=>'POST','class'=>'forms-sample','enctype' => 'multipart/form-data')) !!}

            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Create Guider Travel Info</h4>


                            <div class="form-group">
                                <label for="exampleInputName1">Name</label>
                                <input type="hidden" name="guider_id" value="{!! $id !!}">
                                {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control','id'=>'name')) !!}
                            </div>
                            <div class="form-group">
                                <label for="exampleInputName1">Location</label>
                                {!! Form::text('location', null, array('placeholder' => 'Location','class' => 'form-control ','id'=>'autocomplete')) !!}
                            </div>

                            <div class="form-group" id="">
                                <label for="exampleInputEmail3">About Details</label>
                                {!! Form::textarea('description', null, array('placeholder' => 'Description','class' => 'form-control summernote','cols'=>'30','rows'=>'10')) !!}

                            </div>

                            <div class="row">

                                <div class="col-lg-12 grid-margin stretch-card">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title d-flex">Gallery Images
                                                <small class="ml-auto align-self-end">
                                                    <a href="dropify.html" class="font-weight-light" target="_blank"></a>
                                                </small>
                                            </h4>
                                            <input type="file" name="images[]" class="" multiple />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Duration</label>
                                    {!! Form::text('days', null, array('placeholder' => 'Days','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Min Person</label>
                                    {!! Form::number('price', null, array('placeholder' => 'Price','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                <button class="btn btn-light">Cancel</button>
                {!! Form::close() !!}
            </div>
            <div class="col-lg-12 grid-margin d-flex stretch-card">
                <div class="row flex-grow">
                    <div class="col-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Rooms</h4>
                                <div class="card ">
                                    <div class="card-body ">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="table-responsive">
                                                    <table id="order-listing" class="table">
                                                        <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Name</th>
                                                            <th>Location</th>
                                                            <th>Days</th>
                                                            <th>Price (per day)</th>
                                                            <th>Status</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach ($guiderDetails as $key => $details)
                                                            <tr>
                                                                <td>{{ ++$i }}</td>
                                                                <td>{{ $details->name }}</td>
                                                                <td>{{ $details->location }}</td>
                                                                <td>{{ $details->days }}</td>
                                                                <td>{{ $details->price }}</td>

                                                                <td>
                                                                    <input data-id="{{$details->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $details->publish_status ? 'checked' : '' }}>
                                                                </td>
                                                                <td>
                                                                    <a class="btn btn-outline-primary" style="margin-bottom: 2px"
                                                                       data-toggle="modal"
                                                                       data-target="#exampleModal"
                                                                       onclick="traveld(this)"
                                                                       data-images="{{$details->images->pluck('image_name')}}"
                                                                       data-id='{{$details->id}}'

                                                                    >View</a>
                                                                    @can('role-edit')
                                                                        <a class="btn btn-outline-info" href="{{ route('guider-details.edit',$details->id) }}">Edit</a>
                                                                    @endcan
                                                                    @can('role-delete')
                                                                        {!! Form::open(['method' => 'DELETE','route' => ['guider-details.destroy', $details->id],'style'=>'display:inline']) !!}
                                                                        {!! Form::submit('Delete', ['class' => 'btn btn-outline-danger','data-toggle'=>'confirmation']) !!}
                                                                        {!! Form::close() !!}
                                                                    @endcan
                                                                </td>
                                                            </tr>
                                                        @endforeach


                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--tag strats-->

                    <!--tag ends-->
                </div>
            </div>
        </div>
        <div class="modal fade" id="showModal" data-backdrop="static" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="width: 60%;left: 20%">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"> Images</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="row">


                        <div class="row" id="images_div">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>

        @section('scripts')
            <script src="{{asset('backend/js/data-table.js')}}"></script>
            <script>
                function traveld(item) {
                    let id = item.dataset.id;
                    let images = JSON.parse(item.dataset.images);


                    let html = '';
                    let base_path = "{{asset('')}}";

                    $.each(images, function (index, value) {
                        html += "<div class='col-md-6 p-3'>"
                        html += "<img class='img img-responsive w-100' src='" + base_path + value + "'/>"
                        html += "</div>"
                    });

                    $("#images_div").empty();
                    $("#images_div").append(html);



                    $('#showModal').modal('show')
                }
                $(function() {
                    $('.toggle-class').change(function() {
                        var publish_status = $(this).prop('checked') == true ? 1 : 0;
                        var travel_id = $(this).data('id');

                        $.ajax({
                            type: "GET",
                            dataType: "json",
                            url: '/travelStatus',
                            data: {'publish_status': publish_status, 'travel_id': travel_id},
                            success: function(data){
                                toastr.success('Review successfully');
                                console.log(data.success)


                            },
                            error: function (msg) {
                                toastr.error( 'Inconceivable!');
                            }
                        });
                    })
                })
            </script>


            <script type="text/javascript">
                $(document).ready(function(){
                    var maxField = 10; //Input fields increment limitation
                    var addButton = $('.add_button'); //Add button selector
                    var wrapper = $('.field_wrapper'); //Input field wrapper
                    var fieldHTML = ' <div class="row "><div class="form-group col-md-10"><input type="text" name="includes[]" value="" class="form-control"></div><a href="javascript:void(0);" class="remove_button"><img src="{{asset('backend/remove-icon.png')}}"/></a></div>'; //New input field html
                    var x = 1; //Initial field counter is 1

                    //Once add button is clicked
                    $(addButton).click(function(){
                        //Check maximum number of input fields
                        if(x < maxField){
                            x++; //Increment field counter
                            $(wrapper).append(fieldHTML); //Add field html
                        }
                    });

                    //Once remove button is clicked
                    $(wrapper).on('click', '.remove_button', function(e){
                        e.preventDefault();
                        $(this).parent('div').remove(); //Remove field html
                        x--; //Decrement field counter
                    });
                });
            </script>
            <script type="text/javascript">
                $(document).ready(function(){
                    var maxField = 10; //Input fields increment limitation
                    var addButton = $('.add_button2'); //Add button selector
                    var wrapper = $('.field_wrapper2'); //Input field wrapper
                    var fieldHTML = ' <div class="row "><div class="form-group col-md-10"><input type="text" name="excludes[]" value="" class="form-control"></div><a href="javascript:void(0);" class="remove_button"><img src="{{asset('backend/remove-icon.png')}}"/></a></div>'; //New input field html
                    var x = 1; //Initial field counter is 1

                    //Once add button is clicked
                    $(addButton).click(function(){
                        //Check maximum number of input fields
                        if(x < maxField){
                            x++; //Increment field counter
                            $(wrapper).append(fieldHTML); //Add field html
                        }
                    });

                    //Once remove button is clicked
                    $(wrapper).on('click', '.remove_button', function(e){
                        e.preventDefault();
                        $(this).parent('div').remove(); //Remove field html
                        x--; //Decrement field counter
                    });
                });
            </script>
            <script type="text/javascript">
                $(document).ready(function(){
                    var maxField = 10; //Input fields increment limitation
                    var addButton = $('.add_button3'); //Add button selector
                    var wrapper = $('.field_wrapper3'); //Input field wrapper
                    var fieldHTML = ' <div class="row "><div class="form-group col-md-10"><input type="text" name="available_times[]" value="" class="form-control" placeholder="Eg:Saturday 8:00am to 4:00pm"></div><a href="javascript:void(0);" class="remove_button"><img src="{{asset('backend/remove-icon.png')}}"/></a></div>'; //New input field html
                    var x = 1; //Initial field counter is 1

                    //Once add button is clicked
                    $(addButton).click(function(){
                        //Check maximum number of input fields
                        if(x < maxField){
                            x++; //Increment field counter
                            $(wrapper).append(fieldHTML); //Add field html
                        }
                    });

                    //Once remove button is clicked
                    $(wrapper).on('click', '.remove_button', function(e){
                        e.preventDefault();
                        $(this).parent('div').remove(); //Remove field html
                        x--; //Decrement field counter
                    });
                });
            </script>
            <script type="text/javascript">
                $(document).ready(function(){
                    var maxField = 10; //Input fields increment limitation
                    var addButton = $('.add_button4'); //Add button selector
                    var wrapper = $('.field_wrapper4'); //Input field wrapper
                    var fieldHTML = ' <div class="row "> <div class="form-group col-md-6 "><input type="date" name="days[]" value="" class="form-control"></div><div class="form-group col-md-4 "><input type="time" name="times[]" value="" class="form-control"></div><a href="javascript:void(0);" class="remove_button"><img src="{{asset('backend/remove-icon.png')}}"/></a></div>'; //New input field html
                    var x = 1; //Initial field counter is 1

                    //Once add button is clicked
                    $(addButton).click(function(){
                        //Check maximum number of input fields
                        if(x < maxField){
                            x++; //Increment field counter
                            $(wrapper).append(fieldHTML); //Add field html
                        }
                    });

                    //Once remove button is clicked
                    $(wrapper).on('click', '.remove_button', function(e){
                        e.preventDefault();
                        $(this).parent('div').remove(); //Remove field html
                        x--; //Decrement field counter
                    });
                });
            </script>
            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyANuk6NO4FfxgvcQrb8N70ZDCKF-vWh6Jg&libraries=places"></script>
            <script>

                $(document).ready(function() {
                    $("#lat_area").addClass("d-none");
                    $("#long_area").addClass("d-none");
                });
            </script>
            <script>
                google.maps.event.addDomListener(window, 'load', initialize);

                function initialize() {
                    var input = document.getElementById('autocomplete');
                    var autocomplete = new google.maps.places.Autocomplete(input);
                    autocomplete.addListener('place_changed', function() {
                        var place = autocomplete.getPlace();
                        $('#latitude').val(place.geometry['location'].lat());
                        $('#longitude').val(place.geometry['location'].lng());
                        $("#lat_area").removeClass("d-none");
                        $("#long_area").removeClass("d-none");
                    });
                }

            </script>

@endsection
@endsection
