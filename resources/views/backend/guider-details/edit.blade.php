@extends('backend.layouts.master')
@section('content')

    <div class="main-panel">
        {!! Form::model($details,['route' => ['guider-details.update',$details->id],'method'=>'PATCH','class'=>'forms-sample','files'=>'true']) !!}

        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Edit Guider Travel Info
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Forms</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Tour Packages</li>
                    </ol>
                </nav>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Create Guider Travel Info</h4>


                            <div class="form-group">
                                <label for="exampleInputName1">Name</label>

                                {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control','id'=>'name')) !!}
                            </div>
                            <div class="form-group">
                                <label for="exampleInputName1">Location</label>
                                {!! Form::text('location', null, array('placeholder' => 'Location','class' => 'form-control ','id'=>'autocomplete')) !!}
                            </div>

                            <div class="form-group" id="">
                                <label for="exampleInputEmail3">About Details</label>
                                {!! Form::textarea('description', null, array('placeholder' => 'Description','class' => 'form-control summernote','cols'=>'30','rows'=>'10')) !!}

                            </div>

                            <div class="row">

                                <div class="col-lg-12 grid-margin stretch-card">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title d-flex">Gallery Images
                                                <small class="ml-auto align-self-end">
                                                    <a href="dropify.html" class="font-weight-light" target="_blank"></a>
                                                </small>
                                            </h4>
                                            <input type="file" name="images[]" class="" multiple />
                                            <div class="row">
                                                @foreach($details->images as $vs)
                                                        <div class="col-lg-3 col-md-6 col-sm-6">
                                                            <img src="{{ asset($vs->image_name) }}" id="images" style="width: auto;height: 120px;margin-top: 15px;">
                                                            <a href="/traveldetail/imagedelete/{{$vs->id}}" class="badge badge-danger p-2 mb-3">Delete</a><br>

                                                        </div>

                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Duration</label>
                                    {!! Form::text('days', null, array('placeholder' => 'Days','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Min Person</label>
                                    {!! Form::number('price', null, array('placeholder' => 'Price','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                <button class="btn btn-light">Cancel</button>
                {!! Form::close() !!}
            </div>

            @section('scripts')

                <script src="{{asset('backend/js/data-table.js')}}"></script>

                <script>
                    function traveld(item) {
                        let id = item.dataset.id;
                        let images = JSON.parse(item.dataset.images);


                        let html = '';
                        let base_path = "{{asset('')}}";

                        $.each(images, function (index, value) {
                            html += "<div class='col-md-6 p-3'>"
                            html += "<img class='img img-responsive w-100' src='" + base_path + value + "'/>"
                            html += "</div>"
                        });

                        $("#images_div").empty();
                        $("#images_div").append(html);



                        $('#showModal').modal('show')
                    }

                </script>


                <script type="text/javascript">
                    $(document).ready(function(){
                        var maxField = 10; //Input fields increment limitation
                        var addButton = $('.add_button'); //Add button selector
                        var wrapper = $('.field_wrapper'); //Input field wrapper
                        var fieldHTML = ' <div class="row "><div class="form-group col-md-10"><input type="text" name="includes[]" value="" class="form-control"></div><a href="javascript:void(0);" class="remove_button"><img src="{{asset('backend/remove-icon.png')}}"/></a></div>'; //New input field html
                        var x = 1; //Initial field counter is 1

                        //Once add button is clicked
                        $(addButton).click(function(){
                            //Check maximum number of input fields
                            if(x < maxField){
                                x++; //Increment field counter
                                $(wrapper).append(fieldHTML); //Add field html
                            }
                        });

                        //Once remove button is clicked
                        $(wrapper).on('click', '.remove_button', function(e){
                            e.preventDefault();
                            $(this).parent('div').remove(); //Remove field html
                            x--; //Decrement field counter
                        });
                    });
                </script>
                <script type="text/javascript">
                    $(document).ready(function(){
                        var maxField = 10; //Input fields increment limitation
                        var addButton = $('.add_button2'); //Add button selector
                        var wrapper = $('.field_wrapper2'); //Input field wrapper
                        var fieldHTML = ' <div class="row "><div class="form-group col-md-10"><input type="text" name="excludes[]" value="" class="form-control"></div><a href="javascript:void(0);" class="remove_button"><img src="{{asset('backend/remove-icon.png')}}"/></a></div>'; //New input field html
                        var x = 1; //Initial field counter is 1

                        //Once add button is clicked
                        $(addButton).click(function(){
                            //Check maximum number of input fields
                            if(x < maxField){
                                x++; //Increment field counter
                                $(wrapper).append(fieldHTML); //Add field html
                            }
                        });

                        //Once remove button is clicked
                        $(wrapper).on('click', '.remove_button', function(e){
                            e.preventDefault();
                            $(this).parent('div').remove(); //Remove field html
                            x--; //Decrement field counter
                        });
                    });
                </script>
                <script type="text/javascript">
                    $(document).ready(function(){
                        var maxField = 10; //Input fields increment limitation
                        var addButton = $('.add_button3'); //Add button selector
                        var wrapper = $('.field_wrapper3'); //Input field wrapper
                        var fieldHTML = ' <div class="row "><div class="form-group col-md-10"><input type="text" name="available_times[]" value="" class="form-control" placeholder="Eg:Saturday 8:00am to 4:00pm"></div><a href="javascript:void(0);" class="remove_button"><img src="{{asset('backend/remove-icon.png')}}"/></a></div>'; //New input field html
                        var x = 1; //Initial field counter is 1

                        //Once add button is clicked
                        $(addButton).click(function(){
                            //Check maximum number of input fields
                            if(x < maxField){
                                x++; //Increment field counter
                                $(wrapper).append(fieldHTML); //Add field html
                            }
                        });

                        //Once remove button is clicked
                        $(wrapper).on('click', '.remove_button', function(e){
                            e.preventDefault();
                            $(this).parent('div').remove(); //Remove field html
                            x--; //Decrement field counter
                        });
                    });
                </script>
                <script type="text/javascript">
                    $(document).ready(function(){
                        var maxField = 10; //Input fields increment limitation
                        var addButton = $('.add_button4'); //Add button selector
                        var wrapper = $('.field_wrapper4'); //Input field wrapper
                        var fieldHTML = ' <div class="row "> <div class="form-group col-md-6 "><input type="date" name="days[]" value="" class="form-control"></div><div class="form-group col-md-4 "><input type="time" name="times[]" value="" class="form-control"></div><a href="javascript:void(0);" class="remove_button"><img src="{{asset('backend/remove-icon.png')}}"/></a></div>'; //New input field html
                        var x = 1; //Initial field counter is 1

                        //Once add button is clicked
                        $(addButton).click(function(){
                            //Check maximum number of input fields
                            if(x < maxField){
                                x++; //Increment field counter
                                $(wrapper).append(fieldHTML); //Add field html
                            }
                        });

                        //Once remove button is clicked
                        $(wrapper).on('click', '.remove_button', function(e){
                            e.preventDefault();
                            $(this).parent('div').remove(); //Remove field html
                            x--; //Decrement field counter
                        });
                    });
                </script>
                <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyANuk6NO4FfxgvcQrb8N70ZDCKF-vWh6Jg&libraries=places"></script>
                <script>

                    $(document).ready(function() {
                        $("#lat_area").addClass("d-none");
                        $("#long_area").addClass("d-none");
                    });
                </script>
                <script>
                    google.maps.event.addDomListener(window, 'load', initialize);

                    function initialize() {
                        var input = document.getElementById('autocomplete');
                        var autocomplete = new google.maps.places.Autocomplete(input);
                        autocomplete.addListener('place_changed', function() {
                            var place = autocomplete.getPlace();
                            $('#latitude').val(place.geometry['location'].lat());
                            $('#longitude').val(place.geometry['location'].lng());
                            $("#lat_area").removeClass("d-none");
                            $("#long_area").removeClass("d-none");
                        });
                    }

                </script>

@endsection
@endsection
