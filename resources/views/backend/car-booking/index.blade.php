@extends('backend.layouts.master')
@section('content')

    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Car Booking table
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Tables</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Booking table</li>
                    </ol>
                </nav>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"> Booking List</h4>
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="order-listing" class="table">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Ref No</th>
                                        <th>Booking From</th>
                                        <th>Booking To</th>
                                        <th>Days</th>
                                        <th>Total price</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($booking as $key => $book)
                                        <tr>
                                            <td>{{ $book->id }}</td>
                                            <td>{{ $book->car->title }}</td>
                                            <td>{{ $book->ref_no }}</td>
                                            <td>{{ $book->from }}</td>
                                            <td>{{ $book->to }}</td>
                                            <td>{{ $book->days }}</td>
                                            <td>{{ $book->amount }}</td>

                                            @can('car-booking-approve')
                                                @if (Auth::user()->hasRole(['Admin','Super Admin']) )
                                                    <td>
                                                        @if($book->approve==1)
                                                            <button type="button" class="btn btn-primary">Approve </button>
                                                        @else
                                                            <button type="button" class="btn btn-danger">pending Approve</button>
                                                        @endif

                                                    </td>

                                                @else
                                                    <td>
                                                        <input data-id="{{$book->id}}" class="toggle-class publish-car" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $book->approve ? 'checked' : '' }}>
                                                    </td>
                                                @endif
                                                @endcan

                                                <td>
                                                    {{--                                                <a class="btn btn-outline-primary" href="{{ route('blogs.show',$blog->id) }}">View</a>--}}
                                                    @can('car-booking-edit')
                                                        {{--                                                    <a class="btn btn-outline-info" href="{{ route('blogs.edit',$book->id) }}">Edit</a>--}}
                                                    @endcan

                                                    @can('car-booking-delete')
                                                        {!! Form::open(['method' => 'DELETE','route' => ['accomodation-booking.destroy', $book->id],'style'=>'display:inline']) !!}
                                                        {!! Form::submit('Delete', ['class' => 'btn btn-outline-danger','data-toggle'=>'confirmation']) !!}
                                                        {!! Form::close() !!}
                                                    @endcan
                                                </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--        @can('role-create')--}}
        {{--            <a href="{{ route('blogs.create') }}">--}}
        {{--                <div class="theme-setting-wrapper">--}}
        {{--                    <div id="settings-trigger"><i class="fas fa-plus"> </i></div>--}}
        {{--                </div></a>--}}
        {{--        @endcan--}}
        @endsection
        @section('scripts')
            <script src="{{asset('backend/js/data-table.js')}}"></script>
            <script>
                $(function() {
                    $('.publish-car').change(function() {
                        var approve = $(this).prop('checked') == true ? 1 : 0;
                        var booking_id = $(this).data('id');

                        $.ajax({
                            type: "GET",
                            dataType: "json",
                            url: '/carbookStatus',
                            data: {'approve': approve, 'booking_id': booking_id},
                            success: function(data){
                                toastr.success('Review successfully');
                                console.log(data.success)


                            },
                            error: function (msg) {
                                toastr.error( 'Inconceivable!');
                            }
                        });
                    })
                })
            </script>
@endsection


