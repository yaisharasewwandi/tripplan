@extends('backend.layouts.master')
@section('content')

    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Edit Room
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Forms</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create Room</li>
                    </ol>
                </nav>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::model($room,['route' => ['accommodation-rooms.update',$room->id],'method'=>'PATCH','class'=>'forms-sample','files'=>'true']) !!}

            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Create Room</h4>


                            <div class="form-group">
                                <label for="exampleInputName1">Room Name</label>
                                <input type="hidden" name="accommodation_id" value="">
                                {!! Form::text('room_name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                            </div>
                            <div class="col-lg-6 grid-margin stretch-card">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title d-flex">Feature Image
                                            <small class="ml-auto align-self-end">
                                                <a href="dropify.html" class="font-weight-light" target="_blank"></a>
                                            </small>
                                        </h4>
                                        <input type="file" name="feature_image" class=""/>
                                        @foreach($room->images as $vs)
                                            @if($vs->is_featured == 0 && $vs->is_featured !== null)
                                                <div class="col-lg-4 col-md-6 col-sm-6">
                                                    <img src="{{ asset($vs->room_image_name) }}" id="banner_image"
                                                         style="width: auto;height: 120px;margin-top: 15px;">
                                                    <a href="/accommodations-room/imagedelete/{{$vs->id}}"
                                                       class="badge badge-danger p-2 mb-3">Delete</a><br>

                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title d-flex">Gallery Images
                                            <small class="ml-auto align-self-end">
                                                <a href="dropify.html" class="font-weight-light" target="_blank"></a>
                                            </small>
                                        </h4>
                                        <input type="file" name="gallery_images[]" class="" multiple/>
                                        <div class="row">
                                            @foreach($room->images as $vs)

                                                @if($vs->is_featured == null && $vs->is_featured !== 0)

                                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                                        <img src="{{ asset($vs->room_image_name) }}" class="w-100"
                                                             id="banner_image"
                                                             style="width: auto;height: 120px;margin-top: 15px;">
                                                        <a href="/accommodations-room/imagedelete/{{$vs->id}}"
                                                           class="badge badge-danger p-2 mb-3">Delete</a><br>

                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Price (LKR)</label>
                                    {!! Form::number('price', null, array('placeholder' => 'Price','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Number of Rooms</label>
                                    {!! Form::number('number_of_room', null, array('placeholder' => 'Number of Rooms','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Number of Beds</label>
                                    {!! Form::number('number_of_bed', null, array('placeholder' => 'Number of Beds','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Rooms Size (Feet)</label>
                                    {!! Form::number('room_size', null, array('placeholder' => 'Rooms Size ','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Max Adults</label>
                                    {!! Form::number('max_adult', null, array('placeholder' => 'Max Adults','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Max Children</label>
                                    {!! Form::number('max_children', null, array('placeholder' => 'Max Children','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                            </div>
                            <div class="col-12 grid-margin stretch-card">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">Room Amenities</h4>
                                        <div class="card testDiv">
                                            <div class="card-body ">
                                                @foreach($roomfeatures as $roomfeature)
                                                    <div class="form-group">
                                                        <div class="form-check">
                                                            <label class="form-check-label">
                                                                <label>{{ Form::checkbox('room_features[]', $roomfeature->id, in_array($roomfeature->id, $roomFacility) ? true : false, array('class' => 'form-check-input','id'=>'room_features')) }}
                                                                    {{ $roomfeature->name }}</label>

                                                            </label>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <button type="submit" class="btn btn-primary mr-2">Submit</button>
            <button class="btn btn-light">Cancel</button>
            {!! Form::close() !!}

        </div>
        @endsection

        @section('scripts')
            <script src="{{asset('backend/js/data-table.js')}}"></script>


@endsection
