@extends('backend.layouts.master')
@section('content')

    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Create Room for {{$accommodation->title}}
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Forms</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create Hotel</li>
                    </ol>
                </nav>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::open(array('route' => 'accommodation-rooms.store','method'=>'POST','class'=>'forms-sample','enctype' => 'multipart/form-data')) !!}

            <div class="row">
                <div class="col-lg-4 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Create Room</h4>


                            <div class="form-group">
                                <label for="exampleInputName1">Room Name</label>
                                <input type="hidden" name="accommodation_id" value="{!! $id !!}">
                                {!! Form::text('room_name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                            </div>
                            <div class="col-lg-12 grid-margin stretch-card">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title d-flex">Feature Image
                                            <small class="ml-auto align-self-end">
                                                <a href="dropify.html" class="font-weight-light" target="_blank"></a>
                                            </small>
                                        </h4>
                                        <input type="file" name="feature_image" class=""/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputName1">Gallery Images</label>
                                <input type="file" name="gallery_images[]" class="" multiple/>

                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Price</label>
                                    {!! Form::number('price', null, array('placeholder' => 'Price','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Number of Rooms</label>
                                    {!! Form::number('number_of_room', null, array('placeholder' => 'Number of Rooms','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Number of Beds</label>
                                    {!! Form::number('number_of_bed', null, array('placeholder' => 'Number of Beds','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Rooms Size</label>
                                    {!! Form::number('room_size', null, array('placeholder' => 'Rooms Size ','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Max Adults</label>
                                    {!! Form::number('max_adult', null, array('placeholder' => 'Max Adults','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Max Children</label>
                                    {!! Form::number('max_children', null, array('placeholder' => 'Max Children','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                            </div>
                            <div class="col-12 grid-margin stretch-card">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">Room Amenities</h4>
                                        <div class="card testDiv">
                                            <div class="card-body ">
                                                @foreach($roomfeatures as $roomfeature)
                                                    <div class="form-group">
                                                        <div class="form-check">
                                                            <label class="form-check-label">
                                                                <input type="checkbox" name="room_features[]"
                                                                       class="form-check-input"
                                                                       value="{{$roomfeature['id']}}">
                                                                {{$roomfeature['name']}}
                                                            </label>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="col-md-2 btn btn-primary mr-2" >Submit</button>
                                <button class="col-md-2 btn btn-light">Cancel</button>
                                {!! Form::close() !!}
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-lg-8 grid-margin d-flex stretch-card">
                    <div class="row flex-grow">
                        <div class="col-12 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Rooms</h4>
                                    <div class="card ">
                                        <div class="card-body ">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="table-responsive">
                                                        <table id="order-listing" class="table">
                                                            <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Name</th>
                                                                <th>No of Rooms</th>
                                                                <th>No of Beds</th>
                                                                <th>Room Size</th>
                                                                <th>Status</th>
                                                                <th>Actions</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach ($rooms as $key => $room)
                                                                <tr>
                                                                    <td>{{ ++$i }}</td>
                                                                    <td>{{ $room->room_name }}</td>
                                                                    <td>{{ $room->number_of_room }}</td>
                                                                    <td>{{ $room->number_of_bed }}</td>
                                                                    <td>{{ $room->room_size }}</td>

                                                                    <td>
                                                                        <input data-id="{{$room->id}}"
                                                                               class="toggle-class" type="checkbox"
                                                                               data-onstyle="success"
                                                                               data-offstyle="danger"
                                                                               data-toggle="toggle" data-on="Active"
                                                                               data-off="InActive" {{ $room->publish_status ? 'checked' : '' }}>
                                                                    </td>
                                                                    <td>


                                                                        <a class="btn btn-outline-primary" style="margin-bottom: 2px"
                                                                           data-toggle="modal"
                                                                           data-target="#exampleModal"
                                                                           onclick="editss(this)"
                                                                           data-features="{{$room->roomFeatures->pluck('name')}}"
                                                                           data-images="{{$room->images->pluck('room_image_name')}}"
                                                                           data-id='{{$room->id}}'
                                                                           data-name='{{$room->room_name}}'
                                                                           data-numberofroom="{{$room->number_of_room}}"
                                                                           data-numberofbed="{{$room->number_of_bed}}"
                                                                           data-roomsize="{{$room->room_size}}"
                                                                           data-price="{{$room->price}}"
                                                                           data-maxadult="{{$room->max_adult}}"
                                                                           data-maxchildren="{{$room->max_children}}">View</a>
                                                                        @can('role-edit')
                                                                            <a class="btn btn-outline-info"
                                                                               href="{{ route('accommodation-rooms.edit',$room->id) }}">Edit</a>
                                                                        @endcan
                                                                        @can('role-delete')
                                                                            {!! Form::open(['method' => 'DELETE','route' => ['accommodation-rooms.destroy', $room->id],'style'=>'display:inline']) !!}
                                                                            {!! Form::submit('Delete', ['class' => 'btn btn-outline-danger','data-toggle'=>'confirmation']) !!}
                                                                            {!! Form::close() !!}
                                                                        @endcan
                                                                    </td>
                                                                </tr>
                                                            @endforeach


                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--tag strats-->

                        <!--tag ends-->
                    </div>
                </div>
            </div>



        </div>
        <div class="modal fade" id="showModal" data-backdrop="static" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="width: 60%;left: 20%">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Room Details</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-inline">
                                    <label for="recipient-name" class="col-md-6">Room Name:</label>
                                    <p id="name">

                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-inline">
                                    <label for="recipient-name" class="col-md-6">Number of Rooms:</label>
                                    <p id="number_of_room">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-inline">
                                    <label for="recipient-name" class="col-md-6 ">Number of Beds:</label>
                                    <p id="number_of_bed">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-inline">
                                    <label for="recipient-name" class="col-md-6 ">Room Size(feets):</label>
                                    <p id="room_size">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-inline">
                                    <label for="recipient-name" class="col-md-6 ">Price:</label>
                                    <p id="price">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-inline">
                                    <label for="recipient-name" class="col-md-6 ">Max Adult:</label>
                                    <p id="max_adult">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-inline">
                                    <label for="recipient-name" class="col-md-6 ">Max Children:</label>
                                    <p id="max_children">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="recipient-name" class="col-md-6 ">Features</label>
                            </div>
                            <div class="col-md-9 p-0" id="features_div"></div>
                        </div>

                        <div class="row" id="images_div">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>


        @section('scripts')
            <script src="{{asset('backend/js/data-table.js')}}"></script>
            <script>
                function editss(item) {
                    let id = item.dataset.id;
                    let name = item.dataset.name;
                    let number_of_room = item.dataset.numberofroom;
                    let number_of_bed = item.dataset.numberofbed;
                    let room_size = item.dataset.roomsize;
                    let price = item.dataset.price;
                    let max_adult = item.dataset.maxadult;
                    let max_children = item.dataset.maxchildren;
                    let images = JSON.parse(item.dataset.images);
                    let features = JSON.parse(item.dataset.features);

                    let html = '';
                    let base_path = "{{asset('')}}";

                    $.each(images, function (index, value) {
                        html += "<div class='col-md-6 p-3'>"
                        html += "<img class='img img-responsive w-100' src='" + base_path + value + "'/>"
                        html += "</div>"
                    });

                    $("#images_div").empty();
                    $("#images_div").append(html);


                    let features_label = '';
                    $.each(features, function (index, value) {
                        features_label += "<label class='badge badge-info badge-pill mr-1'>" + value + "</label>"
                    });


                    $("#features_div").empty();
                    $("#features_div").append(features_label);
                    console.log(features_label)

                    $("#name").empty();
                    $('#name').append(name);
                    $("#number_of_room").empty();
                    $('#number_of_room').append(number_of_room);
                    $("#number_of_bed").empty();
                    $('#number_of_bed').append(number_of_bed);
                    $("#room_size").empty();
                    $('#room_size').append(room_size);
                    $("#price").empty();
                    $('#price').append(price);
                    $("#max_adult").empty();
                    $('#max_adult').append(max_adult);
                    $("#max_children").empty();
                    $('#max_children').append(max_children);
                    $('#showModal').modal('show')
                }

                $(function () {
                    $('.toggle-class').change(function () {
                        var publish_status = $(this).prop('checked') == true ? 1 : 0;
                        var accommodation_room_id = $(this).data('id');

                        $.ajax({
                            type: "GET",
                            dataType: "json",
                            url: '/accommodationroomchangeStatus',
                            data: {'publish_status': publish_status, 'accommodation_room_id': accommodation_room_id},
                            success: function (data) {
                                toastr.success('Review successfully');
                                console.log(data.success)


                            },
                            error: function (msg) {
                                toastr.error('Inconceivable!');
                            }
                        });
                    })
                })
            </script>
@endsection
@endsection
