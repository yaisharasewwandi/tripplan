<!DOCTYPE html>
<html lang="en">


<head>
 <!-- Required meta tags -->
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 <title>Trip Plan</title>
 <!-- plugins:css -->
@include('backend.layouts.partials.style')
@yield('style')
    @toastr_css
 <!-- endinject -->
 <link rel="shortcut icon" href="http://www.urbanui.com/" />
</head>
<body>
@include('backend.layouts.partials.header')
  <!-- partial -->
@include('backend.layouts.partials.sidebar')

  <!-- partial -->

@yield('content')

   <!-- content-wrapper ends -->
   <!-- partial:partials/_footer.html -->

<!-- container-scroller -->
  @include('backend.layouts.partials.footer')

<!-- plugins:js -->
@include('backend.layouts.partials.javascript')
@yield('scripts')
@toastr_js
@toastr_render
</body>


</html>

