<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item nav-profile">
      <div class="nav-link">
        <div class="profile-image">
          @if (Auth::user() )
              @if(!empty(Auth::user()->image_path))
            <img src="{{ asset(asset(Auth::user()->image_path)) }}" alt="image"/>
                @else
                    <img src="{{ asset('backend/images/faces/face5.jpg') }}" alt="image"/>
                @endif
            @else
          <img src="{{ asset('backend/images/faces/face5.jpg') }}" alt="image"/>
            @endif
        </div>
        <div class="profile-name">
          <p class="name">
            Welcome {{ Auth::user()->name }}
          </p>
          <p class="designation">
            {{ str_replace(array('[',']','"'),' ',Auth::user()->getRoleNames()) }}
          </p>
        </div>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ route('home') }}">
        <i class="fa fa-home menu-icon"></i>
        <span class="menu-title">Dashboard</span>
      </a>
    </li>
    @can('role-list')
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#page-layouts" aria-expanded="false" aria-controls="page-layouts">
        <i class="fab fa-trello menu-icon"></i>
        <span class="menu-title">Permission</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="page-layouts">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item d-none d-lg-block"> <a class="nav-link" href="{{ route('users.index') }}">User</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{ route('roles.index') }}">Role</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{ route('permissions.index') }}">permission</a></li>
        </ul>
      </div>
    </li>
    @endcan
    @can('hotel-list')
    <li class="nav-item d-none d-lg-block">
      <a class="nav-link" data-toggle="collapse" href="#sidebar-layouts" aria-expanded="false" aria-controls="sidebar-layouts">
        <i class="fas fa-columns menu-icon"></i>
        <span class="menu-title">Hotel</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="sidebar-layouts">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{route('accommodations.index')}}">Hotel</a></li>
        </ul>
      </div>
    </li>
    @endcan
    @can('restaurant-list')
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
        <i class="far fa-compass menu-icon"></i>
        <span class="menu-title">Restaurant</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="ui-basic">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{route('restaurants.index')}}">Restaurants</a></li>
           </ul>
      </div>
    </li>
    @endcan
    @can('car-list')
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#ui-advanced" aria-expanded="false" aria-controls="ui-advanced">
        <i class="fas fa-clipboard-list menu-icon"></i>
        <span class="menu-title">Car Renter</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="ui-advanced">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{route('cars.index')}}">Cars</a></li>

         </ul>
      </div>
    </li>
    @endcan
    @can('travelagency-list')
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#form-elements" aria-expanded="false" aria-controls="form-elements">
        <i class="fab fa-wpforms menu-icon"></i>
        <span class="menu-title">Agency</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="form-elements">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"><a class="nav-link" href="{{route('travel-agencies.index')}}">Agencies</a></li>
          </ul>
      </div>
    </li>
    @endcan
    @can('guider-list')
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#editors" aria-expanded="false" aria-controls="editors">
        <i class="fas fa-pen-square menu-icon"></i>
        <span class="menu-title">Guider</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="editors">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"><a class="nav-link" href="{{route('guiders.index')}}">Guiders</a></li>
        </ul>
      </div>
    </li>
    @endcan
{{--    <li class="nav-item">--}}
{{--      <a class="nav-link" data-toggle="collapse" href="#maps" aria-expanded="false" aria-controls="maps">--}}
{{--        <i class="fas fa-map-marker-alt menu-icon"></i>--}}
{{--        <span class="menu-title">Locations</span>--}}
{{--        <i class="menu-arrow"></i>--}}
{{--      </a>--}}
{{--      <div class="collapse" id="maps">--}}
{{--        <ul class="nav flex-column sub-menu">--}}
{{--          <li class="nav-item"> <a class="nav-link" href="{{route('locations.index')}}">Locations</a></li>--}}

{{--        </ul>--}}
{{--      </div>--}}
{{--    </li>--}}

      <li class="nav-item">
          <a class="nav-link" data-toggle="collapse" href="#a-book" aria-expanded="false" aria-controls="maps">
              <i class="fas fa-columns menu-icon"></i>
              <span class="menu-title">Booking</span>
              <i class="menu-arrow"></i>
          </a>

          <div class="collapse" id="a-book">
              <ul class="nav flex-column sub-menu">
                  @can('hotel-booking-list')
                  <li class="nav-item"> <a class="nav-link" href="{{route('accomodation-booking.index')}}">Hotels</a></li>
                  @endcan
                  @can('car-booking-list')
                          <li class="nav-item"> <a class="nav-link" href="{{route('car-booking.index')}}">Car</a></li>
                      @endcan
                      @can('package-booking-list')
                          <li class="nav-item"> <a class="nav-link" href="{{route('package-booking.index')}}">Tour Package</a></li>
                      @endcan
              </ul>
          </div>


      </li>

    <li class="nav-item">
      <a class="nav-link" href="{{route('blogs.index')}}">
        <i class="fas fa-window-restore menu-icon"></i>
        <span class="menu-title">Blog</span>
      </a>
    </li>

{{--    <li class="nav-item">--}}
{{--      <a class="nav-link" data-toggle="collapse" href="#general-pages" aria-expanded="false" aria-controls="general-pages">--}}
{{--        <i class="fas fa-file menu-icon"></i>--}}
{{--        <span class="menu-title">Review</span>--}}
{{--        <i class="menu-arrow"></i>--}}
{{--      </a>--}}
{{--      <div class="collapse" id="general-pages">--}}
{{--        <ul class="nav flex-column sub-menu">--}}
{{--          <li class="nav-item"> <a class="nav-link" href="pages/samples/blank-page.html"> Hotels </a></li>--}}
{{--          <li class="nav-item"> <a class="nav-link" href="pages/samples/profile.html"> Profile </a></li>--}}
{{--        </ul>--}}
{{--      </div>--}}
{{--    </li>--}}

  </ul>
</nav>
















{{--<aside class="main-sidebar"> --}}
    {{--<div class="sidebar"> --}}
      {{--<!-- Sidebar user panel -->--}}
      {{--<div class="user-panel">--}}
        {{--<div class="image text-center"><img src="{{asset('backend/dist/img/img1.jpg')}}" class="img-circle" alt="User Image"> </div>--}}
        {{--<div class="info">--}}
          {{--<p> {{ Auth::user()->name }} </p>--}}
          {{--</div>--}}
      {{--</div>--}}
      {{----}}
      {{--<!-- sidebar menu: : style can be found in sidebar.less -->--}}
      {{--<ul class="sidebar-menu" data-widget="tree">--}}
        {{--<li class="header">Permist</li>--}}
        {{--<li class="active treeview"> <a href="#"> <i class="fa fa-dashboard"></i> <span>Dashboard</span>  </a>--}}
        {{--</li>--}}
        {{--<li class=" treeview"> <a href="#"> <i class="fa fa-edit"></i> <span>Permision</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>--}}
          {{--<ul class="treeview-menu">--}}
            {{--<li><a href="{{ route('users.index') }}">User</a></li>--}}
            {{--<li><a href="{{ route('roles.index') }}">Roles</a></li>--}}
          {{--</ul>--}}
        {{--</li>--}}
        {{--<li class=" treeview"> <a href="#"> <i class="fa fa-edit"></i> <span>Locations</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>--}}
          {{--<ul class="treeview-menu">--}}
            {{--<li><a href="form-elements.html">District</a></li>--}}
            {{----}}
          {{--</ul>--}}
        {{--</li>--}}
        {{----}}
        {{----}}
      {{--</ul>--}}
    {{--</div>--}}

  {{--</aside>--}}
