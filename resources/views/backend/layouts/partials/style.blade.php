<link rel="stylesheet" href="{{asset('backend/vendors/iconfonts/font-awesome/css/all.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/vendors/css/vendor.bundle.base.css')}}">
<link rel="stylesheet" href="{{asset('backend/vendors/css/vendor.bundle.addons.css')}}">
<link rel="stylesheet" href="{{asset('backend/css/style.css')}}">
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">

{{--<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />--}}

{{--<!-- v4.0.0-alpha.6 -->--}}
{{--<link rel="stylesheet" href="{{asset('backend/dist/bootstrap/css/bootstrap.min.css')}}">--}}

{{--<!-- Google Font -->--}}
{{--<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">--}}

{{--<!-- Theme style -->--}}
{{--<link rel="stylesheet" href="{{asset('backend/dist/css/style.css')}}">--}}
{{--<link rel="stylesheet" href="{{asset('backend/dist/css/font-awesome/css/font-awesome.min.css')}}">--}}
{{--<link rel="stylesheet" href="{{asset('backend/dist/css/et-line-font/et-line-font.css')}}">--}}
{{--<link rel="stylesheet" href="{{asset('backend/dist/css/themify-icons/themify-icons.css')}}">--}}

{{--<!-- Chartist CSS -->--}}
{{--<link rel="stylesheet" href="{{asset('backend/dist/plugins/chartist-js/chartist.min.css')}}">--}}
{{--<link rel="stylesheet" href="{{asset('backend/dist/plugins/chartist-js/chartist-plugin-tooltip.css')}}">--}}
