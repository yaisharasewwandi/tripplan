<!-- plugins:js -->
<script src="{{asset('backend/vendors/js/vendor.bundle.base.js')}}"></script>
<script src="{{asset('backend/vendors/js/vendor.bundle.addons.js')}}"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="{{asset('backend/js/off-canvas.js')}}"></script>
<script src="{{asset('backend/js/hoverable-collapse.js')}}"></script>
<script src="{{asset('backend/js/misc.js')}}"></script>
<script src="{{asset('backend/js/settings.js')}}"></script>
<script src="{{asset('backend/js/todolist.js')}}"></script>
<script src="{{asset('backend/js/dropify.js')}}"></script>
<script src="{{asset('backend/js/form-addons.js')}}"></script>
<script src="{{asset('backend/js/select2.js')}}"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{asset('backend/js/dashboard.js')}}"></script>
{{--<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>--}}

<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
<script>
$('.summernote').summernote({height: 300});

    </script>
<script type="text/javascript" src="{{asset('backend/scroll/jquery.slimscroll.js')}}"></script>
<link href="{{asset('backend/scroll/libs/prettify/prettify.css')}}" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="{{asset('backend/scroll/libs/prettify/prettify.js')}}"></script>

<script type="text/javascript">
    $(function(){
      $('.testDiv').slimScroll({
          height: 'auto'
      });

    });
</script>

