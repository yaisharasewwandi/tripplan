@extends('backend.layouts.master')
@section('content')

  <div class="main-panel">
    <div class="content-wrapper">
      <div class="page-header">
        <h3 class="page-title">
          Dashboard
        </h3>
      </div>
      <div class="row grid-margin">
        <div class="col-12">
          <div class="card card-statistics">
            <div class="card-body">
              <div class="d-flex flex-column flex-md-row align-items-center justify-content-between">
                <div class="statistics-item">
                  <p>
                    <i class="icon-sm fa fa-user mr-2"></i>
                     users
                  </p>
                  <h2>{{$users}}</h2>
                  <label class="badge badge-outline-success badge-pill">2.7% increase</label>
                </div>
                <div class="statistics-item">
                  <p>
                    <i class="icon-sm fas fa-hourglass-half mr-2"></i>
                    Hotels Booking
                  </p>
                  <h2>{{$hotels}}</h2>
                  <label class="badge badge-outline-danger badge-pill">30% decrease</label>
                </div>
                <div class="statistics-item">
                  <p>
                    <i class="icon-sm fas fa-cloud-download-alt mr-2"></i>
                    Cars Booking
                  </p>
                  <h2>{{$cars}}</h2>
                  <label class="badge badge-outline-success badge-pill">12% increase</label>
                </div>
                <div class="statistics-item">
                  <p>
                    <i class="icon-sm fas fa-check-circle mr-2"></i>
                    Tour Packages Booking
                  </p>
                  <h2>{{$tour}}</h2>
                  <label class="badge badge-outline-success badge-pill">57% increase</label>
                </div>
                <div class="statistics-item">
                  <p>
                    <i class="icon-sm fas fa-chart-line mr-2"></i>
                    Hotels
                  </p>
                  <h2>{{$acc}}</h2>
                  <label class="badge badge-outline-success badge-pill">10% increase</label>
                </div>
                <div class="statistics-item">
                  <p>
                    <i class="icon-sm fas fa-circle-notch mr-2"></i>
                    Pending
                  </p>
                  <h2>0</h2>
                  <label class="badge badge-outline-danger badge-pill">16% decrease</label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>




    </div>

 @endsection
