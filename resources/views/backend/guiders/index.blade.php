@extends('backend.layouts.master')
@section('content')

    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Guiders table
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Tables</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Travel Agency table</li>
                    </ol>
                </nav>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"> Guiders List</h4>
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="order-listing" class="table">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Contact No</th>
                                        <th>Address</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($guiders as $key => $guider)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $guider->user->name }}</td>
                                            <td>{{ $guider->user->email }}</td>
                                            <td>{{ $guider->user->contact_number }}</td>
                                            <td>{{ $guider->address }}</td>
                                            @can('guider-publish')
                                                <td>
                                                    <input data-id="{{$guider->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $guider->publish_status ? 'checked' : '' }}>
                                                </td>
                                            @endcan
                                            <td>
                                                @can('guider-location')
                                                    <a class="btn btn-outline-success" style="margin-bottom: 2px" href="{{route('guider-details.show',$guider->id)}}">Add Location</a>
                                                @endcan
                                                @can('guider-view')
                                                    <a class="btn btn-outline-primary" href="{{ route('guiders.show',$guider->id) }}">View</a>
                                                @endcan
                                                @can('guider-edit')
                                                    <a class="btn btn-outline-info" href="{{ route('guiders.edit',$guider->id) }}">Edit</a>
                                                @endcan
                                                @can('guider-delete')
                                                    {!! Form::open(['method' => 'DELETE','route' => ['guiders.destroy', $guider->id],'style'=>'display:inline']) !!}
                                                    {!! Form::submit('Delete', ['class' => 'btn btn-outline-danger','data-toggle'=>'confirmation']) !!}
                                                    {!! Form::close() !!}
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @can('guider-create')
            <a href="{{ route('guiders.create') }}">
                <div class="theme-setting-wrapper">
                    <div id="settings-trigger"><i class="fas fa-plus"> </i></div>
                </div></a>
        @endcan
        @section('scripts')
            <script src="{{asset('backend/js/data-table.js')}}"></script>
            <script>
                $(function() {
                    $('.toggle-class').change(function() {
                        var publish_status = $(this).prop('checked') == true ? 1 : 0;
                        var aguider_id = $(this).data('id');

                        $.ajax({
                            type: "GET",
                            dataType: "json",
                            url: '/guiderchangeStatus',
                            data: {'publish_status': publish_status, 'aguider_id': aguider_id},
                            success: function(data){
                                toastr.success('Review successfully');
                                console.log(data.success)


                            },
                            error: function (msg) {
                                toastr.error( 'Inconceivable!');
                            }
                        });
                    })
                })
            </script>
@endsection

@endsection
