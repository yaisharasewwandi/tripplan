@extends('backend.layouts.master')
@section('content')

    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Restaurant table
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Tables</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Restaurant table</li>
                    </ol>
                </nav>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"> Restaurant List</h4>
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="order-listing" class="table">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Title</th>
                                        <th>Email</th>
                                        <th>Contact No</th>
                                        <th>Address</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($restaurants as $key => $restaurant)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $restaurant->title }}</td>
                                            <td>{{ $restaurant->email }}</td>
                                            <td>{{ $restaurant->contact_number }}</td>
                                            <td>{{ $restaurant->address }}</td>
                                            @can('restaurant-publish')
                                            <td>
                                                <input data-id="{{$restaurant->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $restaurant->publish_status ? 'checked' : '' }}>
                                            </td>
                                            @endcan
                                            <td>
                                                @can('restaurant-item')
                                                <a class="btn btn-outline-success" style="margin-bottom: 2px" href="{{ route('items.show',$restaurant->id) }}">Add Items</a>
                                                @endcan
                                                @can('restaurant-view')
                                                <a class="btn btn-outline-primary" href="{{ route('restaurants.show',$restaurant->id) }}">View</a>
                                                @endcan
                                                @can('restaurant-edit')
                                                <a class="btn btn-outline-info" href="{{ route('restaurants.edit',$restaurant->id) }}">Edit</a>
                                                @endcan
                                                @can('restaurant-delete')
                                                    {!! Form::open(['method' => 'DELETE','route' => ['restaurants.destroy', $restaurant->id],'style'=>'display:inline']) !!}
                                                    {!! Form::submit('Delete', ['class' => 'btn btn-outline-danger','data-toggle'=>'confirmation']) !!}
                                                    {!! Form::close() !!}
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @can('restaurant-create')
            <a href="{{ route('restaurants.create') }}">
                <div class="theme-setting-wrapper">
                    <div id="settings-trigger"><i class="fas fa-plus"> </i></div>
                </div></a>
        @endcan
        @section('scripts')
            <script src="{{asset('backend/js/data-table.js')}}"></script>
            <script>
  $(function() {
    $('.toggle-class').change(function() {
        var publish_status = $(this).prop('checked') == true ? 1 : 0;
        var restaurant_id = $(this).data('id');

        $.ajax({
            type: "GET",
            dataType: "json",
            url: '/restaurantStatus',
            data: {'publish_status': publish_status, 'restaurant_id': restaurant_id},
            success: function(data){
             toastr.success('Review successfully');
              console.log(data.success)


        },
        error: function (msg) {
            toastr.error( 'Inconceivable!');
        }
        });
    })
  })
</script>
@endsection

@endsection
