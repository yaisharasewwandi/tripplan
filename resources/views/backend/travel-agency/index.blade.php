@extends('backend.layouts.master')
@section('content')

    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Travel Agency table
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Tables</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Travel Agency table</li>
                    </ol>
                </nav>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"> Travel Agency List</h4>
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="order-listing" class="table">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Title</th>
                                        <th>Email</th>
                                        <th>Contact No</th>
                                        <th>Address</th>
                                        @can('travelagency-publish')
                                        <th>Status</th>
                                        @endcan
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($agencies as $key => $agency)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $agency->title }}</td>
                                            <td>{{ $agency->email }}</td>
                                            <td>{{ $agency->contact_number }}</td>
                                            <td>{{ $agency->address }}</td>
                                            @can('travelagency-publish')
                                            <td>
                                                <input data-id="{{$agency->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $agency->publish_status ? 'checked' : '' }}>
                                            </td>
                                            @endcan
                                            <td>
                                                @can('travelagency-package')
                                                <a class="btn btn-outline-success" href="{{route('tour-packages.show',$agency->id)}}">Add Packages</a>
                                                @endcan
                                                @can('travelagency-view')
                                                <a class="btn btn-outline-primary" href="{{ route('travel-agencies.show',$agency->id) }}">View</a>
                                                @endcan
                                                @can('travelagency-edit')
                                                <a class="btn btn-outline-info" href="{{ route('travel-agencies.edit',$agency->id) }}">Edit</a>
                                                @endcan
                                                @can('travelagency-delete')
                                                    {!! Form::open(['method' => 'DELETE','route' => ['travel-agencies.destroy', $agency->id],'style'=>'display:inline']) !!}
                                                    {!! Form::submit('Delete', ['class' => 'btn btn-outline-danger','data-toggle'=>'confirmation']) !!}
                                                    {!! Form::close() !!}
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @can('travelagency-create')
            <a href="{{ route('travel-agencies.create') }}">
                <div class="theme-setting-wrapper">
                    <div id="settings-trigger"><i class="fas fa-plus"> </i></div>
                </div></a>
        @endcan
        @section('scripts')
            <script src="{{asset('backend/js/data-table.js')}}"></script>
            <script>
  $(function() {
    $('.toggle-class').change(function() {
        var publish_status = $(this).prop('checked') == true ? 1 : 0;
        var agency_id = $(this).data('id');

        $.ajax({
            type: "GET",
            dataType: "json",
            url: '/agencychangeStatus',
            data: {'publish_status': publish_status, 'agency_id': agency_id},
            success: function(data){
             toastr.success('Review successfully');
              console.log(data.success)


        },
        error: function (msg) {
            toastr.error( 'Inconceivable!');
        }
        });
    })
  })
</script>
@endsection

@endsection
