@extends('backend.layouts.master')
@section('content')

    <div class="main-panel">
        {!! Form::model($packages,['route' => ['tour-packages.update',$packages->id],'method'=>'PATCH','class'=>'forms-sample','files'=>'true']) !!}

        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Edit Tour Packages
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Forms</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Tour Packages</li>
                    </ol>
                </nav>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Edit Tour Packages</h4>


                            <div class="form-group">
                                <label for="exampleInputName1">Title</label>
                                <input type="hidden" name="travel_agency_id" value="{!! $id !!}">
                                {!! Form::text('title', null, array('placeholder' => 'Title','class' => 'form-control','id'=>'title')) !!}
                            </div>
                            <div class="form-group">
                                <label for="exampleInputName1">Slug</label>
                                {!! Form::text('slug', null, array('placeholder' => 'Slug','class' => 'form-control','id'=>'slug')) !!}
                            </div>

                            <div class="form-group" id="">
                                <label for="exampleInputEmail3">About Tour Packages</label>
                                {!! Form::textarea('description', null, array('placeholder' => 'Description','class' => 'form-control summernote','cols'=>'30','rows'=>'10')) !!}

                            </div>
                            <div class="form-group">
                                <label for="exampleInputName1">Location</label>
                                {!! Form::text('location', null, array('placeholder' => 'Location','class' => 'form-control ','id'=>'autocomplete')) !!}
                            </div>
                            <div class="row">
                                <div class="col-lg-6 grid-margin stretch-card">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title d-flex"> Feature Image
                                                <small class="ml-auto align-self-end">
                                                    <a href="dropify.html" class="font-weight-light" target="_blank"></a>
                                                </small>
                                            </h4>
                                            <input type="file" name="feature_image" class="" />
                                            @foreach($packages->images as $vs)
                                                @if($vs->is_featured == 1 && $vs->is_featured !== null)
                                                    <div class="col-lg-4 col-md-6 col-sm-6">
                                                        <img src="{{ asset($vs->image_name) }}" id="banner_image"
                                                             style="width: auto;height: 120px;margin-top: 15px;">
                                                        <a href="/tour/imagedelete/{{$vs->id}}"
                                                           class="badge badge-danger p-2 mb-3">Delete</a><br>

                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 grid-margin stretch-card">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title d-flex">Gallery Images
                                                <small class="ml-auto align-self-end">
                                                    <a href="dropify.html" class="font-weight-light" target="_blank"></a>
                                                </small>
                                            </h4>
                                            <input type="file" name="images[]" class="" multiple />
                                            <div class="row">
                                                    @foreach($packages->images as $vs)
                                                        @if($vs->is_featured !== 1)
                                                            <div class="col-lg-4 col-md-6 col-sm-6">
                                                                <img src="{{ asset($vs->image_name) }}" id="banner_image" style="width: auto;height: 120px;margin-top: 15px;">
                                                                <a href="/tour/imagedelete/{{$vs->id}}" class="badge badge-danger p-2 mb-3">Delete</a><br>

                                                            </div>
                                                        @endif
                                                    @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Duration</label>
                                    {!! Form::text('duration', null, array('placeholder' => 'Duration','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Min Person</label>
                                    {!! Form::number('min_person', null, array('placeholder' => 'Min Person','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Max Person</label>
                                    {!! Form::number('max_person', null, array('placeholder' => 'Max Person','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Budget</label>
                                    {!! Form::number('budget', null, array('placeholder' => 'Budget','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 grid-margin stretch-card">
                                    <div class="card">
                                        <div class="card-body testDiv">
                                            <h4 class="card-title">Includes</h4>
                                            <div class="field_wrapper">
                                                <div class="row ">
                                                    <div class="form-group col-md-10 ">
                                                        <input type="text" name="includes[]" value="" class="form-control">
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <a href="javascript:void(0);" class="add_button" title="Add field"><img src="{{asset('backend/add-icon.png')}}"/></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6 grid-margin stretch-card">
                                    <div class="card">
                                        <div class="card-body testDiv">
                                            <h4 class="card-title">Excludes</h4>
                                            <div class="field_wrapper2">
                                                <div class="row ">
                                                    <div class="form-group col-md-10 ">
                                                        <input type="text" name="excludes[]" value="" class="form-control">
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <a href="javascript:void(0);" class="add_button2" title="Add field"><img src="{{asset('backend/add-icon.png')}}"/></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 grid-margin stretch-card">
                                    <div class="card">
                                        <div class="card-body testDiv">
                                            <h4 class="card-title">Available Times</h4>

                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <div class="form-check ">
                                                        <label class="form-check-label">
                                                            <input type="radio" name="special_day"  class="form-check-input" id="optionsRadios1" value="1" {{ $packages->special_day == 1 ? 'checked' : '' }}>
                                                            Special Event
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <div class="form-check ">
                                                        <label class="form-check-label">
                                                            <input type="radio" name="special_day"  class="form-check-input" id="optionsRadios1" value="0" {{ $packages->special_day == 0 ? 'checked' : '' }}>
                                                            Normal
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="field_wrapper3" id="normal">
                                                <div class="row " >
                                                    <div class="form-group col-md-10 ">
                                                        <input type="text" name="available_times[]" value="" class="form-control available_times" placeholder="Eg:Saturday 8:00am to 4:00pm">
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <a href="javascript:void(0);" class="add_button3" title="Add field"><img src="{{asset('backend/add-icon.png')}}"/></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="field_wrapper4" id="special">
                                                <div class="row " >
                                                    <div class="form-group col-md-6 ">
                                                        <input type="date" name="days[]" value="" class="form-control days">
                                                    </div>
                                                    <div class="form-group col-md-4 ">
                                                        <input type="time" name="times[]" value="" class="form-control times">
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <a href="javascript:void(0);" class="add_button4" title="Add field"><img src="{{asset('backend/add-icon.png')}}"/></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-6 grid-margin stretch-card">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title">Room Amenities</h4>
                                            <div class="card testDiv">
                                                <div class="card-body ">
                                                    @foreach($tourcategories as $tourcategory)
                                                        <div class="form-group">
                                                            <div class="form-check">
                                                                <label class="form-check-label">
                                                                    <label>{{ Form::checkbox('categories[]', $tourcategory->id, in_array($tourcategory->id, $selectcategory) ? true : false, array('class' => 'form-check-input','id'=>'categories')) }}
                                                                        {{ $tourcategory->name }}</label>

                                                                </label>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                <button class="btn btn-light">Cancel</button>
                {!! Form::close() !!}
            </div>

@section('scripts')

                <script src="{{asset('backend/js/data-table.js')}}"></script>

                <script>

                    $(function() {
                        $('input[name="special_day"]').on('click', function() {
                            if ($(this).val() == 1) {
                                $('#special').show();
                                $('#normal').hide();
                                $('.available_times').removeAttr('disabled').attr('disabled', 'disabled');
                                $('.days').removeAttr('disabled');
                                $('.times').removeAttr('disabled');
                            }
                            else {
                                $('#normal').show();
                                $('#special').hide();
                                $('.days').removeAttr('disabled').attr('disabled', 'disabled');
                                $('.times').removeAttr('disabled').attr('disabled', 'disabled');
                                $('.available_times').removeAttr('disabled');
                            }
                        });
                        $('#special').hide();
                        $('#normal').hide();
                    });

                </script>
                <script>
                    $('#title').change(function(e) {
                        $.get('{{ route('package.check_slug') }}',
                            { 'title': $(this).val() },
                            function( data ) {
                                $('#slug').val(data.slug);
                            }
                        );
                    });
                </script>
                <script type="text/javascript">
                    $(document).ready(function(){
                        var maxField = 10; //Input fields increment limitation
                        var addButton = $('.add_button'); //Add button selector
                        var wrapper = $('.field_wrapper'); //Input field wrapper
                        var fieldHTML = ' <div class="row "><div class="form-group col-md-10"><input type="text" name="includes[]" value="" class="form-control"></div><a href="javascript:void(0);" class="remove_button"><img src="{{asset('backend/remove-icon.png')}}"/></a></div>'; //New input field html
                        var x = 1; //Initial field counter is 1

                        //Once add button is clicked
                        $(addButton).click(function(){
                            //Check maximum number of input fields
                            if(x < maxField){
                                x++; //Increment field counter
                                $(wrapper).append(fieldHTML); //Add field html
                            }
                        });

                        //Once remove button is clicked
                        $(wrapper).on('click', '.remove_button', function(e){
                            e.preventDefault();
                            $(this).parent('div').remove(); //Remove field html
                            x--; //Decrement field counter
                        });
                    });
                </script>
                <script type="text/javascript">
                    $(document).ready(function(){
                        var maxField = 10; //Input fields increment limitation
                        var addButton = $('.add_button2'); //Add button selector
                        var wrapper = $('.field_wrapper2'); //Input field wrapper
                        var fieldHTML = ' <div class="row "><div class="form-group col-md-10"><input type="text" name="excludes[]" value="" class="form-control"></div><a href="javascript:void(0);" class="remove_button"><img src="{{asset('backend/remove-icon.png')}}"/></a></div>'; //New input field html
                        var x = 1; //Initial field counter is 1

                        //Once add button is clicked
                        $(addButton).click(function(){
                            //Check maximum number of input fields
                            if(x < maxField){
                                x++; //Increment field counter
                                $(wrapper).append(fieldHTML); //Add field html
                            }
                        });

                        //Once remove button is clicked
                        $(wrapper).on('click', '.remove_button', function(e){
                            e.preventDefault();
                            $(this).parent('div').remove(); //Remove field html
                            x--; //Decrement field counter
                        });
                    });
                </script>
                <script type="text/javascript">
                    $(document).ready(function(){
                        var maxField = 10; //Input fields increment limitation
                        var addButton = $('.add_button3'); //Add button selector
                        var wrapper = $('.field_wrapper3'); //Input field wrapper
                        var fieldHTML = ' <div class="row "><div class="form-group col-md-10"><input type="text" name="available_times[]" value="" class="form-control" placeholder="Eg:Saturday 8:00am to 4:00pm"></div><a href="javascript:void(0);" class="remove_button"><img src="{{asset('backend/remove-icon.png')}}"/></a></div>'; //New input field html
                        var x = 1; //Initial field counter is 1

                        //Once add button is clicked
                        $(addButton).click(function(){
                            //Check maximum number of input fields
                            if(x < maxField){
                                x++; //Increment field counter
                                $(wrapper).append(fieldHTML); //Add field html
                            }
                        });

                        //Once remove button is clicked
                        $(wrapper).on('click', '.remove_button', function(e){
                            e.preventDefault();
                            $(this).parent('div').remove(); //Remove field html
                            x--; //Decrement field counter
                        });
                    });
                </script>
                <script type="text/javascript">
                    $(document).ready(function(){
                        var maxField = 10; //Input fields increment limitation
                        var addButton = $('.add_button4'); //Add button selector
                        var wrapper = $('.field_wrapper4'); //Input field wrapper
                        var fieldHTML = ' <div class="row "> <div class="form-group col-md-6 "><input type="date" name="days[]" value="" class="form-control"></div><div class="form-group col-md-4 "><input type="time" name="times[]" value="" class="form-control"></div><a href="javascript:void(0);" class="remove_button"><img src="{{asset('backend/remove-icon.png')}}"/></a></div>'; //New input field html
                        var x = 1; //Initial field counter is 1

                        //Once add button is clicked
                        $(addButton).click(function(){
                            //Check maximum number of input fields
                            if(x < maxField){
                                x++; //Increment field counter
                                $(wrapper).append(fieldHTML); //Add field html
                            }
                        });

                        //Once remove button is clicked
                        $(wrapper).on('click', '.remove_button', function(e){
                            e.preventDefault();
                            $(this).parent('div').remove(); //Remove field html
                            x--; //Decrement field counter
                        });
                    });
                </script>
                <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyANuk6NO4FfxgvcQrb8N70ZDCKF-vWh6Jg&libraries=places"></script>
                <script>

                    $(document).ready(function() {
                        $("#lat_area").addClass("d-none");
                        $("#long_area").addClass("d-none");
                    });
                </script>
                <script>
                    google.maps.event.addDomListener(window, 'load', initialize);

                    function initialize() {
                        var input = document.getElementById('autocomplete');
                        var autocomplete = new google.maps.places.Autocomplete(input);
                        autocomplete.addListener('place_changed', function() {
                            var place = autocomplete.getPlace();
                            $('#latitude').val(place.geometry['location'].lat());
                            $('#longitude').val(place.geometry['location'].lng());
                            $("#lat_area").removeClass("d-none");
                            $("#long_area").removeClass("d-none");
                        });
                    }

                </script>

@endsection
@endsection
