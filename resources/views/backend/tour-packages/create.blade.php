@extends('backend.layouts.master')
@section('content')

    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Create Tour Packages
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Forms</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create Tour Packages</li>
                    </ol>
                </nav>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::open(array('route' => 'tour-packages.store','method'=>'POST','class'=>'forms-sample','enctype' => 'multipart/form-data')) !!}

            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Create Tour Packages</h4>


                            <div class="form-group">
                                <label for="exampleInputName1">Title</label>
                                <input type="hidden" name="travel_agency_id" value="{!! $id !!}">
                                {!! Form::text('title', null, array('placeholder' => 'Title','class' => 'form-control','id'=>'title')) !!}
                            </div>
                            <div class="form-group">
                                <label for="exampleInputName1">Slug</label>
                                {!! Form::text('slug', null, array('placeholder' => 'Slug','class' => 'form-control','id'=>'slug')) !!}
                            </div>

                            <div class="form-group" id="">
                                <label for="exampleInputEmail3">About Tour Packages</label>
                                {!! Form::textarea('description', null, array('placeholder' => 'Description','class' => 'form-control summernote','cols'=>'30','rows'=>'10')) !!}

                            </div>
                            <div class="form-group">
                                <label for="exampleInputName1">Location</label>
                                {!! Form::text('location', null, array('placeholder' => 'Location','class' => 'form-control ','id'=>'autocomplete')) !!}
                            </div>
                            <div class="row">
                            <div class="col-lg-6 grid-margin stretch-card">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title d-flex"> Feature Image
                                            <small class="ml-auto align-self-end">
                                                <a href="dropify.html" class="font-weight-light" target="_blank"></a>
                                            </small>
                                        </h4>
                                        <input type="file" name="feature_image" class="" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 grid-margin stretch-card">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title d-flex">Gallery Images
                                            <small class="ml-auto align-self-end">
                                                <a href="dropify.html" class="font-weight-light" target="_blank"></a>
                                            </small>
                                        </h4>
                                        <input type="file" name="images[]" class="" multiple />
                                    </div>
                                </div>
                            </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Duration</label>
                                    {!! Form::text('duration', null, array('placeholder' => 'Duration','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Min Person</label>
                                    {!! Form::number('min_person', null, array('placeholder' => 'Min Person','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Max Person</label>
                                    {!! Form::number('max_person', null, array('placeholder' => 'Max Person','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Budget</label>
                                    {!! Form::number('budget', null, array('placeholder' => 'Budget','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 grid-margin stretch-card">
                                    <div class="card">
                                        <div class="card-body testDiv">
                                            <h4 class="card-title">Includes</h4>
                                            <div class="field_wrapper">
                                            <div class="row ">
                                                <div class="form-group col-md-10 ">
                                                    <input type="text" name="includes[]" value="" class="form-control">
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <a href="javascript:void(0);" class="add_button" title="Add field"><img src="{{asset('backend/add-icon.png')}}"/></a>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6 grid-margin stretch-card">
                                    <div class="card">
                                        <div class="card-body testDiv">
                                            <h4 class="card-title">Excludes</h4>
                                            <div class="field_wrapper2">
                                                <div class="row ">
                                                    <div class="form-group col-md-10 ">
                                                        <input type="text" name="excludes[]" value="" class="form-control">
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <a href="javascript:void(0);" class="add_button2" title="Add field"><img src="{{asset('backend/add-icon.png')}}"/></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 grid-margin stretch-card">
                                    <div class="card">
                                        <div class="card-body testDiv">
                                            <h4 class="card-title">Available Times</h4>

                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <div class="form-check ">
                                                            <label class="form-check-label">
                                                                <input type="radio" name="special_day"  class="form-check-input" id="optionsRadios1" value="1">
                                                                Special Event
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <div class="form-check ">
                                                            <label class="form-check-label">
                                                                <input type="radio" name="special_day"  class="form-check-input" id="optionsRadios1" value="0">
                                                                Normal
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                            <div class="field_wrapper3" id="normal">
                                                <div class="row " >
                                                    <div class="form-group col-md-10 ">
                                                        <input type="text" name="available_times[]" value="" class="form-control available_times" placeholder="Eg:Saturday 8:00am to 4:00pm">
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <a href="javascript:void(0);" class="add_button3" title="Add field"><img src="{{asset('backend/add-icon.png')}}"/></a>
                                                    </div>
                                                </div>
                                            </div>
                                                <div class="field_wrapper4" id="special">
                                                <div class="row " >
                                                    <div class="form-group col-md-6 ">
                                                        <input type="date" name="days[]" value="" class="form-control days">
                                                    </div>
                                                    <div class="form-group col-md-4 ">
                                                        <input type="time" name="times[]" value="" class="form-control times">
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <a href="javascript:void(0);" class="add_button4" title="Add field"><img src="{{asset('backend/add-icon.png')}}"/></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-6 grid-margin stretch-card">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title">Room Amenities</h4>
                                            <div class="card testDiv">
                                                <div class="card-body ">
                                                    @foreach($tourcategories as $tourcategory)
                                                        <div class="form-group">
                                                            <div class="form-check">
                                                                <label class="form-check-label">
                                                                    <input type="checkbox" name="categories[]" class="form-check-input" value="{{$tourcategory['id']}}">
                                                                    {{$tourcategory['name']}}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                <button class="btn btn-light">Cancel</button>
                {!! Form::close() !!}
            </div>
                <div class="col-lg-12 grid-margin d-flex stretch-card">
                    <div class="row flex-grow">
                        <div class="col-12 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Rooms</h4>
                                    <div class="card ">
                                        <div class="card-body ">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="table-responsive">
                                                        <table id="order-listing" class="table">
                                                            <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Name</th>
                                                                <th>Location</th>
                                                                <th>Duration</th>
                                                                <th>Min Person</th>
                                                                <th>Max Person</th>
                                                                <th>Budget</th>
                                                                <th>Status</th>
                                                                <th>Actions</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach ($packages as $key => $package)
                                                                <tr>
                                                                    <td>{{ ++$i }}</td>
                                                                    <td>{{ $package->title }}</td>
                                                                    <td>{{ $package->location }}</td>
                                                                    <td>{{ $package->duration }}</td>
                                                                    <td>{{ $package->min_person }}</td>
                                                                    <td>{{ $package->max_person }}</td>
                                                                    <td>{{ $package->budget }}</td>

                                                                    <td>
                                                                        <input data-id="{{$package->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $package->publish_status ? 'checked' : '' }}>
                                                                    </td>
                                                                    <td>
                                                                        <a class="btn btn-outline-primary" style="margin-bottom: 2px"
                                                                           data-toggle="modal"
                                                                           data-target="#exampleModal"
                                                                           onclick="editss(this)"
                                                                           data-features="{{$package->categories->pluck('name')}}"
                                                                           data-images="{{$package->images->pluck('image_name')}}"
                                                                           data-id='{{$package->id}}'
                                                                           data-title='{{$package->title}}'
                                                                           data-description="{{$package->description}}"
                                                                           data-duration="{{$package->duration}}"
                                                                           data-min_person="{{$package->min_person}}"
                                                                           data-max_person="{{$package->max_person}}"
                                                                           data-price="{{$package->budget}}"
                                                                           >View</a>
                                                                        @can('role-edit')
                                                                            <a class="btn btn-outline-info" href="{{ route('tour-packages.edit',$package->id) }}">Edit</a>
                                                                        @endcan
                                                                        @can('role-delete')
                                                                            {!! Form::open(['method' => 'DELETE','route' => ['tour-packages.destroy', $package->id],'style'=>'display:inline']) !!}
                                                                            {!! Form::submit('Delete', ['class' => 'btn btn-outline-danger','data-toggle'=>'confirmation']) !!}
                                                                            {!! Form::close() !!}
                                                                        @endcan
                                                                    </td>
                                                                </tr>
                                                            @endforeach


                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--tag strats-->

                        <!--tag ends-->
                    </div>
                </div>
        </div>
        <div class="modal fade" id="showModal" data-backdrop="static" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="width: 60%;left: 20%">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Package Details</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-inline">
                                    <label for="recipient-name" class="col-md-6">Name:</label>
                                    <p id="name">

                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-inline">
                                    <label for="recipient-name" class="col-md-6 ">Durations:</label>
                                    <p id="duration">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-inline">
                                    <label for="recipient-name" class="col-md-6 ">Min Person:</label>
                                    <p id="min_person">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-inline">
                                    <label for="recipient-name" class="col-md-6 ">Max Person:</label>
                                    <p id="max_person">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-inline">
                                    <label for="recipient-name" class="col-md-6 ">Price:</label>
                                    <p id="price">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="recipient-name" class="col-md-6 ">Features</label>
                            </div>
                            <div class="col-md-9 p-0" id="features_div"></div>
                        </div>

                        <div class="row" id="images_div">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>

    @section('scripts')
            <script src="{{asset('backend/js/data-table.js')}}"></script>
            <script>
                function editss(item) {
                    let id = item.dataset.id;
                    let name = item.dataset.title;
                    let description = item.dataset.description;
                    let duration = item.dataset.duration;
                    let min_person = item.dataset.min_person;
                    let max_person = item.dataset.max_person;
                    let price = item.dataset.price;
                    let images = JSON.parse(item.dataset.images);
                    let features = JSON.parse(item.dataset.features);

                    let html = '';
                    let base_path = "{{asset('')}}";

                    $.each(images, function (index, value) {
                        html += "<div class='col-md-6 p-3'>"
                        html += "<img class='img img-responsive w-100' src='" + base_path + value + "'/>"
                        html += "</div>"
                    });

                    $("#images_div").empty();
                    $("#images_div").append(html);


                    let features_label = '';
                    $.each(features, function (index, value) {
                        features_label += "<label class='badge badge-info badge-pill mr-1'>" + value + "</label>"
                    });


                    $("#features_div").empty();
                    $("#features_div").append(features_label);
                    console.log(features_label)

                    $("#name").empty();
                    $('#name').append(name);
                    $("#description").empty();
                    $('#description').append(description);
                    $("#duration").empty();
                    $('#duration').append(duration);
                    $("#min_person").empty();
                    $('#min_person').append(min_person);
                    $("#price").empty();
                    $('#price').append(price);
                    $("#max_person").empty();
                    $('#max_person').append(max_person);
                    $('#showModal').modal('show')
                }
  $(function() {
    $('.toggle-class').change(function() {
        var publish_status = $(this).prop('checked') == true ? 1 : 0;
        var packages_id = $(this).data('id');

        $.ajax({
            type: "GET",
            dataType: "json",
            url: '/packagesStatus',
            data: {'publish_status': publish_status, 'packages_id': packages_id},
            success: function(data){
             toastr.success('Review successfully');
              console.log(data.success)


        },
        error: function (msg) {
            toastr.error( 'Inconceivable!');
        }
        });
    })
  })
</script>
            <script>

        $(function() {
        $('input[name="special_day"]').on('click', function() {
        if ($(this).val() == 1) {
        $('#special').show();
        $('#normal').hide();
        $('.available_times').removeAttr('disabled').attr('disabled', 'disabled');
        $('.days').removeAttr('disabled');
        $('.times').removeAttr('disabled');
        }
        else {
        $('#normal').show();
        $('#special').hide();
        $('.days').removeAttr('disabled').attr('disabled', 'disabled');
        $('.times').removeAttr('disabled').attr('disabled', 'disabled');
        $('.available_times').removeAttr('disabled');
        }
        });
        $('#special').hide();
        $('#normal').hide();
        });

    </script>
<script>
  $('#title').change(function(e) {
    $.get('{{ route('package.check_slug') }}',
      { 'title': $(this).val() },
      function( data ) {
        $('#slug').val(data.slug);
      }
    );
  });
</script>
<script type="text/javascript">
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var fieldHTML = ' <div class="row "><div class="form-group col-md-10"><input type="text" name="includes[]" value="" class="form-control"></div><a href="javascript:void(0);" class="remove_button"><img src="{{asset('backend/remove-icon.png')}}"/></a></div>'; //New input field html
    var x = 1; //Initial field counter is 1

    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });

    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>
            <script type="text/javascript">
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button2'); //Add button selector
    var wrapper = $('.field_wrapper2'); //Input field wrapper
    var fieldHTML = ' <div class="row "><div class="form-group col-md-10"><input type="text" name="excludes[]" value="" class="form-control"></div><a href="javascript:void(0);" class="remove_button"><img src="{{asset('backend/remove-icon.png')}}"/></a></div>'; //New input field html
    var x = 1; //Initial field counter is 1

    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });

    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>
            <script type="text/javascript">
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button3'); //Add button selector
    var wrapper = $('.field_wrapper3'); //Input field wrapper
    var fieldHTML = ' <div class="row "><div class="form-group col-md-10"><input type="text" name="available_times[]" value="" class="form-control" placeholder="Eg:Saturday 8:00am to 4:00pm"></div><a href="javascript:void(0);" class="remove_button"><img src="{{asset('backend/remove-icon.png')}}"/></a></div>'; //New input field html
    var x = 1; //Initial field counter is 1

    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });

    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button4'); //Add button selector
    var wrapper = $('.field_wrapper4'); //Input field wrapper
    var fieldHTML = ' <div class="row "> <div class="form-group col-md-6 "><input type="date" name="days[]" value="" class="form-control"></div><div class="form-group col-md-4 "><input type="time" name="times[]" value="" class="form-control"></div><a href="javascript:void(0);" class="remove_button"><img src="{{asset('backend/remove-icon.png')}}"/></a></div>'; //New input field html
    var x = 1; //Initial field counter is 1

    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });

    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyANuk6NO4FfxgvcQrb8N70ZDCKF-vWh6Jg&libraries=places"></script>
<script>

   $(document).ready(function() {
        $("#lat_area").addClass("d-none");
        $("#long_area").addClass("d-none");
   });
</script>
            <script>
   google.maps.event.addDomListener(window, 'load', initialize);

   function initialize() {
       var input = document.getElementById('autocomplete');
       var autocomplete = new google.maps.places.Autocomplete(input);
       autocomplete.addListener('place_changed', function() {
           var place = autocomplete.getPlace();
           $('#latitude').val(place.geometry['location'].lat());
           $('#longitude').val(place.geometry['location'].lng());
           $("#lat_area").removeClass("d-none");
           $("#long_area").removeClass("d-none");
       });
   }

</script>

@endsection
@endsection
