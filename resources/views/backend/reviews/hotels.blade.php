@extends('backend.layouts.master')
@section('content')

    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Hotel Review table
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Tables</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Hotel Review table</li>
                    </ol>
                </nav>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"> Hotel Review List</h4>
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="order-listing" class="table">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Author</th>
                                        <th>Review Content</th>

                                        <th>Submitted On</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($hotelreviews as $key => $reviews)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $reviews->user['email'] }}</td>
                                            <td>{{ $reviews->comment }}<br> <select id="example-css" name="rating" autocomplete="off">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select></td>

                                            <td>{{ $reviews->created_at }}</td>
                                            {{--<td>--}}
                                            {{--<input data-id="{{$location->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $location->publish_status ? 'checked' : '' }}>--}}
                                            {{--</td>--}}
                                            <td>


                                                @can('role-delete')
                                                    {!! Form::open(['method' => 'DELETE','route' => ['locations.destroy', $reviews->id],'style'=>'display:inline']) !!}
                                                    {!! Form::submit('Delete', ['class' => 'btn btn-outline-danger','data-toggle'=>'confirmation']) !!}
                                                    {!! Form::close() !!}
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @can('role-create')
            <a href="{{ route('locations.create') }}">
                <div class="theme-setting-wrapper">
                    <div id="settings-trigger"><i class="fas fa-plus"> </i></div>
                </div></a>
        @endcan
        @section('scripts')
            <script src="{{asset('backend/js/data-table.js')}}"></script>
            <script>
                $(function() {
                    $('.toggle-class').change(function() {
                        var publish_status = $(this).prop('checked') == true ? 1 : 0;
                        var accommodation_id = $(this).data('id');

                        $.ajax({
                            type: "GET",
                            dataType: "json",
                            url: '/accommodationchangeStatus',
                            data: {'publish_status': publish_status, 'accommodation_id': accommodation_id},
                            success: function(data){
                                toastr.success('Review successfully');
                                console.log(data.success)


                            },
                            error: function (msg) {
                                toastr.error( 'Inconceivable!');
                            }
                        });
                    })
                })
            </script>
@endsection

@endsection