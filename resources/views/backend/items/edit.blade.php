@extends('backend.layouts.master')
@section('content')

    <div class="main-panel">
        {!! Form::model($items,['route' => ['items.update',$items->id],'method'=>'PATCH','class'=>'forms-sample','files'=>'true']) !!}

         <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Create Hotel
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Forms</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create Hotel</li>
                    </ol>
                </nav>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Edit Items</h4>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Title</label>
                                    <input type="hidden" name="restaurants_id" value="{!! $id !!}">
                                    {!! Form::text('title', null, array('placeholder' => 'Title','class' => 'form-control','id'=>'title')) !!}
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Slug</label>
                                    {!! Form::text('slug', null, array('placeholder' => 'Slug','class' => 'form-control','id'=>'slug', 'readonly' => 'true')) !!}
                                </div>
                            </div>
                            <div class="form-group" id="">
                                <label for="exampleInputEmail3">About Item</label>
                                {!! Form::textarea('description', null, array('placeholder' => 'Description','class' => 'form-control summernote','cols'=>'30','rows'=>'10')) !!}

                            </div>
                            <div class="row">
                                <div class="col-lg-6 grid-margin stretch-card">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title d-flex">Gallery Images
                                                <small class="ml-auto align-self-end">
                                                    <a href="dropify.html" class="font-weight-light" target="_blank"></a>
                                                </small>
                                            </h4>
                                            <input type="file" name="images[]" class="" multiple />
                                            <div class="row">
                                            @foreach($items->images as $vs)

                                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                                        <img src="{{ asset($vs->item_image) }}" class="w-100"
                                                             id="banner_image"
                                                             style="width: auto;height: 120px;margin-top: 15px;">
                                                        <a href="/items/imagedelete/{{$vs->id}}"
                                                           class="badge badge-danger p-2 mb-3">Delete</a><br>

                                                    </div>
                                            @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6 grid-margin stretch-card">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title">Item Type</h4>
                                            <div class="card testDiv">
                                                <div class="card-body ">
                                                    @foreach($types as $type)
                                                        <div class="form-group">
                                                            <div class="form-check">
                                                                <label class="form-check-label">
                                                                    <input type="radio" name="item_type_id"  class="form-check-input" id="optionsRadios1" value="{{$type['id']}}" {{ $items->item_type_id == $type->id ? 'checked' : '' }}>
                                                                    {{$type['name']}}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Price</label>
                                    {!! Form::number('price', null, array('placeholder' => 'Price','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Special Price</label>
                                    {!! Form::number('special_price', null, array('placeholder' => 'Min Person','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="exampleInputName1">Nutritions (Optional)</label>
                                    {!! Form::text('nutritions', null, array('placeholder' => 'Nutritions','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputName1">Benifits (Optional)</label>
                                    {!! Form::text('benifits', null, array('placeholder' => 'Benifits','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputName1">Contains (Optional)</label>
                                    {!! Form::text('contains', null, array('placeholder' => 'Contains','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="exampleInputName1">Sugar Level (Optional)</label>
                                    {!! Form::text('sugar_level', null, array('placeholder' => 'Sugar Level','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputName1">Fat Level (Optional)</label>
                                    {!! Form::text('fat_level', null, array('placeholder' => 'Fat Level','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputName1">Salt Level (Optional)</label>
                                    {!! Form::text('salt_level', null, array('placeholder' => 'Salt Level','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6 grid-margin stretch-card">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title">Adult Product </h4>
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <div class="form-check ">
                                                        <label class="form-check-label">
                                                            <input type="radio" name="is_adult_product"  class="form-check-input" id="optionsRadios1" value="1" {{ $items->is_adult_product == 1 ? 'checked' : '' }}>
                                                            Yes
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <div class="form-check ">
                                                        <label class="form-check-label">
                                                            <input type="radio" name="is_adult_product"  class="form-check-input" id="optionsRadios1" value="0" {{ $items->is_adult_product == 0 ? 'checked' : '' }}>
                                                            No
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="adult-product">
                                                <div class="row">
                                                    <div class="form-group col-md-12">
                                                        <label for="exampleInputName1">Government Regulations</label>
                                                        {!! Form::text('governmet_regulations', null, array('placeholder' => 'Government Regulations','class' => 'form-control  governmet_regulations','id'=>'governmet_regulations')) !!}
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 grid-margin stretch-card">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title">Delivery </h4>
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <div class="form-check ">
                                                        <label class="form-check-label">
                                                            <input type="radio" name="is_delivery"  class="form-check-input" id="optionsRadios1" value="1" {{ $items->is_delivery == 1 ? 'checked' : '' }}>
                                                            Delivery with charges
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <div class="form-check ">
                                                        <label class="form-check-label">
                                                            <input type="radio" name="is_delivery"  class="form-check-input" id="optionsRadios1" value="2" {{ $items->is_delivery == 2 ? 'checked' : '' }}>
                                                            Delivery Free
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <div class="form-check ">
                                                        <label class="form-check-label">
                                                            <input type="radio" name="is_delivery"  class="form-check-input" id="optionsRadios1" value="0" {{ $items->is_delivery == 0 ? 'checked' : '' }}>
                                                            Delivery Not Available
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="adelivery-fee">
                                                <div class="row">
                                                    <div class="form-group col-md-12">
                                                        <label for="exampleInputName1">Fee</label>
                                                        {!! Form::text('delivery_fee', null, array('placeholder' => 'Fee','class' => 'form-control  delivery_fee','id'=>'delivery_fee')) !!}
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-lg-12 grid-margin stretch-card">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title">Open Hours </h4>
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <div class="form-check ">
                                                        <label class="form-check-label">
                                                            <input type="radio" name="any_time"  class="form-check-input" id="optionsRadios1" value="1" {{ $items->any_time == 1 ? 'checked' : '' }}>
                                                            Any Time
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <div class="form-check ">
                                                        <label class="form-check-label">
                                                            <input type="radio" name="any_time"  class="form-check-input" id="optionsRadios1" value="0" {{ $items->any_time == 0 ? 'checked' : '' }}>
                                                            Flex Time
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="open-hours">
                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <label for="exampleInputName1">Day</label>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="exampleInputName1">Start Time</label>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="exampleInputName1">End Time</label>
                                                    </div>
                                                </div>
                                                @foreach($days as $key=>$day)
                                                    @foreach($slots as $slot)
                                                        @if($slot->day==$day)
                                                    <div class="row">
                                                        <div class="form-group col-md-4">
                                                            {!! Form::text('days[]', $day, array('placeholder' => '','class' => 'form-control mb-2 mr-sm-2 days','id'=>'days')) !!}
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            {!! Form::time('start_time[]', $slot->start_time, array('placeholder' => '','class' => 'form-control mb-2 mr-sm-2 start_time')) !!}
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            {!! Form::time('end_time[]', $slot->end_time, array('placeholder' => '','class' => 'form-control mb-2 mr-sm-2 end_time')) !!}
                                                        </div>
                                                    </div>
                                                        @endif
                                                    @endforeach
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

            <button type="submit" class="btn btn-primary mr-2">Submit</button>
            <button class="btn btn-light">Cancel</button>
            {!! Form::close() !!}

        </div>



@endsection
@section('scripts')
    <script>
        $('#title').change(function(e) {
            $.get('{{ route('item.check_slug') }}',
                { 'title': $(this).val() },
                function( data ) {
                    $('#slug').val(data.slug);
                }
            );
        });
    </script>
    <script>

        $(function() {
            $('input[name="any_time"]').on('click', function() {
                if ($(this).val() == 1) {
                    $('#open-hours').hide();
                    $('.days').removeAttr('disabled').attr('disabled', 'disabled');
                    $('.start_time').removeAttr('disabled').attr('disabled', 'disabled');
                    $('.end_time').removeAttr('disabled').attr('disabled', 'disabled');
                }
                else {
                    $('#open-hours').show();
                    $('.days').removeAttr('disabled');
                    $('.start_time').removeAttr('disabled');
                    $('.end_time').removeAttr('disabled');
                }
            });
            $('#open-hours').hide();
        });

    </script>
    <script>

        $(function() {
            $('input[name="is_adult_product"]').on('click', function() {
                if ($(this).val() == 1) {
                    $('#adult-product').show();
                    $('.governmet_regulations').removeAttr('disabled');
                }
                else {
                    $('#adult-product').hide();
                    $('.governmet_regulations').removeAttr('disabled').attr('disabled', 'disabled');
                }
            });
            $('#adult-product').hide();
        });

    </script>
    <script>

        $(function() {
            $('input[name="is_delivery"]').on('click', function() {
                if ($(this).val() == 1) {
                    $('#adelivery-fee').show();
                    $('.is_delivery').removeAttr('disabled');
                }
                else if ($(this).val() == 2) {
                    $('#adelivery-fee').hide();
                    $('.is_delivery').removeAttr('disabled').attr('disabled', 'disabled');
                } else {
                    $('#adelivery-fee').hide();
                    $('.is_delivery').removeAttr('disabled').attr('disabled', 'disabled');
                }
            });
            $('#adelivery-fee').hide();
        });

    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            var maxField = 10; //Input fields increment limitation
            var addButton = $('.add_button'); //Add button selector
            var wrapper = $('.field_wrapper'); //Input field wrapper
            var fieldHTML = ' <div class="row "><div class="form-group col-md-10"><input type="text" name="includes[]" value="" class="form-control"></div><a href="javascript:void(0);" class="remove_button"><img src="{{asset('backend/remove-icon.png')}}"/></a></div>'; //New input field html
            var x = 1; //Initial field counter is 1

            //Once add button is clicked
            $(addButton).click(function(){
                //Check maximum number of input fields
                if(x < maxField){
                    x++; //Increment field counter
                    $(wrapper).append(fieldHTML); //Add field html
                }
            });

            //Once remove button is clicked
            $(wrapper).on('click', '.remove_button', function(e){
                e.preventDefault();
                $(this).parent('div').remove(); //Remove field html
                x--; //Decrement field counter
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            var maxField = 10; //Input fields increment limitation
            var addButton = $('.add_button2'); //Add button selector
            var wrapper = $('.field_wrapper2'); //Input field wrapper
            var fieldHTML = ' <div class="row "><div class="form-group col-md-10"><input type="text" name="excludes[]" value="" class="form-control"></div><a href="javascript:void(0);" class="remove_button"><img src="{{asset('backend/remove-icon.png')}}"/></a></div>'; //New input field html
            var x = 1; //Initial field counter is 1

            //Once add button is clicked
            $(addButton).click(function(){
                //Check maximum number of input fields
                if(x < maxField){
                    x++; //Increment field counter
                    $(wrapper).append(fieldHTML); //Add field html
                }
            });

            //Once remove button is clicked
            $(wrapper).on('click', '.remove_button', function(e){
                e.preventDefault();
                $(this).parent('div').remove(); //Remove field html
                x--; //Decrement field counter
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            var maxField = 10; //Input fields increment limitation
            var addButton = $('.add_button3'); //Add button selector
            var wrapper = $('.field_wrapper3'); //Input field wrapper
            var fieldHTML = ' <div class="row "><div class="form-group col-md-10"><input type="text" name="available_times[]" value="" class="form-control"></div><a href="javascript:void(0);" class="remove_button"><img src="{{asset('backend/remove-icon.png')}}"/></a></div>'; //New input field html
            var x = 1; //Initial field counter is 1

            //Once add button is clicked
            $(addButton).click(function(){
                //Check maximum number of input fields
                if(x < maxField){
                    x++; //Increment field counter
                    $(wrapper).append(fieldHTML); //Add field html
                }
            });

            //Once remove button is clicked
            $(wrapper).on('click', '.remove_button', function(e){
                e.preventDefault();
                $(this).parent('div').remove(); //Remove field html
                x--; //Decrement field counter
            });
        });
    </script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyANuk6NO4FfxgvcQrb8N70ZDCKF-vWh6Jg&libraries=places"></script>
    <script>

        $(document).ready(function() {
            $("#lat_area").addClass("d-none");
            $("#long_area").addClass("d-none");
        });
    </script>
    <script>
        google.maps.event.addDomListener(window, 'load', initialize);

        function initialize() {
            var input = document.getElementById('autocomplete');
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace();
                $('#latitude').val(place.geometry['location'].lat());
                $('#longitude').val(place.geometry['location'].lng());
                $("#lat_area").removeClass("d-none");
                $("#long_area").removeClass("d-none");
            });
        }

    </script>

@endsection
