@extends('backend.layouts.master')
@section('content')

    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Create Items
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Forms</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create Items</li>
                    </ol>
                </nav>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::open(array('route' => 'items.store','method'=>'POST','class'=>'forms-sample','enctype' => 'multipart/form-data')) !!}

            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Create Items</h4>
                            <div class="row">
                            <div class="form-group col-md-6">
                                <label for="exampleInputName1">Title</label>
                                <input type="hidden" name="restaurants_id" value="{!! $id !!}">
                                {!! Form::text('title', null, array('placeholder' => 'Title','class' => 'form-control','id'=>'title')) !!}
                            </div>
                            <div class="form-group col-md-6">
                                <label for="exampleInputName1">Slug</label>
                                {!! Form::text('slug', null, array('placeholder' => 'Slug','class' => 'form-control','id'=>'slug', 'readonly' => 'true')) !!}
                            </div>
                            </div>
                            <div class="form-group" id="">
                                <label for="exampleInputEmail3">About Item</label>
                                {!! Form::textarea('description', null, array('placeholder' => 'Description','class' => 'form-control summernote','cols'=>'30','rows'=>'10')) !!}

                            </div>
                            <div class="row">
                                <div class="col-lg-6 grid-margin stretch-card">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title d-flex">Gallery Images
                                                <small class="ml-auto align-self-end">
                                                    <a href="dropify.html" class="font-weight-light" target="_blank"></a>
                                                </small>
                                            </h4>
                                            <input type="file" name="images[]" class="" multiple />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6 grid-margin stretch-card">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title">Item Type</h4>
                                            <div class="card testDiv">
                                                <div class="card-body ">
                                                    @foreach($types as $type)
                                                        <div class="form-group">
                                                            <div class="form-check">
                                                                <label class="form-check-label">
                                                                    <input type="radio" name="item_type_id"  class="form-check-input" id="optionsRadios1" value="{{$type['id']}}">
                                                                    {{$type['name']}}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Price</label>
                                    {!! Form::number('price', null, array('placeholder' => 'Price','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Special Price</label>
                                    {!! Form::number('special_price', null, array('placeholder' => 'Min Person','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="exampleInputName1">Nutritions (Optional)</label>
                                    {!! Form::text('nutritions', null, array('placeholder' => 'Nutritions','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputName1">Benifits (Optional)</label>
                                    {!! Form::text('benifits', null, array('placeholder' => 'Benifits','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputName1">Contains (Optional)</label>
                                    {!! Form::text('contains', null, array('placeholder' => 'Contains','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="exampleInputName1">Sugar Level (Optional)</label>
                                    {!! Form::text('sugar_level', null, array('placeholder' => 'Sugar Level','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputName1">Fat Level (Optional)</label>
                                    {!! Form::text('fat_level', null, array('placeholder' => 'Fat Level','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputName1">Salt Level (Optional)</label>
                                    {!! Form::text('salt_level', null, array('placeholder' => 'Salt Level','class' => 'form-control mb-2 mr-sm-2')) !!}
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6 grid-margin stretch-card">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title">Adult Product </h4>
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <div class="form-check ">
                                                        <label class="form-check-label">
                                                            <input type="radio" name="is_adult_product"  class="form-check-input" id="optionsRadios1" value="1">
                                                            Yes
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <div class="form-check ">
                                                        <label class="form-check-label">
                                                            <input type="radio" name="is_adult_product"  class="form-check-input" id="optionsRadios1" value="0">
                                                            No
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="adult-product">
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label for="exampleInputName1">Government Regulations</label>
                                                            {!! Form::text('governmet_regulations', null, array('placeholder' => 'Government Regulations','class' => 'form-control  governmet_regulations','id'=>'governmet_regulations')) !!}
                                                        </div>
                                                    </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 grid-margin stretch-card">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title">Delivery </h4>
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <div class="form-check ">
                                                        <label class="form-check-label">
                                                            <input type="radio" name="is_delivery"  class="form-check-input" id="optionsRadios1" value="1">
                                                            Delivery with charges
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <div class="form-check ">
                                                        <label class="form-check-label">
                                                            <input type="radio" name="is_delivery"  class="form-check-input" id="optionsRadios1" value="2">
                                                            Delivery Free
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <div class="form-check ">
                                                        <label class="form-check-label">
                                                            <input type="radio" name="is_delivery"  class="form-check-input" id="optionsRadios1" value="0">
                                                            Delivery Not Available
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="adelivery-fee">
                                                <div class="row">
                                                    <div class="form-group col-md-12">
                                                        <label for="exampleInputName1">Fee</label>
                                                        {!! Form::text('delivery_fee', null, array('placeholder' => 'Fee','class' => 'form-control  delivery_fee','id'=>'delivery_fee')) !!}
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-lg-12 grid-margin stretch-card">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title">Open Hours </h4>
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <div class="form-check ">
                                                        <label class="form-check-label">
                                                            <input type="radio" name="any_time"  class="form-check-input" id="optionsRadios1" value="1">
                                                            Any Time
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <div class="form-check ">
                                                        <label class="form-check-label">
                                                            <input type="radio" name="any_time"  class="form-check-input" id="optionsRadios1" value="0">
                                                            Flex Time
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="open-hours">
                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <label for="exampleInputName1">Day</label>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="exampleInputName1">Start Time</label>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="exampleInputName1">End Time</label>
                                                    </div>
                                                </div>
                                                @foreach($days as $key=>$day)
                                                    <div class="row">
                                                        <div class="form-group col-md-4">
                                                            {!! Form::text('days[]', $day, array('placeholder' => '','class' => 'form-control mb-2 mr-sm-2 days','id'=>'days')) !!}
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            {!! Form::time('start_time[]', $key, array('placeholder' => '','class' => 'form-control mb-2 mr-sm-2 start_time')) !!}
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            {!! Form::time('end_time[]', $key, array('placeholder' => '','class' => 'form-control mb-2 mr-sm-2 end_time')) !!}
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                <button class="btn btn-light">Cancel</button>
                {!! Form::close() !!}
            </div>
            <div class="col-lg-12 grid-margin d-flex stretch-card">
                <div class="row flex-grow">
                    <div class="col-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Rooms</h4>
                                <div class="card ">
                                    <div class="card-body ">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="table-responsive">
                                                    <table id="order-listing" class="table">
                                                        <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Name</th>
                                                            <th>Price</th>
                                                            <th>Special Price</th>
                                                            <th>Type</th>
                                                            <th>Status</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach ($items as $key => $item)
                                                            <tr>
                                                                <td>{{ ++$i }}</td>
                                                                <td>{{ $item->title }}</td>
                                                                <td>{{ $item->price }}</td>
                                                                <td>{{ $item->special_price }}</td>
                                                                <td>{{ $item->types->name }}</td>

                                                                <td>
                                                                    <input data-id="{{$item->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $item->publish_status ? 'checked' : '' }}>
                                                                </td>
                                                                <td>

                                                                    <a class="btn btn-outline-primary" style="margin-bottom: 2px"
                                                                       data-toggle="modal"
                                                                       data-target="#exampleModal"
                                                                       onclick="viewitem(this)"
                                                                       data-type="{{$item->types->name}}"
                                                                       data-images="{{$item->images->pluck('item_image')}}"
                                                                       data-available="{{$item->itemAvailableHourse->pluck('day','start_time','end_time')}}"
                                                                       data-id='{{$item->id}}'
                                                                       data-title='{{$item->title}}'
                                                                       data-description="{{$item->description}}"
                                                                       data-price="{{$item->price}}"
                                                                       data-special_price="{{$item->special_price}}"
                                                                       data-nutritions="{{$item->nutritions}}"
                                                                       data-benifits="{{$item->benifits}}"
                                                                       data-contains="{{$item->contains}}"
                                                                       data-sugar_level="{{$item->sugar_level}}"
                                                                       data-fat_level="{{$item->fat_level}}"
                                                                       data-salt_level="{{$item->salt_level}}"
                                                                       data-any_time="{{$item->any_time}}"
                                                                    >View</a>
                                                                    @can('role-edit')
                                                                        <a class="btn btn-outline-info" href="{{ route('items.edit',$item->id) }}">Edit</a>
                                                                    @endcan
                                                                    @can('role-delete')
                                                                        {!! Form::open(['method' => 'DELETE','route' => ['items.destroy', $item->id],'style'=>'display:inline']) !!}
                                                                        {!! Form::submit('Delete', ['class' => 'btn btn-outline-danger','data-toggle'=>'confirmation']) !!}
                                                                        {!! Form::close() !!}
                                                                    @endcan
                                                                </td>
                                                            </tr>
                                                        @endforeach


                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--tag strats-->

                    <!--tag ends-->
                </div>
            </div>
        </div>
        <div class="modal fade" id="showModal" data-backdrop="static" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="width: 60%;left: 20%">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Item Details</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-inline">
                                    <label for="recipient-name" class="col-md-6">Name:</label>
                                    <p id="title">

                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-inline">
                                    <label for="recipient-name" class="col-md-6">Type:</label>
                                    <p id="type">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-inline">
                                    <label for="recipient-name" class="col-md-6 ">Price(LKR):</label>
                                    <p id="price">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-inline">
                                    <label for="recipient-name" class="col-md-6 ">Special Price:</label>
                                    <p id="special_price">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-inline">
                                    <label for="recipient-name" class="col-md-6 ">Nutritions:</label>
                                    <p id="nutritions">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-inline">
                                    <label for="recipient-name" class="col-md-6 ">Benifits:</label>
                                    <p id="benifits">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-inline">
                                    <label for="recipient-name" class="col-md-6 ">Contains:</label>
                                    <p id="contains">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-inline">
                                    <label for="recipient-name" class="col-md-6 ">Sugar Level:</label>
                                    <p id="sugar_level">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-inline">
                                    <label for="recipient-name" class="col-md-6 ">Fat Level:</label>
                                    <p id="fat_level">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-inline">
                                    <label for="recipient-name" class="col-md-6 ">Salt Level:</label>
                                    <p id="salt_level">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-inline">
                                <label for="recipient-name" class="col-md-6 ">Description:</label>
                                <p id="description">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="recipient-name" class="col-md-6 ">Available Time</label>
                            </div>
                            <div class="col-md-9 p-0" id="available_div"></div>
                        </div>

                        <div class="row" id="images_div">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>

    @section('scripts')
            <script src="{{asset('backend/js/data-table.js')}}"></script>
            <script>

                function viewitem(item) {
                    let id = item.dataset.id;
                    let title = item.dataset.title;
                    let type = item.dataset.type;
                    let description = item.dataset.description;
                    let price = item.dataset.price;
                    let special_price = item.dataset.special_price;
                    let nutritions = item.dataset.nutritions;
                    let benifits = item.dataset.benifits;
                    let contains = item.dataset.contains;
                    let sugar_level = item.dataset.sugar_level;
                    let fat_level = item.dataset.fat_level;
                    let salt_level = item.dataset.salt_level;
                    let any_time = item.dataset.any_time;
                    let images = JSON.parse(item.dataset.images);
                    let available = JSON.parse(item.dataset.available);

                    let html = '';
                    let base_path = "{{asset('')}}";

                    $.each(images, function (index, value) {
                        html += "<div class='col-md-6 p-3'>"
                        html += "<img class='img img-responsive w-100' src='" + base_path + value + "'/>"
                        html += "</div>"
                    });

                    $("#images_div").empty();
                    $("#images_div").append(html);


                    let available_label = '';
                    $.each(available, function (index, value) {
                        available_label += "<label class='badge badge-info badge-pill mr-1'>" + value + "</label>"
                    });


                    $("#available_div").empty();
                    $("#available_div").append(available_label);
                    console.log(available_label)

                    $("#title").empty();
                    $('#title').append(title);
                    $("#type").empty();
                    $('#type').append(type);
                    $("#description").empty();
                    $('#description').append(description);
                    $("#price").empty();
                    $('#price').append(price);
                    $("#special_price").empty();
                    $('#special_price').append(special_price);
                    $("#nutritions").empty();
                    $('#nutritions').append(nutritions);
                    $("#benifits").empty();
                    $('#benifits').append(benifits);
                    $("#contains").empty();
                    $('#contains').append(contains);
                    $("#sugar_level").empty();
                    $('#sugar_level').append(sugar_level);
                    $("#fat_level").empty();
                    $('#fat_level').append(fat_level);
                     $("#salt_level").empty();
                    $('#salt_level').append(salt_level);
                     $("#any_time").empty();
                    $('#any_time').append(any_time);

                    $('#showModal').modal('show')
                }
  $(function() {
    $('.toggle-class').change(function() {
        var publish_status = $(this).prop('checked') == true ? 1 : 0;
        var item_id = $(this).data('id');

        $.ajax({
            type: "GET",
            dataType: "json",
            url: '/itemStatus',
            data: {'publish_status': publish_status, 'item_id': item_id},
            success: function(data){
             toastr.success('Review successfully');
              console.log(data.success)


        },
        error: function (msg) {
            toastr.error( 'Inconceivable!');
        }
        });
    })
  })
</script>
            <script>
  $('#title').change(function(e) {
    $.get('{{ route('item.check_slug') }}',
      { 'title': $(this).val() },
      function( data ) {
        $('#slug').val(data.slug);
      }
    );
  });
</script>
<script>

        $(function() {
        $('input[name="any_time"]').on('click', function() {
        if ($(this).val() == 1) {
        $('#open-hours').hide();
        $('.days').removeAttr('disabled').attr('disabled', 'disabled');
        $('.start_time').removeAttr('disabled').attr('disabled', 'disabled');
        $('.end_time').removeAttr('disabled').attr('disabled', 'disabled');
        }
        else {
        $('#open-hours').show();
        $('.days').removeAttr('disabled');
        $('.start_time').removeAttr('disabled');
        $('.end_time').removeAttr('disabled');
        }
        });
        $('#open-hours').hide();
        });

</script>
<script>

        $(function() {
        $('input[name="is_adult_product"]').on('click', function() {
        if ($(this).val() == 1) {
        $('#adult-product').show();
        $('.governmet_regulations').removeAttr('disabled');
        }
        else {
        $('#adult-product').hide();
        $('.governmet_regulations').removeAttr('disabled').attr('disabled', 'disabled');
        }
        });
        $('#adult-product').hide();
        });

</script>
<script>

        $(function() {
        $('input[name="is_delivery"]').on('click', function() {
        if ($(this).val() == 1) {
        $('#adelivery-fee').show();
        $('.is_delivery').removeAttr('disabled');
        }
        else if ($(this).val() == 2) {
        $('#adelivery-fee').hide();
        $('.is_delivery').removeAttr('disabled').attr('disabled', 'disabled');
        } else {
        $('#adelivery-fee').hide();
        $('.is_delivery').removeAttr('disabled').attr('disabled', 'disabled');
        }
        });
        $('#adelivery-fee').hide();
        });

</script>
<script type="text/javascript">
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var fieldHTML = ' <div class="row "><div class="form-group col-md-10"><input type="text" name="includes[]" value="" class="form-control"></div><a href="javascript:void(0);" class="remove_button"><img src="{{asset('backend/remove-icon.png')}}"/></a></div>'; //New input field html
    var x = 1; //Initial field counter is 1

    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });

    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>
            <script type="text/javascript">
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button2'); //Add button selector
    var wrapper = $('.field_wrapper2'); //Input field wrapper
    var fieldHTML = ' <div class="row "><div class="form-group col-md-10"><input type="text" name="excludes[]" value="" class="form-control"></div><a href="javascript:void(0);" class="remove_button"><img src="{{asset('backend/remove-icon.png')}}"/></a></div>'; //New input field html
    var x = 1; //Initial field counter is 1

    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });

    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>
            <script type="text/javascript">
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button3'); //Add button selector
    var wrapper = $('.field_wrapper3'); //Input field wrapper
    var fieldHTML = ' <div class="row "><div class="form-group col-md-10"><input type="text" name="available_times[]" value="" class="form-control"></div><a href="javascript:void(0);" class="remove_button"><img src="{{asset('backend/remove-icon.png')}}"/></a></div>'; //New input field html
    var x = 1; //Initial field counter is 1

    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });

    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>
            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyANuk6NO4FfxgvcQrb8N70ZDCKF-vWh6Jg&libraries=places"></script>
            <script>

   $(document).ready(function() {
        $("#lat_area").addClass("d-none");
        $("#long_area").addClass("d-none");
   });
</script>
            <script>
   google.maps.event.addDomListener(window, 'load', initialize);

   function initialize() {
       var input = document.getElementById('autocomplete');
       var autocomplete = new google.maps.places.Autocomplete(input);
       autocomplete.addListener('place_changed', function() {
           var place = autocomplete.getPlace();
           $('#latitude').val(place.geometry['location'].lat());
           $('#longitude').val(place.geometry['location'].lng());
           $("#lat_area").removeClass("d-none");
           $("#long_area").removeClass("d-none");
       });
   }

</script>

@endsection
@endsection
