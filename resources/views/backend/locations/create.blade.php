@extends('backend.layouts.master')
@section('content')

    <div class="main-panel">
        {!! Form::open(array('route' => 'locations.store','method'=>'POST','class'=>'forms-sample','files'=>'true')) !!}
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Create Location
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Forms</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create Location</li>
                    </ol>
                </nav>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                <div class="col-lg-12 grid-margin  stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Location </h4>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="exampleInputName1">Name</label>

                                    {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control mb-2 mr-sm-2','id' =>'name')) !!}
                                </div>
                            </div>
                            <div class="form-group col-md-6">

                                {!! Form::text('searchInput', null, array('placeholder' => 'Location','class' => 'form-control col-md-8','id'=>'searchInput')) !!}
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group ">
                                        <div class="map" id="map" style="width: 100%; height: 300px;"></div>
                                    </div>
                                </div>


                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="exampleInputName1">Location</label>

                                    {!! Form::text('location', null, array('placeholder' => 'Location','class' => 'form-control mb-2 mr-sm-2','id' =>'location')) !!}
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="exampleInputName1">Latitude</label>
                                    {!! Form::text('lat', null, array('placeholder' => 'Latitude','class' => 'form-control mb-2 mr-sm-2','id' =>'lat')) !!}
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="exampleInputName1">Longitude</label>
                                    {!! Form::text('lng', null, array('placeholder' => 'Longitude','class' => 'form-control mb-2 mr-sm-2','id'=>'lng')) !!}
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="exampleInputName1">City</label>
                                    {!! Form::text('city', null, array('placeholder' => 'city','class' => 'form-control mb-2 mr-sm-2','id'=>'city')) !!}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>



            <button type="submit" class="btn btn-primary mr-2">Submit</button>
            <button class="btn btn-light">Cancel</button>
            {!! Form::close() !!}

        </div>


        @section('scripts')
            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyANuk6NO4FfxgvcQrb8N70ZDCKF-vWh6Jg&libraries=places"></script>
            <script>
/* script */
var geocoder;
function initialize() {
   var latlng = new google.maps.LatLng(6.927079,79.861244);
    var map = new google.maps.Map(document.getElementById('map'), {
      center: latlng,
      zoom: 13
    });
    var marker = new google.maps.Marker({
      map: map,
      position: latlng,
      draggable: true,
      anchorPoint: new google.maps.Point(0, -29)
   });
    var input = document.getElementById('searchInput');
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    var geocoder = new google.maps.Geocoder();
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);
    var infowindow = new google.maps.InfoWindow();


    autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
console.log(place);

        if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }

        marker.setPosition(place.geometry.location);
        marker.setVisible(true);
var location = [];

    for (var ac = 0; ac < place.address_components.length; ac++)
    {
        var component = place.address_components[ac];

        switch(component.types[0])
        {
            case 'locality':
                location['city'] = component.long_name;
                break;
            case 'administrative_area_level_1':
                location['state'] = component.long_name;
                break;
            case 'country':
                location['country'] = component.long_name;
                break;
        }
    };

   var itemLocality = location['city'];

        bindDataToForm(place.formatted_address,place.geometry.location.lat(),place.geometry.location.lng(),itemLocality);
        infowindow.setContent(place.formatted_address);
        infowindow.open(map, marker);


 });

    // this function will work on marker move event into map
    google.maps.event.addListener(marker, 'dragend', function() {
        geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {

        var arrAddress = results;
        console.log(results);
        $.each(arrAddress, function(i, address_component) {
          if (address_component.types[0] == "locality") {
            console.log("City: " + address_component.address_components[0].long_name);
            itemLocality = address_component.address_components[0].long_name;
          }
        });
         bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng(),itemLocality);
              infowindow.setContent(results[0].formatted_address);
              infowindow.open(map, marker);
      } else {
        alert("No results found");
      }

        }
        });
    });
}
function bindDataToForm(address,lat,lng,itemLocality){
   document.getElementById('location').value = address;
   document.getElementById('lat').value = lat;
   document.getElementById('lng').value = lng;
   document.getElementById('city').value = itemLocality;

}

function getcity(itemLocality){
    var city = itemLocality;
    $.ajax({
            type: "GET",
            dataType: "json",
            url: '/city_search',
            data: {'city': city},
            success: function(data){

            }
        });

}

google.maps.event.addDomListener(window, 'load', initialize);

</script>
@endsection

@endsection